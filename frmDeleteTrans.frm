VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDeleteTrans 
   Caption         =   "Database Utility"
   ClientHeight    =   9660
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   15195
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   9660
   ScaleWidth      =   15195
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdSinkronStock 
      Caption         =   "Sinkronisasi Stock"
      Height          =   465
      Left            =   12510
      TabIndex        =   22
      Top             =   8550
      Width           =   1680
   End
   Begin VB.OptionButton Option4 
      Caption         =   "per Barang"
      Height          =   285
      Left            =   2115
      TabIndex        =   21
      Top             =   405
      Width           =   1455
   End
   Begin VB.OptionButton Option3 
      Caption         =   "per Nota"
      Height          =   285
      Left            =   2115
      TabIndex        =   20
      Top             =   45
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   645
      Left            =   3915
      TabIndex        =   17
      Top             =   90
      Visible         =   0   'False
      Width           =   4650
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   18
         Top             =   180
         Width           =   2220
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   19
         Top             =   225
         Width           =   1050
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   600
      Left            =   3915
      TabIndex        =   13
      Top             =   135
      Width           =   4650
      Begin VB.OptionButton Option1 
         Caption         =   "per Tanggal"
         Height          =   420
         Left            =   45
         TabIndex        =   16
         Top             =   90
         Value           =   -1  'True
         Width           =   1365
      End
      Begin VB.OptionButton Option2 
         Caption         =   "per Bulan"
         Height          =   420
         Left            =   1440
         TabIndex        =   15
         Top             =   90
         Width           =   1230
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   2745
         TabIndex        =   14
         Top             =   90
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   108199939
         CurrentDate     =   40045
      End
   End
   Begin VB.CommandButton cmdSynchronize 
      Caption         =   "Sinkronisasi"
      Height          =   465
      Left            =   12510
      TabIndex        =   11
      Top             =   7920
      Width           =   1680
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Refresh"
      Height          =   375
      Left            =   8640
      TabIndex        =   8
      Top             =   315
      Width           =   1050
   End
   Begin VB.CommandButton cmdSort 
      Caption         =   "Urutkan"
      Height          =   465
      Left            =   12510
      TabIndex        =   7
      Top             =   7335
      Width           =   1680
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Hapus"
      Height          =   465
      Left            =   12510
      TabIndex        =   6
      Top             =   6750
      Width           =   1680
   End
   Begin MSFlexGridLib.MSFlexGrid DBGrid2 
      Height          =   5370
      Left            =   8775
      TabIndex        =   3
      Top             =   810
      Width           =   6270
      _ExtentX        =   11060
      _ExtentY        =   9472
      _Version        =   393216
      Cols            =   4
      BackColorBkg    =   -2147483633
      FocusRect       =   2
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   465
      Left            =   10125
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSFlexGridLib.MSFlexGrid DBGrid3 
      Height          =   2805
      Left            =   90
      TabIndex        =   9
      Top             =   6795
      Width           =   12075
      _ExtentX        =   21299
      _ExtentY        =   4948
      _Version        =   393216
      Cols            =   8
      BackColorBkg    =   -2147483633
      FocusRect       =   2
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid DBGrid1 
      Height          =   5550
      Left            =   225
      TabIndex        =   10
      Top             =   810
      Width           =   8385
      _ExtentX        =   14790
      _ExtentY        =   9790
      _Version        =   393216
      Cols            =   5
      BackColorBkg    =   -2147483633
      FocusRect       =   2
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblProgress 
      Height          =   510
      Left            =   12195
      TabIndex        =   12
      Top             =   9135
      Width           =   2985
   End
   Begin VB.Label lblTotalHapus 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12420
      TabIndex        =   5
      Top             =   6300
      Width           =   1905
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10980
      TabIndex        =   4
      Top             =   6300
      Width           =   1230
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5850
      TabIndex        =   2
      Top             =   6390
      Width           =   1230
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7110
      TabIndex        =   1
      Top             =   6390
      Width           =   2400
   End
   Begin VB.Label Label1 
      Caption         =   "Nota"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      TabIndex        =   0
      Top             =   315
      Width           =   1680
   End
   Begin VB.Menu mnuExit 
      Caption         =   "E&xit"
   End
End
Attribute VB_Name = "frmDeleteTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim queryJual, queryBeli As String
Dim total As Long
Dim totalhapus As Long
Const querybrg As String = "select [kode barang],[nama barang], Harga from ms_barang where flag='1'"

Private Sub cmdDelete_Click()
Dim rs1 As New ADODB.Recordset
Dim qty As Integer
    conn.Open strcon
    If db2 Then conn_fake.Open strconasli
    For i = 1 To DBGrid2.rows - 2
        rs.Open "select * from t_penjualand where id='" & DBGrid2.TextMatrix(i, 1) & "' and [kode barang]='" & DBGrid2.TextMatrix(i, 1) & "'", conn
        While Not rs.EOF
            qty = rs("qty")
            rs1.Open "select * from t_pembeliand where [kode barang]='" & rs("kode barang") & "' order by id desc", conn, adOpenStatic, adLockOptimistic
            While Not rs1.EOF And qty > 0
                If rs1("qty") > qty Then
                    rs1("qty") = rs1("qty") - qty
                    qty = 0
                Else
                    qty = qty - rs1("qty")
                    rs1.Delete adAffectCurrent
                End If
                rs1.MoveNext
            Wend
            rs1.UpdateBatch adAffectAllChapters
            rs1.Close
            rs.MoveNext
        Wend
        rs.Close
        conn.Execute "update t_penjualanh set total=total-" & DBGrid2.TextMatrix(i, 3) & " ,[jumlah bayar]=[jumlah bayar]-" & DBGrid2.TextMatrix(i, 3) & " where id='" & DBGrid2.TextMatrix(i, 1) & "'"
        conn.Execute "delete from t_penjualand where id='" & DBGrid2.TextMatrix(i, 1) & "' and [kode barang]='" & DBGrid2.TextMatrix(i, 2) & "'"
        conn.Execute "delete from hst_penjualand where id='" & DBGrid2.TextMatrix(i, 1) & "' and [kode barang]='" & DBGrid2.TextMatrix(i, 2) & "'"
        conn.Execute "update t_pembayaran set tunai=0,kartu=0,jumlahbayar=jumlahbayar-" & DBGrid2.TextMatrix(i, 3) & " where no_jual='" & DBGrid2.TextMatrix(i, 1) & "'"
        rs.Open "select * from t_penjualanh where id='" & DBGrid2.TextMatrix(i, 1) & "'", conn
        
        If db2 And rs.EOF Then conn_fake.Execute "update t_penjualanh set pk='' where pk='" & DBGrid2.TextMatrix(i, 1) & "'"
        rs.Close
    Next
    conn.Execute "delete from t_pembelianh where not exists (select * from t_pembeliand where id=t_pembelianh.id)"
    conn.Execute "delete from t_penjualanh where not exists (select * from t_penjualand where id=t_penjualanh.id)"
    conn.Execute "delete from hst_penjualanh where not exists (select * from t_penjualand where id=t_penjualanh.id)"
    conn.Execute "delete from t_pembayaran where not exists (select * from t_penjualand where id=t_pembayaran.no_jual)"
    DropConnection
    reset_form
    loadgrid
    showdetail
End Sub

Private Sub cmdSinkronStock_Click()
On Error GoTo err
Dim lastid, lastpk As String
Dim rs1 As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim mode As Byte
    mode = 1
    If Not db2 Or Not db1 Then
        Exit Sub
    End If
    
    cmdDelete.Enabled = False
    cmdSort.Enabled = False
    cmdSynchronize.Enabled = False
    
    conn.Open strconasli
    conn_fake.Open strcon
    Dim id As String
    Dim id2 As String
    id = ""
    id2 = ""
    mode = 1
    lblProgress = "Mulai sinkronisasi data stock"
    rs.Open "select gudang,s.[kode barang],stock from stock s inner join ms_barang m on m.[kode barang]=s.[kode barang] where flag='1'", conn_fake, adOpenForwardOnly, adLockReadOnly
    While Not rs.EOF
        updatestock3 rs("gudang"), rs("kode barang"), rs("stock"), conn
        DoEvents
        rs.MoveNext
    Wend
    rs.Close
    Dim totalcount As Long
    Dim count As Long
    rs.Open "select count(*) from stock1 s inner join ms_barang m on m.[kode barang]=s.[kode barang] where flag='1'", conn_fake, adOpenForwardOnly, adLockReadOnly
    If Not rs.EOF Then
        totalcount = IIf(IsNull(rs(0)), 0, rs(0))
    Else
        totalcount = 0
    End If
    rs.Close
    count = 0
    rs.Open "select gudang,s.[kode barang],stock from stock1 s inner join ms_barang m on m.[kode barang]=s.[kode barang] where flag='1'", conn_fake, adOpenForwardOnly, adLockReadOnly
    While Not rs.EOF
        'If rs("stock") <> getStock3(rs("kode barang"), rs("gudang"), strconasli) Then
        updatestock4 rs("gudang"), rs("kode barang"), rs("stock"), conn
        'End If
        count = count + 1
        lblProgress = "Mulai sinkronisasi data stock2 " & Format(count / totalcount, "#0.00") & "%"
        DoEvents
        
        rs.MoveNext
    Wend
    rs.Close
    
    lblProgress = "Proses sinkronisasi selesai"
off:
err:
    cmdDelete.Enabled = True
    cmdSort.Enabled = True
    cmdSynchronize.Enabled = True
    If mode = 2 Then
        conn.RollbackTrans
        conn_fake.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection


End Sub

Private Sub cmdSort_Click()
'On Error GoTo err
Dim rs1 As New ADODB.Recordset
Dim selisih As Integer
Dim id As String
Dim finish As Boolean
Dim mode As Byte
    mode = 1
    finish = False
    If db2 Then
        conn.Open strconasli

    End If
    conn_fake.Open strcon
    While Not finish
    rs.Open "SELECT * from t_penjualanh h where not exists (select * from t_penjualanh where id=left(h.id,8) & format(right(h.id,3)-1,'000')) and right(h.id,3)>1 order by id", conn_fake
    If Not rs.EOF Then
        id = rs("id")
        If db2 Then conn.BeginTrans
        conn_fake.BeginTrans
        mode = 2
        rs1.Open "select * from t_penjualanh where left(id,8)='" & Left(rs("id"), 8) & "' and id<'" & rs("id") & "' order by id desc", conn_fake
        If Not rs1.EOF Then
            selisih = Right(rs("id"), 3) - Right(rs1("id"), 3) - 1
        Else
            selisih = Right(rs("id"), 3) - 1
        End If
        rs1.Close
'        conn_fake.Execute "update t_penjualand set id=cstr(left(id,8) & format(right(id,3)-" & selisih & ", '000')) where left(id,8)='" & Left(id, 8) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_penjualanh set pk=cstr(left(id,8) & format(right(id,3)-" & selisih & ", '000')) where left(id,8)='" & Left(id, 8) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_penjualanh set id=cstr(mid(id,3,6) & format(cint(right(id,3))-" & selisih & ", '000')) where left(id,8)='" & Left(id, 8) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_pembayaran set no_jual=cstr(mid(no_jual,3,6) & format(right(no_jual,3)-" & selisih & ", '000')) where left(no_jual,8)='" & Left(id, 8) & "' and no_jual>='" & id & "'"
        conn_fake.Execute "update t_penjualanh set id='PJ'+id where len(id)=9"
        conn_fake.CommitTrans
        conn_fake.Execute "update t_pembayaran set no_jual='PJ'+no_jual where len(no_jual)=9"
        If db2 Then conn.Execute "update t_penjualanh set pk=cstr(left(pk,8) & format(right(pk,3)-" & selisih & ", '000')) where left(pk,8)='" & Left(id, 8) & "' and id>='" & id & "'"
        If db2 Then conn.CommitTrans
        conn_fake.CommitTrans
        mode = 1
        rs.MoveNext
    Else
        finish = True
    End If
    rs.Close
    Wend
    DropConnection
    loadgrid
    MsgBox "Proses mengurutkan selesai"
    Exit Sub
err:
    MsgBox err.Description
    If mode = 2 Then conn_fake.RollbackTrans
    DropConnection
End Sub

Private Sub cmdSynchronize_Click()
On Error GoTo err
Dim lastid, lastpk As String
Dim rs1 As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim mode As Byte
    mode = 1
    If Not db2 Or Not db1 Then
        Exit Sub
    End If
    
    cmdDelete.Enabled = False
    cmdSort.Enabled = False
    cmdSynchronize.Enabled = False
    
    conn.Open strconasli
    conn_fake.Open strcon
    Dim id As String
    Dim id2 As String
    id = ""
    id2 = ""
    lblProgress = "Mulai sinkronisasi data penjualan"
    conn.BeginTrans
    conn_fake.BeginTrans
    mode = 2
    rs.Open "select * from t_penjualanh where keterangan='.' and bayar='1' order by id", conn_fake, adOpenStatic, adLockOptimistic
    While Not rs.EOF
        id2 = id
        If Left(rs("id"), 8) = Left(id, 8) Then
            id = Left(id, 8) & Format(CInt(Mid(id, 9, Len(id))) + 1, "000")
        Else
            rs2.Open "select top 1 * from t_penjualanh where left(id,8)='" & Left(rs("id"), 8) & "' order by id desc", conn
            If Not rs2.EOF Then
                id = rs2("id")
                id = Left(id, 8) & Format(CInt(Mid(id, 9, Len(id))) + 1, "000")
                
            Else
                id = Left(rs("id"), 8) & "001"
            End If
            If rs2.State Then rs2.Close
            id2 = id
        End If
        
        rs1.Open "select * from t_penjualanh where pk='" & rs("id") & "'", conn
        If Not rs1.EOF Then
            id = rs1("id")
            conn.Execute "delete from t_pembayaran where no_jual='" & rs1("id") & "'"
            conn.Execute "delete from t_penjualand where id='" & rs1("id") & "'"
            conn.Execute "delete from t_penjualanh where id='" & rs1("id") & "'"
            
        End If
        rs1.Close
        
        rs("keterangan") = ""
        rs.Update
        add_dataheader id, Format(rs("tanggal"), "yyyy/MM/dd hh:mm:ss"), rs("kode customer"), rs("gudang"), rs("keterangan"), rs("disc_tipe"), rs("disc"), rs("disc_rp"), rs("userid"), rs("id"), rs("bayar"), rs("total"), rs("jumlah bayar"), rs("charge")
        rs1.Open "select * from t_penjualand where id='" & rs("id") & "'", conn_fake
        While Not rs1.EOF
            add_datadetail id, rs1("kode barang"), rs1("qty"), rs1("harga"), rs1("disc_no"), rs1("disc_tipe"), rs1("disc"), rs1("disc2"), rs1("disc2_tipe"), rs1("hpp"), rs1("urut"), rs1("promosi")
            rs1.MoveNext
        Wend
        rs1.Close
        rs1.Open "select * from t_pembayaran where no_jual='" & rs("id") & "'", conn_fake
        While Not rs1.EOF
            conn.Execute "insert into t_pembayaran values ('" & rs1("id") & "','" & Format(rs1("tanggal"), "yyyy/MM/dd") & "','" & id & "'," & rs1("tunai") & "," & rs1("kartu") & "," & rs1("jumlahbayar") & ",'" & rs1("keterangan") & "')"
            rs1.MoveNext
        Wend
        rs1.Close
        DoEvents
        If Left(id, 8) = Left(id2, 8) And id2 <> "" Then
        If CInt(Mid(id2, 9, Len(id2))) + 1 <> CInt(Mid(id, 9, Len(id))) Then
            id = id2
        End If
        End If
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn_fake.CommitTrans
    mode = 1
    lblProgress = "Mulai sinkronisasi data retur penjualan"
    conn.BeginTrans
    conn_fake.BeginTrans
    mode = 2
    rs.Open "select * from t_returjualh where pk='' order by id", conn_fake, adOpenStatic, adLockOptimistic
    While Not rs.EOF
        If Left(rs("id"), 6) = Left(id, 6) Then
            id = Left(id, 6) & Format(CInt(Right(id, 5)) + 1, "00000")
        Else
            rs2.Open "select top 1 * from t_returjualh where left(id,6)='" & Left(rs("id"), 6) & "' order by id desc", conn
            If Not rs2.EOF Then
                id = rs2("id")
                id = Left(id, 6) & Format(CInt(Right(id, 5)) + 1, "00000")
            Else
                id = Left(rs("id"), 6) & "00001"
            End If
            If rs2.State Then rs2.Close
        End If
        
        add_dataheaderretur id, Format(rs("tanggal"), "yyyy/MM/dd hh:mm:ss"), rs("no_nota"), rs("gudang"), rs("keterangan"), rs("status"), rs("userid"), rs("id")
        rs1.Open "select * from t_returjuald where id='" & rs("id") & "'", conn_fake
        While Not rs1.EOF
            add_datadetailretur id, rs1("kode barang"), rs1("qty"), rs1("harga_jual"), rs1("harga_beli"), rs1("urut")
            rs1.MoveNext
        Wend
        rs1.Close
        rs1.Open "select pk from t_penjualanh where id='" & rs("no_nota") & "'", conn
        If Not rs1.EOF Then
            If rs1("pk") <> "" Then
            rs("no_nota") = rs1("pk")
            rs("pk") = rs("id")
            rs.Update
            Else
                conn.Execute "update t_returjualh set pk='' where pk='" & rs("id") & "'"
                conn_fake.Execute "delete from t_returjuald where id='" & rs("id") & "'"
                rs.Delete adAffectCurrent
                
            End If
        Else
            conn.Execute "update t_returjualh set no_nota='' where pk='" & rs("id") & "'"
'            conn.Execute "delete from t_returjuald where id='" & rs("id") & "'"
'            rs.Delete adAffectCurrent
        End If
        If rs1.State Then rs1.Close
        DoEvents
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn_fake.CommitTrans
    mode = 1
    urutkan "returjual", "RJ"
    
    
    lblProgress = "Mulai sinkronisasi data transfer barang"
    conn.BeginTrans
    conn_fake.BeginTrans
    mode = 2
    rs.Open "select * from t_transfergudangh where pk='' order by id", conn_fake, adOpenStatic, adLockOptimistic
    While Not rs.EOF
        If Left(rs("id"), 6) = Left(id, 6) Then
            id = Left(id, 6) & Format(CInt(Right(id, 5)) + 1, "00000")
        Else
            rs2.Open "select top 1 * from t_transfergudangh where left(id,6)='" & Left(rs("id"), 6) & "' order by id desc", conn
            If Not rs2.EOF Then
                id = rs2("id")
                id = Left(id, 6) & Format(CInt(Right(id, 5)) + 1, "00000")
            Else
                id = Left(rs("id"), 6) & "00001"
            End If
            If rs2.State Then rs2.Close
        End If
        
        add_dataheadertransfer id, Format(rs("tanggal"), "yyyy/MM/dd hh:mm:ss"), rs("gudang"), rs("tujuan"), rs("keterangan"), rs("status"), rs("userid"), rs("id")
        rs1.Open "select * from t_transfergudangd where id='" & rs("id") & "'", conn_fake
        While Not rs1.EOF
            add_datadetailtransfer id, rs1("kode barang"), rs1("qty"), rs1("hpp"), rs1("urut")
            rs1.MoveNext
        Wend
        rs1.Close
        rs("pk") = id
        If rs1.State Then rs1.Close
        DoEvents
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn_fake.CommitTrans
    mode = 1
    urutkan "transfergudang", "TG"
    
    
    lblProgress = "Mulai sinkronisasi data Promosi"
    conn.BeginTrans
    conn_fake.BeginTrans
    mode = 2
    conn.Execute "delete from ms_promosi"
    rs.Open "select * from ms_promosi", conn_fake
    While Not rs.EOF
        addpromosi rs("id"), Format(rs("tanggal dari"), "yyyy/MM/dd"), rs("fg_tanggal sampai"), Format(rs("tanggal sampai"), "yyyy/MM/dd"), _
        rs("fg_jam"), rs("jam dari"), rs("jam sampai"), rs("kode barang1"), _
        rs("rule"), rs("qty1"), rs("tipe promosi"), rs("kode barang2"), rs("qty2"), rs("disc_tipe"), rs("disc")
        DoEvents
        rs.MoveNext
    Wend
    rs.Close
    conn.CommitTrans
    conn_fake.CommitTrans
    mode = 1
'    lblProgress = "Mulai sinkronisasi data stock"
'    rs.Open "select gudang,s.[kode barang],stock from stock s inner join ms_barang m on m.[kode barang]=s.[kode barang] where flag='1'", conn_fake, adOpenForwardOnly, adLockReadOnly
'    While Not rs.EOF
'        updatestock3 rs("gudang"), rs("kode barang"), rs("stock"), conn
'        DoEvents
'        rs.MoveNext
'    Wend
'    rs.Close
'    rs.Open "select gudang,s.[kode barang],stock from stock1 s inner join ms_barang m on m.[kode barang]=s.[kode barang] where flag='1'", conn_fake, adOpenForwardOnly, adLockReadOnly
'    While Not rs.EOF
'        'If rs("stock") <> getStock3(rs("kode barang"), rs("gudang"), strconasli) Then
'        updatestock4 rs("gudang"), rs("kode barang"), rs("stock"), conn
'        'End If
'        DoEvents
'
'        rs.MoveNext
'    Wend
'    rs.Close
    
    lblProgress = "Proses sinkronisasi selesai"
off:
err:
    cmdDelete.Enabled = True
    cmdSort.Enabled = True
    cmdSynchronize.Enabled = True
    If mode = 2 Then
        conn.RollbackTrans
        conn_fake.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection

End Sub
Private Sub urutkan(tablename As String, singkatan As String)

Dim rs1 As New ADODB.Recordset
Dim selisih As Integer
Dim id As String
Dim rs As New ADODB.Recordset
    rs.Open "SELECT * from t_" & tablename & "h h where not exists (select * from t_" & tablename & "h where id=left(h.id,6) & format(right(h.id,5)-1,'00000')) and right(h.id,5)>1 order by id", conn_fake
    While Not rs.EOF
        id = rs("id")
        If db2 Then conn.BeginTrans
        conn_fake.BeginTrans
        rs1.Open "select * from t_" & tablename & "h where left(id,6)='" & Left(rs("id"), 6) & "' and id<'" & rs("id") & "' order by id desc", conn_fake
        If Not rs1.EOF Then
            selisih = Right(rs("id"), 5) - Right(rs1("id"), 5) - 1
        Else
            selisih = Right(rs("id"), 5) - 1
        End If
        rs1.Close
        'conn_fake.Execute "update t_"& tablename &"d set id=cstr(left(id,6) & format(right(id,5)-" & selisih & ", '00000')) where left(id,6)='" & Left(id, 6) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_" & tablename & "h set pk=cstr(left(id,6) & format(right(id,5)-" & selisih & ", '00000')) where left(id,6)='" & Left(id, 6) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_" & tablename & "h set id=cstr(mid(id,3,4) & format(cint(right(id,5))-" & selisih & ", '00000')) where left(id,6)='" & Left(id, 6) & "' and id>='" & id & "'"
        conn_fake.Execute "update t_" & tablename & "h set id='" & singkatan & "'+id where len(id)=9"
        conn.Execute "update t_" & tablename & "h set pk=cstr(left(pk,6) & format(right(pk,5)-" & selisih & ", '00000')) where left(pk,6)='" & Left(id, 6) & "' and id>='" & id & "'"
        conn.CommitTrans
        conn_fake.CommitTrans
        rs.MoveNext
    Wend
    rs.Close
    
End Sub

Private Sub addpromosi(id As String, tanggal_dari As String, fg_tanggal_sampai As String, tanggal_sampai As String, fg_jam As String, jam_dari As String, _
    jam_sampai As String, kodebarang1 As String, rule As String, qty1 As String, tipe_promosi As String, kodebarang2 As String, qty2 As String, disc_tipe As String, disc As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(15)
    ReDim nilai(15)

    table_name = "ms_promosi"
    fields(0) = "id"
    fields(1) = "[tanggal dari]"
    fields(2) = "[fg_tanggal sampai]"
    fields(3) = "[tanggal sampai]"
    fields(4) = "fg_jam"
    fields(5) = "[jam dari]"
    fields(6) = "[jam sampai]"
    fields(7) = "[kode barang1]"
    fields(8) = "rule"
    fields(9) = "qty1"
    fields(10) = "[tipe promosi]"
    fields(11) = "[kode barang2]"
    fields(12) = "qty2"
    fields(13) = "disc_tipe"
    fields(14) = "disc"
    

    nilai(0) = id
    nilai(1) = tanggal_dari
    nilai(2) = fg_tanggal_sampai
    nilai(3) = tanggal_sampai
    nilai(4) = fg_jam
    nilai(5) = jam_dari
    nilai(6) = jam_sampai
    nilai(7) = kodebarang1
    nilai(8) = rule
    nilai(9) = qty1
    nilai(10) = tipe_promosi
    nilai(11) = kodebarang2
    nilai(12) = qty2
    nilai(13) = disc_tipe
    nilai(14) = disc
    
    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub
Private Sub add_dataheader(id As String, tanggal As String, customer As String, gudang As String, keterangan As String, disc_tipe As String, disc As String, disc_rp As String, userid As String, pk As String, bayar As String, total As Currency, jmlbayar As Currency, charge As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(14)
    ReDim nilai(14)
    
    table_name = "T_PENJUALANH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "[kode customer]"
    fields(3) = "gudang"
    fields(4) = "keterangan"
'    fields(5) = "total"
    fields(5) = "disc_tipe"
    fields(6) = "disc"
    fields(7) = "disc_rp"
    fields(8) = "[userid]"
    fields(9) = "pk"
    fields(10) = "total"
    fields(11) = "[jumlah bayar]"
    fields(12) = "bayar"
    fields(13) = "charge"
    
    nilai(0) = id
    nilai(1) = tanggal
    nilai(2) = customer
    nilai(3) = gudang
    nilai(4) = keterangan
'    nilai(5) = lblTotal
    nilai(5) = disc_tipe
    nilai(6) = disc
    nilai(7) = disc_rp
    nilai(8) = userid
    nilai(9) = pk
    nilai(10) = total
    nilai(11) = jmlbayar
    nilai(12) = bayar
    nilai(13) = charge
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetail(id As String, kodebarang As String, qty As Long, harga As Currency, disc_no As String, disc_tipe As String, disc As String, disc2 As String, disc2_tipe As String, hpp As String, row As Integer, promosi As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(12)
    ReDim nilai(12)
    
    table_name = "T_PENJUALAND"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "harga"
    fields(4) = "disc_no"
    fields(5) = "disc_tipe"
    fields(6) = "disc"
    fields(7) = "disc2"
    fields(8) = "disc2_tipe"
    fields(9) = "urut"
    fields(10) = "hpp"
    fields(11) = "promosi"
    
    nilai(0) = id
    nilai(1) = kodebarang
    nilai(2) = qty
    nilai(3) = harga
    nilai(4) = disc_no
    nilai(5) = disc_tipe
    nilai(6) = disc
    nilai(7) = disc2
    nilai(8) = disc2_tipe
    nilai(9) = row
    nilai(10) = hpp
    nilai(11) = promosi

    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub
Private Sub add_dataheaderretur(id As String, tanggal As String, no_nota As String, gudang As String, keterangan As String, status As String, userid As String, pk As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(8)
    ReDim nilai(8)
    
    table_name = "T_RETURJUALH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "no_nota"
    fields(3) = "gudang"
    fields(4) = "keterangan"
'    fields(5) = "total"
    fields(5) = "status"
    fields(6) = "[userid]"
    fields(7) = "pk"
    
    nilai(0) = id
    nilai(1) = tanggal
    nilai(2) = no_nota
    nilai(3) = gudang
    nilai(4) = keterangan
'    nilai(5) = lblTotal
    nilai(5) = status
    nilai(6) = userid
    nilai(7) = pk
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetailretur(id As String, kodebarang As String, qty As Long, harga_jual As Currency, harga_beli As Currency, urut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "T_RETURJUALD"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "harga_beli"
    fields(4) = "harga_jual"
    fields(5) = "urut"
    
    
    nilai(0) = id
    nilai(1) = kodebarang
    nilai(2) = qty
    nilai(3) = harga_beli
    nilai(4) = harga_jual
    nilai(5) = urut
    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub
Private Sub add_dataheadertransfer(id As String, tanggal As String, asal As String, tujuan As String, keterangan As String, status As String, userid As String, pk As String)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(8)
    ReDim nilai(8)
    
    table_name = "T_transfergudangH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "tujuan"
    fields(4) = "keterangan"
'    fields(5) = "total"
    fields(5) = "status"
    fields(6) = "[userid]"
    fields(7) = "pk"
    
    nilai(0) = id
    nilai(1) = tanggal
    nilai(2) = asal
    nilai(3) = tujuan
    nilai(4) = keterangan
'    nilai(5) = lblTotal
    nilai(5) = status
    nilai(6) = userid
    nilai(7) = pk
    conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub add_datadetailtransfer(id As String, kodebarang As String, qty As Long, hpp As Currency, urut As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "T_transfergudangD"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "hpp"
    fields(4) = "urut"
    
    
    nilai(0) = id
    nilai(1) = kodebarang
    nilai(2) = qty
    nilai(3) = hpp
    nilai(4) = urut
    conn.Execute tambah_data2(table_name, fields, nilai)

End Sub
Private Sub Command1_Click()
    loadgrid
End Sub

Private Sub DBGrid1_DblClick()
    showdetail
End Sub
Private Sub showdetail()
Dim row As Integer
With DBGrid3
    .rows = 1
    .rows = 2
    row = 1
    conn.Open strcon
    If Option3.Value = True Then
    rs.Open "select d.[kode barang],m.[nama barang],qty,m.satuan, d.harga,d.disc_no,d.disc_tipe,d.disc,d.disc2,d.disc2_tipe,promosi from t_penjualand d inner join ms_barang m on d.[kode barang]=m.[kode barang] where d.id='" & DBGrid1.TextMatrix(DBGrid1.row, 1) & "' order by urut", conn
   ' rs.Open "select d.[kode barang],m.[nama barang],qty,m.satuan, d.harga,d.disc,d.disctotal,d.charge from Q_omset d inner join ms_barang m on d.[kode barang]=m.[kode barang] where d.id='" & DBGrid1.TextMatrix(DBGrid1.row, 1) & "' order by urut", conn
    Else
    rs.Open "select h.id,h.tanggal,d.[kode barang],m.[nama barang],qty,m.satuan, d.harga,d.disc_no,d.disc_tipe,d.disc,d.disc2,d.disc2_tipe,promosi from (t_penjualand d inner join ms_barang m on d.[kode barang]=m.[kode barang]) inner join t_penjualanh h on d.id=h.id where d.[kode barang]='" & DBGrid1.TextMatrix(DBGrid1.row, 1) & "' order by urut", conn
    End If
    While Not rs.EOF
        If Option3.Value = True Then
        .TextMatrix(row, 1) = rs(0)
        .TextMatrix(row, 2) = rs(1)
        .TextMatrix(row, 3) = rs(2)
        .TextMatrix(row, 4) = Format(rs(4), "#,##0")
        .TextMatrix(row, 5) = rs(4) - IIf(rs("disc2_tipe") = "0", IIf(rs("disc_tipe") = "1", rs(7), (rs(4) * rs(7) / 100)), IIf(rs("disc2_tipe") = "1", rs("disc2"), rs(4) * rs("disc2") / 100))
        '.TextMatrix(row, 5) = rs(4) - rs(5) - rs(6)
        .TextMatrix(row, 6) = (.TextMatrix(row, 3) * .TextMatrix(row, 5)) + rs(7)
        .TextMatrix(row, 7) = DBGrid1.TextMatrix(DBGrid1.row, 1)
        Else
        .TextMatrix(row, 1) = rs(0)
        .TextMatrix(row, 2) = rs(1)
        .TextMatrix(row, 3) = rs(4)
        .TextMatrix(row, 4) = Format(rs(5), "#,##0")
        .TextMatrix(row, 5) = rs(6) - IIf(rs("disc2_tipe") = "0", IIf(rs("disc_tipe") = "1", rs(9), (rs(6) * rs(9) / 100)), IIf(rs("disc2_tipe") = "1", rs("disc2"), rs(6) * rs("disc2") / 100))
        .TextMatrix(row, 6) = .TextMatrix(row, 3) * .TextMatrix(row, 5)
        .TextMatrix(row, 7) = rs(2)
        End If
'        flxGrid.TextMatrix(row, 5) = Format(flxGrid.TextMatrix(row, 5), "#,##0")
'        flxGrid.TextMatrix(row, 6) = Format(flxGrid.TextMatrix(row, 6), "#,##0")
'        flxGrid.TextMatrix(row, 7) = rs(10)
'        flxGrid.TextMatrix(row, 8) = rs(4)
'        flxGrid.TextMatrix(row, 9) = rs(5)
'        flxGrid.TextMatrix(row, 10) = rs(6)
'        flxGrid.TextMatrix(row, 11) = rs(7)
'        flxGrid.TextMatrix(row, 12) = rs(8)
'        flxGrid.TextMatrix(row, 13) = rs(9)
'        qty = qty + rs(2)
'        total = total + flxGrid.TextMatrix(row, 6)
        row = row + 1
        .rows = .rows + 1
        rs.MoveNext
    Wend
    
    If rs.State Then rs.Close
    DropConnection
End With
End Sub

Private Sub DBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then showdetail
    If KeyCode = vbKeyDelete Then
        i = DBGrid1.row
        j = DBGrid1.RowSel
        
        For x = IIf(i < j, i, j) To IIf(i < j, j, i)
        DBGrid1.row = x
        showdetail
        For a = 1 To DBGrid3.rows - 1
            DBGrid3.row = a
            DBGrid3_DblClick
            DoEvents
        Next
        Next
    End If
End Sub

Private Sub DBGrid2_DblClick()
    batalhapus
End Sub

Private Sub DBGrid2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        batalhapus
    End If
End Sub
Private Sub batalhapus()
        row = DBGrid2.row
        If DBGrid2.TextMatrix(row, 1) <> "" Then
        totalhapus = totalhapus - (DBGrid2.TextMatrix(row, 3))
        For row = row To DBGrid2.rows - 2
            If row = DBGrid2.rows Then
                For col = 1 To 3
                    DBGrid2.TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf DBGrid2.TextMatrix(row + 1, 1) = "" Then
                For col = 1 To 3
                    DBGrid2.TextMatrix(row, col) = ""
                Next
            ElseIf DBGrid2.TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To 3
                DBGrid2.TextMatrix(row, col) = DBGrid2.TextMatrix(row + 1, col)
                Next
            End If
        Next
        DBGrid2.rows = DBGrid2.rows - 1
        lblTotalHapus = Format(totalhapus, "#,##0")
        End If
End Sub
Private Sub DBGrid3_DblClick()
        For i = 1 To DBGrid2.rows - 1
            If DBGrid2.TextMatrix(i, 1) = DBGrid3.TextMatrix(DBGrid3.row, 7) And DBGrid2.TextMatrix(i, 2) = DBGrid3.TextMatrix(DBGrid3.row, 1) Then
                Exit Sub
            End If
        Next
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 1) = DBGrid3.TextMatrix(DBGrid3.row, IIf(Option3.Value, 7, 1))
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 2) = DBGrid3.TextMatrix(DBGrid3.row, IIf(Option3.Value, 1, 7))
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 3) = DBGrid3.TextMatrix(DBGrid3.row, 6)
        totalhapus = totalhapus + DBGrid3.TextMatrix(DBGrid3.row, 6)
        DBGrid2.rows = DBGrid2.rows + 1
        lblTotalHapus = Format(totalhapus, "#,##0")
End Sub


Private Sub DBGrid3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then DBGrid3_DblClick
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    queryJual = "Select ID,Tanggal,[Kode Customer] as Customer,Total from t_penjualanh"
'    loadgrid
    DBGrid2.ColWidth(0) = 500
    DBGrid2.ColWidth(1) = 1900
    DBGrid2.ColWidth(2) = 2100
    DBGrid2.ColWidth(3) = 1500
    DBGrid2.TextMatrix(0, 1) = "ID"
    DBGrid2.TextMatrix(0, 2) = "Barang"
    DBGrid2.TextMatrix(0, 3) = "Total"
    DTPicker1 = Now
    DBGrid3.ColWidth(0) = 500
    DBGrid3.ColWidth(1) = 2500
    DBGrid3.ColWidth(2) = 3000
    DBGrid3.ColWidth(3) = 1000
    DBGrid3.ColWidth(4) = 1500
    DBGrid3.ColWidth(5) = 1500
    DBGrid3.ColWidth(6) = 2000
    DBGrid3.ColWidth(7) = 0
    DBGrid3.ColAlignment(1) = 1
    DBGrid3.TextMatrix(0, 1) = "KD Barang"
    DBGrid3.TextMatrix(0, 2) = "Nama Barang"
    DBGrid3.TextMatrix(0, 3) = "Qty"
    DBGrid3.TextMatrix(0, 4) = "Harga"
    DBGrid3.TextMatrix(0, 5) = "Disc"
    DBGrid3.TextMatrix(0, 6) = "Total"
    DBGrid1.ColWidth(0) = 500
    DBGrid1.ColWidth(1) = 1900
    DBGrid1.ColWidth(2) = 2800
    DBGrid1.ColWidth(3) = 1400
    DBGrid1.ColWidth(4) = 1500
    DBGrid1.ColAlignment(2) = 1
    DBGrid1.TextMatrix(0, 1) = "ID"
    DBGrid1.TextMatrix(0, 2) = "Tanggal"
    DBGrid1.TextMatrix(0, 3) = "Kode Customer"
    DBGrid1.TextMatrix(0, 4) = "Total"
    reset_form
End Sub
Private Sub reset_form()
totalhapus = 0
total = 0
lblTotal = Format(total, "#,##0")
lblTotalHapus = Format(totalhapus, "#,##0")
DBGrid2.rows = 1
DBGrid2.rows = 2
End Sub
Private Sub loadgrid()
On Error GoTo err
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
Dim where As String
    If Not db1 Then Exit Sub
    Adodc1.ConnectionString = strcon
    If Option3.Value = True Then
        
        If Option1.Value Then
            where = " format(tanggal,'dd/MM/yyyy')='" & Format(DTPicker1, "dd/MM/yyyy") & "'"
        End If
        If Option2.Value Then
            where = " format(tanggal,'MM/yyyy')='" & Format(DTPicker1, "MM/yyyy") & "'"
        End If
        Adodc1.RecordSource = queryJual & " where " & where & " order by id"
    ElseIf Option4.Value = True Then
        If txtKdBrg.text <> "" Then
            where = " and [nama barang] like '%" & txtKdBrg.text & "%'"
        End If
        Adodc1.RecordSource = querybrg & " " & where & " order by [kode barang]"
    End If
    
    
    
    
'    Set DBGrid1.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid1.rows = 1
    DBGrid1.rows = 2
    
    If Not Adodc1.Recordset.EOF Then
    Adodc1.Recordset.MoveFirst
    row = 1

    While Not Adodc1.Recordset.EOF
    DBGrid1.TextMatrix(row, 1) = Adodc1.Recordset(0)
    DBGrid1.TextMatrix(row, 2) = Adodc1.Recordset(1)
    DBGrid1.TextMatrix(row, 3) = Adodc1.Recordset(2)
    If Option3.Value = True Then
    DBGrid1.TextMatrix(row, 4) = Format(Adodc1.Recordset(3), "#,##0")
    Else
    DBGrid1.TextMatrix(row, 4) = ""
    End If
    Adodc1.Recordset.MoveNext
    row = row + 1
    DBGrid1.rows = DBGrid1.rows + 1
    DoEvents
    Wend

    
    End If
    total = 0
    If Option3.Value Then
    For i = 1 To DBGrid1.rows - 2
    total = total + DBGrid1.TextMatrix(i, 4)
    Next
    End If
    lblTotal = Format(total, "#,##0")
'    DBGrid1.Refresh
    
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub mnuExit_Click()
Unload Me
End Sub

Private Sub Option1_Click()
    DTPicker1.CustomFormat = "dd/MM/yyyy"
End Sub

Private Sub Option2_Click()
    DTPicker1.CustomFormat = "MMMM - yyyy"
End Sub

Private Sub Option3_Click()
    Frame1.Visible = True
    Frame2.Visible = False
    DBGrid1.ColWidth(1) = 1900
    DBGrid1.ColWidth(2) = 2800
    DBGrid1.ColWidth(3) = 1400
    DBGrid1.ColWidth(4) = 1500

    DBGrid1.TextMatrix(0, 1) = "ID"
    DBGrid1.TextMatrix(0, 2) = "Tanggal"
    DBGrid1.TextMatrix(0, 3) = "Kode Customer"
    DBGrid1.TextMatrix(0, 4) = "Total"
    loadgrid
    DBGrid3.ColWidth(0) = 500
    DBGrid3.ColWidth(1) = 2500
    DBGrid3.ColWidth(2) = 3000
    DBGrid3.ColWidth(3) = 1000
    DBGrid3.ColWidth(4) = 1500
    DBGrid3.ColWidth(5) = 1500
    DBGrid3.ColWidth(6) = 2000
    DBGrid3.ColWidth(7) = 0
    DBGrid3.ColAlignment(1) = 1
    DBGrid3.TextMatrix(0, 1) = "KD Barang"
    DBGrid3.TextMatrix(0, 2) = "Nama Barang"
    DBGrid3.TextMatrix(0, 3) = "Qty"
    DBGrid3.TextMatrix(0, 4) = "Harga"
    DBGrid3.TextMatrix(0, 5) = "Disc"
    DBGrid3.TextMatrix(0, 6) = "Total"
End Sub

Private Sub Option4_Click()
    DBGrid1.TextMatrix(0, 1) = "Kode Barang"
    DBGrid1.TextMatrix(0, 2) = "Nama"
    DBGrid1.TextMatrix(0, 3) = "Harga"
    DBGrid1.ColWidth(1) = 2000
    DBGrid1.ColWidth(2) = 3000
    DBGrid1.ColWidth(3) = 1400
    DBGrid1.ColWidth(4) = 0
    DBGrid1.ColAlignment(1) = 1
    Frame1.Visible = False
    Frame2.Visible = True
    DBGrid3.ColWidth(0) = 500
    DBGrid3.ColWidth(1) = 1900
    DBGrid3.ColWidth(2) = 2800
    DBGrid3.ColWidth(3) = 1500
    DBGrid3.ColWidth(4) = 1500
    DBGrid3.ColWidth(5) = 1500
    DBGrid3.ColWidth(6) = 2000
    DBGrid3.ColWidth(7) = 0
    DBGrid3.ColAlignment(1) = 1
    DBGrid3.ColAlignment(2) = 1
    DBGrid3.TextMatrix(0, 1) = "ID"
    DBGrid3.TextMatrix(0, 2) = "Tanggal"
    DBGrid3.TextMatrix(0, 3) = "Qty"
    DBGrid3.TextMatrix(0, 4) = "Harga"
    DBGrid3.TextMatrix(0, 5) = "Disc"
    DBGrid3.TextMatrix(0, 6) = "Total"
    DBGrid3.rows = 1
    DBGrid3.rows = 2
    loadgrid
    
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
    If KeyPress = vbKeyReturn Then loadgrid
End Sub
