VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmTutupKasir 
   Caption         =   "Tutup Kasir"
   ClientHeight    =   1815
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4380
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   1815
   ScaleWidth      =   4380
   StartUpPosition =   3  'Windows Default
   Begin Crystal.CrystalReport CRPrint 
      Left            =   3690
      Top             =   1230
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComCtl2.DTPicker dtTanggalTutup 
      Height          =   315
      Left            =   2100
      TabIndex        =   6
      Top             =   270
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   556
      _Version        =   393216
      Format          =   93388801
      CurrentDate     =   41744
   End
   Begin VB.TextBox txtUangTutup 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2100
      TabIndex        =   1
      Top             =   750
      Width           =   1950
   End
   Begin VB.CommandButton cmdTutup 
      Caption         =   "Tutup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1530
      TabIndex        =   0
      Top             =   1230
      Width           =   1185
   End
   Begin VB.Label Label2 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1920
      TabIndex        =   5
      Top             =   330
      Width           =   45
   End
   Begin VB.Label Label1 
      Caption         =   "Tanggal Tutup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   390
      TabIndex        =   4
      Top             =   300
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Jumlah Setoran"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   390
      TabIndex        =   3
      Top             =   765
      Width           =   1410
   End
   Begin VB.Label Label4 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1905
      TabIndex        =   2
      Top             =   765
      Width           =   75
   End
End
Attribute VB_Name = "frmTutupKasir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim id_tutupkasir As String
Private Sub cmdTutup_Click()
    Dim rs As New ADODB.Recordset
    Dim modal_awal As Double
    Dim total_penjualan As Double
    Dim total_retur As Double
    Dim total As Double
    Dim selisih As Double
    If txtUangTutup.text = "" Then
        MsgBox "Silahkan masukkan nominal uang yang akan disetorkan"
        txtUangTutup.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    modal_awal = 0
    rs.Open "select * from t_modalawal where format(tanggal,'yyyy/MM/dd')='" & Format(dtTanggalTutup.value, "yyyy/MM/dd") & "' and userid='" & user & "' and status='buka'", conn
    If Not rs.EOF Then
        modal_awal = rs!modal_awal
    End If
    rs.Close
    total_penjualan = 0
    rs.Open "select sum(harga) as harga from t_penjualand where id in (select id from t_penjualanh where format(tanggal, 'yyyy/MM/dd') = '" & Format(dtTanggalTutup.value, "yyyy/MM/dd") & "')", conn
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            total_penjualan = rs(0)
        End If
    End If
    rs.Close
    total_retur = 0
    rs.Open "select sum(harga_jual) as harga_jual from t_returjuald where id in (select id from t_returjualh where format(tanggal, 'yyyy/MM/dd') = '" & Format(dtTanggalTutup.value, "yyyy/MM/dd") & "')", conn
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            total_retur = rs(0)
        End If
    End If
    rs.Close
    conn.Execute "update t_modalawal set status = 'tutup' where format(tanggal,'yyyy/MM/dd')='" & Format(Now, "yyyy/MM/dd") & "' and userid='" & user & "' and status='buka'"
    total = (modal_awal + total_penjualan) - total_retur
    selisih = total - CDbl(txtUangTutup.text)
    nomor_baru
    conn.Execute "insert into t_tutupkasir (id,tanggal,tanggal_tutup,[user],modal_awal,total_penjualan,total_returjual,saldo_akhir,setor,selisih) values ('" & id_tutupkasir & "','" & Now & "','" & dtTanggalTutup.value & "','" & user & "','" & modal_awal & "','" & total_penjualan & "','" & total_retur & "','" & total & "','" & txtUangTutup.text & "','" & selisih & "')"
    conn.Close
    cetaknota
    Unload Me
    End
End Sub

Private Sub Form_Activate()
    dtTanggalTutup.value = Now
    txtUangTutup.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
End Sub

Private Sub cetaknota()
Dim query As String
Dim qryName As String
Dim rs As New ADODB.Recordset
On Error GoTo err
    'cetaknota = False
       
    With CRPrint
        .ReportFileName = App.Path & "\Report\print out tutupkasir.rpt"
        
        For i = 0 To .RetrieveDataFiles - 1
           .DataFiles(i) = servname
        Next
        .Password = Chr$(10) & dbpwd
        .ParameterFields(0) = "namaperusahaan;" & GNamaPerusahaan & ";true"
        .ParameterFields(1) = "alamat;" & GAlamatPerusahaan & ";true"
        .ParameterFields(2) = "login;" & user & ";true"
        .ParameterFields(3) = "tanggal;" & Format(dtTanggalTutup.value, "dd MMMM yyyy") & ";true"
        .ParameterFields(4) = "jumlah_setor;" & txtUangTutup.text & ";true"

        Dim PrinterLoop As printer
        For Each PrinterLoop In Printers

        If PrinterLoop.DeviceName = printername Then
                .printername = printername
                .PrinterDriver = PrinterLoop.DriverName
                .PrinterPort = PrinterLoop.Port

        End If

        Next
        .ProgressDialog = False
        
        .destination = crptToPrinter
        .action = 1
    End With
    
    Exit Sub
err:
    If err.Number <> -2147217865 Then MsgBox err.Description Else Resume Next

End Sub

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from t_tutupkasir where left(ID,4)='TK" & Format(Now, "yy") & "' and mid(ID,5,2)='" & Format(Now, "MM") & "'  and mid(ID,7,2)='" & Format(Now, "dd") & "' order by clng(mid(ID,3,len(id))) desc", conn
    If Not rs.EOF Then
        No = "TK" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format((CLng(Mid(rs(0), 9, (Len(rs(0)) - 8))) + 1), "000")
    Else
        No = "TK" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format("1", "000")
    End If
    rs.Close
    id_tutupkasir = No
End Sub
