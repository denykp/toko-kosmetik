Attribute VB_Name = "modProcedure"
Public Type POINTAPI
        X As Long
        y As Long
End Type
Public Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal y As Long) As Long
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
'Public Sub main()
'    Dim filename As String
'
'    Open filename For Input As #1
'    Input #1, strconn
'    Close #1
'    filename = Left(filename, Len(filename) - 6)
'
'End Sub

Public katakunci, kunci As String
Public gBulanAwal As String
Public gTahunAwal As String

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' U/ mambatasi input hanya bisa numeric tidak bisa huruf
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub NumberOnly(ByRef key As Integer)
    Select Case key
        Case Asc("0") To Asc("9"):
        Case 8:
        Case 13:
        Case 46:
        Case Else:
            key = 0
    End Select
End Sub
Function FormatEngineering(Number As Variant, Optional DecimalPlaces As Long = 1) As String
FormatEngineering = Format(Split(Format(Number, "0.0#############E+0"), _
    "E")(0) * 10 ^ (Split(Format(Number, _
    "0.0#############E+0"), "E")(1) - 3 * Int( _
    Split(Format(Number, "0.0#############E+0"), _
    "E")(1) / 3)), "0." & String(DecimalPlaces, _
    "0")) & "E" & Format(3 * Int(Split(Format( _
    Number, "0.0#############E+0"), "E")(1) / _
    3), "+0;-0")
End Function
Public Sub SetComboText(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If combo.List(Index) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub SetComboTextLeft(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If Left(combo.List(Index), Len(teks)) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub SetComboTextRight(ByVal teks As String, ByRef combo As ComboBox)
    Dim Index As Integer

    For Index = 0 To combo.ListCount - 1
        If Right(combo.List(Index), Len(teks)) = teks Then
            combo.ListIndex = Index
            Exit For
        End If
    Next Index
End Sub
Public Sub ClearText(frm As Form, NumberOfField As Integer)
    Dim i As Integer
    Dim ControlNm As String
    
    For i = 0 To NumberOfField
        ControlNm = "Field" & CStr(i)
        With frm.Controls(ControlNm)
            Select Case .Tag
                Case "Text": .text = ""
                Case "Combo": Call SetComboText("", frm.Controls(ControlNm))
                Case "Label": .Caption = ""
                Case "Option": .value = False
                Case "Number": .Caption = "0"
                Case "Date": .value = Date
            End Select
        End With
    Next
    
End Sub

Public Sub SetFormPosition(frm As Form)
    frm.Left = 0
    frm.top = 0
End Sub

' Kalau Sub Form dibuka(Sts = false), Main Form & MDI Form di-disable
Public Sub EnableForm(frm As Form, Sts As Boolean)
    If Sts = True Then
        frm.Enabled = True
'        frmMDI.mnuMain(1).Enabled = True
    Else
        frm.Enabled = False
'        frmMDI.mnuMain(1).Enabled = False
    End If
End Sub


Public Sub Get_Data(Form As Form, Start_Array As Single, End_Array As Single, Table_Nm As String, Condition As String)
    Dim strSQL As String
    Dim i As Single
    Dim fields As String
    Dim rs1 As New ADODB.Recordset
    
    For i = Start_Array To End_Array
        fields = fields & Form.Label(i).Tag & ","
    Next i
    fields = Left(fields, Len(fields) - 1)
    
    strSQL = "SELECT " & fields & " FROM " & Table_Nm
    strSQL = strSQL & " WHERE " & Condition
    rs1.Open strSQL, conn
    If Not rs1.EOF Then
        For i = Start_Array To End_Array
            Form.Label(i).Caption = rs1.fields(i - 1)
        Next i
    Else
        For i = Start_Array To End_Array
            Form.Label(i).Caption = ""
        Next i
    End If
    rs1.Close
End Sub


'Public Sub PrintOut(FileName As String, Formula As String, CRPrint As CrystalReport, Parameter As String)
'    Dim i As Single
'    With CRPrint
'        .ReportFileName = App.Path & FileName
'
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";UID=" & Conn.Properties(68).value & ";PWD=" & Conn.Properties(66).value & ";DSQ=" & Conn.DefaultDatabase
'        Next
'        .SelectionFormula = "{absensi.tanggal}<'" & Date & "'"
'        'If Parameter <> "" Then .ParameterFields(0) = "USER;" + Parameter + ";True"
'        .PrinterName = ""
'        .PrinterSelect
'        .Destination = crptToWindow
'        .WindowTitle = "Print Out"
'        .WindowState = crptMaximized
'        .WindowShowExportBtn = False
'        If .PrinterName <> "" Then .Action = 1
'    End With
'End Sub



Public Sub activateMDI()
On Error GoTo err
Dim rs1 As New ADODB.Recordset
    If LCase(login) = "sa" Then
        rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and ms_menu.idx=access.idx and userid='" & login & "' order by ms_menu.nm_menu", conn
        While Not rs1.EOF
            frmMDI.Controls(rs1(0))(rs1(1)).Visible = True
            frmMDI.Controls(rs1(0))(rs1(1)).Enabled = True
            rs1.MoveNext
        Wend
        rs1.Close
    Else
        rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and ms_menu.idx=access.idx and userid='" & login & "' order by ms_menu.nm_menu", conn
        While Not rs1.EOF
            If rs1(2) = "1" Then
                'If rs1(3) = "0" Then
                frmMDI.Controls(rs1(0))(rs1(1)).Visible = True
                'Else
                'frmMDI.Controls(rs1(0))(rs1(1)).Enabled = True
                'End If
            Else
                'If rs1(3) = "0" Then
                frmMDI.Controls(rs1(0))(rs1(1)).Visible = False
                'Else
                'frmMDI.Controls(rs1(0))(rs1(1)).Enabled = False
                'End If
            End If
        rs1.MoveNext
        Wend
        rs1.Close
    End If
    Exit Sub
err:
    Resume Next
End Sub
Public Sub deactivateMDI()
On Error GoTo err
Dim rs1 As New ADODB.Recordset
rs1.Open "select ms_menu.nm_menu,ms_menu.idx,isnull(access,0),ms_menu.tipe from ms_menu left join access on ms_menu.nm_menu=access.nm_menu and userid='" & login & "' order by ms_menu.nm_menu", conn
While Not rs1.EOF
    'If rs1(3) = "0" Then
        frmMDI.Controls(rs1(0))(CInt(rs1(1))).Visible = False
    'Else
    '    frmMDI.Controls(rs1(0))(CInt(rs1(1))).Enabled = False
    'End If
rs1.MoveNext
Wend
rs1.Close
Exit Sub
err:
    Resume Next
End Sub
Public Function tambah_data2(table_name As String, field() As String, nilai() As String) As String
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & field(i) & ","
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    
    tambah_data2 = query
    'MsgBox query
End Function
Public Sub tambah_data(table_name As String, field() As String, nilai() As String)
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & field(i) & ","
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    conn.Execute query
    'MsgBox query
End Sub
Public Sub tambah_data3(table_name As String, field() As String, nilai() As String, conn2 As ADODB.Connection)
Dim query As String
    query = "insert into " & table_name & " ("
    For i = 0 To UBound(field) - 1
        query = query & field(i) & ","
    Next
    query = Left(query, Len(query) - 1) & ") values ("
    For i = 0 To UBound(nilai) - 1
        query = query & "'" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & ")"
    conn2.Execute query
    'MsgBox query
End Sub

Public Sub update_data(table_name As String, field() As String, nilai() As String, where As String, cn As ADODB.Connection)
Dim query As String
    query = "update " & table_name & " set "
    For i = 0 To UBound(field) - 1
        query = query & field(i) & "='" & nilai(i) & "',"
    Next
    query = Left(query, Len(query) - 1) & " where " & where
    cn.Execute query
    'MsgBox query
End Sub

Public Sub exportdataT(id As String, modul As String, namafile As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim A As New Scripting.FileSystemObject
Dim query, from, output As String
Dim field(), tipe() As String
Dim panjang(), counter As Byte
    'If a.FileExists(namafile) Then a.DeleteFile namafile
    conn.ConnectionString = strcon
    conn.Open
    A.CreateTextFile namafile, True
    query = ""
    ReDim field(1)
    ReDim tipe(1)
    ReDim panjang(1)
    counter = 1
    rs.Open "select * from mapping_export where modul='" & modul & "' order by urutan", conn
    While Not rs.EOF
        ReDim Preserve field(counter)
        ReDim Preserve tipe(counter)
        ReDim Preserve panjang(counter)
        field(counter - 1) = rs(2)
        tipe(counter - 1) = rs(3)
        panjang(counter - 1) = rs(4)
        query = query & rs(2) & ","
        If from = "" Then from = rs(1)
        rs.MoveNext
        counter = counter + 1
    Wend
    rs.Close
    query = "select " & Left(query, Len(query) - 1) & " from " & from & " where id='" & id & "'"

   Open namafile For Output As #1
    rs.Open query, conn
    While Not rs.EOF
        output = modul & ";"
        For i = 0 To rs.fields.Count - 1
            Select Case LCase(tipe(i))
            Case "varchar":
                output = output & rs(i) & Space(CInt(panjang(i)) - Len(rs(i))) & ";"
            Case "datetime":
                output = output & Format(rs(i), "MM/dd/yy") & Space(CInt(panjang(i)) - Len(Format(rs(i), "MM/dd/yy"))) & ";"
            Case "numeric":
                output = output & Format(rs(i), "#,##0.00") & Space(CInt(panjang(i)) - Len(Format(rs(i), "#,##0.00"))) & ";"
            End Select
        Next
        Print #1, output
        rs.MoveNext
    Wend
    rs.Close
    Close #1
End Sub
Public Sub updatestock(ByVal gudang As String, ByVal kdbrg As String, qty As Long, cn As ADODB.Connection)
Dim rs As New ADODB.Recordset
If qty <> 0 Then
    rs.Open "select * from stock where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'", cn
    If Not rs.EOF Then
    cn.Execute "update stock set stock=stock+" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
    Else
    cn.Execute "insert into stock ([kode barang],gudang,stock) values ('" & kdbrg & "','" & gudang & "'," & qty & ")"
    End If
    'cn.Execute "update stock1 set stock=stock+" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
End If
Set rs = Nothing
End Sub
Public Sub updatestock2(ByVal gudang As String, ByVal kdbrg As String, qty As Long, cn As ADODB.Connection)
    cn.Execute "update stock set stock=" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
    'cn.Execute "update stock1 set stock=" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
End Sub
Public Sub updatestock3(ByVal gudang As String, ByVal kdbrg As String, qty As Long, cn As ADODB.Connection)
    cn.Execute "update stock set stock=" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
End Sub
Public Sub updatestock4(ByVal gudang As String, ByVal kdbrg As String, qty As Long, cn As ADODB.Connection)
    'cn.Execute "update stock1 set stock=" & qty & " where gudang='" & gudang & "' and [kode barang]='" & kdbrg & "'"
End Sub


Public Sub generate_dailystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
conn.Open strcon
conn.Execute "delete from tmp_stockharian where tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"

    conn.Execute "insert into tmp_stockharian (tahun,bulan,tanggal,[kode barang],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "','" & Format(tanggal, "MM") & "','" & Format(tanggal, "dd") & "',ms_barang.[kode barang],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode barang]= kode barang] and  format(h.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(q.tanggal,'MM/dd/yyyy')<'" & Format(tanggal, "MM/dd/yyyy") & "' and [kode barang]=ms_barang.[kode barang])),0," & _
    "(select sum(qty) from q_mutasistock q where format(q.tanggal,'MM/dd/yyyy')<'" & Format(tanggal, "MM/dd/yyyy") & "' and [kode barang]=ms_barang.[kode barang])) as stockawal from ms_barang"
'rs.Open "select * from tmp_stockharian", conn
'While Not rs.EOF
'    conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "MM/dd/yyyy") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
'conn.Close
End Sub
Public Sub generate_dailyreport(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.Execute "delete from tmp_lapharian "
'    rs.Open "select distinct userid from t_penjualanh where format(tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "'", conn
'    While Not rs.EOF
    conn.Execute "insert into tmp_lapharian (tanggal,[modal awal],tunai,kartu,retur,userid,pk,keuntungan) select p.tanggal,(select modal_awal from t_modalawal where format(tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and userid=l.username)," & _
    "sum(jumlahbayar) as t,sum(kartu) as k,(select iif(isnull(sum(qty*harga_jual)),0,sum(qty*harga_jual)) from t_returjuald inner join t_returjualh on t_returjuald.id=t_returjualh.id where format(tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and userid=l.username) ,[username],pk,(select sum(iif(tipe=1,(qty*(harga-disc-disctotal-hpp))+charge,(qty*(harga-hpp)*-1))) from q_omset qd where format(qd.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and qd.userid=l.username) " & _
    "from ([login] l left join t_penjualanh h on h.userid=l.username) left join t_pembayaran p on h.id=p.no_jual  where format(p.tanggal,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "'  group by [username],p.tanggal,pk"
'    rs.MoveNext
'    Wend
End Sub
Public Sub generate_RL(tanggal1 As Date, tanggal2 As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
    conn.Execute "delete from tmp_RL"
    rs.Open "select sum(total) from t_penjualanh h where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (1,1,'Pendapatan','Penjualan','" & IIf(IsNull(rs(0)), 0, rs(0)) & "',0)"
    End If
    rs.Close
    rs.Open "select sum(qty*(harga_jual)) from t_returjuald d inner join t_returjualh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (2,1,'Pendapatan','Retur Jual',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    rs.Open "select sum(qty*hpp) from t_penjualand d inner join t_penjualanh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (3,2,'HPP','HPP',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    Dim counter As Integer
    counter = 4
    rs.Open "select t.kode_biaya,m.nama_biaya,sum(jumlah) as jumlah from t_biaya t inner join ms_biaya m on t.kode_biaya=m.kode_biaya where  format(t.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' group by t.kode_biaya,m.nama_biaya", conn
    While Not rs.EOF
        conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (" & counter & ",3,'Biaya','" & rs!nama_biaya & "',0,'" & IIf(IsNull(rs(2)), 0, rs(2)) & "')"
        counter = counter + 1
    rs.MoveNext
    Wend
    rs.Close
    
    rs.Open "select sum(qty*hpp) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (" & counter & ",4,'Biaya Lain-lain','Pengambilan Barang',0,'" & IIf(IsNull(rs(0)), 0, rs(0)) & "')"
    End If
    rs.Close
    counter = counter + 1
    rs.Open "select sum(selisih*hpp) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where format(h.tanggal,'yyyy/MM/dd') between '" & Format(tanggal1, "yyyy/MM/dd") & "' and '" & Format(tanggal2, "yyyy/MM/dd") & "' and status='1'", conn
    If Not rs.EOF Then
    conn.Execute "insert into tmp_RL (urut,urutgrup,grup,description,debit,kredit) values (" & counter & ",4,'Biaya Lain-lain','Stock Opname','" & IIf(IsNull(rs(0)), 0, IIf(rs(0) < 0, 0, rs(0))) & "','" & IIf(IsNull(rs(0)), 0, IIf(rs(0) > 0, 0, rs(0))) & "')"
    End If
    rs.Close
    conn.Close
End Sub

Public Sub generate_monthlystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
conn.Execute "delete from tmp_stockbulanan where tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "'"
    conn.Execute "insert into tmp_stockbulanan (tahun,bulan,[kode barang],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "','" & Format(tanggal, "MM") & "',ms_barang.[kode barang],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'MM/yyyy')='" & Format(tanggal, "MM/yyyy") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(h.tanggal,'MM/yyyy')<'" & Format(tanggal, "MM/yyyy") & "' and [kode barang]=ms_barang.[kode barang])),0," & _
    "(select sum(qty) from q_mutasistock q where format(h.tanggal,'MM/yyyy')<'" & Format(tanggal, "MM/yyyy") & "' and [kode barang]=ms_barang.[kode barang])) as stockawal from ms_barang"
'rs.Open "select * from ms_barang", conn
'While Not rs.EOF
    'conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "MM/yyyy") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
conn.Close
End Sub
Public Sub generate_yearlystock(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    conn.Open strcon
conn.Execute "delete from tmp_stocktahunan where tahun='" & Format(tanggal, "yyyy") & "'"
'rs.Open "select * from ms_barang", conn
'While Not rs.EOF
    conn.Execute "insert into tmp_stocktahunan (tahun,[kode barang],beli,jual,returbeli,returjual,adjustment,opname,stockawal) " & _
    "select '" & Format(tanggal, "yyyy") & "',ms_barang.[kode barang],  " & _
    "iif(isnull((select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' and status='1')),0, " & _
    "(select sum(Qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' and status='1')) as beli, " & _
    "iif(isnull((select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_penjualand d inner join t_penjualanh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as jual, " & _
    "iif(isnull((select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returbelid d inner join t_returbelih h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as returbeli, " & _
    "iif(isnull((select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_returjuald d inner join t_returjualh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as returjual, " & _
    "iif(isnull((select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(Qty) from t_adjustmentd d inner join t_adjustmenth h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as adjustment, " & _
    "iif(isnull((select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')),0, " & _
    "(select sum(selisih) from t_stockopnamed d inner join t_stockopnameh h on d.id=h.id where [kode barang]=ms_barang.[kode barang] and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "')) as stockopname, " & _
    "iif(isnull((select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy')<'" & Format(tanggal, "yyyy") & "' and [kode barang]=ms_barang.[kode barang])),0," & _
    "(select sum(qty) from q_mutasistock q where format(q.tanggal,'yyyy')<'" & Format(tanggal, "yyyy") & "' and [kode barang]=ms_barang.[kode barang])) as stockawal from ms_barang"
    'conn.Execute "update tmp_stockharian set beli=(select sum(qty) from t_pembeliand d inner join t_pembelianh h on d.id=h.id where [kode barang]='" & rs(0) & "' and h.tanggal='" & Format(tanggal, "yyyy") & "') where  tahun='" & Format(tanggal, "yyyy") & "' and bulan='" & Format(tanggal, "MM") & "' and tanggal='" & Format(tanggal, "dd") & "'"
'    rs.MoveNext
'Wend
'rs.Close
conn.Close
End Sub
Public Sub genomset(tanggaldari As Date, tanggalsampai As Date, Optional kodebarang As String)
Dim conn As New ADODB.Connection
conn.Open strcon
conn.Execute "delete from tmp_omset"
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'yyyy/MM/dd')>='" & Format(tanggaldari, "yyyy/MM/dd") & "' and format(tanggal,'yyyy/MM/dd')<='" & Format(tanggalsampai, "yyyy/MM/dd") & "'" & IIf(kodebarang = "", "", " and [kode barang]='" & kodebarang & "'")
conn.Close
End Sub
Public Sub genomsetmonth(tahun As String, bulan As String, Optional kodebarang As String)
Dim conn As New ADODB.Connection
conn.Open strcon
conn.Execute "delete from tmp_omset"
If bulan <> "" Then
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'MM/yyyy')>='" & bulan & "/" & tahun & "'" & IIf(kodebarang = "", "", " and [kode barang]='" & kodebarang & "'")
Else
conn.Execute "insert into tmp_omset select * from q_omset where format(tanggal,'yyyy')>='" & tahun & "'" & IIf(kodebarang = "", "", " and [kode barang]='" & kodebarang & "'")
End If
conn.Close
End Sub

Public Sub generate_kartustock(tanggal As Date, tanggal2 As Date, Optional kategori As String, Optional search As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim stockawal, stockakhir As Long
Dim baru As Boolean
    conn.Open strcon
conn.Execute "delete from tmp_kartustock"
If kategori <> "" And search <> "" Then
conn.Execute "insert into tmp_kartustock (tipe,gudang,id,tanggal,[kode barang],masuk,keluar) select * from q_kartustock where format(tanggal,'yyyy/MM/dd')>'" & Format(tanggal - 1, "MM/dd/yyyy") & "' and format(tanggal,'yyyy/MM/dd')<'" & Format(tanggal2 + 1, "yyyy/MM/dd") & "' and " & kategori & "='" & search & "'"
Else
conn.Execute "insert into tmp_kartustock (tipe,gudang,id,tanggal,[kode barang],masuk,keluar) select * from q_kartustock where format(tanggal,'yyyy/MM/dd')>'" & Format(tanggal - 1, "MM/dd/yyyy") & "' and format(tanggal,'yyyy/MM/dd')<'" & Format(tanggal2 + 1, "yyyy/MM/dd") & "'"
End If
rs.Open "select distinct [kode barang],gudang from tmp_kartustock", conn
While Not rs.EOF
    stockawal = 0
    baru = True
    rs2.Open "select * from tmp_kartustock where [kode barang]='" & rs(0) & "' and gudang='" & rs(1) & "' order by tanggal desc", conn
    While Not rs2.EOF
        If baru Then
            stockakhir = getstockakhirkartu(rs2("kode barang"), rs2("id"), rs2("tanggal"), rs2("gudang"))
        End If
        conn.Execute "update tmp_kartustock set stockawal=" & stockakhir & "+keluar-masuk where  id='" & rs2("id") & "' and [kode barang]='" & rs2("kode barang") & "' and gudang='" & rs2("gudang") & "'"
        stockakhir = stockakhir - rs2("masuk") + rs2("keluar")
        baru = False
        rs2.MoveNext
    Wend
    rs2.Close
    rs.MoveNext
Wend
rs.Close
conn.Close
End Sub
Public Function getstockakhirkartu(kodebarang As String, id As String, tanggal As Date, gudang As String) As Long
Dim con As New ADODB.Connection
Dim rs As New ADODB.Recordset
con.Open strcon
rs.Open "select stock-(select iif(isnull(sum(qty)),0,sum(qty)) from t_pembelianh bh inner join t_pembeliand bd on bh.id=bd.id where bd.[kode barang]=jd.[kode barang] and format(bh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and bh.gudang='" & gudang & "' and bh.status='1' )+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returbelih rbh inner join t_returbelid rbd on rbh.id=rbd.id where rbd.[kode barang]=jd.[kode barang] and format(rbh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and rbh.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returjualh rjh inner join t_returjuald rjd on rjh.id=rjd.id where rjd.[kode barang]=jd.[kode barang] and format(rjh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and rjh.gudang='" & gudang & "' and rjh.status='1' )+ " & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_penjualanh tjh inner join t_penjualand tjd on tjh.id=tjd.id where tjd.[kode barang]=jd.[kode barang] and format(tjh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and tjh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_adjustmenth eh inner join t_adjustmentd ed on eh.id=ed.id where ed.[kode barang]=jd.[kode barang] and format(eh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and eh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode barang]=jd.[kode barang] and format(th.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and th.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode barang]=jd.[kode barang] and format(th.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and th.tujuan='" & gudang & "')-" & _
"(select iif(isnull(sum(selisih)),0,sum(selisih)) from t_stockopnameh soh inner join t_stockopnamed sod on soh.id=sod.id where sod.[kode barang]=jd.[kode barang] and format(soh.tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and soh.gudang='" & gudang & "' and soh.status='1')" & _
" from ms_barang jd inner join stock s on jd.[kode barang]=s.[kode barang] where jd.[kode barang]='" & kodebarang & "' and s.gudang='" & gudang & "'", con
getstockakhirkartu = IIf(IsNull(rs(0)), 0, rs(0))
End Function

Public Function getstockawalkartu(kodebarang As String, id As String, tanggal As Date, gudang As String) As Long
Dim con As New ADODB.Connection
Dim rs As New ADODB.Recordset
con.Open strcon
rs.Open "select stockawal+(select iif(isnull(sum(qty)),0,sum(qty)) from t_pembelianh bh inner join t_pembeliand bd on bh.id=bd.id where bd.[kode barang]=jd.[kode barang] and format(bh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and bh.gudang='" & gudang & "' and bh.status='1' )-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returbelih rbh inner join t_returbelid rbd on rbh.id=rbd.id where rbd.[kode barang]=jd.[kode barang] and format(rbh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and rbh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_returjualh rjh inner join t_returjuald rjd on rjh.id=rjd.id where rjd.[kode barang]=jd.[kode barang] and format(rjh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and rjh.gudang='" & gudang & "' and rjh.status='1' )- " & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_penjualanh tjh inner join t_penjualand tjd on tjh.id=tjd.id where tjd.[kode barang]=jd.[kode barang] and format(tjh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and tjh.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_adjustmenth eh inner join t_adjustmentd ed on eh.id=ed.id where ed.[kode barang]=jd.[kode barang] and format(eh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and eh.gudang='" & gudang & "')+" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode barang]=jd.[kode barang] and format(th.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and th.gudang='" & gudang & "')-" & _
"(select iif(isnull(sum(qty)),0,sum(qty)) from t_transfergudangh th inner join t_transfergudangd td on th.id=td.id where td.[kode barang]=jd.[kode barang] and format(th.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and th.tujuan='" & gudang & "')+" & _
"(select iif(isnull(sum(selisih)),0,sum(selisih)) from t_stockopnameh soh inner join t_stockopnamed sod on soh.id=sod.id where sod.[kode barang]=jd.[kode barang] and format(soh.tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' and soh.gudang='" & gudang & "' and soh.status='1')" & _
" from ms_barang jd where [kode barang]='" & kodebarang & "'", con
getstockawalkartu = IIf(IsNull(rs(0)), 0, rs(0))
End Function
Public Sub exportstock()
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend

exws.range(Chr(0 + 65) & "1") = "KodeBarang"
exws.range(Chr(1 + 65) & "1") = "Nama"
exws.range(Chr(2 + 65) & "1") = "Stok"
i = 2
conn.Open strcon
    rs.Open "select m.[kode barang],m.[nama barang],stock from ms_barang m inner join stock s on m.[kode barang]=s.[kode barang] where flag='1'", conn
    While Not rs.EOF
        exws.range(Chr(0 + 65) & CStr(i)) = rs(0)
        exws.range(Chr(1 + 65) & CStr(i)) = rs(1)
        exws.range(Chr(2 + 65) & CStr(i)) = rs(2)
        
        i = i + 1
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
err:
exwb.SaveAs App.Path & "\stock.xls"
exwb.Close
exapp.Application.Quit
MsgBox "export selesai"
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub
Public Sub addprintstatus(notrans As String)
On Error Resume Next
    conn_fake.Execute "update t_penjualanh set print=print+1 where id='" & notrans & "'"
    conn_fake.Close
End Sub
Public Sub ReplaceQuery(query() As String, filtername As String, filter As String)
    For i = 0 To UBound(query)
        If InStr(1, query(i), filtername) > 0 Then
            If filter <> "" Then
                If InStr(1, query(i), "^^") > 0 Then
                    query(i) = Replace(query(i), "^^", "where")
                End If
                query(i) = Replace(query(i), filtername, " and " & filter)
            Else
                query(i) = Replace(query(i), filtername, filter)
            End If
        End If
    Next
End Sub

Public Sub generate_mutasistock_harian(tanggal As Date, tanggal2 As Date, gudang As String)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim stockawal As Long, stockakhir As Long
Dim sBulanPrev As String, sTahunPrev As String
Dim baru As Boolean
    conn.Open strcon
    
    conn.Execute "delete from tmp_mutasi"
    
    If Val(Format(tanggal, "M")) = 1 Then
        sBulanPrev = "12"
        sTahunPrev = Val(Format(tanggal, "yyyy")) - 1
    Else
        sBulanPrev = Val(Format(tanggal, "M")) - 1
        sTahunPrev = Format(tanggal, "yyyy")
    End If

    
    If gBulanAwal = "9" And gTahunAwal = "2010" Then
        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], (i.stockawal+IIf(IsNull(ab.awbeli),0,ab.awbeli)-IIf(IsNull(aj.awjual),0,aj.awjual)-IIf(IsNull(arb.awreturbeli),0,arb.awreturbeli)+IIf(IsNull(arj.awreturjual),0,arj.awreturjual)+IIf(IsNull(awo.awopname),0,awo.awopname)) AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname " & _
                     "FROM (((((((((ms_barang AS i LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awbeli  from t_pembeliand d inner join t_pembelianh h  on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS ab ON i.[kode barang] = ab.[KODE BARANG]) LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awjual  from t_penjualand d inner join t_penjualanh h  on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS aj ON i.[kode barang] = aj.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awreturbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS arb ON i.[kode barang] = arb.[KODE BARANG]) LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as awreturjual  from t_returjuald d inner join  t_returjualh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS arj ON i.[kode barang] = arj.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.selisih) as awopname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS awo ON i.[kode barang] = awo.[KODE BARANG]) LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG]"
    Else
        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], (IIf(IsNull(mb.saldoakhir),0,mb.saldoakhir)+IIf(IsNull(ab.awbeli),0,ab.awbeli)-IIf(IsNull(aj.awjual),0,aj.awjual)-IIf(IsNull(arb.awreturbeli),0,arb.awreturbeli)+IIf(IsNull(arj.awreturjual),0,arj.awreturjual)+IIf(IsNull(awo.awopname),0,awo.awopname)) AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname " & _
                     "FROM ((((((((((ms_barang AS i LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awbeli  from t_pembeliand d inner join t_pembelianh h  on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS ab ON i.[kode barang] = ab.[KODE BARANG]) LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awjual  from t_penjualand d inner join t_penjualanh h  on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS aj ON i.[kode barang] = aj.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as awreturbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS arb ON i.[kode barang] = arb.[KODE BARANG]) LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as awreturjual  from t_returjuald d inner join  t_returjualh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS arj ON i.[kode barang] = arj.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.selisih) as awopname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')<'" & Format(tanggal, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS awo ON i.[kode barang] = awo.[KODE BARANG]) LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'yyyy/MM/dd')>='" & Format(tanggal, "yyyy/MM/dd") & "' and  format(h.tanggal,'yyyy/MM/dd')<='" & Format(tanggal2, "yyyy/MM/dd") & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG]) " & _
                     "LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & sBulanPrev & "%' and tahun='" & sTahunPrev & "') AS mb ON i.[kode barang] = mb.kodebarang"
    End If
    
    conn.Close
End Sub


Public Sub generate_mutasistock_bulanan(tanggal As Date)
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim stockawal As Long, stockakhir As Long
Dim sBulanPrev As String, sTahunPrev As String
Dim baru As Boolean
    conn.Open strcon
    
    conn.Execute "delete from tmp_mutasi"
    
    If Val(Format(tanggal, "M")) = 1 Then
        sBulanPrev = "12"
        sTahunPrev = Val(Format(tanggal, "yyyy")) - 1
    Else
        sBulanPrev = Val(Format(tanggal, "M")) - 1
        sTahunPrev = Format(tanggal, "yyyy")
    End If

    
    If Format(tanggal, "M") = gBulanAwal And Format(tanggal, "yyyy") = gTahunAwal Then
        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], (i.stockawal) AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname " & _
                     "FROM ((((ms_barang AS i   " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG]"
    Else
    
        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], IIf(IsNull(mb.saldoakhir),0,mb.saldoakhir) AS awal, IIf(IsNull(m.beli),0,m.beli) AS beli,IIf(IsNull(m.returbeli),0,-m.returbeli) AS returbeli, IIf(IsNull(m.jual),0,-m.jual) AS jual,  IIf(IsNull(m.returjual),0,m.returjual) AS returjual, iif(isnull(m.opname),0,m.opname) AS opname " & _
                     "FROM (ms_barang AS i LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & Format(tanggal, "M") & "%' and tahun='" & Format(tanggal, "yyyy") & "') AS m ON i.[kode barang] = m.kodebarang) " & _
                     "LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & sBulanPrev & "%' and tahun='" & sTahunPrev & "') AS mb ON i.[kode barang] = mb.kodebarang"
        
    End If
    
    conn.Close
End Sub


'Public Sub generate_mutasistock_tahunan(tanggal As Date)
'Dim conn As New ADODB.Connection
'Dim rs As New ADODB.Recordset
'Dim rs2 As New ADODB.Recordset
'Dim stockawal As Long, stockakhir As Long
'Dim sTahunPrev As String
'Dim baru As Boolean
'    conn.Open strcon
'
'    conn.Execute "delete from tmp_mutasi"
'
'    sTahunPrev = Val(Format(tanggal, "yyyy")) - 1
'
'    If gTahunAwal = "2010" Then
'        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], (i.stockawal) AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname " & _
'                     "FROM ((((ms_barang AS i   " & _
'                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
'                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
'                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
'                     "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
'                     "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & Format(tanggal, "M") & "' and  format(h.tanggal,'yyyy')='" & Format(tanggal, "yyyy") & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG]"
'    Else
'
'        conn.Execute "insert into tmp_mutasi (gudang,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname) SELECT '" & gudang & "',i.[kode barang], IIf(IsNull(mb.saldoakhir),0,mb.saldoakhir) AS awal, IIf(IsNull(m.beli),0,m.beli) AS beli,IIf(IsNull(m.returbeli),0,-m.returbeli) AS returbeli, IIf(IsNull(m.jual),0,-m.jual) AS jual,  IIf(IsNull(m.returjual),0,m.returjual) AS returjual, iif(isnull(m.opname),0,m.opname) AS opname " & _
'                     "FROM (ms_barang AS i LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & Format(tanggal, "M") & "%' and tahun='" & Format(tanggal, "yyyy") & "') AS m ON i.[kode barang] = m.kodebarang) " & _
'                     "LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & sBulanPrev & "%' and tahun='" & sTahunPrev & "') AS mb ON i.[kode barang] = mb.kodebarang"
'
'    End If
'
'    conn.Close
'End Sub


Public Sub insertkartustock(tipe As String, No As String, tanggal As Date, kode_barang As String, qty As Integer, gudang As String, keterangan As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim stockawal As Integer
Dim masuk, keluar As Integer
Dim hpp As Double
rs.Open "select top 1 stockawal+masuk-keluar from tmp_kartustock where gudang='" & gudang & "' and [kode barang]='" & kode_barang & "' and format(tanggal,'yyyy/MM/dd hh:mm:ss')<'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "' order by tanggal desc,id desc", conn
If Not rs.EOF Then
stockawal = rs(0)
Else
stockawal = getStock2(kode_barang, gudang, conn.ConnectionString)
End If
rs.Close
rs.Open "select hpp from hpp where [kode barang]='" & kode_barang & "'", conn
If Not rs.EOF Then
hpp = rs(0)
Else
hpp = 0
End If
rs.Close

masuk = 0
keluar = 0
If qty > 0 Then masuk = qty
If qty < 0 Then keluar = qty * -1
conn.Execute "insert into tmp_kartustock (tipe,gudang,id,tanggal,[kode barang],masuk,keluar,stockawal,keterangan,hppawal,hpp,harga_beli) values " & _
"('" & tipe & "','" & gudang & "','" & No & "','" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "','" & kode_barang & "'," & masuk & "," & keluar & "," & stockawal & ",'" & keterangan & "'," & hpp & "," & hpp & ",0)"
conn.Execute "update tmp_kartustock set stockawal=(stockawal+" & masuk & "-" & keluar & ") where gudang='" & gudang & "' and [kode barang]='" & kode_barang & "' and format(tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "'"
End Sub
Public Sub deletekartustock(tipe As String, No As String, conn As ADODB.Connection)
Dim rs As New ADODB.Recordset
Dim tanggal As Date
Dim masuk, keluar As Integer
Dim kode_barang As String

rs.Open "select * from  tmp_kartustock where id='" & No & "' and tipe='" & tipe & "'", conn
While Not rs.EOF
    tanggal = rs!tanggal
    masuk = rs!masuk
    keluar = rs!keluar
    kode_barang = rs("kode barang")
    conn.Execute "update tmp_kartustock set stockawal=(stockawal-" & masuk & "+" & keluar & ") where [kode barang]='" & kode_barang & "' and format(tanggal,'yyyy/MM/dd hh:mm:ss')>'" & Format(tanggal, "yyyy/MM/dd hh:mm:ss") & "'"
    
    rs.MoveNext
Wend
rs.Close
conn.Execute "delete from tmp_kartustock where id='" & No & "' and tipe='" & tipe & "'"

End Sub



