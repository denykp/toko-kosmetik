VERSION 5.00
Begin VB.Form frmGantiPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ganti Password"
   ClientHeight    =   1905
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   5205
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1905
   ScaleWidth      =   5205
   Begin VB.TextBox txtPasscode2 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   855
      Width           =   2625
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   345
      Left            =   600
      TabIndex        =   3
      Top             =   1350
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   345
      Left            =   3330
      TabIndex        =   4
      Top             =   1350
      Width           =   1125
   End
   Begin VB.TextBox txtPasscode1 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   495
      Width           =   2625
   End
   Begin VB.TextBox txtPasscode 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2340
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   135
      Width           =   2625
   End
   Begin VB.Label Label3 
      Caption         =   "Konfirmasi Password Baru :"
      Height          =   255
      Left            =   315
      TabIndex        =   7
      Top             =   885
      Width           =   1995
   End
   Begin VB.Label Label2 
      Caption         =   "Password Baru :"
      Height          =   255
      Left            =   300
      TabIndex        =   6
      Top             =   525
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Password Lama  :"
      Height          =   255
      Left            =   300
      TabIndex        =   5
      Top             =   165
      Width           =   1695
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmGantiPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim asc1 As New AscSecurity.Cls_security
Private Sub cmdCancel_Click()
    Unload Me

End Sub

Private Sub cmdOK_Click()
On Error GoTo err
Dim result As String

    If txtPasscode1.text <> "" And txtPasscode2.text <> "" Then

        
        conn.ConnectionString = strcon
        conn.Open
        

        rs.Open "select * from login where username='" & user & "'", conn
        If Not rs.EOF Then
            result = rs(1)
        End If
        If rs.State Then rs.Close
        If result <> RC4(txtPasscode.text, katasandi) Then
            MsgBox "Password yang anda masukkan salah"
            Exit Sub
        End If
        If LCase(txtPasscode1.text) = LCase(txtPasscode2.text) Then
            conn.Execute "update login set [password]='" & RC4(LCase(txtPasscode1.text), katasandi) & "' where username='" & user & "'"
            
            MsgBox "Password anda sudah berhasil dirubah"
            reset
        Else
            MsgBox "Passcode baru yang anda masukkan tidak sama"
            txtPasscode1.text = ""
            txtPasscode2.text = ""
            txtPasscode1.SetFocus
        End If
        DropConnection
    Else
        MsgBox "Silahkan isi Password yang baru"
    End If
    Exit Sub
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    DropConnection
End Sub

Private Sub reset()
    txtPasscode.text = ""
    txtPasscode1.text = ""
    txtPasscode2.text = ""
    txtPasscode.SetFocus
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub mnuCancel_Click()
    cmdCancel_Click
End Sub

Private Sub mnuok_Click()
    cmdOK_Click
End Sub
