VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmJatuhTempo 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Jatuh Tempo"
   ClientHeight    =   8565
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   11970
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8565
   ScaleWidth      =   11970
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   4185
      Picture         =   "frmDeletingTrans.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   7830
      Width           =   1230
   End
   Begin MSDataGridLib.DataGrid DBGrid1 
      Height          =   2985
      Left            =   135
      TabIndex        =   3
      Top             =   810
      Width           =   9555
      _ExtentX        =   16854
      _ExtentY        =   5265
      _Version        =   393216
      AllowUpdate     =   0   'False
      BorderStyle     =   0
      HeadLines       =   1
      RowHeight       =   24
      RowDividerStyle =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSDataGridLib.DataGrid DBGrid2 
      Height          =   3300
      Left            =   90
      TabIndex        =   4
      Top             =   4410
      Width           =   11670
      _ExtentX        =   20585
      _ExtentY        =   5821
      _Version        =   393216
      AllowUpdate     =   0   'False
      BorderStyle     =   0
      HeadLines       =   1
      RowHeight       =   24
      RowDividerStyle =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   465
      Left            =   6705
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc Adodc2 
      Height          =   465
      Left            =   8235
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   "Hutang Jatuh Tempo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   90
      TabIndex        =   2
      Top             =   3915
      Width           =   3795
   End
   Begin VB.Label Label1 
      Caption         =   "Stock Habis"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      TabIndex        =   1
      Top             =   315
      Width           =   3795
   End
End
Attribute VB_Name = "frmJatuhTempo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim queryJual, queryBeli As String
Private Sub cmdKeluar_Click()
    Unload Me
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    queryJual = "SELECT Gudang,m.[kode barang] as [Kode Barang],m.[nama barang] as [Nama Barang],q.Stock as Stock from stock q inner join ms_barang m on q.[kode barang]=m.[kode barang] where q.stock<=m.minqty and m.alert='1' order by q.gudang,m.[kode barang]"
    queryBeli = "select h.id,h.tanggal,h.[kode supplier], h.jatuhtempo,sum(d.qty*d.harga) as total from t_pembelianh h inner join t_pembeliand d on h.id=d.id where h.id not in (select no_beli from t_bayarhutang) and format(jatuhtempo,'MM/dd/yyyy')<=format(now,'MM/dd/yyyy') group by h.id,h.tanggal,h.[kode supplier], h.jatuhtempo"
    If db2 Then
    Adodc1.ConnectionString = strconasli
    ElseIf db1 Then
    Adodc1.ConnectionString = strcon
    End If
    If db2 Then
    Adodc2.ConnectionString = strconasli
    ElseIf db1 Then
    Adodc2.ConnectionString = strcon
    End If
    Adodc1.RecordSource = ""
    Adodc1.RecordSource = queryJual
    Adodc1.Refresh
    If Not Adodc1.Recordset.EOF Then
        Set DBGrid1.DataSource = Adodc1
        DBGrid1.Refresh
    End If
    Adodc2.RecordSource = ""
    Adodc2.RecordSource = queryBeli
    Adodc2.Refresh
    If Not Adodc2.Recordset.EOF Then
        Set DBGrid2.DataSource = Adodc2
        DBGrid2.Refresh
    End If
    If Adodc1.Recordset.EOF And Adodc2.Recordset.EOF Then
        Unload Me
    Else
        loadgrid
    End If
End Sub
Private Sub loadgrid()
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
    If Not Adodc1.Recordset.EOF Then
    For i = 0 To Adodc1.Recordset.fields.Count - 1
        If Adodc1.Recordset(i).DefinedSize > 50 Then
            panjang = 50
        Else
            panjang = Adodc1.Recordset(i).DefinedSize
        End If
    DBGrid1.Columns(i).Width = (((panjang * 2) / 3) * 100) + 600
    Next
    End If
    If Not Adodc2.Recordset.EOF Then
    For i = 0 To Adodc2.Recordset.fields.Count - 1
        If Adodc2.Recordset(i).DefinedSize > 50 Then
            panjang = 50
        Else
            panjang = Adodc2.Recordset(i).DefinedSize
        End If
    DBGrid2.Columns(i).Width = (((panjang * 2) / 3) * 100) + 600
    Next
    End If
End Sub

