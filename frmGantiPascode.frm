VERSION 5.00
Begin VB.Form frmGantiPascode 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New User"
   ClientHeight    =   2010
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   5460
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   5460
   Begin VB.TextBox txtPasscode2 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2295
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   990
      Width           =   2625
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   345
      Left            =   945
      TabIndex        =   3
      Top             =   1485
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   345
      Left            =   3420
      TabIndex        =   4
      Top             =   1485
      Width           =   1125
   End
   Begin VB.TextBox txtPasscode1 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2295
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   630
      Width           =   2625
   End
   Begin VB.TextBox txtPasscode 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2295
      TabIndex        =   0
      Top             =   270
      Width           =   2625
   End
   Begin VB.Label Label3 
      Caption         =   "Konfirmasi Passcode Baru :"
      Height          =   255
      Left            =   270
      TabIndex        =   7
      Top             =   1020
      Width           =   1995
   End
   Begin VB.Label Label2 
      Caption         =   "Passcode Baru :"
      Height          =   255
      Left            =   255
      TabIndex        =   6
      Top             =   660
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Passcode  :"
      Height          =   255
      Left            =   255
      TabIndex        =   5
      Top             =   300
      Width           =   1695
   End
   Begin VB.Menu mnuok 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmGantiPascode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()

    If txtPasscode.text <> "" And txtPasscode1.text <> "" And txtPasscode2.text <> "" Then
        
        conn.ConnectionString = strcon
        
        conn.Open
        rs.Open "select * from passcode where pw=convert(binary,'" & txtPasscode.text & "')", conn
        If Not rs.EOF Then
            If txtPasscode1.text = txtPasscode2.text Then
                conn.Execute "update passcode set pw=convert(binary,'" & txtPasscode1.text & "') where pw=convert(binary,'" & txtPasscode.text & "')"
            Else
                MsgBox "Passcode baru yang anda masukkan tidak sama"
                txtPasscode1.text = ""
                txtPasscode2.text = ""
                txtPasscode1.SetFocus
            End If
        Else
            MsgBox "Passcode yang anda masukkan salah"
            reset
        End If
        conn.Close
        
    Else
        MsgBox "semua field harus diisi"
    End If
End Sub

Private Sub reset()
    txtPasscode.text = ""
    txtPasscode1.text = ""
    txtPasscode2.text = ""
    txtPasscode.SetFocus
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub mnuCancel_Click()
    cmdCancel_Click
End Sub

Private Sub mnuok_Click()
    cmdOK_Click
End Sub
