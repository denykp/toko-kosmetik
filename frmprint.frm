VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmprint 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1035
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3330
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1035
   ScaleWidth      =   3330
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkProperties 
      Caption         =   "Print Properties"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   2
      Top             =   675
      Width           =   2085
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   2745
      Top             =   90
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdPrinter 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Printer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1710
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   180
      Width           =   1455
   End
   Begin VB.CommandButton cmdScreen 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Screen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
End
Attribute VB_Name = "frmprint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strfilename, strFormula, param1, param2, param3, param4 As String
Public sort1, sort2 As String
Dim sname As String
Public qryName As String
Public query As String

Private Sub cmdScreen_Click()
cetak 1
End Sub
Private Sub cetak(destination As Byte)
On Error GoTo err
Dim qry As String
    conn.Open strcon
    conn.Execute "drop view " & qryName
    conn.Execute "create view " & qryName & " as " & query
    conn.Close
    With CRPrint
        .reset
        .ReportFileName = App.Path & strfilename
        For i = 0 To .RetrieveDataFiles - 1
            .DataFiles(i) = servname
        Next
        .Password = Chr$(10) & dbpwd
        
        .ParameterFields(0) = "login;" + user + ";True"
        .ParameterFields(1) = "tglDari;" + param1 + ";True"
        .ParameterFields(2) = "tglSampai;" + param2 + ";True"
        If param3 <> "" Then .ParameterFields(3) = "Detail;" + param3 + ";True"
        .ParameterFields(4) = "group1;" + param4 + ";True"
        If destination = 1 Then
        .destination = crptToWindow
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        Else
        .destination = crptToPrinter
        End If
         If chkProperties.value = "1" Then .PrinterSelect
        
        Me.Hide
        .action = 1
    End With

    
    
    Exit Sub
err:
     If err.Number <> -2147217865 Then MsgBox err.Description Else Resume Next
    If conn.State Then conn.Close

End Sub
Private Sub CmdPrinter_Click()
cetak 2
End Sub

Private Sub Form_Activate()
'    SetConnection1 "", ""
End Sub

Private Sub Form_Deactivate()
'    DropConnection
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
   
    sname = servname
       
    
End Sub
