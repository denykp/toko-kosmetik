Attribute VB_Name = "Mod_transferPLU"
''****************************************************************
''Microsoft SQL Server 2000
''Visual Basic file generated for DTS Package
''File Name: C:\Documents and Settings\BuQ\My Documents\Visual Studio Projects\VB6\Toko Buah\Transfer_PLU.bas
''Package Name: Transfer_PLU
''Package Description:
''Generated Date: 12/5/2006
''Generated Time: 2:39:00 PM
''****************************************************************
'
'Option Explicit
'Public goPackageOld As New DTS.Package
'Public goPackage As DTS.Package2
'Private Sub Main()
'        Set goPackage = goPackageOld
'
'        goPackage.Name = "Transfer_PLU"
'        goPackage.WriteCompletionStatusToNTEventLog = False
'        goPackage.FailOnError = False
'        goPackage.PackagePriorityClass = 2
'        goPackage.MaxConcurrentSteps = 4
'        goPackage.LineageOptions = 0
'        goPackage.UseTransaction = True
'        goPackage.TransactionIsolationLevel = 4096
'        goPackage.AutoCommitTransaction = True
'        goPackage.RepositoryMetadataOptions = 0
'        goPackage.UseOLEDBServiceComponents = True
'        goPackage.LogToSQLServer = False
'        goPackage.LogServerFlags = 0
'        goPackage.FailPackageOnLogFailure = False
'        goPackage.ExplicitGlobalVariables = False
'        goPackage.PackageType = 0
'
'
'
''---------------------------------------------------------------------------
'' create package connection information
''---------------------------------------------------------------------------
'
'Dim oConnection As DTS.Connection2
'
''------------- a new connection defined below.
''For security purposes, the password is never scripted
'
'Set oConnection = goPackage.Connections.New("SQLOLEDB")
'
'        oConnection.ConnectionProperties("Integrated Security") = "SSPI"
'        oConnection.ConnectionProperties("Persist Security Info") = True
'        oConnection.ConnectionProperties("Initial Catalog") = "POSDB"
'        oConnection.ConnectionProperties("Data Source") = "(local)"
'        oConnection.ConnectionProperties("Application Name") = "DTS Designer"
'
'        oConnection.Name = "Microsoft OLE DB Provider for SQL Server"
'        oConnection.id = 1
'        oConnection.Reusable = True
'        oConnection.ConnectImmediate = False
'        oConnection.DataSource = "(local)"
'        oConnection.ConnectionTimeout = 60
'        oConnection.Catalog = "POSDB"
'        oConnection.UseTrustedConnection = True
'        oConnection.UseDSL = False
'
'        'If you have a password for this connection, please uncomment and add your password below.
'        'oConnection.Password = "<put the password here>"
'
'goPackage.Connections.Add oConnection
'Set oConnection = Nothing
'
''------------- a new connection defined below.
''For security purposes, the password is never scripted
'
'Set oConnection = goPackage.Connections.New("MSDASQL")
'
'        oConnection.ConnectionProperties("Persist Security Info") = True
'        oConnection.ConnectionProperties("Data Source") = "db1"
'
'        oConnection.Name = "Driver do Microsoft Access (*.mdb)"
'        oConnection.id = 2
'        oConnection.Reusable = True
'        oConnection.ConnectImmediate = False
'        oConnection.DataSource = "db1"
'        oConnection.ConnectionTimeout = 60
'        oConnection.UseTrustedConnection = False
'        oConnection.UseDSL = False
'
'        'If you have a password for this connection, please uncomment and add your password below.
'        'oConnection.Password = "<put the password here>"
'
'goPackage.Connections.Add oConnection
'Set oConnection = Nothing
'
''---------------------------------------------------------------------------
'' create package steps information
''---------------------------------------------------------------------------
'
'Dim oStep As DTS.Step2
'Dim oPrecConstraint As DTS.PrecedenceConstraint
'
''------------- a new step defined below
'
'Set oStep = goPackage.Steps.New
'
'        oStep.Name = "DTSStep_DTSExecuteSQLTask_1"
'        oStep.Description = "Execute SQL Task: undefined"
'        oStep.ExecutionStatus = 4
'        oStep.TaskName = "DTSTask_DTSExecuteSQLTask_1"
'        oStep.CommitSuccess = False
'        oStep.RollbackFailure = False
'        oStep.ScriptLanguage = "VBScript"
'        oStep.AddGlobalVariables = True
'        oStep.RelativePriority = 3
'        oStep.CloseConnection = False
'        oStep.ExecuteInMainThread = False
'        oStep.IsPackageDSORowset = False
'        oStep.JoinTransactionIfPresent = False
'        oStep.DisableStep = False
'        oStep.FailPackageOnError = False
'
'goPackage.Steps.Add oStep
'Set oStep = Nothing
'
''------------- a new step defined below
'
'Set oStep = goPackage.Steps.New
'
'        oStep.Name = "DTSStep_DTSDataPumpTask_1"
'        oStep.Description = "Transform Data Task: undefined"
'        oStep.ExecutionStatus = 4
'        oStep.TaskName = "DTSTask_DTSDataPumpTask_1"
'        oStep.CommitSuccess = False
'        oStep.RollbackFailure = False
'        oStep.ScriptLanguage = "VBScript"
'        oStep.AddGlobalVariables = True
'        oStep.RelativePriority = 3
'        oStep.CloseConnection = False
'        oStep.ExecuteInMainThread = False
'        oStep.IsPackageDSORowset = False
'        oStep.JoinTransactionIfPresent = False
'        oStep.DisableStep = False
'        oStep.FailPackageOnError = False
'
'goPackage.Steps.Add oStep
'Set oStep = Nothing
'
''------------- a precedence constraint for steps defined below
'
'Set oStep = goPackage.Steps("DTSStep_DTSDataPumpTask_1")
'Set oPrecConstraint = oStep.precedenceConstraints.New("DTSStep_DTSExecuteSQLTask_1")
'        oPrecConstraint.StepName = "DTSStep_DTSExecuteSQLTask_1"
'        oPrecConstraint.PrecedenceBasis = 1
'        oPrecConstraint.value = 0
'
'oStep.precedenceConstraints.Add oPrecConstraint
'Set oPrecConstraint = Nothing
'
''---------------------------------------------------------------------------
'' create package tasks information
''---------------------------------------------------------------------------
'
''------------- call Task_Sub1 for task DTSTask_DTSExecuteSQLTask_1 (Execute SQL Task: undefined)
'Call Task_Sub1(goPackage)
'
''------------- call Task_Sub2 for task DTSTask_DTSDataPumpTask_1 (Transform Data Task: undefined)
'Call Task_Sub2(goPackage)
'
''---------------------------------------------------------------------------
'' Save or execute package
''---------------------------------------------------------------------------
'
''goPackage.SaveToSQLServer "(local)", "sa", ""
'goPackage.Execute
'goPackage.Uninitialize
''to save a package instead of executing it, comment out the executing package line above and uncomment the saving package line
'Set goPackage = Nothing
'
'Set goPackageOld = Nothing
'
'End Sub
'
'
''------------- define Task_Sub1 for task DTSTask_DTSExecuteSQLTask_1 (Execute SQL Task: undefined)
'Public Sub Task_Sub1(ByVal goPackage As Object)
'
'Dim oTask As DTS.Task
'Dim oLookup As DTS.Lookup
'
'Dim oCustomTask1 As DTS.ExecuteSQLTask2
'Set oTask = goPackage.Tasks.New("DTSExecuteSQLTask")
'Set oCustomTask1 = oTask.CustomTask
'
'        oCustomTask1.Name = "DTSTask_DTSExecuteSQLTask_1"
'        oCustomTask1.Description = "Execute SQL Task: undefined"
'        oCustomTask1.SQLStatement = "delete" & vbCrLf
'        oCustomTask1.SQLStatement = oCustomTask1.SQLStatement & "FROM         PLU"
'        oCustomTask1.ConnectionID = 2
'        oCustomTask1.CommandTimeout = 0
'        oCustomTask1.OutputAsRecordset = False
'
'goPackage.Tasks.Add oTask
'Set oCustomTask1 = Nothing
'Set oTask = Nothing
'
'End Sub
'
''------------- define Task_Sub2 for task DTSTask_DTSDataPumpTask_1 (Transform Data Task: undefined)
'Public Sub Task_Sub2(ByVal goPackage As Object)
'
'Dim oTask As DTS.Task
'Dim oLookup As DTS.Lookup
'
'Dim oCustomTask2 As DTS.DataPumpTask2
'Set oTask = goPackage.Tasks.New("DTSDataPumpTask")
'Set oCustomTask2 = oTask.CustomTask
'
'        oCustomTask2.Name = "DTSTask_DTSDataPumpTask_1"
'        oCustomTask2.Description = "Transform Data Task: undefined"
'        oCustomTask2.SourceConnectionID = 1
'        oCustomTask2.SourceObjectName = "[POSDB].[dbo].[PLU]"
'        oCustomTask2.DestinationConnectionID = 2
'        oCustomTask2.DestinationObjectName = "`C:\Documents and Settings\BuQ\My Documents\db1`.`PLU`"
'        oCustomTask2.ProgressRowCount = 1000
'        oCustomTask2.MaximumErrorCount = 0
'        oCustomTask2.FetchBufferSize = 1
'        oCustomTask2.UseFastLoad = True
'        oCustomTask2.InsertCommitSize = 0
'        oCustomTask2.ExceptionFileColumnDelimiter = "|"
'        oCustomTask2.ExceptionFileRowDelimiter = vbCrLf
'        oCustomTask2.AllowIdentityInserts = False
'        oCustomTask2.FirstRow = 0
'        oCustomTask2.LastRow = 0
'        oCustomTask2.FastLoadOptions = 2
'        oCustomTask2.ExceptionFileOptions = 1
'        oCustomTask2.DataPumpOptions = 0
'
'Call oCustomTask2_Trans_Sub1(oCustomTask2)
'Call oCustomTask2_Trans_Sub2(oCustomTask2)
'Call oCustomTask2_Trans_Sub3(oCustomTask2)
'Call oCustomTask2_Trans_Sub4(oCustomTask2)
'Call oCustomTask2_Trans_Sub5(oCustomTask2)
'Call oCustomTask2_Trans_Sub6(oCustomTask2)
'Call oCustomTask2_Trans_Sub7(oCustomTask2)
'Call oCustomTask2_Trans_Sub8(oCustomTask2)
'Call oCustomTask2_Trans_Sub9(oCustomTask2)
'
'
'goPackage.Tasks.Add oTask
'Set oCustomTask2 = Nothing
'Set oTask = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub1(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__1"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("PLU1", 1)
'                        oColumn.Name = "PLU1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("PLU1", 1)
'                        oColumn.Name = "PLU1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub2(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__2"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("KODE1", 1)
'                        oColumn.Name = "KODE1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("KODE1", 1)
'                        oColumn.Name = "KODE1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub3(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__3"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("HARGA1", 1)
'                        oColumn.Name = "HARGA1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 120
'                        oColumn.Size = 0
'                        oColumn.DataType = 131
'                        oColumn.Precision = 18
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("HARGA1", 1)
'                        oColumn.Name = "HARGA1"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 116
'                        oColumn.Size = 0
'                        oColumn.DataType = 5
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub4(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__4"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("PLU2", 1)
'                        oColumn.Name = "PLU2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("PLU2", 1)
'                        oColumn.Name = "PLU2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub5(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__5"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("KODE2", 1)
'                        oColumn.Name = "KODE2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("KODE2", 1)
'                        oColumn.Name = "KODE2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub6(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__6"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("HARGA2", 1)
'                        oColumn.Name = "HARGA2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 120
'                        oColumn.Size = 0
'                        oColumn.DataType = 131
'                        oColumn.Precision = 18
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("HARGA2", 1)
'                        oColumn.Name = "HARGA2"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 116
'                        oColumn.Size = 0
'                        oColumn.DataType = 5
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub7(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__7"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("PLU3", 1)
'                        oColumn.Name = "PLU3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("PLU3", 1)
'                        oColumn.Name = "PLU3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub8(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__8"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("KODE3", 1)
'                        oColumn.Name = "KODE3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 104
'                        oColumn.Size = 20
'                        oColumn.DataType = 129
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("KODE3", 1)
'                        oColumn.Name = "KODE3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 100
'                        oColumn.Size = 20
'                        oColumn.DataType = 130
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
'Public Sub oCustomTask2_Trans_Sub9(ByVal oCustomTask2 As Object)
'
'        Dim oTransformation As DTS.Transformation2
'        Dim oTransProps As DTS.Properties
'        Dim oColumn As DTS.Column
'        Set oTransformation = oCustomTask2.Transformations.New("DTS.DataPumpTransformCopy")
'                oTransformation.Name = "DTSTransformation__9"
'                oTransformation.TransformFlags = 63
'                oTransformation.ForceSourceBlobsBuffered = 0
'                oTransformation.ForceBlobsInMemory = False
'                oTransformation.InMemoryBlobSize = 1048576
'                oTransformation.TransformPhases = 4
'
'                Set oColumn = oTransformation.SourceColumns.New("HARGA3", 1)
'                        oColumn.Name = "HARGA3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 120
'                        oColumn.Size = 0
'                        oColumn.DataType = 131
'                        oColumn.Precision = 18
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.SourceColumns.Add oColumn
'                Set oColumn = Nothing
'
'                Set oColumn = oTransformation.DestinationColumns.New("HARGA3", 1)
'                        oColumn.Name = "HARGA3"
'                        oColumn.Ordinal = 1
'                        oColumn.Flags = 116
'                        oColumn.Size = 0
'                        oColumn.DataType = 5
'                        oColumn.Precision = 0
'                        oColumn.NumericScale = 0
'                        oColumn.Nullable = True
'
'                oTransformation.DestinationColumns.Add oColumn
'                Set oColumn = Nothing
'
'        Set oTransProps = oTransformation.TransformServerProperties
'
'
'        Set oTransProps = Nothing
'
'        oCustomTask2.Transformations.Add oTransformation
'        Set oTransformation = Nothing
'
'End Sub
'
