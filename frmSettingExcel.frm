VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSettingExcel 
   Caption         =   "Form1"
   ClientHeight    =   5700
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8580
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5700
   ScaleWidth      =   8580
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      DragIcon        =   "frmSettingExcel.frx":0000
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4470
      Left            =   135
      TabIndex        =   4
      Top             =   720
      Width           =   2535
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "&Open File"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1080
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   180
      Width           =   1125
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8010
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   4515
      Left            =   2880
      TabIndex        =   3
      Top             =   720
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   7964
      _Version        =   393216
      Cols            =   3
      BorderStyle     =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblFileName 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2295
      TabIndex        =   1
      Top             =   270
      Width           =   180
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama File : "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   0
      Top             =   270
      Width           =   915
   End
End
Attribute VB_Name = "frmSettingExcel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
Public list As String
Public modul As String
Private Sub cmdOpen_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    lblFileName = CommonDialog1.filename
    If fs.FileExists(CommonDialog1.filename) Then
        load_excel
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub load_excel()
Dim con As New ADODB.Connection
Dim rs As New ADODB.Recordset
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & lblFileName & ";Persist Security Info=False;Extended Properties=""Excel 8.0;IMEX=1;TypeGuessRows=0"""
con.Open stcon
List1.Clear
rs.Open "select * from [sheet1$]", con
For i = 0 To rs.fields.Count - 1
    List1.AddItem rs(i).Name
Next
rs.Close
con.Close

End Sub

Private Sub flxGrid_DragDrop(Source As Control, x As Single, y As Single)
    If LCase(Source.Name) <> "list1" Then Exit Sub

    ' use the flex grid mouse tracking to decide which cell to
    ' copy the text to...or use a specific row and/or column if
    ' you need to limit the copy to a certain cell
    flxGrid.TextMatrix(flxGrid.MouseRow, 2) = Source.text
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 2500
    flxGrid.ColWidth(2) = 2000
    flxGrid.TextMatrix(0, 1) = "Nama"
    flxGrid.TextMatrix(0, 2) = "Field"
    Dim row() As String
    row = Split(list, ",")
    flxGrid.rows = UBound(row) + 2
    For i = 0 To UBound(row)
        flxGrid.TextMatrix(i + 1, 1) = row(i)
        flxGrid.TextMatrix(i + 1, 2) = sGetINI(App.Path & "\import.ini", modul, row(i), "")
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
On Error Resume Next
    For i = 1 To flxGrid.rows
        writeINI App.Path & "\import.ini", modul, flxGrid.TextMatrix(i, 1), flxGrid.TextMatrix(i, 2)
    Next
End Sub

'
Private Sub List1_Click()

End Sub

Private Sub List1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = vbLeftButton Then List1.Drag vbBeginDrag
End Sub
