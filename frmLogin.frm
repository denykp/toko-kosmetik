VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Login"
   ClientHeight    =   2265
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5550
   Icon            =   "frmLogin.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2265
   ScaleWidth      =   5550
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   180
      Top             =   1845
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSearchDB 
      Caption         =   "F3"
      Height          =   285
      Left            =   4635
      TabIndex        =   8
      Top             =   135
      Width           =   420
   End
   Begin VB.ComboBox txtServerName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1485
      Style           =   1  'Simple Combo
      TabIndex        =   7
      Top             =   135
      Width           =   3075
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2910
      TabIndex        =   5
      Top             =   1530
      Width           =   1500
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "&Login"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   1140
      TabIndex        =   4
      Top             =   1530
      Width           =   1500
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1485
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   900
      Width           =   1905
   End
   Begin VB.TextBox txtUserName 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   1
      Top             =   495
      Width           =   1905
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "DB"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   6
      Top             =   180
      Width           =   1140
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   2
      Top             =   945
      Width           =   1005
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   0
      Top             =   540
      Width           =   1005
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public mSQLServer As New SQLDMO.SQLServer
'Public mApplication As New SQLDMO.Application
Private OpenForm_Fg As Boolean

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLogin_Click()
Dim modal As Long
On Error GoTo err

    Call SetConnection(txtServerName.text, txtUserName.text, txtPassword.text)
    
    conn.Open strcon
    rs.Open "select * from login where username='" & txtUserName.text & "' and modul='" & modul & "'", conn

    If Not rs.EOF Then
        
        If rs(1) = RC4(txtPassword.text, katasandi) Then
        Else
            MsgBox "Password yang anda masukkan salah"
            txtPassword.text = ""
            txtPassword.SetFocus
            GoTo ex
        End If
    Else
        MsgBox "Username tidak ditemukan"
        txtPassword.text = ""
        txtUserName.text = ""
        txtUserName.SetFocus
        GoTo ex
    End If
    rs.Close

    user = txtUserName.text
    rs.Open "select * from var_gudang where property='1'", conn
    If Not rs.EOF Then
        gudang = rs(0)
    End If
    rs.Close
    frmMain.Show
    
    If modul = "inventory" Then
        rs.Open "select * from var_usergroup g inner join user_group u on  g.[group]=u.[group] and g.modul=u.modul where [username]='" & user & "' and g.modul='" & modul & "'", conn
        If Not rs.EOF Then
            gudang = rs!gudang
        End If
        rs.Close
    End If
    
    rs.Open "select * from user_groupmenu g inner join user_group u on g.[group]=u.[group] and g.modul=u.modul where username='" & user & "' and g.modul='" & modul & "'", conn
    While Not rs.EOF
        frmMain.Controls(rs("namamenu"))(rs("idx")).Visible = CBool(rs("value"))
        rs.MoveNext
    Wend
    rs.Close
    
    

    rs.Open "select * from user_group u inner join var_usergroup v on u.[group]=v.[group] and v.modul=u.modul where [username]='" & user & "' and u.modul='" & modul & "'", conn
    If Not rs.EOF Then
        group = rs("u.group")
        right_alert = rs!alert
        right_harga = rs!ubahharga
        right_hpp = rs!hpp
        right_stock = rs!stock
        right_hargaManual = rs!hargaManual
        right_TipeHarga = rs!harga
        right_export = rs!Export
    End If
    rs.Close
    
    rs.Open "Select * from awal_pakai", conn
    If rs.EOF Then
        conn.Execute "Insert into awal_pakai(bulan,tahun) values ('" & Format(Date, "M") & "','" & Format(Date, "yyyy") & "') "
    End If
    rs.Close
    
    rs.Open "Select * from awal_pakai", conn
    gBulanAwal = rs("bulan")
    gTahunAwal = rs("tahun")
    rs.Close
    
    rs.Open "Select * from setting_perusahaan", conn
    If Not rs.EOF Then
        GNamaPerusahaan = rs(0)
        GAlamatPerusahaan = rs(1)
    End If
    rs.Close
    modal = 0
    rs.Open "select * from setting", conn
    If Not rs.EOF Then modal = rs(0)
    rs.Close
    rs.Open "select * from t_modalawal where format(tanggal,'yyyy/MM/dd')='" & Format(Now, "yyyy/MM/dd") & "' and userid='" & txtUserName & "' and status='buka'", conn
    If rs.EOF Then
        conn.Execute "insert into t_modalawal (tanggal,modal_awal,[userid],status) values ('" & Format(Now, "yyyy/MM/dd") & "','" & modal & "','" & txtUserName & "','buka')"
    End If
    rs.Close
    Call DropConnection
    
    
    OpenForm_Fg = True
    Unload Me
    Exit Sub
err:
    MsgBox err.Description
ex:
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub

Private Sub cmdSearchDB_Click()
On Error GoTo err
'Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "Access File|*.mdb"
    CommonDialog1.filename = ""
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
        txtServerName = CommonDialog1.filename
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
 If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub

Private Sub Form_Load()
   Dim i As Integer

    OpenForm_Fg = False
    getConnString ""
    
    txtServerName.text = servname
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If OpenForm_Fg = False Then Call DropConnection
End Sub

Private Sub Label5_Click()
    txtServerName.Visible = True
    Label4.Visible = True
End Sub

Private Sub txtPassword_GotFocus()
 txtPassword.SelStart = 0
 txtPassword.SelLength = Len(txtPassword)
End Sub

Private Sub txtServerName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchDB_Click
End Sub

Private Sub txtUserName_GotFocus()
   txtUserName.SelStart = 0
   txtUserName.SelLength = Len(txtUserName)
End Sub
