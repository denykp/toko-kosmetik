VERSION 5.00
Begin VB.Form frmMasterMembership 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Membership"
   ClientHeight    =   2175
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   5310
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2175
   ScaleWidth      =   5310
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "frmMasterMembership.frx":0000
      Left            =   1710
      List            =   "frmMasterMembership.frx":0013
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   675
      Width           =   1275
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3593
      Picture         =   "frmMasterMembership.frx":0026
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1973
      Picture         =   "frmMasterMembership.frx":0128
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3195
      Picture         =   "frmMasterMembership.frx":022A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   270
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   270
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   488
      Picture         =   "frmMasterMembership.frx":032C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1440
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1170
      TabIndex        =   12
      Top             =   720
      Width           =   150
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   11
      Top             =   720
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Discount"
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   720
      Width           =   1320
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   9
      Top             =   315
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   1080
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   6
      Top             =   315
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Membership"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   5
      Top             =   315
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterMembership"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from ms_customer where membership='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
    
    
    conn.BeginTrans
    
    i = 1
    
        conn.Execute "delete from ms_membership where membership='" & txtField(0).text & "'"
        conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
        frmSearch.connstr = strcon
    
    frmSearch.query = "select membership,harga from ms_membership" ' order by membership"
    frmSearch.nmform = "frmMastermembership"
    frmSearch.nmctrl = "txtField"
    frmSearch.col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from ms_membership where membership='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        conn.Execute "delete from ms_membership where membership='" & txtField(0).text & "'"
        
    End If
    rs.Close
    conn.BeginTrans
    
    i = 1
    
    conn.Execute "insert into ms_membership values ('" & txtField(0).text & "','" & Combo1.text & "')"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
    txtField(0).text = ""
    txtField(0).SetFocus
End Sub
Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_membership where membership='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        SetComboText rs(1), Combo1
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub



