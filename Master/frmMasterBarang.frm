VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterBarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Barang"
   ClientHeight    =   6675
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8985
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   8985
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   9
      Left            =   1845
      TabIndex        =   11
      Text            =   "0"
      Top             =   4095
      Width           =   1680
   End
   Begin VB.CommandButton cmdSetting 
      BackColor       =   &H008080FF&
      Caption         =   "Setting Import"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6750
      TabIndex        =   64
      Top             =   5445
      Width           =   1230
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   1845
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1395
      Width           =   3210
   End
   Begin VB.CommandButton cmdMerkbaru 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&New"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5130
      Style           =   1  'Graphical
      TabIndex        =   60
      Top             =   1395
      Width           =   780
   End
   Begin VB.TextBox txtField 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   8
      Left            =   2925
      TabIndex        =   10
      Text            =   "0"
      Top             =   3690
      Width           =   510
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   7
      Left            =   2385
      TabIndex        =   9
      Text            =   "0"
      Top             =   3690
      Width           =   510
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   6
      Left            =   1845
      TabIndex        =   8
      Text            =   "0"
      Top             =   3690
      Width           =   510
   End
   Begin VB.CommandButton cmdSaveExel 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Save Exel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6863
      TabIndex        =   56
      Top             =   4995
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   5
      Left            =   1845
      TabIndex        =   5
      Text            =   "0"
      Top             =   2160
      Width           =   1680
   End
   Begin VB.CheckBox chkAktif 
      Caption         =   "Aktif"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   315
      TabIndex        =   53
      Top             =   5175
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.CommandButton cmdTipeBaru 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&New"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5130
      Style           =   1  'Graphical
      TabIndex        =   52
      Top             =   1035
      Width           =   780
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   4
      Left            =   1845
      TabIndex        =   7
      Text            =   "0"
      Top             =   2970
      Width           =   1680
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   3
      Left            =   1845
      Locked          =   -1  'True
      TabIndex        =   6
      Text            =   "0"
      Top             =   2565
      Width           =   1680
   End
   Begin VB.Frame frStock 
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      Height          =   330
      Left            =   5130
      TabIndex        =   43
      Top             =   1890
      Visible         =   0   'False
      Width           =   3345
      Begin VB.TextBox lblStock 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1755
         TabIndex        =   19
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label Label32 
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   45
         Top             =   0
         Width           =   1140
      End
      Begin VB.Label Label31 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1530
         TabIndex        =   44
         Top             =   0
         Width           =   105
      End
   End
   Begin VB.Frame frHPP 
      BorderStyle     =   0  'None
      Caption         =   "Frame6"
      Height          =   375
      Left            =   180
      TabIndex        =   40
      Top             =   4725
      Visible         =   0   'False
      Width           =   3345
      Begin VB.TextBox lblHPP 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1755
         TabIndex        =   18
         Top             =   0
         Width           =   1455
      End
      Begin VB.Label Label23 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga Pokok"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   42
         Top             =   45
         Width           =   1140
      End
      Begin VB.Label Label24 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1530
         TabIndex        =   41
         Top             =   45
         Width           =   105
      End
   End
   Begin VB.CommandButton cmdLoad 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Load Excel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6750
      TabIndex        =   39
      Top             =   4590
      Width           =   1230
   End
   Begin VB.CommandButton cmdBaru 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3795
      Picture         =   "frmMasterBarang.frx":0000
      TabIndex        =   14
      Top             =   5940
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrev 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Prev"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3570
      TabIndex        =   20
      Top             =   5445
      UseMaskColor    =   -1  'True
      Width           =   1050
   End
   Begin VB.CommandButton cmdNext 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Next"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4875
      TabIndex        =   21
      Top             =   5445
      UseMaskColor    =   -1  'True
      Width           =   1005
   End
   Begin VB.CommandButton cmdHistory 
      BackColor       =   &H00A6FC94&
      Caption         =   "&History"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6720
      Picture         =   "frmMasterBarang.frx":0532
      TabIndex        =   16
      Top             =   5940
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   2
      Left            =   1845
      Locked          =   -1  'True
      TabIndex        =   4
      Text            =   "0"
      Top             =   1755
      Width           =   1680
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   1845
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1035
      Width           =   3210
   End
   Begin VB.ComboBox cmbField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   6750
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   1440
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   840
      Picture         =   "frmMasterBarang.frx":0634
      TabIndex        =   12
      Top             =   5940
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BackColor       =   &H00A6FC94&
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3285
      TabIndex        =   22
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdHapus 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2325
      Picture         =   "frmMasterBarang.frx":0736
      TabIndex        =   13
      Top             =   5940
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H00A6FC94&
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5250
      Picture         =   "frmMasterBarang.frx":0838
      TabIndex        =   15
      Top             =   5940
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1845
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1845
      TabIndex        =   0
      Top             =   180
      Width           =   1410
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8100
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   1440
      TabIndex        =   67
      Top             =   4140
      Width           =   150
   End
   Begin VB.Label Label33 
      BackStyle       =   0  'Transparent
      Caption         =   "Spc Code"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   66
      Top             =   4140
      Width           =   1140
   End
   Begin VB.Label Label30 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   65
      Top             =   4140
      Width           =   105
   End
   Begin VB.Label Label27 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   63
      Top             =   1440
      Width           =   150
   End
   Begin VB.Label Label25 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   62
      Top             =   1440
      Width           =   105
   End
   Begin VB.Label Label20 
      BackStyle       =   0  'Transparent
      Caption         =   "Merk"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   61
      Top             =   1440
      Width           =   1320
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   "Pcs"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2970
      TabIndex        =   59
      Top             =   3375
      Width           =   420
   End
   Begin VB.Label Label18 
      BackStyle       =   0  'Transparent
      Caption         =   "Lsn"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2385
      TabIndex        =   58
      Top             =   3375
      Width           =   420
   End
   Begin VB.Label Label17 
      BackStyle       =   0  'Transparent
      Caption         =   "Krtn"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1845
      TabIndex        =   57
      Top             =   3375
      Width           =   420
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Disc"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   55
      Top             =   2205
      Width           =   1140
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   54
      Top             =   2205
      Width           =   105
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   51
      Top             =   3015
      Width           =   105
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Qty Grosir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   50
      Top             =   3015
      Width           =   1140
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   1440
      TabIndex        =   49
      Top             =   3015
      Width           =   150
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   48
      Top             =   2610
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga Jual Grosir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   47
      Top             =   2610
      Width           =   1365
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   46
      Top             =   2610
      Width           =   150
   End
   Begin VB.Label Label37 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   38
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label36 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   37
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label35 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   36
      Top             =   1080
      Width           =   150
   End
   Begin VB.Label Label29 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   35
      Top             =   1800
      Width           =   150
   End
   Begin VB.Label Label28 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   34
      Top             =   3735
      Width           =   150
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   33
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   32
      Top             =   225
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   31
      Top             =   5625
      Width           =   2130
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga Jual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   30
      Top             =   1800
      Width           =   1140
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   29
      Top             =   1800
      Width           =   105
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Isi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   28
      Top             =   3735
      Width           =   1320
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   27
      Top             =   3735
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   26
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Barang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   225
      TabIndex        =   25
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   24
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   23
      Top             =   675
      Width           =   105
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuSettingExcel 
         Caption         =   "Setting Import Excel"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim foc As Byte
Dim colname() As String
Private Sub cmdBaru_Click()
    reset_form
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
        conn.ConnectionString = strcon
        conn.Open
        conn.BeginTrans
    i = 1
    rs.Open "select * from (select [kode barang] from t_penjualand where [kode barang]='" & txtField(0).text & "' union select [kode barang] from t_pembeliand where [kode barang]='" & txtField(0).text & "' union select [kode barang] from t_stockopnamed where [kode barang]='" & txtField(0).text & "' union select [kode barang] from t_adjustmentd where [kode barang]='" & txtField(0).text & "' union select [kode barang] from t_returbelid where [kode barang]='" & txtField(0).text & "' union select [kode barang] from t_returjuald where [kode barang]='" & txtField(0).text & "') a", conn
    If Not rs.EOF Then
        conn.Execute "update ms_Barang set flag='0' where [kode barang]='" & txtField(0).text & "'"
        'conn.Execute "delete from stock where [kode barang]='" & txtField(0).text & "'"
    Else
            conn.Execute "delete from ms_Barang where [kode barang]='" & txtField(0).text & "'"
            conn.Execute "delete from stock where [kode barang]='" & txtField(0).text & "'"
            conn.Execute "delete from hpp where [kode barang]='" & txtField(0).text & "'"
    End If
    If rs.State Then rs.Close
    'conn.Execute "insert into HST_MS_barang values ('" & txtField(0).Text & "','" & Replace(txtField(1).Text, "'", "''") & "','" & Replace(txtField(2).Text, "'", "''") & "','" & Trim(Right(cmbField(0).Text, 20)) & "','" & Trim(Right(cmbField(3).Text, 20)) & "','" & cmbField(1).Text & "','" & cmbField(2).Text & "','" & txtField(3).Text & "','" & txtField(4).Text & "','" & txtField(5).Text & "','" & txtField(6).Text & "','" & chkAktif.value & "','Delete','" & Format(Now, "MM/dd/yyyy HH:mm:ss") & "','" & user & "')"
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    Set conn = Nothing
    cmdPrev_Click
    cmdNext_Click
    'reset_form
    Exit Sub
err:
    If err.Description <> "" Then MsgBox err.Description
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdHistory_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [kode barang] , [Nama Barang] , " & _
    "Harga1,Harga2,userid,tanggal   from hst_ms_Barang" ' order by [kode barang]"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.keyIni = "historyBarang"
    frmSearch.nmctrl = ""
    frmSearch.proc = ""
    frmSearch.col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.Show vbModal
        
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdMerkbaru_Click()
On Error GoTo err
Dim i As Byte
Dim merk As String
i = 0
    merk = InputBox("Silahkan masukkan merk", "Merk Baru", "")
    If merk <> "" Then
        conn.ConnectionString = strcon
        conn.Open
        conn.BeginTrans
        i = 1
        conn.Execute "insert into var_merk values ('" & merk & "')"
        conn.CommitTrans
        conn.Close
        loadcombomerk
        i = 0
    End If
    Exit Sub
err:
    MsgBox err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub
Private Sub loadcombomerk()
    merk = cmbField(2).text
    conn.ConnectionString = strcon
    conn.Open
    cmbField(2).clear
    cmbField(2).AddItem ""
    rs.Open "select * from var_merk order by merk", conn
    While Not rs.EOF
        cmbField(2).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

    If cmbField(2).ListCount > 0 Then cmbField(2).ListIndex = 0
    If merk <> "" Then SetComboText merk, cmbField(0)
    conn.Close
End Sub
Private Sub cmdNext_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode barang] from ms_barang where [kode barang]>'" & txtField(0).text & "' order by [kode barang]", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    
    cari_data
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdPrev_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select top 1 [kode barang] from ms_barang where [kode barang]<'" & txtField(0).text & "' order by [kode barang] desc", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        GoTo search
    End If
    rs.Close
    conn.Close
    Exit Sub
search:
    If rs.State Then rs.Close
    DropConnection
    cari_data
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdSaveExel_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject

    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowSave
    If CommonDialog1.filename <> "" And fs.FileExists(CommonDialog1.filename) Then
        saveexcelfile CommonDialog1.filename
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = querybrg  ' order by [kode barang]"
    frmSearch.nmform = "frmMasterBarang"
    frmSearch.nmctrl = "txtField"
    frmSearch.keyIni = "ms_barang"
    frmSearch.proc = "cari_data"
    frmSearch.col = 0
    frmSearch.Index = 0
    Set frmSearch.frm = Me
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSetting_Click()
    frmSettingExcel.list = "Kode Barang,Kategori,Merk,Nama,Harga Beli,Harga Jual1,Harga Jual2,Qty Grosir,Disc,Qty,Konversi1,Konversi2"
    frmSettingExcel.modul = "ms_barang"
    frmSettingExcel.Show vbModal

End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 2
    If txtField(J).text = "" Then
        MsgBox "semua field yang bertanda * harus diisi"
        txtField(J).SetFocus
        Exit Sub
    End If
    Next J
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    'conn.Execute "delete from ms_barang where [kode barang]='" & txtField(0).text & "'"
    
    rs.Open "select * from stock where [kode barang]='" & txtField(0).text & "'", conn
    If rs.EOF Then
        rs.Close
        conn.Execute "insert into stock  (gudang,[kode barang],stock)  select gudang,'" & txtField(0).text & "',0 from var_gudang"
    End If
    If rs.State Then rs.Close
    
        rs.Open "select * from hpp where [kode barang]='" & txtField(0).text & "'", conn
        If rs.EOF Then
            rs.Close
            conn.Execute "insert into hpp  ([kode barang],hpp)  values ('" & txtField(0).text & "',0)"
        End If
        If rs.State Then rs.Close
        conn.Execute "delete from rpt_ms_barang where [kode barang]='" & txtField(0).text & "'"
        conn.Execute "insert into rpt_ms_barang ([kode barang],[nama barang],kategori,merk) values ('" & txtField(0).text & "','" & Replace(txtField(1).text, "'", "''") & "','" & cmbField(0).text & "','" & cmbField(2).text & "')"
        
        If rs.State Then rs.Close
    
        rs.Open "select * from ms_barang where [kode barang]='" & txtField(0).text & "'", conn
        If Not rs.EOF Then
            add_dataheader 2, conn
        Else
            add_dataheader 1, conn
        End If
        If rs.State Then rs.Close

    If rs.State Then rs.Close
    If lblHPP.Locked = False Then conn.Execute "update hpp set hpp=" & lblHPP & " where [kode barang]='" & txtField(0).text & "'"
    If lblStock.Locked = False Then conn.Execute "update stock set stock=" & lblStock & " where [kode barang]='" & txtField(0).text & "' and gudang='" & gudang & "'"
'        conn.Execute "update ms_barang set hppawal=" & lblHPP & " and stockawal=" & lblStock & " where [kode barang]='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    add_datahst conn
    
    
    DropConnection
    reset_form
    
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = "0"
    txtField(3).text = "0"
    txtField(4).text = "0"
    txtField(5).text = "0"
    txtField(6).text = "1"
    txtField(7).text = "12"
    txtField(8).text = "1"
    txtField(9).text = ""
    
    cmbField(0).ListIndex = 0
    cmbField(1).ListIndex = 0
    lblStock.Locked = False
    lblHPP.Locked = False
    lblStock = "0"
    lblHPP = "0"
    txtField(0).SetFocus
    cmdPrev.Enabled = False
    cmdNext.Enabled = False
End Sub
Private Sub add_dataheader(action As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(13)
    ReDim nilai(13)

    table_name = "MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "[MERK]"
    fields(4) = "SATUAN"
    fields(5) = "Harga1"
    fields(6) = "harga2"
    
    fields(7) = "disc"
    fields(8) = "qtygrosir"
    fields(9) = "konversi1"
    fields(10) = "konversi2"
    fields(11) = "flag"
    fields(12) = "kode"
    
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(2).text
    nilai(4) = cmbField(1).text
    nilai(5) = txtField(2).text
    nilai(6) = txtField(3).text
    nilai(7) = txtField(5).text
    nilai(8) = txtField(4).text
    nilai(9) = txtField(6).text
    nilai(10) = txtField(7).text
    nilai(11) = chkAktif
    nilai(12) = txtField(9).text
    
    If action = 1 Then
    cn.Execute tambah_data2(table_name, fields, nilai)
    Else
    update_data table_name, fields, nilai, "[kode barang]='" & txtField(0).text & "'", cn
    End If
End Sub
Private Sub add_datahst(cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(15)
    ReDim nilai(15)

    table_name = "hst_MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "[MERK]"
    fields(4) = "SATUAN"
    fields(5) = "Harga1"
    fields(6) = "harga2"
    
    fields(7) = "disc"
    fields(8) = "qtygrosir"
    fields(9) = "konversi1"
    fields(10) = "konversi2"
    fields(11) = "kode"
    fields(12) = "flag"
    fields(13) = "[userid]"
    fields(14) = "tanggal"
    
    nilai(0) = txtField(0).text
    nilai(1) = Replace(txtField(1).text, "'", "''")
    nilai(2) = cmbField(0).text
    nilai(3) = cmbField(2).text
    nilai(4) = cmbField(1).text
    nilai(5) = txtField(2).text
    nilai(6) = txtField(3).text
    
    nilai(7) = txtField(5).text
    nilai(8) = txtField(4).text
    nilai(9) = txtField(6).text
    nilai(10) = txtField(7).text
    nilai(11) = txtField(9).text
    nilai(12) = chkAktif
    nilai(13) = user
    nilai(14) = Format(Now, "yyyy/mm/dd hh:mm:ss")
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Public Sub cari_data()
    If txtField(0).text = "" Then Exit Sub
    conn.ConnectionString = strcon
    
'    conn.mode = adModeShareDenyNone
    conn.Open
    rs.Open "select * from ms_barang where [kode barang]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        cmdNext.Enabled = True
        cmdPrev.Enabled = True
        txtField(1).text = rs("nama barang")
        txtField(2).text = rs!harga1
        txtField(3).text = rs!harga2
        txtField(4).text = rs!qtygrosir
        txtField(5).text = rs!disc
        
        txtField(6).text = rs!konversi1
        txtField(7).text = rs!konversi2
        txtField(9).text = rs!kode
        
        SetComboTextRight rs!kategori, cmbField(0)
        SetComboTextRight rs!satuan, cmbField(1)
        SetComboTextRight rs!merk, cmbField(2)
        chkAktif.value = IIf(IsNull(rs("flag")), 1, rs("flag"))
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        If rs.State Then rs.Close
        lblHPP = getHpp(txtField(0).text, strcon)
        'lblHPP.Locked = True
        lblStock = getStock2(txtField(0).text, gudang, strcon)
        
        lblStock.Locked = True
    Else
        cmdPrev.Enabled = False
        cmdPrev.Enabled = False
        cmdHapus.Enabled = False
        mnuHapus.Enabled = True
        lblHPP = 0
        lblStock = 0
        lblHPP.Locked = False
        lblStock.Locked = False
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Function cek_barcode(ByVal kodebrg As String, ByVal key As String) As Boolean
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_barang where [kode barang]<>'" & kodebrg & "'", conn
    If Not rs.EOF Then
        cek_barcode = False
    Else
        cek_barcode = True
    End If
    rs.Close
    conn.Close
End Function
Private Sub load_combo()
    
    conn.ConnectionString = strcon
    
    conn.Open
'    rs.Open "select access from user_priviledge where userid='" & user & "' and kode_priviledge='5'", conn
'    If Not rs.EOF Then
'        If rs(0) = "1" Then
'            Label32.Visible = True
'            Label33.Visible = True
'            lblHPP.Visible = True
'        End If
'    End If
'    rs.Close
    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select * from var_merk order by merk", conn
    While Not rs.EOF
        cmbField(2).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
'    cmbField(0).AddItem "Parcel"
    If cmbField(0).ListCount > 0 Then cmbField(0).ListIndex = 0
    rs.Open "select * from var_satuan order by satuan", conn
    While Not rs.EOF
        cmbField(1).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    If cmbField(0).ListCount > 0 Then cmbField(0).ListIndex = 0
    If cmbField(1).ListCount > 0 Then cmbField(1).ListIndex = 0
    If cmbField(2).ListCount > 0 Then cmbField(2).ListIndex = 0

    conn.Close
End Sub


Private Sub cmdTipeBaru_Click()
On Error GoTo err
Dim i As Byte
Dim kategori As String
i = 0
    kategori = InputBox("Silahkan masukkan Tipe", "Tipe Baru", "")
    If kategori <> "" Then
        conn.ConnectionString = strcon
        conn.Open
        conn.BeginTrans
        i = 1
        conn.Execute "insert into var_kategori values ('" & kategori & "')"
        conn.CommitTrans
        conn.Close
        loadcombo
        i = 0
    End If
    Exit Sub
err:
    MsgBox err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub
Private Sub loadcombo()
    kategori = cmbField(0).text
    conn.ConnectionString = strcon
    conn.Open
    cmbField(0).clear
    cmbField(0).AddItem ""
    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbField(0).AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close

    If cmbField(0).ListCount > 0 Then cmbField(0).ListIndex = 0
    If kategori <> "" Then SetComboText kategori, cmbField(0)
    conn.Close
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If foc = 1 And txtField(0).text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{Tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    load_combo
    reset_form
'    If right_hpp Then frHPP.Visible = True
'    If right_stock Then frStock.Visible = True
    If right_harga Then
        txtField(2).Locked = False
        txtField(3).Locked = False
        
    End If
End Sub

Private Sub lblHPP_GotFocus()
    lblHPP.SelStart = 0
    lblHPP.SelLength = Len(lblHPP.text)
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub


Private Sub mnuSettingExcel_Click()
    frmSettingExcel.list = "Kode barang,Nama,Kategori,Merk,Harga Jual2,Harga Jual2,Qty Grosir,Disc,Harga Jual3,Konversi1,Konversi2"
    
    frmSettingExcel.modul = "ms_barang"
    frmSettingExcel.Show vbModal
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index).text)
    foc = 1
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    foc = 0
    If Index = 0 And txtField(0).text <> "" Then cari_data
End Sub


Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
        MsgBox "Loading selesai"
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub loadexcelfile1(filename As String)
On Error GoTo err
Dim exapp As Object
Dim exwb As Object
Dim exws As Object
Dim range As String
Dim col As Byte
Dim i As Integer
Set exapp = CreateObject("excel.application")
Set exwb = exapp.Workbooks.Open(filename)
Set exws = exwb.Sheets("Sheet1")
col = Asc("A")
range = Chr(col) & "1"
Dim kodebarang As String
Dim nama As String
Dim kategori  As String
Dim merk As String
Dim hargajual1 As Currency
Dim hargajual2 As Currency
Dim hargajual3 As Currency
Dim qtygrosir As Integer
Dim disc As Double
Dim konversi1 As Double
Dim konversi2 As Double


Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset
Dim colkodebarang As String
Dim colnama As String
Dim colkategori As String
Dim colqty As String
Dim colhargabeli As String
Dim colhargajual1 As String
Dim colhargajual2 As String

Dim colqtygrosir As String
Dim coldisc As String
Dim colmerk As String
Dim colkonversi1 As String
Dim colkonversi2 As String

Dim colindexkodebarang As String
Dim colindexnama As String
Dim colindexkategori As String
Dim colindexmerk As String
Dim colindexqty As String
Dim colindexhargabeli As String
Dim colindexhargajual1 As String
Dim colindexhargajual2 As String

Dim colindexdisc As String
Dim colindexqtygrosir As String
Dim colindexkonversi1 As String
Dim colindexkonversi2 As String

exws.Activate

colkodebarang = sGetINI(App.Path & "\import.ini", "ms_barang", "Kode Barang", "kodebarang")
colnama = sGetINI(App.Path & "\import.ini", "ms_barang", "Nama", "nama")
colkategori = sGetINI(App.Path & "\import.ini", "ms_barang", "Kategori", "kategori")
colmerk = sGetINI(App.Path & "\import.ini", "ms_barang", "merk", "merk")
colhargajual1 = sGetINI(App.Path & "\import.ini", "ms_barang", "Harga Jual1", "hargajual1")
colhargajual2 = sGetINI(App.Path & "\import.ini", "ms_barang", "Harga Jual2", "hargajual2")
colqty = sGetINI(App.Path & "\import.ini", "ms_barang", "Qty", "stok")
colqtygrosir = sGetINI(App.Path & "\import.ini", "ms_barang", "Qty Grosir", "qtygrosir")
coldisc = sGetINI(App.Path & "\import.ini", "ms_barang", "Disc", "disc")

colkonversi1 = sGetINI(App.Path & "\import.ini", "ms_barang", "Konversi1", "konversi1")
colkonversi2 = sGetINI(App.Path & "\import.ini", "ms_barang", "Konversi2", "konversi2")


While exws.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exws.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

colindexkodebarang = getcolindex(colkodebarang, colname)
colindexnama = getcolindex(colnama, colname)
colindexkategori = getcolindex(colkategori, colname)
colindexmerk = getcolindex(colmerk, colname)
colindexqty = getcolindex(colqty, colname)
colindexhargabeli = getcolindex(colhargabeli, colname)
colindexhargajual1 = getcolindex(colhargajual1, colname)
colindexhargajual2 = getcolindex(colhargajual2, colname)

colindexqtygrosir = getcolindex(colqtygrosir, colname)
colindexdisc = getcolindex(coldisc, colname)
colindexkonversi1 = getcolindex(colkonversi1, colname)
colindexkonversi2 = getcolindex(colkonversi2, colname)


conn.Open strcon

i = 2
With exws
While .range("A" & CStr(i)) <> ""
    kodebarang = .range(Chr(colindexkodebarang + 65) & CStr(i))
    nama = .range(Chr(colindexnama + 65) & CStr(i))
    kategori = .range(Chr(colindexkategori + 65) & CStr(i))
    merk = .range(Chr(colindexmerk + 65) & CStr(i))
    hargajual1 = .range(Chr(colindexhargajual1 + 65) & CStr(i))
    hargajual2 = .range(Chr(colindexhargajual2 + 65) & CStr(i))
    
    qtygrosir = .range(Chr(colindexqtygrosir + 65) & CStr(i))
    disc = .range(Chr(colindexdisc + 65) & CStr(i))
    konversi1 = .range(Chr(colindexkonversi1 + 65) & CStr(i))
    konversi2 = .range(Chr(colindexkonversi2 + 65) & CStr(i))
        rs2.Open "select * from ms_barang where [kode barang]='" & kodebarang & "'", conn
        If rs2.EOF Then
            newitem kodebarang, nama, kategori, merk, hargajual1, hargajual2, qtygrosir, disc, konversi1, konversi2, conn
            conn.Execute "insert into stock  (gudang,[kode barang],stock)  select gudang,'" & kodebarang & "','" & stok & "' from var_gudang"
        Else
            updateitem kodebarang, nama, kategori, merk, hargajual1, hargajual2, qtygrosir, disc, konversi1, konversi2, conn
        End If
        rs2.Close
        If Not cek_kategori(kategori, conn) Then
            conn.Execute "insert into var_kategori values ('" & kategori & "')"
        End If
        If Not cek_merk(merk, conn) Then
            conn.Execute "insert into var_merk values ('" & merk & "')"
        End If

    i = i + 1
Wend
End With

exwb.Close
exapp.Application.Quit
Set exapp = Nothing
Set exwb = Nothing
Set exws = Nothing
DropConnection
Exit Sub
err:
MsgBox err.Description
exwb.Close
exapp.Application.Quit
Set exapp = Nothing
End Sub
Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset
Dim colkodebarang As String
Dim colnama As String
Dim colkategori As String
Dim colmerk As String
Dim colqty As String
Dim colhargabeli As String
Dim colhargajual1 As String
Dim colhargajual2 As String
Dim colhargajual3 As String
Dim colqtygrosir As String
Dim coldisc As String
Dim colkonversi1 As String
Dim colkonversi2 As String


colkodebarang = sGetINI(App.Path & "\import.ini", "ms_barang", "Kode Barang", "Kode Barang")
colnama = sGetINI(App.Path & "\import.ini", "ms_barang", "Nama", "Nama")
colkategori = sGetINI(App.Path & "\import.ini", "ms_barang", "Kategori", "Kategori")
colmerk = sGetINI(App.Path & "\import.ini", "ms_barang", "Merk", "Merk")
colhargajual1 = sGetINI(App.Path & "\import.ini", "ms_barang", "Harga Jual1", "Harga Jual1")
colhargajual2 = sGetINI(App.Path & "\import.ini", "ms_barang", "Harga Jual2", "Harga Jual2")
colhargajual3 = sGetINI(App.Path & "\import.ini", "ms_barang", "Harga Jual3", "Harga Jual3")
colqty = sGetINI(App.Path & "\import.ini", "ms_barang", "Qty", "stok")
colqtygrosir = sGetINI(App.Path & "\import.ini", "ms_barang", "Qty Grosir", "Qty Grosir")
coldisc = sGetINI(App.Path & "\import.ini", "ms_barang", "Disc", "disc")
colkonversi1 = sGetINI(App.Path & "\import.ini", "ms_barang", "konversi1", "konversi1")
colkonversi2 = sGetINI(App.Path & "\import.ini", "ms_barang", "konversi2", "konversi2")
Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=Excel 8.0"
con.Open stcon
conn.Open strcon

rs.Open "select * from [sheet1$]", con

While Not rs.EOF
    If rs(0) <> "" Then
        rs2.Open "select * from var_kategori where kategori='" & rs(colkategori) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_kategori values ('" & rs(colkategori) & "')"
        End If
        rs2.Close
        rs2.Open "select * from var_merk where merk='" & rs(colmerk) & "'", conn
        If rs2.EOF Then
            conn.Execute "insert into var_merk values ('" & rs(colmerk) & "')"
        End If
        rs2.Close
        rs2.Open "select * from ms_barang where [kode barang]='" & rs(colkodebarang) & "'", conn
        If rs2.EOF Then
            newitem rs(colkodebarang), rs(colnama), rs(colkategori), rs(colmerk), rs(colhargajual1), rs(colhargajual2), rs(colqtygrosir), rs(coldisc), rs(colkonversi1), rs(colkonversi2), conn
            'conn.Execute "insert into stock  (gudang,[kode barang],stock)  select gudang,'" & rs(colkodebarang) & "','" & rs("stok") & "' from var_gudang"
        Else
            updateitem rs(colkodebarang), rs(colnama), rs(colkategori), rs(colmerk), rs(colhargajual1), rs(colhargajual2), rs(colqtygrosir), rs(coldisc), rs(colkonversi1), rs(colkonversi2), conn
        End If
        conn.Execute "delete from rpt_ms_barang where [kode barang]='" & rs(colkodebarang) & "'"
        conn.Execute "insert into rpt_ms_barang ([kode barang],[nama barang],kategori,merk) values ('" & rs(colkodebarang) & "','" & Replace(rs(colnama), "'", "''") & "','" & rs(colkategori) & "','" & rs(colmerk) & "')"
        rs2.Close
    End If
    rs.MoveNext
Wend
rs.Close
con.Close
DropConnection
Exit Sub
err:
MsgBox err.Description
 Resume Next
'If rs.State Then rs.Close
'If rs2.State Then rs2.Close
'If con.State Then con.Close
'DropConnection
End Sub
Private Sub updateitem(kodebarang As String, namabarang As String, kategori As String, merk As String, hargajual1 As Currency, hargajual2 As Currency, qtygrosir As Integer, disc As Double, konversi1 As Double, konversi2 As Double, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    
    kodebarang = IIf(IsNull(kodebarang), "", kodebarang)
    namabarang = IIf(IsNull(namabarang), "", namabarang)
    hargajual = IIf(IsNull(hargajual), 0, hargajual)
    ReDim fields(8)
    ReDim nilai(8)

    table_name = "MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "[MERK]"
'    fields(4) = "SATUAN"
'    fields(5) = "Harga1"
'    fields(6) = "harga2"

    fields(4) = "disc"
    fields(5) = "qtygrosir"
    fields(6) = "konversi1"
    fields(7) = "konversi2"
'    fields(12) = "flag"
    
    
    nilai(0) = kodebarang
    nilai(1) = Replace(namabarang, "'", "''")
    nilai(2) = kategori
    nilai(3) = merk
'    nilai(4) = "Pcs"
'    nilai(5) = hargajual1
'    nilai(6) = hargajual2

    nilai(4) = disc
    nilai(5) = qtygrosir
    nilai(6) = konversi1
    nilai(7) = konversi2
'    nilai(12) = "1"
    
    update_data table_name, fields, nilai, "[kode barang]='" & nilai(0) & "'", cn
    'conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub newitem(kodebarang As String, namabarang As String, kategori As String, merk As String, hargajual1 As Currency, hargajual2 As Currency, qtygrosir As Integer, disc As Double, konversi1 As Double, konversi2 As Double, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    kodebarang = IIf(IsNull(kodebarang), "", kodebarang)
    namabarang = IIf(IsNull(namabarang), "", namabarang)
    
    ReDim fields(12)
    ReDim nilai(12)

    table_name = "MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "[MERK]"
    fields(4) = "SATUAN"
    fields(5) = "Harga1"
    fields(6) = "harga2"
    
    fields(7) = "disc"
    fields(8) = "qtygrosir"
    fields(9) = "konversi1"
    fields(10) = "konversi2"
    fields(11) = "flag"
    
    nilai(0) = kodebarang
    nilai(1) = Replace(namabarang, "'", "''")
    nilai(2) = kategori
    nilai(3) = merk
    nilai(4) = "Pcs"
    nilai(5) = hargajual1
    nilai(6) = hargajual2
    
    nilai(7) = disc
    nilai(8) = qtygrosir
    nilai(9) = konversi1
    nilai(10) = konversi2
    nilai(11) = "1"
    cn.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub saveexcelfile(filename As String)
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend
exws.range(Chr(0 + 65) & "1") = "H"
exws.range(Chr(1 + 65) & "1") = "[KODE BARANG]"
exws.range(Chr(3 + 65) & "1") = "[NAMA barang]"
exws.range(Chr(2 + 65) & "1") = "[KATEGORI]"
exws.range(Chr(4 + 65) & "1") = "SATUAN"
exws.range(Chr(5 + 65) & "1") = "Harga1"
exws.range(Chr(6 + 65) & "1") = "Harga2"
exws.range(Chr(7 + 65) & "1") = "disc"

    
i = 2
Dim harga1 As Long
Dim harga2 As Double
Dim kategori, nama_barang, kode, satuan, disc As String

conn.Open strcon
    
        rs.Open "select * from ms_barang", conn
        While Not rs.EOF
            kode = rs![kode barang]
            nama_barang = rs![nama barang]
            kategori = rs("kategori")
            satuan = rs("satuan")
            harga1 = rs("harga1")
            harga2 = rs("harga2")
            disc = rs!disc
            
'
'        Else
'            harga1 = 0
'            harga2 = 0
'            kategori = ""
'        End If
'        rs.Close
        exws.range(Chr(0 + 65) & CStr(i)) = i - 1
        exws.range(Chr(1 + 65) & CStr(i)) = kode
        exws.range(Chr(2 + 65) & CStr(i)) = nama_barang
        exws.range(Chr(3 + 65) & CStr(i)) = kategori
        exws.range(Chr(4 + 65) & CStr(i)) = satuan
        exws.range(Chr(5 + 65) & CStr(i)) = harga1
        exws.range(Chr(6 + 65) & CStr(i)) = harga2
        exws.range(Chr(7 + 65) & CStr(i)) = disc
        
        i = i + 1
    Wend
    rs.Close
    conn.Close
err:
exwb.SaveAs filename
exwb.Close
exapp.Application.Quit
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub

