VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterSales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Sales"
   ClientHeight    =   2070
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7110
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   7110
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterSales.frx":0000
      TabIndex        =   12
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      TabIndex        =   0
      Top             =   225
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4538
      Picture         =   "frmMasterSales.frx":0102
      TabIndex        =   4
      Top             =   1395
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2918
      Picture         =   "frmMasterSales.frx":0204
      TabIndex        =   3
      Top             =   1395
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1343
      Picture         =   "frmMasterSales.frx":0306
      TabIndex        =   2
      Top             =   1395
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6390
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   11
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   10
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   1035
      Width           =   2130
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   8
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   7
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Sales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   5
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterSales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from t_penjualanh where [kode sales]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        rs.Close
        If MsgBox("Data sudah terpakai, anda yakin hendak menghapus?", vbYesNo) = vbYes Then GoTo hapus
        
        DropConnection
        Exit Sub
    End If
    rs.Close
hapus:
    
    conn.BeginTrans
    conn.Execute "delete from ms_sales where [kode sales]='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 1
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode sales], [Nama sales]  from ms_sales " ' order by kode_Member"
    frmSearch.nmform = "frmMastersales"
    frmSearch.nmctrl = "txtField"
    frmSearch.keyIni = "ms_sales"
    frmSearch.connstr = strcon
    
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    Set rs = conn.Execute("select * from ms_sales where [kode sales]='" & txtField(0).text & "'")
    
    If Not rs.EOF Then
        conn.Execute "delete from ms_sales where [kode sales]='" & txtField(0).text & "'"
    End If
    rs.Close
    
        conn.Execute "insert into MS_sales ([kode sales],[nama sales]) values ('" & txtField(0).text & "','" & txtField(1).text & "')"
        
        conn.CommitTrans
    
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(0).SetFocus
'    DTPicker1 = Now
'    DTPicker1.Year = DTPicker1.Year + 1
    
End Sub
Public Sub cari_data()
On Error GoTo err
Dim visit As Integer
Dim omzet As Long
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_sales where [kode sales]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        rs.Close

    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
    Exit Sub
err:
MsgBox err.Description
If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
'        NumberOnly KeyAscii
    End If
    If KeyAscii = 13 And Index = 0 Then
    
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
Private Sub updateitem(kodesales As String, namasales As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(2)
    ReDim nilai(2)


    table_name = "MS_sales"
    fields(0) = "[KODE sales]"
    fields(1) = "[NAMA sales]"
    
    
    nilai(0) = kodesales
    nilai(1) = Replace(namasales, "'", "''")
    
    
    update_data table_name, fields, nilai, "[kode sales]='" & nilai(0) & "'", cn
    
    'conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub newitem(kodesales As String, namasales As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)

    table_name = "MS_sales"
    fields(0) = "[KODE sales]"
    fields(1) = "[NAMA sales]"
    
    nilai(0) = kodesales
    nilai(1) = Replace(namasales, "'", "''")
    
    cn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub



