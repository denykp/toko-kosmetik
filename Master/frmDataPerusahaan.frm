VERSION 5.00
Begin VB.Form frmDataPerusahaan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Data Perusahaan"
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   6405
   FillColor       =   &H8000000F&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   6405
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   510
      Left            =   5805
      TabIndex        =   10
      Top             =   1710
      Width           =   510
   End
   Begin VB.TextBox txtNama 
      Alignment       =   2  'Center
      Height          =   330
      Left            =   1230
      MaxLength       =   50
      TabIndex        =   9
      Top             =   240
      Width           =   4125
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   510
      Left            =   1140
      Picture         =   "frmDataPerusahaan.frx":0000
      TabIndex        =   1
      Top             =   1764
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.TextBox txtAlamat 
      Alignment       =   2  'Center
      Height          =   984
      Left            =   1230
      MaxLength       =   150
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   630
      Width           =   4155
   End
   Begin VB.CommandButton cmdreset 
      Caption         =   "&Reset"
      Height          =   510
      Left            =   2445
      Picture         =   "frmDataPerusahaan.frx":0102
      TabIndex        =   2
      Top             =   1764
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   510
      Left            =   3750
      Picture         =   "frmDataPerusahaan.frx":0204
      TabIndex        =   3
      Top             =   1764
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      Height          =   240
      Left            =   195
      TabIndex        =   8
      Top             =   675
      Width           =   720
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1035
      TabIndex        =   7
      Top             =   690
      Width           =   105
   End
   Begin VB.Label lblParent 
      AutoSize        =   -1  'True
      Height          =   240
      Left            =   3375
      TabIndex        =   6
      Top             =   330
      Width           =   60
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Perusahaan"
      Height          =   240
      Left            =   195
      TabIndex        =   5
      Top             =   255
      Width           =   780
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1035
      TabIndex        =   4
      Top             =   255
      Width           =   105
   End
   Begin VB.Menu mune1 
      Caption         =   "Data"
      Begin VB.Menu menu2 
         Caption         =   "Open "
         Shortcut        =   {F5}
      End
      Begin VB.Menu menu3 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmDataPerusahaan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdKeluar_Click()
Unload Me
End Sub

Private Sub cmdReset_Click()
reset_form
End Sub



Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from setting_perusahaan ", conn
    If Not rs.EOF Then
        txtNama.text = rs(0)
        txtAlamat.text = rs(1)
    End If
    rs.Close
   
    conn.Close
End Sub


Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from setting_perusahaan"
    conn.Execute "insert into setting_perusahaan (nama_perusahaan,alamat) " & _
                 "values ('" & txtNama.text & "','" & txtAlamat.text & "')"
    
    conn.CommitTrans
    i = 0
    DropConnection
    GNamaPerusahaan = txtNama
    GAlamatPerusahaan = txtAlamat
    MsgBox "Data sudah disimpan !", vbInformation
    Exit Sub
err:
    
    conn.RollbackTrans
    
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description

End Sub

Private Sub Command1_Click()
Form1.Show
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    cari_data
End Sub
Private Sub menu3_Click()
Unload Me
End Sub
Sub reset_form()
    txtNama = ""
    txtAlamat = ""

End Sub
