VERSION 5.00
Begin VB.Form frmMasterBiaya 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Biaya"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   5550
   FillColor       =   &H00E0E0E0&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00404040&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   5550
   Begin VB.ComboBox cmbJenis 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmMasterBiaya.frx":0000
      Left            =   1290
      List            =   "frmMasterBiaya.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   675
      Width           =   2685
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4095
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   6
      Top             =   1785
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2880
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   4
      Top             =   1785
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1665
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   3
      Top             =   1785
      Width           =   1230
   End
   Begin VB.TextBox txtNama 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1305
      TabIndex        =   1
      Top             =   1125
      Width           =   2700
   End
   Begin VB.CommandButton cmdSearchBiaya 
      BackColor       =   &H00C0C0FF&
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4050
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   5
      Top             =   180
      Width           =   600
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H00C0C0FF&
      Caption         =   "Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   450
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   2
      Top             =   1785
      Width           =   1230
   End
   Begin VB.Label lblNamaDana 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1350
      TabIndex        =   11
      Top             =   1620
      Width           =   45
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   135
      TabIndex        =   10
      Top             =   270
      Width           =   795
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Jenis Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   135
      TabIndex        =   9
      Top             =   720
      Width           =   795
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   180
      TabIndex        =   8
      Top             =   1170
      Width           =   840
   End
   Begin VB.Label lblKodeBiaya 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   1305
      TabIndex        =   7
      Top             =   270
      Width           =   2220
   End
   Begin VB.Menu mnuData 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmMasterBiaya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    If lblKodeBiaya = "-" Then Exit Sub
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from t_pembayaran_biaya where kode_biaya='" & lblKodeBiaya & "' ", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapt dihapus!  "
    Else
        conn.Execute "delete from ms_biaya where kode_biaya='" & lblKodeBiaya & "' "
        MsgBox "Data sudah Terhapus"
    End If
    rs.Close
    conn.Close
    reset_form
    Exit Sub
err:
    
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description

End Sub

Private Sub cmdReset_Click()
reset_form
End Sub

Private Sub cmdSearchBiaya_Click()
    Dim str As String
    frmSearch.connstr = strcon
    frmSearch.query = "select * from ms_biaya "
    frmSearch.nmform = "frmMasterbiaya"
    frmSearch.nmctrl = "lblKodebiaya"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cari_data"
    
    frmSearch.loadgrid frmSearch.query
    SetComboText "kode_biaya", frmSearch.cmbSortby
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    
    
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub



Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim id As String
On Error GoTo err
    i = 0
    id = lblKodeBiaya
    If txtNama.text = "" Then
        MsgBox "Semua field harus diisi"
        txtNama.SetFocus
        Exit Sub
    End If
  
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    If lblKodeBiaya <> "-" Then
        conn.Execute "update ms_biaya set nama_biaya='" & Replace(txtNama.text, "'", "''") & "',jenis_biaya='" & cmbJenis.text & "' where kode_biaya='" & lblKodeBiaya & "'"
    Else
        nomor_baru
        conn.Execute "insert into ms_biaya (kode_biaya,jenis_biaya,nama_biaya) values ('" & lblKodeBiaya & "','" & cmbJenis.text & "','" & Replace(txtNama.text, "'", "''") & "')"
    End If
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
    lblKodeBiaya = id
End Sub




Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearchBiaya_Click
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
'    If KeyCode = vbKeyF3 Then cmdSearchDana_Click
End Sub

Private Sub nomor_baru()
Dim No As String
    
    rs.Open "SELECT TOP 1 kode_biaya from ms_biaya ORDER BY kode_biaya DESC ", conn
    If Not rs.EOF Then
        No = Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
         No = Format("1", "00000")
    End If
    rs.Close
    lblKodeBiaya.Caption = No

End Sub
Private Sub reset_form()
On Error Resume Next
    lblKodeBiaya = "-"
    
    txtNama.text = ""
    cmbJenis.SetFocus

End Sub

Public Sub cari_data()
   
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_biaya where kode_biaya='" & lblKodeBiaya & "'", conn
    If Not rs.EOF Then
        SetComboText rs(2), cmbJenis
        txtNama.text = rs(1)
    Else
        txtNama.text = ""
    End If
    rs.Close
    conn.Close
    
End Sub



Private Sub mnuExit_Click()
Unload Me
End Sub

Private Sub mnuOpen_Click()
cmdSearchBiaya_Click
End Sub
