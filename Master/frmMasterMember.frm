VERSION 5.00
Begin VB.Form frmMasterMember 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Customer"
   ClientHeight    =   3735
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7110
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   7110
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterMember.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   3
      Left            =   1890
      TabIndex        =   3
      Top             =   1845
      Width           =   2040
   End
   Begin VB.TextBox txtField 
      Height          =   735
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1035
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   1
      Left            =   1890
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1890
      TabIndex        =   0
      Top             =   225
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   4538
      Picture         =   "frmMasterMember.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2880
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   2918
      Picture         =   "frmMasterMember.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2880
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   1343
      Picture         =   "frmMasterMember.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2880
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   17
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   16
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   2520
      Width           =   2130
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1665
      TabIndex        =   13
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1665
      TabIndex        =   11
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1665
      TabIndex        =   10
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1665
      TabIndex        =   9
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Member"
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Customer"
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterMember"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_penjualanh where kode_customer='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
   
    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_customer where kode_customer='" & txtField(0).text & "'"
    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [Kode_Customer], [Nama_Customer] , [Alamat], [Telp] from ms_customer " ' order by kode_Member"
    frmSearch.nmform = "frmMasterMember"
    frmSearch.nmctrl = "txtField"
    frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSimpan_Click()
Dim i, j As Byte
On Error GoTo err
    i = 0
    For j = 0 To 1
        If txtField(j).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(j).SetFocus
            Exit Sub
        End If
    Next

    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    Set rs = conn.Execute("select * from ms_customer where kode_customer='" & txtField(0).text & "'")
    If Not rs.EOF Then
        conn.Execute "delete from ms_customer where kode_customer='" & txtField(0).text & "'"
    End If
    rs.Close
    conn.Execute "insert into MS_customer values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "')"
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(0).SetFocus
    
End Sub
Public Sub cari_data(ByVal key As String)
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_customer where kode_customer='" & key & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        
        txtField(1).text = ""
        txtField(2).text = ""
        txtField(3).text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then cmdSearch_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    load_combo

End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data (txtField(0).text)
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data (txtField(0).text)
End Sub
