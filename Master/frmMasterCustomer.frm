VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMasterCustomer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Customer"
   ClientHeight    =   3825
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7110
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3825
   ScaleWidth      =   7110
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmMasterCustomer.frx":0000
      Left            =   1890
      List            =   "frmMasterCustomer.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   2250
      Width           =   915
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      Height          =   330
      Left            =   4050
      Picture         =   "frmMasterCustomer.frx":0014
      TabIndex        =   18
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   3
      Left            =   1890
      TabIndex        =   3
      Top             =   1845
      Width           =   2040
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   1890
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1035
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1890
      TabIndex        =   1
      Top             =   630
      Width           =   3480
   End
   Begin VB.TextBox txtField 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1890
      TabIndex        =   0
      Top             =   225
      Width           =   2040
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4538
      Picture         =   "frmMasterCustomer.frx":0116
      TabIndex        =   6
      Top             =   3015
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2918
      Picture         =   "frmMasterCustomer.frx":0218
      TabIndex        =   5
      Top             =   3015
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1343
      Picture         =   "frmMasterCustomer.frx":031A
      TabIndex        =   4
      Top             =   3015
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6390
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   20
      Top             =   2295
      Width           =   105
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   19
      Top             =   2295
      Width           =   150
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   17
      Top             =   675
      Width           =   150
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   16
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   2655
      Width           =   2130
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Telp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   13
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Alamat"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   11
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   10
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   9
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kode Customer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   270
      Width           =   1320
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   21
      Top             =   2295
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    
    rs.Open "select * from t_penjualanh where [kode customer]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        rs.Close
        If MsgBox("Data sudah terpakai, anda yakin hendak menghapus?", vbYesNo) = vbYes Then GoTo hapus
        
        DropConnection
        Exit Sub
    End If
    rs.Close
hapus:
    
    conn.BeginTrans
    conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    conn.CommitTrans
    
    i = 1
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode customer], [Nama Customer] , [Alamat], [Telp],Harga from ms_customer " ' order by kode_Member"
    frmSearch.nmform = "frmMasterCustomer"
    frmSearch.nmctrl = "txtField"
    frmSearch.keyIni = "ms_customer"
    frmSearch.connstr = strcon
    
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    If Combo1.text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        Combo1.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    Set rs = conn.Execute("select * from ms_customer where [kode customer]='" & txtField(0).text & "'")
    
    If Not rs.EOF Then
        conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
        conn.Execute "delete from rpt_ms_customer where [kode customer]='" & txtField(0).text & "'"
        
    End If
    rs.Close
    
        conn.Execute "insert into MS_customer ([kode customer],[nama customer],alamat,telp,harga) values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "','" & Combo1.text & "')"
        conn.Execute "insert into rpt_MS_customer ([kode customer],[nama customer],alamat,telp) values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "')"
        conn.CommitTrans
    
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(0).SetFocus
'    DTPicker1 = Now
'    DTPicker1.Year = DTPicker1.Year + 1
    
End Sub
Public Sub cari_data()
On Error GoTo err
Dim visit As Integer
Dim omzet As Long
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_customer where [kode customer]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        SetComboText rs(4), Combo1
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        rs.Close

    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        
    End If
    If rs.State Then rs.Close
    conn.Close
    Exit Sub
err:
MsgBox err.Description
If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
'        NumberOnly KeyAscii
    End If
    If KeyAscii = 13 And Index = 0 Then
    
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
Private Sub updateitem(kodecustomer As String, namacustomer As String, alamat As String, telp As String, harga As String, expired As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)


    table_name = "MS_customer"
    fields(0) = "[KODE customer]"
    fields(1) = "[NAMA customer]"
    fields(2) = "[alamat]"
    fields(3) = "telp"
    fields(4) = "harga"
    
    nilai(0) = kodecustomer
    nilai(1) = Replace(namacustomer, "'", "''")
    nilai(2) = alamat
    nilai(3) = telp
    nilai(4) = harga
    
    update_data table_name, fields, nilai, "[kode customer]='" & nilai(0) & "'", cn
    
    'conn.Execute tambah_data2(table_name, fields, nilai)
End Sub
Private Sub newitem(kodecustomer As String, namacustomer As String, alamat As String, telp As String, harga As String, expired As String, cn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)

    table_name = "MS_customer"
    fields(0) = "[KODE customer]"
    fields(1) = "[NAMA customer]"
    fields(2) = "[alamat]"
    fields(3) = "telp"
    fields(4) = "harga"
    
    nilai(0) = kodecustomer
    nilai(1) = Replace(namacustomer, "'", "''")
    nilai(2) = alamat
    nilai(3) = telp
    nilai(4) = harga
    
    cn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub



