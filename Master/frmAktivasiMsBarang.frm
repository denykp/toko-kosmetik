VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmAktivasiMsBarang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barang"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12660
   ClipControls    =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8205
   ScaleWidth      =   12660
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdout 
      Caption         =   "[ESC]  &Exit"
      Height          =   615
      Left            =   6060
      TabIndex        =   4
      Top             =   7560
      Width           =   1335
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid flxgrid 
      Height          =   6480
      Left            =   90
      TabIndex        =   3
      Top             =   990
      Width           =   12255
      _ExtentX        =   21616
      _ExtentY        =   11430
      _Version        =   393216
      FixedCols       =   0
      FocusRect       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   10215
      Top             =   360
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmd_refresh 
      Caption         =   "&Refresh"
      Height          =   375
      Left            =   3600
      TabIndex        =   2
      Top             =   360
      Width           =   975
   End
   Begin VB.TextBox txtKey 
      Height          =   330
      Left            =   1320
      TabIndex        =   1
      Top             =   360
      Width           =   2040
   End
   Begin MSAdodcLib.Adodc Adodc2 
      Height          =   330
      Left            =   8055
      Top             =   180
      Visible         =   0   'False
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "frmAktivasiMsBarang.frx":0000
      Height          =   6495
      Left            =   90
      TabIndex        =   5
      Top             =   990
      Width           =   12480
      _ExtentX        =   22013
      _ExtentY        =   11456
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   19
      TabAction       =   1
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   6
      BeginProperty Column00 
         DataField       =   "Kode Barang"
         Caption         =   "Kode Barang"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "Nama Barang"
         Caption         =   "Nama Barang"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "harga1"
         Caption         =   "Harga1"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "Harga2"
         Caption         =   "Harga2"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "QtyGrosir"
         Caption         =   "QtyGrosir"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "disc"
         Caption         =   "Disc"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            Locked          =   -1  'True
         EndProperty
         BeginProperty Column01 
            Locked          =   -1  'True
            ColumnWidth     =   3509.858
         EndProperty
         BeginProperty Column02 
         EndProperty
         BeginProperty Column03 
         EndProperty
         BeginProperty Column04 
         EndProperty
         BeginProperty Column05 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Barang"
      Height          =   240
      Left            =   120
      TabIndex        =   0
      Top             =   390
      Width           =   1320
   End
End
Attribute VB_Name = "frmAktivasiMsBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public tipe As Byte
Dim conn2 As New ADODB.Connection
Private Sub cmd_refresh_Click()
go_search
End Sub

Private Sub cmdout_Click()
Unload Me
End Sub


Private Sub DataGrid1_BeforeColUpdate(ByVal ColIndex As Integer, OldValue As Variant, Cancel As Integer)
    If Not IsNumeric(DataGrid1.Columns(ColIndex).text) Then
        MsgBox "harus berupa angka"
        Cancel = 1
    End If
End Sub

Private Sub flxgrid_DblClick()
    aktif
End Sub
Private Sub aktif()
On Error GoTo err

conn.Open strcon

i = flxGrid.row
J = flxGrid.RowSel

For A = IIf(i < J, i, J) To IIf(i < J, J, i)
 If flxGrid.TextMatrix(A, 5) = 1 Then
 conn.Execute "update ms_barang set flag = '" & 0 & "' where [kode barang] = '" & flxGrid.TextMatrix(A, 0) & "'"
 Else
 conn.Execute "update ms_barang set flag = '" & 1 & "' where [kode barang] = '" & flxGrid.TextMatrix(A, 0) & "'"
 End If

Next A
DropConnection
Adodc1.Refresh
flxGrid.Refresh
    Exit Sub
err:
MsgBox err.Description

End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo err
If KeyCode = 13 Then
aktif
End If

Exit Sub
err:
MsgBox err.Description


End Sub

Private Sub flxgrid_KeyUp(KeyCode As Integer, Shift As Integer)
'On Error GoTo err
'If KeyCode = 13 Then
'aktif
'End If
'
'If KeyCode = 27 Then
'Unload Me
'End If
'Exit Sub
'err:
'MsgBox err.Description
'
End Sub

Private Sub Form_Load()
If tipe = 1 Then
    flxGrid.Visible = True
    DataGrid1.Visible = False
End If
If tipe = 2 Then
    flxGrid.Visible = False
    DataGrid1.Visible = True
End If

Adodc1.ConnectionString = strcon

'Adodc1.RecordSource = "Select  m.[Kode Barang],[Nama Barang],Harga,Stock,Flag from ms_barang m inner join stock on m.[kode barang]=stock.[kode barang] where gudang='" & gudang & "' " ', 'strcon, adOpenStatic, adLockBatchOptimistic
'Adodc1.Refresh
'Set flxgrid.DataSource = Adodc1
go_search
If tipe = 1 Then
With flxGrid
    .ColWidth(0) = 2000
    .ColWidth(1) = 5800
    .ColWidth(2) = 2000
    .ColWidth(3) = 0
    .ColWidth(4) = 1000
   
'    .TextMatrix(0, 0) = "Kode Barang"
'    .ColAlignment(2) = 1 'vbAlignLeft
'    .TextMatrix(0, 1) = "Nama Barang"
'    .TextMatrix(0, 2) = "Harga"
'    .TextMatrix(0, 3) = "Stock"
'    .TextMatrix(0, 4) = "Flag"
End With
flxGrid.Refresh
End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Terminate()
    If db2 Then conn2.Close
End Sub

Private Sub txtKey_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyUp Then
        If Not Adodc1.Recordset.BOF Then Adodc1.Recordset.MovePrevious
    ElseIf KeyCode = vbKeyDown Then
        If Not Adodc1.Recordset.EOF Then Adodc1.Recordset.MoveNext
    End If
End Sub

Private Sub txtKey_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
go_search
End If

End Sub

Sub go_search()
If tipe = 1 Then
 Adodc1.RecordSource = "Select  m.[kode barang],m.[nama barang],harga1,Harga2,sum(stock) as stock,flag from ms_barang m inner join stock on m.[kode barang]=stock.[kode barang] where m.[kode barang]+[nama barang] like '%" & txtKey & "%' group by m.[kode barang],m.[nama barang],harga1,Harga2,flag order by m.[kode barang]"
 Adodc1.Refresh
 Set flxGrid.DataSource = Adodc1
 flxGrid.Refresh
 End If
If tipe = 2 Then
 
 Adodc1.RecordSource = "Select  [kode barang],[nama barang],kategori,Harga1,harga2,qtygrosir,disc from ms_barang where [kode barang]+[nama barang] like '%" & txtKey & "%' order by [kode barang]"
 Adodc1.Refresh
 Set DataGrid1.DataSource = Adodc1
 DataGrid1.Refresh
 
 End If
End Sub
