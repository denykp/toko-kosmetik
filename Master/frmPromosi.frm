VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPromosi 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Customer"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9150
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4695
   ScaleWidth      =   9150
   Begin VB.Frame frDiskon 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   735
      Left            =   45
      TabIndex        =   25
      Top             =   2655
      Visible         =   0   'False
      Width           =   8925
      Begin VB.TextBox txtField 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   3375
         TabIndex        =   29
         Top             =   180
         Width           =   1005
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   330
         Left            =   1890
         TabIndex        =   26
         Top             =   180
         Width           =   1050
         Begin VB.OptionButton Opt1 
            Caption         =   "Rp"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            TabIndex        =   28
            Top             =   45
            Value           =   -1  'True
            Width           =   510
         End
         Begin VB.OptionButton Opt1 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   585
            TabIndex        =   27
            Top             =   45
            Width           =   510
         End
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Disc"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   32
         Top             =   225
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   4545
         TabIndex        =   31
         Top             =   270
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   3060
         TabIndex        =   30
         Top             =   225
         Width           =   285
      End
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Diskon"
      Height          =   330
      Left            =   2880
      TabIndex        =   24
      Top             =   2205
      Width           =   915
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Bonus"
      Height          =   330
      Left            =   1890
      TabIndex        =   23
      Top             =   2205
      Value           =   -1  'True
      Width           =   915
   End
   Begin VB.Frame frBarang 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   735
      Left            =   45
      TabIndex        =   18
      Top             =   2655
      Width           =   8925
      Begin VB.CommandButton cmdSearchItem2 
         Height          =   330
         Left            =   3195
         Picture         =   "frmPromosi.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   225
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtField 
         Height          =   330
         Index           =   1
         Left            =   1800
         TabIndex        =   19
         Top             =   225
         Width           =   1320
      End
      Begin VB.Label lblNamaBarangBonus 
         BackStyle       =   0  'Transparent
         Height          =   240
         Left            =   3600
         TabIndex        =   22
         Top             =   270
         Width           =   5145
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Barang"
         Height          =   240
         Left            =   45
         TabIndex        =   20
         Top             =   270
         Width           =   1320
      End
   End
   Begin VB.TextBox txtQty 
      Height          =   330
      Index           =   4
      Left            =   3465
      TabIndex        =   17
      Top             =   1800
      Width           =   780
   End
   Begin VB.ComboBox cmbRule 
      Height          =   315
      ItemData        =   "frmPromosi.frx":0102
      Left            =   1890
      List            =   "frmPromosi.frx":010C
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   1800
      Width           =   1500
   End
   Begin VB.CommandButton Command1 
      Height          =   330
      Left            =   3285
      Picture         =   "frmPromosi.frx":0168
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   1395
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTDari 
      Height          =   330
      Left            =   1890
      TabIndex        =   11
      Top             =   630
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   20709379
      CurrentDate     =   39965
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3465
      Picture         =   "frmPromosi.frx":026A
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1890
      TabIndex        =   0
      Top             =   1395
      Width           =   1320
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   5175
      Picture         =   "frmPromosi.frx":036C
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3870
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   3555
      Picture         =   "frmPromosi.frx":046E
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3870
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   1980
      Picture         =   "frmPromosi.frx":0570
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3870
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTSampai 
      Height          =   330
      Left            =   1890
      TabIndex        =   12
      Top             =   1035
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   20709379
      CurrentDate     =   39965
   End
   Begin VB.Label lblNoTrans 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      Height          =   240
      Left            =   1890
      TabIndex        =   33
      Top             =   270
      Width           =   1320
   End
   Begin VB.Label lblNamaBarangPromo 
      BackStyle       =   0  'Transparent
      Height          =   240
      Left            =   3690
      TabIndex        =   15
      Top             =   1440
      Width           =   5145
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Barang"
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   1440
      Width           =   1320
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   "Sampai Tanggal"
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Dari Tanggal"
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   7
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   3510
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1665
      TabIndex        =   5
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmPromosi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_penjualanh where [kode customer]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        MsgBox "Data tidak dapat dihapus karena sudah terpakai"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
   
    conn.BeginTrans
    i = 1
    conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    conn.CommitTrans
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select [kode customer], [Nama Customer] , [Alamat], [Telp],Harga from ms_customer " ' order by kode_Member"
    frmSearch.nmform = "frmMasterCustomer"
    frmSearch.nmctrl = "txtField"
    frmSearch.connstr = strcon
    frmSearch.proc = "cari_data"
    frmSearch.Index = 0
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSimpan_Click()
Dim i, j As Byte
On Error GoTo err
    i = 0
    For j = 0 To 1
        If txtField(j).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(j).SetFocus
            Exit Sub
        End If
    Next
    If Combo1.text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        Combo1.SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    Set rs = conn.Execute("select * from ms_customer where [kode customer]='" & txtField(0).text & "'")
    If Not rs.EOF Then
        conn.Execute "delete from ms_customer where [kode customer]='" & txtField(0).text & "'"
    End If
    rs.Close
    conn.Execute "insert into MS_customer values ('" & txtField(0).text & "','" & txtField(1).text & "','" & txtField(2).text & "','" & txtField(3).text & "','" & Combo1.text & "')"
    conn.CommitTrans
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(2).text = ""
    txtField(3).text = ""
    txtField(0).SetFocus
    
End Sub
Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_customer where [kode customer]='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(0)
        txtField(1).text = rs(1)
        txtField(2).text = rs(2)
        txtField(3).text = rs(3)
        SetComboText rs(4), Combo1
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        
        txtField(1).text = ""
        txtField(2).text = ""
        txtField(3).text = ""
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then cmdSearch_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
    Combo1.ListIndex = 0
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub Opt1_Click(Index As Integer)
Dim a As Integer
    If Index = 0 Then
        lbl1(0).Visible = True
        lbl1(1).Visible = False
    Else
        lbl1(0).Visible = False
        lbl1(1).Visible = True
    End If

End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
