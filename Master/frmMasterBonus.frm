VERSION 5.00
Begin VB.Form frmMasterBonus 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Bonus"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9150
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   9150
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   1800
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   1350
      Width           =   4605
   End
   Begin VB.TextBox txtJumlah 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1800
      TabIndex        =   1
      Top             =   990
      Width           =   1500
   End
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   630
      Width           =   1950
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3465
      Picture         =   "frmMasterBonus.frx":0000
      TabIndex        =   10
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   5175
      Picture         =   "frmMasterBonus.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   3555
      Picture         =   "frmMasterBonus.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   1980
      Picture         =   "frmMasterBonus.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2295
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   17
      Top             =   1395
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Pesan :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   16
      Top             =   1395
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   15
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Lebih Dari"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   14
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   13
      Top             =   675
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1890
      TabIndex        =   12
      Top             =   270
      Width           =   1320
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Kategori"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   11
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   9
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   2025
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1620
      TabIndex        =   7
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterBonus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim no2 As String
Public Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from ms_bonus order by clng(ID) desc", conn
    If Not rs.EOF Then
        No = (CLng(Right(rs(0), 5)) + 1)
    Else
        No = "1"
    End If
    rs.Close
    lblNoTrans = No
    
End Sub
Private Sub chkJam_Click()
    If chkJam.value = 1 Then frJam.Visible = True Else frJam.Visible = False
End Sub

Private Sub chktanggal_Click()
    If chktanggal.value = 1 Then DTSampai.Enabled = True Else DTSampai.Enabled = False
End Sub


Private Sub cmbRule_Click()
    If Right(cmbRule.text, 1) = "1" Then
        
        Option2.Visible = False
        Option1.Visible = True
        Option1.value = True
    Else
        Option2.Visible = True
        Option1.Visible = False
        Option2.value = True
    End If
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    
    conn.Execute "delete from ms_bonus where id='" & lblNoTrans & "'"
    conn.CommitTrans

    
    i = 0
    MsgBox "Data sudah dihapus"
    conn.Close
    
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select * from ms_bonus" ' order by kode_Member"
    frmSearch.nmform = "frmMasterbonus"
    frmSearch.nmctrl = "lblnotrans"
    
    frmSearch.connstr = strcon
    
    frmSearch.proc = "cek_data"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Function validasi() As Boolean
    validasi = True
     conn.Open strcon
    rs.Open "select * from ms_bonus where [kategori]='" & cmbKategori.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Ada bonus yang masih aktif untuk kategori tersebut, dengan ID " & rs(0)
        validasi = False
    End If
    rs.Close
    conn.Close
End Function
Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    If Not validasi Then
        Exit Sub
    End If
'    For j = 0 To 1
'        If txtField(j).text = "" Then
'            MsgBox "Semua field yang bertanda * harus diisi"
'            txtField(j).SetFocus
'            Exit Sub
'        End If
'    Next
'    If Combo1.text = "" Then
'        MsgBox "Semua field yang bertanda * harus diisi"
'        Combo1.SetFocus
'        Exit Sub
'    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    If lblNoTrans = "-" Then
        rs.Open "select * from ms_bonus where [kategori]='" & cmbKategori.text & "'", conn_fake
        If Not rs.EOF Then
            MsgBox "Sudah ada bonus untuk kategori tersebut"
            GoTo err
        End If
        rs.Close

        nomor_baru
        add_dataheader 1
    Else
        add_dataheader 2
    End If

    
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub
Private Sub add_dataheader(action As Byte)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)

    table_name = "MS_bonus"
    fields(0) = "[id]"
    fields(1) = "[kategori]"
    fields(2) = "[lebihdari]"
    fields(3) = "[keterangan]"
    
    nilai(0) = lblNoTrans
    nilai(1) = cmbKategori.text
    nilai(2) = txtJumlah.text
    nilai(3) = txtKeterangan
    
    If action = 1 Then
    conn.Execute tambah_data2(table_name, fields, nilai)
    
    ElseIf action = 2 Then
    update_data table_name, fields, nilai, " id='" & lblNoTrans & "'", conn
    
    End If
    
End Sub
Private Sub reset_form()
On Error Resume Next
    load_combo
    lblNoTrans = "-"
    txtJumlah.text = "0"
    txtKeterangan.text = ""
End Sub
Public Sub cek_data()
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_bonus where [id]='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        SetComboText rs(1), cmbKategori
        txtJumlah.text = rs(2)
        txtKeterangan.text = rs(3)
    Else
        
    End If
    rs.Close
    conn.Close
    
End Sub
Private Sub load_combo()
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
conn.Open strcon
cmbKategori.Clear

rs.Open "select * from var_kategori order by kategori", conn
While Not rs.EOF
    cmbKategori.AddItem rs(0)
    rs.MoveNext
Wend
rs.Close
conn.Close
cmbKategori.ListIndex = 0
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
'    Combo1.ListIndex = 0
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

