VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmMasterPromosi 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Master Promosi"
   ClientHeight    =   4770
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9150
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4770
   ScaleWidth      =   9150
   Begin VB.ComboBox cmbKategori 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1890
      TabIndex        =   39
      Text            =   "Combo1"
      Top             =   1440
      Width           =   1905
   End
   Begin VB.Frame frJam 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4995
      TabIndex        =   37
      Top             =   540
      Visible         =   0   'False
      Width           =   3840
      Begin MSComCtl2.DTPicker DTJam1 
         Height          =   330
         Left            =   90
         TabIndex        =   13
         Top             =   135
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   389611522
         CurrentDate     =   39965.5
      End
      Begin MSComCtl2.DTPicker DTJam2 
         Height          =   330
         Left            =   2250
         TabIndex        =   14
         Top             =   135
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   389611522
         CurrentDate     =   39965
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1710
         TabIndex        =   38
         Top             =   180
         Width           =   330
      End
   End
   Begin VB.CheckBox chktanggal 
      Caption         =   "Sampai Tanggal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   16
      Top             =   1035
      Width           =   1770
   End
   Begin VB.CheckBox chkJam 
      Caption         =   "Jam"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4275
      TabIndex        =   12
      Top             =   675
      Width           =   780
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Diskon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2880
      TabIndex        =   3
      Top             =   2205
      Width           =   915
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Bonus"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1890
      TabIndex        =   2
      Top             =   2205
      Value           =   -1  'True
      Width           =   915
   End
   Begin VB.Frame frBarang 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   45
      TabIndex        =   25
      Top             =   2700
      Width           =   8925
      Begin VB.TextBox txtQty2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         TabIndex        =   5
         Top             =   495
         Width           =   780
      End
      Begin VB.CommandButton cmdSearchItem2 
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3195
         Picture         =   "frmMasterPromosi.frx":0000
         TabIndex        =   27
         Top             =   90
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox txtKodeBarang2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         TabIndex        =   4
         Top             =   90
         Width           =   1320
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Pcs"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2655
         TabIndex        =   36
         Top             =   540
         Width           =   420
      End
      Begin VB.Label lblNamaBarangBonus 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3600
         TabIndex        =   28
         Top             =   135
         Width           =   5145
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Barang"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   26
         Top             =   135
         Width           =   1320
      End
   End
   Begin VB.TextBox txtQty1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3465
      TabIndex        =   1
      Top             =   1800
      Width           =   780
   End
   Begin VB.ComboBox cmbRule 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmMasterPromosi.frx":0102
      Left            =   1890
      List            =   "frmMasterPromosi.frx":010C
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1800
      Width           =   1500
   End
   Begin MSComCtl2.DTPicker DTDari 
      Height          =   330
      Left            =   1890
      TabIndex        =   15
      Top             =   630
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   389677059
      CurrentDate     =   39965
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3465
      Picture         =   "frmMasterPromosi.frx":0168
      TabIndex        =   22
      Top             =   225
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5175
      Picture         =   "frmMasterPromosi.frx":026A
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   3960
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3555
      Picture         =   "frmMasterPromosi.frx":036C
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   3960
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1980
      Picture         =   "frmMasterPromosi.frx":046E
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   3960
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTSampai 
      Height          =   330
      Left            =   1890
      TabIndex        =   17
      Top             =   1035
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   389677059
      CurrentDate     =   39965
   End
   Begin VB.Frame frDiskon 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   90
      TabIndex        =   29
      Top             =   2745
      Visible         =   0   'False
      Width           =   8925
      Begin VB.TextBox txtDisc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3375
         TabIndex        =   8
         Text            =   "0"
         Top             =   180
         Width           =   1005
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   330
         Left            =   1890
         TabIndex        =   30
         Top             =   180
         Width           =   1050
         Begin VB.OptionButton Opt1 
            Caption         =   "Rp"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            TabIndex        =   6
            Top             =   45
            Value           =   -1  'True
            Width           =   510
         End
         Begin VB.OptionButton Opt1 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   585
            TabIndex        =   7
            Top             =   45
            Width           =   510
         End
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Disc"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   225
         TabIndex        =   33
         Top             =   225
         Width           =   1140
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   4545
         TabIndex        =   32
         Top             =   270
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.Label lbl1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   3060
         TabIndex        =   31
         Top             =   225
         Width           =   285
      End
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Pcs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   35
      Top             =   1845
      Width           =   420
   End
   Begin VB.Label lblNoTrans 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1890
      TabIndex        =   34
      Top             =   270
      Width           =   1320
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Barang"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   24
      Top             =   1440
      Width           =   1320
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Dari Tanggal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   23
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1215
      TabIndex        =   21
      Top             =   270
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   20
      Top             =   3690
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   19
      Top             =   270
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   18
      Top             =   270
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterPromosi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim no2 As String
Public Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from ms_promosi order by clng(ID) desc", conn
    If Not rs.EOF Then
        No = (CLng(Right(rs(0), 5)) + 1)
    Else
        No = "1"
    End If
    rs.Close
    lblNoTrans = No
    
End Sub
Private Sub chkJam_Click()
    If chkJam.value = 1 Then frJam.Visible = True Else frJam.Visible = False
End Sub

Private Sub chktanggal_Click()
    If chktanggal.value = 1 Then DTSampai.Enabled = True Else DTSampai.Enabled = False
End Sub


Private Sub cmbRule_Click()
    If Right(cmbRule.text, 1) = "1" Then
        
        Option2.Visible = False
        Option1.Visible = True
        Option1.value = True
    Else
        Option2.Visible = True
        Option1.Visible = False
        Option2.value = True
    End If
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
        conn.Execute "delete from ms_promosi where id='" & lblNoTrans & "'"
        conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    conn.Close
    
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = "select * from Q_Promo" ' order by kode_Member"
    frmSearch.nmform = "frmMasterPromosi"
    frmSearch.nmctrl = "lblnotrans"
        frmSearch.connstr = strcon
    frmSearch.proc = "cek_data"
    frmSearch.Index = -1
    frmSearch.col = 2
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub



Private Sub cmdSearchItem2_Click()
    frmSearch.query = "select [kode barang],[nama barang],kategori,harga from ms_barang" ' order by kode_Member"
    frmSearch.nmform = "frmMasterPromosi"
    frmSearch.nmctrl = "txtkodebarang2"
        frmSearch.connstr = strcon
    
    frmSearch.proc = "cari_databarang"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub
Private Function validasi() As Boolean
    validasi = True
    conn.Open
    rs.Open "select * from ms_promosi where [kategori]='" & cmbKategori.text & "' and  format([tanggal dari],'yyyy/MM/dd')<=#" & Format(DTDari, "yyyy/MM/dd") & "# and ([fg_tanggal sampai]='0' or format([tanggal sampai],'yyyy/MM/dd')>=#" & Format(DTSampai, "yyyy/MM/dd") & "#) and (fg_jam='0' or ([jam dari]<=#" & Format(DTJam1, "hh:mm") & "# and [jam sampai]>= #" & Format(DTJam2, "hh:mm") & "#))", conn
    If Not rs.EOF Then
        MsgBox "Ada Promosi yang masih aktif untuk barang tersebut, dengan ID " & rs(0)
        validasi = False
    End If
    rs.Close
    conn.Close
End Function
Private Sub cmdSimpan_Click()
Dim i, J As Byte
On Error GoTo err
    i = 0
    If Not validasi Then
        Exit Sub
    End If
'    For j = 0 To 1
'        If txtField(j).text = "" Then
'            MsgBox "Semua field yang bertanda * harus diisi"
'            txtField(j).SetFocus
'            Exit Sub
'        End If
'    Next
'    If Combo1.text = "" Then
'        MsgBox "Semua field yang bertanda * harus diisi"
'        Combo1.SetFocus
'        Exit Sub
'    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    If lblNoTrans = "-" Then
        rs.Open "select * from ms_promosi where kategori='" & cmbKategori.text & "'", conn_fake
        If Not rs.EOF Then
            MsgBox "Sudah ada promosi untuk barang tersebut"
            GoTo err
        End If
        rs.Close

        nomor_baru
        add_dataheader 1
    Else
        add_dataheader 2
    End If

    
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub
Private Sub add_dataheader(action As Byte)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(15)
    ReDim nilai(15)

    table_name = "MS_PROMOSI"
    fields(0) = "[id]"
    fields(1) = "[tanggal dari]"
    fields(2) = "[fg_tanggal sampai]"
    fields(3) = "[tanggal sampai]"
    fields(4) = "fg_jam"
    fields(5) = "[jam dari]"
    fields(6) = "[jam sampai]"
    fields(7) = "[kategori]"
    fields(8) = "rule"
    fields(9) = "[qty1]"
    fields(10) = "[tipe promosi]"
    fields(11) = "[kode barang2]"
    fields(12) = "qty2"
    fields(13) = "DISC_tipe"
    fields(14) = "disc"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTDari, "yyyy/MM/dd")
    nilai(2) = chktanggal
    nilai(3) = Format(DTSampai, "yyyy/MM/dd")
    nilai(4) = chkJam
    nilai(5) = Format(DTJam1, "HH:mm")
    nilai(6) = Format(DTJam2, "HH:mm")
    nilai(7) = cmbKategori.text
    nilai(8) = Right(cmbRule, 1)
    nilai(9) = txtQty1.text
    nilai(10) = IIf(Option1.value, "1", "2")
    nilai(11) = txtKodeBarang2.text
    nilai(12) = txtQty2.text
    nilai(13) = IIf(Opt1(0).value = True, 1, 2)
    nilai(14) = txtDisc.text
    
    If action = 1 Then
    conn.Execute tambah_data2(table_name, fields, nilai)
    
    ElseIf action = 2 Then
    update_data table_name, fields, nilai, " id='" & lblNoTrans & "'", conn
    
    End If
    
End Sub
Private Sub reset_form()
On Error Resume Next
    txtDisc.text = "0"
    
    txtKodeBarang2 = ""
    lblNoTrans = "-"
    DTDari = Now
    DTSampai = Now
    cmbRule.ListIndex = 1
    chkJam = 0
    chktanggal = 0
    txtQty1.text = 0
    txtQty2.text = 0
    txtDisc.text = 0
    lblNamaBarangBonus = ""
    lblNamaBarangPromo = ""
    conn.Open strcon
    rs.Open "select * from var_kategori order by kategori", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    conn.Close
    If cmbKategori.ListCount > 0 Then cmbKategori.ListIndex = 0
End Sub
Public Sub cek_data()
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_promosi where [id]='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        SetComboText rs("kategori"), cmbKategori
        txtKodeBarang2.text = rs("kode barang2")
        DTDari = rs("tanggal dari")
        DTSampai = rs("tanggal sampai")
        chkJam = rs("fg_jam")
        DTJam1 = rs("jam dari")
        DTJam2 = rs("jam sampai")
        chktanggal = rs("fg_tanggal sampai")
        If rs("tipe promosi") = "1" Then Option1.value = True Else Option2.value = True
        SetComboTextRight rs("rule"), cmbRule
        txtQty1.text = rs("qty1")
        txtQty2.text = rs("qty2")
        If rs("disc_tipe") = "1" Then Opt1(0).value = True Else Opt1(1).value = True
        txtDisc.text = rs("disc")
    Else
        
        txtKodeBarang2.text = ""
        DTDari = Now
        DTSampai = Now
        chkJam = 1
        chktanggal = 0
        txtQty1.text = 0
        txtQty2.text = 0
        txtDisc.text = 0
    End If
    rs.Close
    conn.Close
    cari_databarang
End Sub
Public Sub cari_databarang()
Dim conn As New ADODB.Connection
    
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama barang] from ms_barang where [kode barang]='" & txtKodeBarang2.text & "'", conn
    If Not rs.EOF Then
        lblNamaBarangBonus = rs(0)
    Else
        lblNamaBarangBonus = ""
    End If
    rs.Close
    conn.Close
End Sub
Private Sub load_combo()
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

Private Sub Form_Load()
'    Combo1.ListIndex = 0
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub Opt1_Click(Index As Integer)
Dim A As Integer
    If Index = 0 Then
        lbl1(0).Visible = True
        lbl1(1).Visible = False
    Else
        lbl1(0).Visible = False
        lbl1(1).Visible = True
    End If

End Sub


Private Sub Option1_Click()
    frBarang.Visible = True
    frDiskon.Visible = False
End Sub

Private Sub Option2_Click()
    frBarang.Visible = False
    frDiskon.Visible = True
End Sub

Private Sub txtDisc_GotFocus()
    With txtDisc
    .SelStart = 0
    .SelLength = Len(txtDisc)
    End With
End Sub

Private Sub txtKodeBarang2_GotFocus()
    With txtKodeBarang2
    .SelStart = 0
    .SelLength = Len(txtKodeBarang2)
    End With
End Sub

Private Sub txtKodeBarang2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchItem2_Click
End Sub

Private Sub txtQty1_GotFocus()
    txtQty1.SelStart = 0
    txtQty1.SelLength = Len(txtQty1)
End Sub
