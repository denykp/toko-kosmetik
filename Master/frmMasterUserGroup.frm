VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMasterUserGroup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "User Group"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   7095
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   7095
   Begin VB.CheckBox chkExport 
      Caption         =   "Export"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   20
      Top             =   3420
      Width           =   2085
   End
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1710
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   720
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.OptionButton OptGrosir 
      Caption         =   "Harga Grosir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5235
      TabIndex        =   16
      Top             =   585
      Width           =   1755
   End
   Begin VB.OptionButton optEceran 
      Caption         =   "Harga Eceran"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5235
      TabIndex        =   15
      Top             =   195
      Width           =   1695
   End
   Begin VB.CheckBox chkHargaManual 
      Caption         =   "Mengubah Harga Manual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   14
      Top             =   3060
      Width           =   2085
   End
   Begin VB.CheckBox chkUbahHarga 
      Caption         =   "Mengubah Harga Jual"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   13
      Top             =   2700
      Width           =   2085
   End
   Begin VB.CheckBox chkAlert 
      Caption         =   "Stock Alert"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   12
      Top             =   2340
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CheckBox chkHPP 
      Caption         =   "Hpp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   3
      Top             =   1980
      Width           =   1185
   End
   Begin VB.CheckBox chkStock 
      Caption         =   "Stock"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4590
      TabIndex        =   2
      Top             =   1575
      Width           =   1140
   End
   Begin MSComctlLib.TreeView tvwMain 
      Height          =   4920
      Left            =   225
      TabIndex        =   1
      Top             =   1575
      Width           =   4200
      _ExtentX        =   7408
      _ExtentY        =   8678
      _Version        =   393217
      LineStyle       =   1
      Style           =   6
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      SingleSel       =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4575
      Picture         =   "frmMasterUserGroup.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   5850
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4575
      Picture         =   "frmMasterUserGroup.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5085
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3195
      Picture         =   "frmMasterUserGroup.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   270
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   270
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4575
      Picture         =   "frmMasterUserGroup.frx":0306
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4365
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   19
      Top             =   720
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   11
      Top             =   315
      Width           =   150
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   1260
      Width           =   2130
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1530
      TabIndex        =   8
      Top             =   315
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Group"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   315
      Width           =   1320
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Default Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   135
      TabIndex        =   18
      Top             =   720
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmMasterUserGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim uncheck As Boolean
Dim xnode As node
Dim eceran As Byte
Dim grosir As Byte

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
        conn.Execute "delete from var_usergroup where [group]='" & txtField(0).text & "'"
        conn.Execute "delete from user_groupmenu where [group]='" & txtField(0).text & "'"
    
    conn.CommitTrans
    
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [group] from var_usergroup" ' order by satuan"
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.nmform = "frmMasteruserGroup"
    frmSearch.nmctrl = "txtField"
    frmSearch.keyIni = "userGroup"
    frmSearch.col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte

On Error GoTo err
    i = 0
    If txtField(0).text = "" Then
        MsgBox "Semua field yang bertanda * harus diisi"
        txtField(0).SetFocus
        Exit Sub
    End If
    conn.ConnectionString = strconasli
    conn.Open
    If optEceran.value = True Then tipeHarga = "Eceran" Else tipeHarga = "Grosir"
    
    rs.Open "select * from var_usergroup where [group]='" & txtField(0).text & "' and modul='" & modul & "'", conn
    If rs.EOF Then
        conn.Execute "insert into var_usergroup values ('" & txtField(0).text & "','" & chkStock.value & "','" & chkHPP.value & "','" & chkAlert.value & "','" & chkUbahHarga.value & "','" & chkHargaManual.value & "','" & tipeHarga & "','" & cmbGudang.text & "','" & chkExport.value & "','" & modul & "')"
    Else
        conn.Execute "update var_usergroup set stock='" & chkStock.value & "',hpp='" & chkHPP.value & "',alert='" & chkAlert.value & "',ubahharga='" & chkUbahHarga.value & "',hargaManual='" & chkHargaManual.value & "',harga='" & tipeHarga & "',export='" & chkHargaManual.value & "' where [group]='" & txtField(0).text & "' and modul='" & modul & "'"
    End If
    rs.Close
    conn.BeginTrans
    
    i = 1
    Dim menu() As String
    
    
    conn.Execute "delete from user_groupmenu where [group]='" & txtField(0).text & "' and modul='" & modul & "'"
    For J = 1 To tvwMain.Nodes.Count
    menu = Split(tvwMain.Nodes.Item(J).key, ".")
    conn.Execute "insert into user_groupmenu values ('" & txtField(0).text & "','" & tvwMain.Nodes.Item(J).key & "','" & menu(0) & "','" & menu(1) & "','" & IIf(tvwMain.Nodes.Item(J).Checked, "1", "0") & "','" & modul & "')"
    Next
    
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Private Sub reset_form()
    txtField(0).text = ""
    txtField(0).SetFocus
End Sub
Public Sub cari_data()

    conn.ConnectionString = strcon
On Error Resume Next
    conn.Open
    rs.Open "select * from var_usergroup where [group]='" & txtField(0).text & "' and modul='" & modul & "'", conn
    If Not rs.EOF Then
        chkHPP = rs("hpp")
        chkStock = rs("stock")
        chkUbahHarga = rs!ubahharga
        chkHargaManual = rs!hargaManual
        chkExport = rs!Export
        If rs!harga = "Eceran" Then optEceran.value = True Else OptGrosir.value = True
        SetComboText rs!gudang, cmbGudang
        
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        chkHPP = 1
        chkStock = 1
        chkUbahHarga = 0
        chkExport = 0
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
    End If
    rs.Close
    rs.Open "select * from user_groupmenu where [group]='" & txtField(0).text & "' and modul='" & modul & "'", conn
    While Not rs.EOF
        tvwMain.Nodes.Item(rs("key").value).Checked = CBool(rs("value"))
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF5 Then cmdSearch_Click
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call MySendKeys("{tab}")
    End If
End Sub
'Private Sub loadmenu(menu As Long, parent)
'Dim A As Integer
'Dim menuinfo As MENUITEMINFO
'Dim un, B As Long
'Dim sub_hmenu As Long
'    For A = 0 To GetMenuItemCount(menu)
'        menuinfo.cbSize = 255
'        menuinfo.fMask = MIIM_STRING
'        menuinfo.cch = MenuName
'        menuinfo.dwTypeData = MenuName
'
'
'        GetMenuItemInfo menu, 0, True, menuinfo
'        Set nodex = tvwMain.Nodes.Add(parent, tvwChild, menuinfo.dwItemData, menuinfo.dwItemData)
'        nodex.Checked = True
'        loadmenu sub_hmenu, sub_name
'    Next
'End Sub
Private Sub Form_Load()
Dim nodex As node
Dim hwnd As Long
Dim hmenu As Long
Dim parent As String
Dim Index As Integer
'    conn.Open strcon
    If LCase(modul) = "inventory" Then
        Label16.Visible = True
        cmbGudang.Visible = True
    End If
    For Each obj In frmMain.Controls
'        hmenu = GetSubMenu(GetMenu(frmMain.hwnd), i)
'        If frmMain.mnuMain.Item(i).Visible = True Then
'            Set nodex = tvwMain.Nodes.Add(Null, Null, frmMain.mnuMain.Item(i).Name & frmMain.mnuMain.Item(i).Index, frmMain.mnuMain.Item(i).Caption)
'            nodex.Checked = True
'            loadmenu hmenu, frmMain.mnuMain.Item(i).Name & frmMain.mnuMain.Item(i).Index
'         End If
          If TypeOf obj Is menu And obj.Enabled = True Then
            
            If nodex Is Nothing Or obj.Name = parent Then
                Set nodex = tvwMain.Nodes.Add(Null, Null, obj.Name & "." & obj.Index, obj.Caption)
                parent = obj.Name
                Index = obj.Index
            Else
                Set nodex = tvwMain.Nodes.Add(parent & "." & Index, tvwChild, obj.Name & "." & obj.Index, obj.Caption)
            End If
            nodex.Checked = True
           End If
    Next
'    rs.Open "select * from menu order by urut", conn
'    While Not rs.EOF
'        Set nodex = tvwMain.Nodes.Add(IIf(rs("parent") = "", Null, rs("parent").value), IIf(rs("parent") = "", Null, tvwChild), rs("key"), rs("nama"))
'        nodex.Checked = True
'        rs.MoveNext
'    Wend
'    rs.Close
'    conn.Close
    conn.Open
    cmbGudang.Clear
    rs.Open "select * from var_gudang order by gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    DropConnection
    SetComboText gudang, cmbGudang


End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub



Private Sub tvwMain_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim nod As node
    If Not uncheck Then Exit Sub
    Set nod = xnode.FirstSibling
    If nod.Checked = True Then Exit Sub
    While nod <> xnode.LastSibling
        Set nod = nod.Next
        If nod.Checked = True Then Exit Sub
    Wend
    
    uncheck = False
gaksip:
    MsgBox "Minimal harus ada satu menu yang dipakai, apabila anda ingin membuat serangkaian menu tidak kelihatan silahkan uncheck pada parent rangkaian menu tersebut."
    xnode.Checked = True
End Sub

Private Sub tvwMain_NodeCheck(ByVal node As MSComctlLib.node)
Dim nod As node
    node.Checked = True
    uncheck = False
    Set nod = node.Child
    For i = 1 To node.children
        nod.Checked = True
        Set nod = nod.Next
    Next
    Set xnode = node
    uncheck = True
    

End Sub


Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
'
'If obj.Parent.Name <> "frmMain" Then
'            Set nodex = tvwMain.Nodes.Add(obj.Parent.Name & obj.Parent.Index, tvwChild, obj.Name & obj.Index, obj.Caption)
'            Else
'            Set nodex = tvwMain.Nodes.Add(Null, Null, obj.Name & obj.Index, obj.Caption)
'            End If
'            nodex.Checked = True
''            Debug.Print obj.Caption, obj.Name
