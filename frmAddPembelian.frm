VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAddPembelian 
   BackColor       =   &H001A64FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembelian"
   ClientHeight    =   8670
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9480
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8670
   ScaleWidth      =   9480
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   1725
      Left            =   7965
      TabIndex        =   30
      Top             =   7335
      Visible         =   0   'False
      Width           =   4155
      Begin VB.TextBox txtDisc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   720
         TabIndex        =   33
         Text            =   "0"
         Top             =   180
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.TextBox txtPPN 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   720
         TabIndex        =   32
         Text            =   "0"
         Top             =   990
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.TextBox txtDiscRp 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2610
         TabIndex        =   31
         Text            =   "0"
         Top             =   540
         Visible         =   0   'False
         Width           =   2130
      End
      Begin VB.Label lblTotal 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2475
         TabIndex        =   46
         Top             =   1350
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1845
         TabIndex        =   45
         Top             =   1350
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Disc"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   44
         Top             =   225
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.Label Label16 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1845
         TabIndex        =   43
         Top             =   180
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label lblDisc 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2475
         TabIndex        =   42
         Top             =   180
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.Label Label18 
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1305
         TabIndex        =   41
         Top             =   225
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   135
         TabIndex        =   40
         Top             =   1350
         Visible         =   0   'False
         Width           =   1320
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1305
         TabIndex        =   39
         Top             =   1035
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Label lblPPN 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2475
         TabIndex        =   38
         Top             =   990
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.Label Label20 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1845
         TabIndex        =   37
         Top             =   990
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label Label21 
         BackStyle       =   0  'Transparent
         Caption         =   "PPN"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   36
         Top             =   1035
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.Label Label23 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1845
         TabIndex        =   35
         Top             =   585
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label Label24 
         BackStyle       =   0  'Transparent
         Caption         =   "Disc Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   34
         Top             =   630
         Visible         =   0   'False
         Width           =   645
      End
   End
   Begin VB.TextBox txtNoInvoice 
      Height          =   330
      Left            =   4860
      TabIndex        =   24
      Top             =   1440
      Width           =   1410
   End
   Begin VB.CommandButton cmdApprove 
      BackColor       =   &H80000003&
      Caption         =   "&Posting"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3015
      Picture         =   "frmAddPembelian.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   7740
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   2295
      Picture         =   "frmAddPembelian.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdSearch 
      Height          =   330
      Left            =   3150
      Picture         =   "frmAddPembelian.frx":0544
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   675
      Width           =   375
   End
   Begin VB.TextBox txtNoTB 
      Height          =   330
      Left            =   1665
      TabIndex        =   0
      Top             =   675
      Width           =   1410
   End
   Begin VB.TextBox txtKeterangan 
      Height          =   285
      Left            =   1665
      TabIndex        =   2
      Top             =   1890
      Width           =   3885
   End
   Begin VB.CommandButton cmdHapus 
      BackColor       =   &H80000003&
      Caption         =   "&Baru"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4590
      Picture         =   "frmAddPembelian.frx":0646
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   7740
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H80000003&
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6165
      Picture         =   "frmAddPembelian.frx":0748
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7740
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H80000003&
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1440
      Picture         =   "frmAddPembelian.frx":084A
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   7740
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   4860
      TabIndex        =   1
      Top             =   630
      Width           =   2220
      _ExtentX        =   3916
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   56950787
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   4695
      Left            =   90
      TabIndex        =   3
      Top             =   2295
      Width           =   8970
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   10
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   14671839
      RowHeight       =   423
      ExtraHeight     =   212
      Columns.Count   =   10
      Columns(0).Width=   2143
      Columns(0).Caption=   "Kode Barang"
      Columns(0).Name =   "Kode Barang"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4868
      Columns(1).Caption=   "Nama Barang"
      Columns(1).Name =   "Nama Barang"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1535
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "Qty"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   3
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1323
      Columns(3).Caption=   "Satuan"
      Columns(3).Name =   "Satuan"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2328
      Columns(4).Caption=   "Harga Satuan"
      Columns(4).Name =   "Harga Satuan"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,##0"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Disc"
      Columns(5).Name =   "Disc"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "Disc Rp"
      Columns(6).Name =   "Disc Rp"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2170
      Columns(7).Caption=   "Jumlah"
      Columns(7).Name =   "Jumlah"
      Columns(7).Alignment=   1
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "#,##0"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Konversi"
      Columns(8).Name =   "Konversi"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Disc_Persen"
      Columns(9).Name =   "Disc_Persen"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      _ExtentX        =   15822
      _ExtentY        =   8281
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   4860
      TabIndex        =   47
      Top             =   1035
      Width           =   2220
      _ExtentX        =   3916
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   56950787
      CurrentDate     =   38927
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4725
      TabIndex        =   49
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   "Jatuh Tempo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3600
      TabIndex        =   48
      Top             =   1125
      Width           =   1050
   End
   Begin VB.Label Label17 
      BackStyle       =   0  'Transparent
      Caption         =   "SubTotal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4410
      TabIndex        =   29
      Top             =   7155
      Width           =   1230
   End
   Begin VB.Label lblSubTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6750
      TabIndex        =   28
      Top             =   7155
      Width           =   2265
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Rp"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   27
      Top             =   7155
      Width           =   555
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Invoice"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   3600
      TabIndex        =   26
      Top             =   1485
      Width           =   1005
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   4725
      TabIndex        =   25
      Top             =   1485
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3600
      TabIndex        =   21
      Top             =   360
      Width           =   780
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4725
      TabIndex        =   20
      Top             =   315
      Width           =   105
   End
   Begin VB.Label lblGudang 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4860
      TabIndex        =   19
      Top             =   315
      Width           =   2085
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3600
      TabIndex        =   18
      Top             =   765
      Width           =   780
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4725
      TabIndex        =   17
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   1485
      TabIndex        =   16
      Top             =   720
      Width           =   105
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Penerimaan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   90
      TabIndex        =   15
      Top             =   720
      Width           =   1320
   End
   Begin VB.Label lblKdSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   14
      Top             =   1125
      Width           =   1500
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   13
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   12
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Label lblNmSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   11
      Top             =   1530
      Width           =   2940
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   9
      Top             =   1125
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   8
      Top             =   1125
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   7
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuPosting 
         Caption         =   "&Posting"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPembelian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select kode_barang,nama_barang, merk, kode_jenis, satuan_2, min_qty, harga from ms_barang"
Const queryPenerimaan As String = "select * from t_poh th where th.id not in (select tb_id from t_pembelianh)"
Const queryBeli As String = "select * from t_Pembelianh th"
Dim subtotal, total As Currency
Dim ppn As Currency
Dim kode_gudang As String
Dim disc As Currency
Private Sub nomor_baru()
Dim No As String
'    rs.Open "select top 1 ID from T_PembelianH where left(ID,2)='" & Format(Now, "MM") & "' and substring(ID,3,2)='" & Format(Now, "YY") & "' order by ID desc", ConnT
'    If Not rs.EOF Then
'        No = Format(Now, "MM") & Format(Now, "yy") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
'    Else
'        No = Format(Now, "MM") & Format(Now, "yy") & Format("1", "00000")
'    End If
'    rs.Close
    lblNoTrans = txtNoTB.text
End Sub

Private Sub cmdApprove_Click()
On Error GoTo err
    ConnT.ConnectionString = strconT
    ConnT.Open
    ConnT.Execute "update t_pembelianh set post_fg='1' where id='" & lblNoTrans & "'"
    'ConnT.Execute "exec sp_updatehrgpkk '" & txtNoTB.Text & "','" & Format(DTPicker1, "MM/dd/yyyy hh:mm:ss") & "','B'"
    ConnT.Execute "insert into HPP_FIFO (tanggal,tipe,no_trans,kode_barang,harga_beli,qty,gudang) (select h.tanggal,'B',h.id,d.kode_barang,d.harga,d.qty,'" & gudang & "' from t_pembelianh h inner join t_pembeliand d on h.id=d.id where h.id='" & lblNoTrans & "')"
    ConnT.Close
    MsgBox "Data pembelian dengan nomor " & lblNoTrans & " telah berhasil di Posting"
    reset_form
    Exit Sub
err:
    MsgBox err.Description
    ConnT.Close
End Sub

Private Sub cmdHapus_Click()
    reset_form
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    frmSearch.query = queryPenerimaan & " and gudang='" & gudang & "'"
    frmSearch.nmform = "frmAddPembelian"
    frmSearch.nmctrl = "txtnoTB"
    frmSearch.connstr = strconT
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.query = queryBeli & " where gudang='" & gudang & "'"
    frmSearch.nmform = "frmAddPembelian"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.connstr = strconT
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show

End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim counter As Integer
On Error GoTo err
    ConnT.ConnectionString = strconT
    ConnT.Open
    ConnT.BeginTrans
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        ConnT.Execute "delete from t_PembelianH where ID='" & lblNoTrans & "'"
        ConnT.Execute "delete from t_PembelianD where ID='" & lblNoTrans & "'"
    End If
    add_dataheader
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.Rows
        If DBGrid.Columns(0).text <> "" Then add_datadetail
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    
    ConnT.CommitTrans
    ConnT.Close
    MsgBox "Data telah tersimpan"
    'reset_form
    cmdApprove.Enabled = True
    mnuPosting.Enabled = True
    DBGrid.SetFocus
    SendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then ConnT.RollbackTrans
    If ConnT.State Then ConnT.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
    lblNoTrans = "-"
    txtNoTB.text = ""
    txtNoInvoice.text = ""
    txtKeterangan.text = ""
    txtDisc.text = "0"
    txtPPN.text = "0"
    txtDiscRp.text = "0"
    DBGrid.RemoveAll
    DTPicker1 = Now
    DTPicker2 = Now
    cmdApprove.Enabled = False
    mnuPosting.Enabled = False
    subtotal = 0
    disc = 0
    ppn = 0
    total = 0
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(10)
    ReDim nilai(10)
    
    table_name = "T_PEMBELIANH"
    fields(0) = "id"
    fields(1) = "tb_id"
    fields(2) = "tanggal"
    fields(3) = "invoice_no"
    fields(4) = "keterangan"
    fields(5) = "diskon"
    fields(6) = "diskon_rp"
    fields(7) = "ppn"
    fields(8) = "gudang"
    fields(9) = "jatuh_tempo"
    
    nilai(0) = lblNoTrans
    nilai(1) = txtNoTB.text
    nilai(2) = Format(DTPicker1, "MM/dd/yyyy hh:mm:ss")
    nilai(3) = txtNoInvoice.text
    nilai(4) = txtKeterangan.text
    nilai(5) = txtDisc.text
    nilai(6) = txtDiscRp.text
    nilai(7) = txtPPN.text
    nilai(8) = gudang
    nilai(9) = Format(DTPicker2, "MM/dd/yyyy hh:mm:ss")
    
    tambah_data table_name, fields, nilai
End Sub
Private Sub add_datadetail()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "T_PembelianD"
    fields(0) = "ID"
    fields(1) = "kode_barang"
    fields(2) = "qty"
    fields(3) = "harga"
'    fields(4) = "diskon"
'    fields(5) = "diskon_rp"
    fields(4) = "KONVERSI"
    fields(5) = "satuan"
    
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(0).text
    nilai(2) = DBGrid.Columns(2).text
    nilai(3) = Format(DBGrid.Columns(4).text, "###0")
'    nilai(4) = DBGrid.Columns(5).text
'    nilai(5) = Format(DBGrid.Columns(6).text, "###0")
    nilai(4) = DBGrid.Columns(8).text
    nilai(5) = DBGrid.Columns(3).text
    
    tambah_data table_name, fields, nilai
End Sub


Private Sub DBGrid_AfterColUpdate(ByVal ColIndex As Integer)
    If DBGrid.Columns(2).text <> "" And DBGrid.Columns(4).text <> "" Then
        DBGrid.Columns(7).text = DBGrid.Columns(2).text * DBGrid.Columns(4).text
        Else
        DBGrid.Columns(7).text = "0"
    End If
    If (ColIndex = 2 Or ColIndex = 4) And (IsNumeric(DBGrid.Columns(2).text) And IsNumeric(DBGrid.Columns(4).text)) Then
        subtotal = subtotal + DBGrid.Columns(7).text '(DBGrid.Columns(2).Text * DBGrid.Columns(4).Text)
'        disc = subtotal * txtDisc.text / 100
'        ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
'        total = subtotal - disc - txtDiscRp + ppn
         lblSubTotal = Format(subtotal, "#,##0.00")
'        lblPPN = Format(ppn, "#,##0.00")
'        lblDisc = Format(disc, "#,##0.00")
'        lblTotal = Format(total, "#,##0.00")
    End If

End Sub

Private Sub DBGrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If ColIndex = 2 And Not IsEmpty(OldValue) And IsNumeric(DBGrid.Columns(4).text) Then
        subtotal = subtotal - DBGrid.Columns(7).text '(OldValue * DBGrid.Columns(4).Text)
    End If
    If ColIndex = 4 And Not IsEmpty(OldValue) And IsNumeric(DBGrid.Columns(2).text) Then
        subtotal = subtotal - DBGrid.Columns(7).text '(OldValue * DBGrid.Columns(2).Text)
    End If

End Sub

Private Sub DBGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then SendKeys "{tab}"
End Sub

Private Sub Form_Activate()
    SendKeys "{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then SendKeys "{tab}"
End Sub

Private Sub Form_Load()
    reset_form
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    If Not rs.EOF Then
        lblGudang = rs(1)
        kode_gudang = gudang
    End If
    rs.Close
    conn.Close
End Sub

Public Sub cekOrder(id As String)
Dim kd_supplier As String
    ConnT.ConnectionString = strconT
    ConnT.Open
    rs.Open "select * from t_pod where id='" & id & "' and kode_barang not in (select kode_barang from " & dbname1 & ".dbo.ms_barang)", ConnT
    If Not rs.EOF Then
        GoTo kodebarangkosong
    End If
    rs.Close
    rs.Open "select b.*,nama_supplier from " & _
    "  t_poh b left join " & dbname1 & ".dbo.ms_supplier c on c.kode_supplier=b.kode_supplier where b.id='" & id & "' and b.id not in (select tb_id from t_pembelianh where id<>'" & lblNoTrans & "') and b.gudang='" & gudang & "'", ConnT
    If Not rs.EOF Then
        lblKdSupplier = rs("kode_supplier")
'        txtDisc.text = rs("diskon")
'        txtDiscRp.text = rs("diskon_rp")
'        txtPPN.text = rs("ppn")
        lblNmSupplier = rs("nama_supplier")
        kode_gudang = rs("gudang")
    End If
    rs.Close
    rs.Open "select nama_gudang from " & dbname1 & ".dbo.ms_gudang where kode_gudang='" & gudang & "'", ConnT
    If Not rs.EOF Then
        lblGudang = rs(0)
    End If
    rs.Close
    subtotal = 0
    total = 0
    disc = 0
    ppn = 0
    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    rs.Open "select b.kode_barang,b.nama_barang,a.qty,a.satuan,0,0,0,0,a.konversi,0 from t_poh th inner join t_pod a on th.id=a.id inner join " & dbname1 & ".dbo.ms_barang b on a.kode_barang=b.kode_barang " & _
            " where a.id='" & id & "' and th.id not in (select tb_id from t_pembelianh where id<>'" & lblNoTrans & "') order by a.no_urut", ConnT
    While Not rs.EOF
        DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & Format(rs(4), "#,##0.##") & "#0#" & rs(8) & "#" & rs(9)
        subtotal = subtotal + rs(7)
        rs.MoveNext
    Wend
    rs.Close
    ConnT.Close
'    disc = subtotal * (100 - getDiscArray(txtDisc.text)) / 100
'    ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
'    total = subtotal - disc - txtDiscRp + ppn
    
'    lblSubTotal = Format(subtotal, "#,##0")
'    lblPPN = Format(ppn, "#,##0")
'    lblDisc = Format(disc, "#,##0")
'    lblTotal = Format(total, "#,##0")
    Exit Sub
kodebarangkosong:
    MsgBox "Ada barang yang belum tercantum di master barang, silahkan merevisi PO tersebut dahulu"
    txtNoTB.text = ""
End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
'    reset_form
    cmdSimpan.Enabled = True
    cmdApprove.Enabled = False
    cmdHapus.Enabled = False
    mnuHapus.Enabled = False
    mnuPosting.Enabled = False
    ConnT.ConnectionString = strconT
    ConnT.Open
    rs.Open "select * from t_pembelianh where id='" & lblNoTrans & "'", ConnT
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtNoTB.text = rs("tb_id")
        txtNoInvoice.text = rs("invoice_no")
        txtKeterangan.text = rs("keterangan")
        txtDisc.text = rs("diskon")
        txtDiscRp.text = rs("diskon_rp")
        txtPPN.text = rs("ppn")
                
        If rs("post_fg") = "0" Then
            cmdApprove.Enabled = True
            mnuPosting.Enabled = True
            cmdHapus.Enabled = True
            mnuHapus.Enabled = True
        Else
            cmdSimpan.Enabled = False
            cmdApprove.Enabled = False
            mnuPosting.Enabled = False
            cmdHapus.Enabled = True
            mnuHapus.Enabled = True
        End If
        rs.Close
        rs.Open "select b.kode_supplier,nama_supplier from " & _
                " t_poh b inner join " & dbname1 & ".dbo.ms_supplier c on c.kode_supplier=b.kode_supplier where b.id='" & txtNoTB.text & "'  and b.gudang='" & gudang & "'", ConnT
        If Not rs.EOF Then
            lblKdSupplier = rs(0)
            lblNmSupplier = rs(1)
        End If
        rs.Close
        rs.Open "select nama_gudang from " & dbname1 & ".dbo.ms_gudang where kode_gudang='" & gudang & "'", ConnT
        If Not rs.EOF Then
            lblGudang = rs(0)
        End If
        rs.Close
        subtotal = 0
        disc = 0
        ppn = 0
        total = 0
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select d.kode_barang,m.nama_barang,qty,d.satuan,d.harga,d.diskon,d.diskon_rp,d.qty*((d.harga*d.diskon_persen/100)-d.diskon_rp),d.konversi,d.diskon_persen from t_pembeliand d inner join " & dbname1 & ".dbo.ms_barang m on d.kode_barang=m.kode_barang where d.id='" & lblNoTrans & "'", ConnT
        While Not rs.EOF
            DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & Format(rs(4), "#,##0") & "#" & rs(5) & "#" & Format(rs(6), "#,##0") & "#" & Format(rs(7), "#,##0") & "#" & rs(8) & "#" & rs(9)
            subtotal = subtotal + (rs(7))
            rs.MoveNext
        Wend
        rs.Close
        disc = subtotal * (100 - getDiscArray(txtDisc.text)) / 100
        ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
        total = subtotal - disc - txtDiscRp + ppn
        
        lblSubTotal = Format(subtotal, "#,##0")
        lblDisc = Format(disc, "#,##0")
        lblPPN = Format(ppn, "#,##0")
        lblTotal = Format(total, "#,##0")
        conn.ConnectionString = strcon
        conn.Open
        
        rs.Open "select access from user_priviledge where userid='" & user & "' and kode_priviledge='2'", conn
        If Not rs.EOF Then
            If rs(0) = "0" Then
                cmdApprove.Enabled = False
                mnuPosting.Enabled = False
            End If
        Else
            cmdApprove.Enabled = False
            mnuPosting.Enabled = False
        End If
        
        conn.Close
    End If
    If rs.State Then rs.Close
    ConnT.Close
End Sub


Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPosting_Click()
    cmdApprove_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub
Private Sub txtPPN_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If Not IsNumeric(txtPPN.text) Then txtPPN.text = 0
        ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
        total = subtotal - disc - txtDiscRp + ppn
        lblDisc = Format(disc, "#,##0")
        lblPPN = Format(ppn, "#,##0")
        lblTotal = Format(total, "#,##0")
    End If
End Sub

Private Sub txtPPN_LostFocus()
    If Not IsNumeric(txtPPN.text) Then txtPPN.text = 0
        ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
        total = subtotal - disc - txtDiscRp + ppn
        lblDisc = Format(disc, "#,##0")
        lblPPN = Format(ppn, "#,##0")
        lblTotal = Format(total, "#,##0")

End Sub
Private Sub txtDisc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        
        disc = subtotal * (100 - getDiscArray(txtDisc.text)) / 100
        ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
        total = subtotal - disc - txtDiscRp + ppn
        lblDisc = Format(disc, "#,##0")
        lblTotal = Format(total, "#,##0")
    End If
End Sub

Private Sub txtDisc_LostFocus()
    
    disc = subtotal * (100 - getDiscArray(txtDisc.text)) / 100
    ppn = (subtotal - disc - txtDiscRp) * txtPPN.text / 100
    total = subtotal - disc - txtDiscRp + ppn
    lblDisc = Format(disc, "#,##0")
    lblTotal = Format(total, "#,##0")

End Sub

Private Sub txtNoTB_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then cmdSearch_Click
End Sub

Private Sub txtNoTB_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cekOrder txtNoTB.text
    End If
End Sub

Private Sub txtNoTB_LostFocus()
    If lblNoTrans = "-" Then cekOrder txtNoTB.text
End Sub
