VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmSearch 
   ClientHeight    =   5835
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   9885
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5835
   ScaleWidth      =   9885
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdExport 
      Caption         =   "Export"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8865
      TabIndex        =   11
      Top             =   90
      Width           =   960
   End
   Begin VB.TextBox txtTop 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   5355
      TabIndex        =   10
      Text            =   "0"
      Top             =   540
      Visible         =   0   'False
      Width           =   690
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   2265
      Left            =   90
      TabIndex        =   9
      Top             =   990
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   3995
      _Version        =   393216
      AllowUpdate     =   0   'False
      BorderStyle     =   0
      HeadLines       =   1
      RowHeight       =   24
      RowDividerStyle =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Timer Timer1 
      Left            =   6570
      Top             =   495
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "&Refresh"
      Height          =   330
      Left            =   4140
      TabIndex        =   8
      Top             =   540
      Width           =   1095
   End
   Begin VB.CheckBox chkDate 
      Height          =   330
      Left            =   5760
      TabIndex        =   7
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   6075
      TabIndex        =   6
      Top             =   90
      Visible         =   0   'False
      Width           =   1680
      _ExtentX        =   2963
      _ExtentY        =   661
      _Version        =   393216
      Format          =   115605505
      CurrentDate     =   39648
   End
   Begin VB.ComboBox cmbSort 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmSearch.frx":0000
      Left            =   3015
      List            =   "frmSearch.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   540
      Width           =   1050
   End
   Begin VB.ComboBox cmbSortby 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1215
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   540
      Width           =   1725
   End
   Begin VB.ComboBox cmbKey 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3780
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   135
      Width           =   1725
   End
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   0
      Top             =   135
      Width           =   2445
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   7155
      Top             =   585
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Urutkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      TabIndex        =   5
      Top             =   540
      Width           =   1095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kata Cari"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      TabIndex        =   4
      Top             =   180
      Width           =   1095
   End
   Begin VB.Menu mnuUtama 
      Caption         =   "Data"
      Begin VB.Menu mnuData 
         Caption         =   "Next"
         Index           =   0
      End
      Begin VB.Menu mnuData 
         Caption         =   "Prev"
         Index           =   1
      End
      Begin VB.Menu mnuData 
         Caption         =   "Ok"
         Index           =   2
      End
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public query As String
Public nmform As String
Public nmctrl As String
Public connstr As String
Public frm As Form
Public group As String
Public Index As Double
Public col As Byte
Public proc As String
Public mode As Byte
Dim ul As Byte
Dim search As String
Public keyIni As String

Private Sub chkDate_Click()
    If chkDate.value Then
    DTPicker1.Enabled = True
    Else
    DTPicker1.Enabled = False
    End If
End Sub

Private Sub cmbKey_Click()
'    requery
End Sub

'Private Sub requery()
'    If cmbKey.text <> "" And cmbSort.text <> "" And cmbSortby.text <> "" Then
'    If InStr(query, "where") > 0 Then
'        Adodc1.RecordSource = query & " and [" & cmbKey.text & "] like '" & txtSearch.text & "' " & group & " order by [" & cmbSortby.text & "] " & cmbSort.text
'    Else
'    Adodc1.RecordSource = query & " where [" & cmbKey.text & "] like '" & txtSearch.text & "' " & group & " order by [" & cmbSortby.text & "] " & cmbSort.text
'    End If
'    Set DBGrid.DataSource = Adodc1
'    Adodc1.Refresh
'    DBGrid.Refresh
'    For i = 0 To Adodc1.Recordset.fields.Count - 1
'    If Adodc1.Recordset(i).Type = adNumeric Or Adodc1.Recordset(i).Type = 5 Then
'        DBGrid.Columns(i).Alignment = dbgRight
'        DBGrid.Columns(i).NumberFormat = "#,##0.00"
'    End If
'    If Adodc1.Recordset(i).DefinedSize > 50 Then
'        panjang = 50
'    Else
'        panjang = Adodc1.Recordset(i).DefinedSize
'    End If
'    If Adodc1.Recordset(i).Type = 200 Then
'    DBGrid.Columns(i).Width = (((panjang * 200) / 3)) + 600
'    ElseIf Adodc1.Recordset(i).Type = 131 Then
'    DBGrid.Columns(i).Width = (((panjang * 200) / 4)) + 600
'    ElseIf Adodc1.Recordset(i).Type = 4 Then
'    DBGrid.Columns(i).Width = (((panjang * 200) / 4)) + 600
'    End If
'    Next
'
'    End If
'End Sub
Public Sub requery()
On Error GoTo err
Dim top As String
Dim key As String
Dim keytanggal As String
    If CLng(txtTop) > 0 Then
    top = " top " & txtTop.text & " "
    Else
    top = ""
    End If
    keytanggal = ""
    If chkDate.value Then
        keytanggal = "format(tanggal,'yyyy/MM/dd')='" & Format(DTPicker1, "yyyy/MM/dd") & "'"
    End If
    If cmbSort.text <> "" And cmbSortby.text <> "" Then
        Sort = " order by [" & cmbSortby.text & "] " & cmbSort.text
        
        If txtSearch.text <> "" Then
            If cmbKey.text = "" Then
            key = ""
            For i = 1 To cmbKey.ListCount - 1
                If ServerType = "mssql" Then
                key = key & "+cast([" & cmbKey.List(i) & "] as nvarchar)"
                ElseIf ServerType = "access" Then
                key = key & "+cstr(iif(isnull([" & cmbKey.List(i) & "]),'',[" & cmbKey.List(i) & "]))"
                End If
            Next
            key = Right(key, Len(key) - 1)
            Else
            key = "[" & cmbKey.text & "]"
            End If
            If InStr(query, "where") > 0 Then
                Adodc1.RecordSource = Left(query, 6) & " " & top & Right(query, Len(query) - 7) & " and " & key & " like '%" & txtSearch.text & "%' " & IIf(keytanggal = "", "", " and " & keytanggal) & Sort
            Else
                Adodc1.RecordSource = Left(query, 6) & " " & top & Right(query, Len(query) - 7) & " where " & key & " like '%" & txtSearch.text & "%' " & IIf(keytanggal = "", "", " and " & keytanggal) & Sort
            End If
        Else
            Adodc1.RecordSource = Left(query, 6) & " " & top & Right(query, Len(query) - 7) & IIf(keytanggal = "", "", IIf(InStr(query, "where") > 0, " and ", " where ") & keytanggal) & Sort
        End If
        
        Adodc1.Refresh
        Set DBGrid.DataSource = Adodc1
        DBGrid.Refresh
    Else
    If cmbKey.text = "" And cmbSort.text <> "" And cmbSortby.text <> "" Then
        If InStr(query, "where") > 0 Then
            Adodc1.RecordSource = Left(query, 6) & " " & top & Right(query, Len(query) - 7) & "  " & IIf(keytanggal = "", "", " where " & keytanggal) & Sort
        Else
        Adodc1.RecordSource = Left(query, 6) & " " & top & Right(query, Len(query) - 7) & "  " & IIf(keytanggal = "", "", IIf(InStr(query, "where") > 0, " and ", " where ") & keytanggal) & Sort
        End If
        
'        Dim key As String
'
'        For i = 1 To cmbKey.ListCount - 1
'            key = key & "[" & cmbKey.List(i) & "]+"
'        Next
'        key = Left(key, Len(key) - 1)
'        If InStr(query, "where") > 0 Then
'            Adodc1.RecordSource = query & " and " & key & " like '" & txtSearch.Text & "' order by [" & cmbSortby.Text & "] " & cmbSort.Text
'        Else
'        Adodc1.RecordSource = query & " where " & key & " like '" & txtSearch.Text & "' order by [" & cmbSortby.Text & "] " & cmbSort.Text
'        End If
    End If
        Adodc1.Refresh
        Set DBGrid.DataSource = Adodc1
        DBGrid.Refresh
    
    End If
    ' menghitung lebar kolom
    If Not Adodc1.Recordset.EOF Then
    For i = 0 To Adodc1.Recordset.fields.Count - 1
      
      If keyIni <> "" Then
           If sGetINI(App.Path & "\size.ini", keyIni, Adodc1.Recordset(i).Name, 0) > 0 Then
                panjang = Format(sGetINI(App.Path & "\size.ini", keyIni, Adodc1.Recordset(i).Name, 0), "###0")
    
            Else ' di file ini belum ada ukurannya
                panjang = Adodc1.Recordset(i).DefinedSize
                writeINI App.Path & "\size.ini", keyIni, Adodc1.Recordset(i).Name, Adodc1.Recordset(i).DefinedSize
            End If
       Else
        panjang = Adodc1.Recordset(i).DefinedSize
       End If
        
        
        If keyhide And i = 0 Then
        DBGrid.Columns(i).Width = 0
        Else
        DBGrid.Columns(i).Width = Format((panjang) * 170, "###0")
        End If
     Next
    End If
    mode = 1
    'DBGrid.AllowRowSizing = False
    Exit Sub
err: MsgBox err.Description
End Sub

Private Sub cmbSort_Click()
'    requery
End Sub

Private Sub cmbSortby_Click()
    'requery
End Sub


Private Sub cmdExport_Click()
    saveexcelfile ""
End Sub

Private Sub cmdPrint_Click()
    printer.Print Me.DBGrid
End Sub

Private Sub cmdSearch_Click()
    requery
End Sub

Private Sub DBGrid_DblClick()
On Error GoTo err
    If nmctrl <> "" Then
    If Not Adodc1.Recordset.EOF Then
    If Index > -1 Then
        frm.Controls(nmctrl)(Index) = DBGrid.Columns(col).text
        If proc <> "" Then CallByName frm, proc, VbMethod
    Else
        frm.Controls(nmctrl) = DBGrid.Columns(col).text
        If proc <> "" Then CallByName frm, proc, VbMethod
    End If
    ul = 1
    Unload Me
    focus
    Else
        MsgBox "Data yang anda cari tidak ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub DBGrid_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
If keyIni <> "" Then writeINI App.Path & "\size.ini", keyIni, Adodc1.Recordset(ColIndex).Name, (DBGrid.Columns(ColIndex).Width) / 170

End Sub
Private Sub focus()
On Error Resume Next
    If Index > -1 Then
        frm.Controls(nmctrl)(Index).SetFocus
    Else
        frm.Controls(nmctrl).SetFocus
    End If
End Sub
Private Sub DBGrid_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        DBGrid_DblClick
        
    End If
End Sub

Private Sub Form_Activate()
'
'    requery
'
End Sub

Private Sub Form_Deactivate()
    group = ""
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    'If KeyCode = 13 Then DBGrid_DblClick
    If KeyCode = vbKeyUp Then
        If Not Adodc1.Recordset.BOF Then Adodc1.Recordset.MovePrevious
    End If
    If KeyCode = vbKeyDown Then
        If Not Adodc1.Recordset.EOF Then Adodc1.Recordset.MoveNext
    End If
End Sub

Private Sub Form_Load()
    search = ""
    mode = 0
    Me.Move 0, 0
    Adodc1.ConnectionString = connstr
    'Adodc1.DatabaseName = servname
    'loadgrid (query)
    focus_search
    ul = 0
    DTPicker1.Enabled = False
    DTPicker1 = Now
'    If right_export = True Then cmdExport.Visible = True
End Sub
Private Sub saveexcelfile(filename As String)
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend
For i = 0 To Adodc1.Recordset.fields.Count - 1
exws.range(Chr(i + 65) & "1") = Adodc1.Recordset(i).Name
Next
    
i = 2
Dim harga1 As Long
Dim harga2 As Double
conn.Open strcon
    
    Adodc1.Recordset.MoveFirst
    While Not Adodc1.Recordset.EOF
        For X = 0 To Adodc1.Recordset.fields.Count - 1
        exws.range(Chr(X + 65) & CStr(i)) = Adodc1.Recordset(X).value
        Next
        Adodc1.Recordset.MoveNext
        i = i + 1
    Wend
    
err:
'exwb.SaveAs filename
exapp.Application.Visible = True
'exwb.Close
'exapp.Application.Quit
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub
Private Sub focus_search()
On Error Resume Next
    txtSearch.SetFocus
    txtSearch.SelStart = 0
End Sub
Public Sub loadgrid(query As String)
On Error GoTo err
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
Dim top As String
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
cmbKey.Clear
cmbSortby.Clear
cmbKey.AddItem ""
top = " top 1 "
    
    Adodc1.RecordSource = ""
    row = 1
    Adodc1.RecordSource = query
requery
'    Adodc1.RecordSource = Left(query, 6) & top & Right(query, Len(query) - 7)
'    Adodc1.Refresh
'    Set DBGrid.DataSource = Adodc1
'    DBGrid.Refresh

    If Not Adodc1.Recordset.EOF Then
        For i = 0 To Adodc1.Recordset.fields.Count - 1
        cmbKey.AddItem Adodc1.Recordset(i).Name
        cmbSortby.AddItem Adodc1.Recordset(i).Name
        Next
        On Error Resume Next
        cmbKey.ListIndex = 0
        cmbSortby.ListIndex = 0
        cmbSort.ListIndex = 0
    End If
    
    Exit Sub
err: MsgBox err.Description
'Resume Next
End Sub


Private Sub Form_Resize()
    If Me.Width > 200 And Me.Height > 1800 Then
    DBGrid.Width = Me.Width - 200
    DBGrid.Height = Me.Height - 1800
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    group = ""
End Sub

Private Sub txtSearch_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        If search = txtSearch.text Then
            DBGrid_DblClick
        Else
            If mode = 1 Then requery
            search = txtSearch.text
        End If
    End If
    
End Sub

Private Sub txtSearch_KeyUp(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyDown Then
'        If Not Adodc1.Recordset.EOF Then Adodc1.Recordset.MoveNext
'    ElseIf KeyCode = vbKeyUp Then
'        If Not Adodc1.Recordset.BOF Then Adodc1.Recordset.MovePrevious
'
'    End If
End Sub

Private Sub txtTop_GotFocus()
    txtTop.SelStart = 0
    txtTop.SelLength = Len(txtTop.text)
End Sub

Private Sub txtTop_LostFocus()
    If Not IsNumeric(txtTop.text) Then
        txtTop.text = "1000"
    End If
End Sub
