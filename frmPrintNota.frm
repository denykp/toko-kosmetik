VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmPrintNota 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2250
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2250
   ScaleWidth      =   7470
   Begin VB.CommandButton Command4 
      Caption         =   "Command4"
      Height          =   465
      Left            =   3150
      TabIndex        =   7
      Top             =   900
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   465
      Left            =   4410
      TabIndex        =   6
      Top             =   1035
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   465
      Left            =   1665
      TabIndex        =   5
      Top             =   1125
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   465
      Left            =   180
      TabIndex        =   4
      Top             =   855
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CommandButton cmdScreen 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3233
      TabIndex        =   0
      Top             =   1665
      Width           =   1005
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   6480
      Top             =   225
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      Destination     =   1
      BoundReportFooter=   -1  'True
      PrinterCollation=   0
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label lblHemat 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   135
      TabIndex        =   3
      Top             =   810
      Width           =   7305
   End
   Begin VB.Label lblKembalian 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   2655
      TabIndex        =   2
      Top             =   90
      Width           =   3705
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kembali"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   270
      TabIndex        =   1
      Top             =   90
      Width           =   2310
   End
End
Attribute VB_Name = "frmPrintNota"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strfilename, strFormula, param1, param2 As String
Private Type DOCINFO
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal _
   hPrinter As Long) As Long
Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal _
   hPrinter As Long) As Long
Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal _
   hPrinter As Long) As Long
Private Declare Function OpenPrinter Lib "winspool.drv" Alias _
   "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, _
    ByVal pDefault As Long) As Long
Private Declare Function StartDocPrinter Lib "winspool.drv" Alias _
   "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, _
   pDocInfo As DOCINFO) As Long
Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal _
   hPrinter As Long) As Long
Private Declare Function WritePrinter Lib "winspool.drv" (ByVal _
   hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, _
   pcWritten As Long) As Long
Dim lhPrinter As Long



Private Sub Command2_Click()
    Dim lReturn As Long
    Dim lpcWritten As Long
    Dim sWrittenData As String
    sWrittenData = "How's that for Magic !!!!" & vbCrLf & "line 2" & vbCrLf
    lReturn = WritePrinter(lhPrinter, ByVal sWrittenData, _
       Len(sWrittenData), lpcWritten)
End Sub

Private Sub Command3_Click()
          Dim lReturn As Long
          lReturn = EndPagePrinter(lhPrinter)
          lReturn = EndDocPrinter(lhPrinter)
          lReturn = ClosePrinter(lhPrinter)

End Sub

Private Sub cmdScreen_Click()
On Error GoTo err
    If cmdScreen.Caption = "&Print" Then
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & strfilename
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(0).value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'
'        .SelectionFormula = strFormula
'        .PrinterSelect
'        .Destination = crptToPrinter
'        .Action = 1
'        If frmAddPenjualan.chkKirim.value = 1 Then .Action = 1
'
'    End With
'    Command1_Click
    cmdScreen.Caption = "&Close"
    ElseIf cmdScreen.Caption = "&Close" Then
        cmdScreen.Caption = "&Print"
        frmAddPenjualan.Enabled = True
        frmAddPenjualan.reset_form
        Unload Me
    End If
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub CmdPrinter_Click()
On Error GoTo err
    With CRPrint
        .reset
        .ReportFileName = App.Path & strfilename
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(0).Value & ";UID=" & user & ";PWD=" & pwd & ";DSQ=" & conn.Properties(0).Value
        Next
        '.Connect = Conn1
        
        .ParameterFields(0) = "login;" + login + ";True"
        .ParameterFields(1) = "TglDari;" + param1 + ";True"
        .ParameterFields(2) = "TglSampai;" + param2 + ";True"
        If strfilename = "\Report\Controller\Laporan Penjualan per dealer per Ranking.rpt" Then
        .ParameterFields(3) = "@tanggalDari;" + Format(frmLapTgl.DTDari, "MM/dd/yyyy") + ";True"
        .ParameterFields(4) = "@tanggalsd;" + Format(frmLapTgl.DTSampai, "MM/dd/yyyy") + ";True"
        End If
        
        .SelectionFormula = strFormula
        .Destination = crptToPrinter
        .WindowShowPrintBtn = False
        '.PrinterName = ""
        '.PrinterSelect
        Me.Hide
        'If .PrinterName <> "" Then
            .action = 1
        'End If
        If LCase(strfilename) = "\report\gudang\surat jalan.rpt" Then
            conn.Execute "update tr_pi_h set srtjln_fg='Y' where no_pi='" & param2 & "'"
            conn.Execute "update sj_h set fg='1' where no_sj='" & param2 & "'"
        ElseIf strfilename = "\Report\Sales\Faktur Pajak.rpt" Or strfilename = "\Report\Sales\Faktur Non Pajak.rpt" Then
            conn.Execute "update tr_pi_h set fktr_fg='Y' where no_pi in (" & nopi & ")"
        ElseIf strfilename = "\Report\Sales\PrintOut_PI.rpt" Then
            conn.Execute "insert into print_pi select no_pi,'1' from tr_pi_h where no_pi in " & nopi & " and no_pi not in (select no_pi from print_pi)"
        End If
    End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub


Private Sub Command4_Click()
Dim a As Printer
    
End Sub

'Private Sub Command1_Click()
'    Open "usb001" For Printer as 1
'    Print
'
'End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAddPenjualan.Enabled = True
    frmAddPenjualan.reset_form
End Sub

