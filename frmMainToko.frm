VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm frmMain 
   BackColor       =   &H8000000C&
   Caption         =   "Inventory"
   ClientHeight    =   7065
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   9975
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   6645
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   741
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Username"
            TextSave        =   "Username"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Gudang"
            TextSave        =   "Gudang"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   9596
            MinWidth        =   9596
            Text            =   "Server"
            TextSave        =   "Server"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4410
            MinWidth        =   4410
            Text            =   "Printer"
            TextSave        =   "Printer"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Master"
      Index           =   0
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Barang"
         Index           =   0
         Shortcut        =   ^M
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Supplier"
         Index           =   20
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Aktivasi Master Barang"
         Index           =   40
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Ubah Harga"
         Index           =   50
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Promosi"
         Index           =   60
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuMaster 
         Caption         =   "Master Biaya"
         Index           =   80
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Transaksi"
      Index           =   10
      Begin VB.Menu mnuTransaksi 
         Caption         =   "Pembelian"
         Index           =   30
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuTransaksi 
         Caption         =   "Retur Pembelian"
         Index           =   80
      End
      Begin VB.Menu mnuTransaksi 
         Caption         =   "Pembayaran Hutang"
         Index           =   90
      End
      Begin VB.Menu mnuTransaksi 
         Caption         =   "Pembayaran Biaya"
         Index           =   100
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Inventory"
      Index           =   20
      Begin VB.Menu mnuStock 
         Caption         =   "Koreksi Stock"
         Index           =   0
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Opname Stock"
         Index           =   10
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Pindah Gudang"
         Index           =   15
      End
      Begin VB.Menu mnuStock 
         Caption         =   "Info Stock"
         Index           =   20
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Report"
      Index           =   30
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Pembelian"
         Index           =   30
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Retur Pembelian"
         Index           =   40
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Stock"
         Index           =   50
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Stock Habis"
         Index           =   60
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Stock Opname"
         Index           =   70
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Kartu Stock"
         Index           =   80
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Koreksi Stock"
         Index           =   95
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Pindah Gudang"
         Index           =   110
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Print List Barang"
         Index           =   120
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Pembayaran Hutang"
         Index           =   125
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Biaya"
         Index           =   130
      End
      Begin VB.Menu mnuReport 
         Caption         =   "Laporan Laba Rugi"
         Index           =   140
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Variabel dan Maintenance"
      Index           =   40
      Begin VB.Menu mnuUtility 
         Caption         =   "Ganti Password"
         Index           =   0
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "User"
         Index           =   10
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Setting Modal Awal"
         Index           =   20
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Setting Printer Kasir"
         Index           =   25
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Gudang"
         Index           =   30
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Satuan"
         Index           =   40
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Kategori"
         Index           =   50
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Security Manager"
         Index           =   70
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Print Barcode"
         Enabled         =   0   'False
         Index           =   90
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Data Perusahaan"
         Index           =   110
      End
      Begin VB.Menu mnuUtility 
         Caption         =   "Ganti Gudang"
         Index           =   120
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Log Out"
      Index           =   50
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Activate()
    getConfig
    StatusBar1.Panels(1).text = "User = " & user
    StatusBar1.Panels(2).text = gudang
    StatusBar1.Panels(3).text = servname
    StatusBar1.Panels(4).text = printername
End Sub

Private Sub MDIForm_Load()

'On Error Resume Next
'    MsgBox "1a"
    getConfig
    
'    Me.BackColor = BGColor1
'    Me.Picture = LoadPicture(BG, 1, , 0, 0)
'
'    If right_alert Then frmJatuhTempo.Show


'    rs.Open "select * from (user_groupmenu g inner join user_group u on g.[group]=u.[group]) inner join menu1 m on m.key=g.key where username='" & user & "' order by urut", conn
'
'    While Not rs.EOF
'        Controls(rs("namamenu"))(rs("idx")).Visible = CBool(rs("value"))
'        rs.MoveNext
'    Wend
'    rs.Close
'
'    rs.Open "select * from var_gudang where property='1'", conn
'    If Not rs.EOF Then
'        gudang = rs(0)
'    Else
'        mnuMain(0).Enabled = False
'        mnuMain(1).Enabled = False
'        mnuMain(2).Enabled = False
'        MsgBox "gudang tidak ditemukan"
'    End If
'    rs.Close

    
End Sub

Private Sub mnuMain_Click(Index As Integer)
    If Index = 50 Then
    Unload Me
    frmLogin.Show
    End If
End Sub

Private Sub mnuMaster_Click(Index As Integer)
    Select Case Index
    Case 0:
        frmMasterBarang.Show
        SetFormPosition frmMasterBarang
    Case 10:
        frmMasterCustomer.Show
        SetFormPosition frmMasterCustomer
    Case 20:
        frmMasterSupplier.Show
        SetFormPosition frmMasterSupplier
    Case 40:
        frmAktivasiMsBarang.tipe = 1
        frmAktivasiMsBarang.Show vbModal
'        SetFormPosition frmAktivasiMsBarang
    Case 50:
        frmAktivasiMsBarang.tipe = 2
        frmAktivasiMsBarang.Show vbModal
    Case 60:
        frmMasterPromosi.Show
        SetFormPosition frmMasterPromosi
    Case 80:
        frmMasterBiaya.Show
        SetFormPosition frmMasterBiaya
    End Select
    
End Sub

Private Sub mnuReport_Click(Index As Integer)
    Select Case Index
    Case 0:
        frmLaporanA.filename = "\Report\Laporan Penjualan"
        'frmLaporanA.propertygudang = "1"
        frmLaporanA.Caption = "Laporan Penjualan"
        frmLaporanA.Show
        
        SetFormPosition frmLaporanA
    Case 10:
        frmLaporanA.filename = "\Report\Laporan Retur Penjualan"
        'frmLaporanA.propertygudang = "1"
        frmLaporanA.Caption = "Laporan Retur Penjualan"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
     Case 30:
        frmLaporanA.filename = "\Report\Laporan Pembelian"
        frmLaporanA.Caption = "Laporan Pembelian"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 40:
        frmLaporanA.filename = "\Report\Laporan Retur Pembelian"
        frmLaporanA.Caption = "Laporan Retur Pembelian"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
     Case 50: ' stock akhir
        frmLaporanA.filename = "\Report\Laporan Stock"
        frmLaporanA.Caption = "Laporan Stock"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
     Case 60: ' stock habis
        frmLaporanA.filename = "\Report\Laporan Stock"
        frmLaporanA.habis = True
        frmLaporanA.Caption = "Laporan Laporan Stock Habis"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
     Case 70:
        frmLaporanA.filename = "\Report\Laporan Stock Opname"
        frmLaporanA.Caption = "Laporan Stock Opname"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 80:
        frmLaporanA.filename = "\Report\Laporan Kartu Stock"
        frmLaporanA.Caption = "Laporan Kartu Stock"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
     Case 95: ' stock habis
        frmLaporanA.filename = "\Report\Laporan Koreksi Stock"
        frmLaporanA.Caption = "Laporan Koreksi Stock"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 110:
        frmLaporanA.filename = "\Report\Laporan Pindah Gudang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 120:
        frmPrintListBarang.Show
        'SetFormPosition frmPrintListBarang
    Case 125:
        frmLaporanA.filename = "\Report\Laporan Bayar Hutang"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 130:
        frmLaporanA.filename = "\Report\Laporan Pembayaran Biaya"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    Case 140:
        frmLaporanA.filename = "\Report\Laporan Laba Rugi"
        frmLaporanA.Show
        SetFormPosition frmLaporanA
    End Select
End Sub

Private Sub mnuStock_Click(Index As Integer)
Select Case Index
    Case 0:
        frmAddAdjustment.Show
        SetFormPosition frmAddAdjustment
                        
    Case 10:
        frmAddStockOpname.Show
        SetFormPosition frmAddStockOpname
    Case 15:
        frmAddTransferGudang.Show
        SetFormPosition frmAddTransferGudang
    Case 20:
        frmSearch.query = "select [Kode Barang],[Nama barang],Stock from Q_Stock where gudang='" & gudang & "' and flag='1'"
        frmSearch.nmctrl = ""
        frmSearch.keyIni = "vwStock"
        frmSearch.connstr = strcon
        frmSearch.proc = ""
        
        frmSearch.loadgrid frmSearch.query
        Set frmSearch.frm = Me
        frmSearch.Show vbModal

        
End Select
End Sub

Private Sub mnuTransaksi_Click(Index As Integer)
    Select Case Index
    
    Case 30:
            frmAddPembelian.Show
            SetFormPosition frmAddPembelian
    Case 80:
            frmAddReturBeli.Show
            SetFormPosition frmAddReturBeli
    Case 90:
            frmBayarHutang.Show
            SetFormPosition frmBayarHutang
    Case 100:
            frmTransBiaya.Show
            SetFormPosition frmTransBiaya
    End Select
End Sub

Private Sub mnuUtility_Click(Index As Integer)
    Select Case Index
    Case 0: frmGantiPassword.Show
        SetFormPosition frmGantiPassword
    Case 10: frmNewUser.Show
        SetFormPosition frmNewUser
    Case 20: frmSetting.Show
        SetFormPosition frmSetting
    Case 25: frmConfig.Show
        SetFormPosition frmConfig
    Case 30: frmMasterGudang.Show
        SetFormPosition frmMasterGudang
    Case 40: frmMasterSatuan.Show
        SetFormPosition frmMasterSatuan
    Case 50: frmMasterKategori.Show
        SetFormPosition frmMasterKategori
'    Case 60: frmConfig.Show
'        SetFormPosition frmConfig
    Case 70: frmMasterUserGroup.Show
        SetFormPosition frmMasterUserGroup

    Case 110:
        frmDataPerusahaan.Show
        SetFormPosition frmDataPerusahaan
    Case 120:
        frmGantiGudang.Show
        SetFormPosition frmGantiGudang
    End Select
End Sub

