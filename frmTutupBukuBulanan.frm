VERSION 5.00
Begin VB.Form frmTutupBukuBulanan 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tutup Buku Bulanan"
   ClientHeight    =   2730
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2730
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbBulan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmTutupBukuBulanan.frx":0000
      Left            =   2160
      List            =   "frmTutupBukuBulanan.frx":0028
      TabIndex        =   4
      Top             =   510
      Width           =   1050
   End
   Begin VB.ComboBox cmbTahun 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2160
      TabIndex        =   3
      Top             =   945
      Width           =   1050
   End
   Begin VB.CommandButton cmdProses 
      Caption         =   "Proses"
      Height          =   480
      Left            =   1365
      TabIndex        =   2
      Top             =   1740
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Tahun"
      Height          =   255
      Left            =   1335
      TabIndex        =   1
      Top             =   1005
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Bulan"
      Height          =   255
      Left            =   1335
      TabIndex        =   0
      Top             =   555
      Width           =   975
   End
End
Attribute VB_Name = "frmTutupBukuBulanan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdProses_Click()
Dim sBulanPrev As String, sTahunPrev As String
Dim SaldoAwal As Long, BulanAwal As String, TahunAwal As String

    If cmbBulan.text = "" Or cmbTahun.text = "" Then
        MsgBox "Bulan dan Tahun harus diisi !", vbExclamation
        Exit Sub
    End If
    
    If Val(cmbBulan.text) <= 0 Or Val(cmbBulan.text) > 12 Then
        MsgBox "Bulan tidak valid !", vbExclamation
        Exit Sub
    End If
    
    cmbBulan.text = Val(cmbBulan.text)
    cmbTahun.text = Val(cmbTahun.text)

    If Val(cmbBulan.text) = 1 Then
        sBulanPrev = "12"
        sTahunPrev = Val(cmbTahun.text) - 1
    Else
        sBulanPrev = Val(cmbBulan.text) - 1
        sTahunPrev = cmbTahun.text
    End If

    conn.ConnectionString = strcon
    
    conn.Open
    
    rs.Open "select * from mutasi_bulanan ", conn
    If rs.EOF Then
        conn.BeginTrans
        conn.Execute "insert into mutasi_bulanan (bulan,tahun,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname,saldoakhir) SELECT '" & cmbBulan.text & "','" & cmbTahun.text & "', i.[kode barang], i.stockawal AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname, " & _
                 "(i.stockawal+IIf(IsNull(b.beli),0,b.beli)+IIf(IsNull(rb.returbeli),0,-rb.returbeli)+IIf(IsNull(j.jual),0,-j.jual)+IIf(IsNull(rj.returjual),0,rj.returjual)+iif(isnull(op.opname),0,op.opname)) AS akhir " & _
                 "FROM ((((ms_barang AS i  " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
                 "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
                 "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG] "
        
        
        rs.Close
        conn.CommitTrans
        conn.Close
        MsgBox "Proses Selesai !", vbInformation
        Exit Sub
    End If
    rs.Close
    
    
        
    If cmbBulan.text <> gBulanAwal Or cmbTahun.text <> gTahunAwal Then
        rs.Open "select * from mutasi_bulanan where bulan='" & sBulanPrev & "' and tahun='" & sTahunPrev & "' ", conn
        If rs.EOF Then
            MsgBox "Bulan sebelumnya belum ditutup !", vbExclamation
            rs.Close
            conn.Close
            Exit Sub
        End If
        rs.Close
    End If
    
    rs.Open "select * from mutasi_bulanan where bulan='" & cmbBulan.text & "' and tahun='" & cmbTahun.text & "' ", conn
    If Not rs.EOF Then
        If MsgBox("Bulan ini sudah ditutup, apakah mau diproses ulang?", vbYesNo + vbQuestion) = vbNo Then
            rs.Close
            conn.Close
            Exit Sub
        Else
            conn.BeginTrans
            conn.Execute "Delete from mutasi_bulanan where bulan='" & cmbBulan.text & "' and tahun='" & cmbTahun.text & "' "
        End If
    End If
    rs.Close
    
    conn.Execute "insert into mutasi_bulanan (bulan,tahun,kodebarang,saldoawal,beli,returbeli,jual,returjual,opname,saldoakhir) SELECT '" & cmbBulan.text & "','" & cmbTahun.text & "', i.[kode barang], IIf(IsNull(mb.saldoakhir),0,mb.saldoakhir) AS awal, IIf(IsNull(b.beli),0,b.beli) AS beli,IIf(IsNull(rb.returbeli),0,-rb.returbeli) AS returbeli, IIf(IsNull(j.jual),0,-j.jual) AS jual,  IIf(IsNull(rj.returjual),0,rj.returjual) AS returjual, iif(isnull(op.opname),0,op.opname) AS opname, " & _
                 "(IIf(IsNull(mb.saldoakhir),0,mb.saldoakhir)+IIf(IsNull(b.beli),0,b.beli)+IIf(IsNull(rb.returbeli),0,-rb.returbeli)+IIf(IsNull(j.jual),0,-j.jual)+IIf(IsNull(rj.returjual),0,rj.returjual)+iif(isnull(op.opname),0,op.opname)) AS akhir " & _
                 "FROM (((((ms_barang AS i  " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as beli  from t_pembeliand d inner join  t_pembelianh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS b ON i.[kode barang] = b.[KODE BARANG]) " & _
                 "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as jual   from t_penjualand d inner join t_penjualanh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS j ON i.[kode barang] = j.[KODE BARANG]) " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.qty) as returbeli  from t_returbelid d inner join  t_returbelih h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS rb ON i.[kode barang] = rb.[KODE BARANG]) " & _
                 "LEFT JOIN (select d.[KODE BARANG],sum(d.qty) as  returjual  from t_returjuald d inner join t_returjualh h on h.id=d.id where  h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS rj ON i.[kode barang] = rj.[KODE BARANG]) " & _
                 "LEFT JOIN (select  d.[KODE BARANG],sum(d.selisih) as opname  from t_stockopnamed d inner join  t_stockopnameh h on h.id=d.id where h.gudang like '%" & gudang & "%' and format(h.tanggal,'M')='" & cmbBulan.text & "' and  format(h.tanggal,'yyyy')='" & cmbTahun.text & "' group by d.[KODE BARANG]) AS op ON i.[kode barang] = op.[KODE BARANG]) " & _
                 "LEFT JOIN (select  kodebarang,saldoakhir  from mutasi_bulanan where bulan = '" & sBulanPrev & "%' and tahun='" & sTahunPrev & "') AS mb ON i.[kode barang] = mb.kodebarang"
    
    conn.CommitTrans
    conn.Close
    MsgBox "Proses Selesai !", vbInformation
End Sub

Private Sub Form_Load()
Dim i As Integer

    
    For i = Year(Now) To "2010" Step -1
        cmbTahun.AddItem i
    Next

End Sub
