Attribute VB_Name = "ModuleIniFile"
Public lpSectionName1 As String
Public lpKeyName1 As String
Public lpValue1 As String
Public lpSectionName2 As String
Public lpKeyName2 As String
Public lpValue2 As String
Public lpSectionName3 As String
Public lpKeyName3 As String
Public lpValue3 As String
Public lpSectionName4 As String
Public lpKeyName4 As String
Public lpValue4 As String
Public lpSectionName5 As String
Public lpKeyName5 As String
Public lpValue5 As String
Public lpFileName As String
Public lpReturnedString1 As String
Public lpReturnedString2 As String
Public lpReturnedString3 As String
Public lpReturnedString4 As String
Public lpReturnedString5 As String
Public nSize1 As Long
Public nSize2 As Long
Public nSize3 As Long
Public nSize4 As Long
Public nSize5 As Long
Public fso As New FileSystemObject
Public LabelKu As String


'API DECLARATIONS
Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpString As Any, _
ByVal lpFileName As String) As Long



   



Public Function sGetINI(sINIFile As String, sSection As String, sKey _
As String, sDefault As String) As String
Dim sTemp As String * 256
Dim nLength As Integer
sTemp = Space$(256)
nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, _
255, sINIFile)
sGetINI = Left$(sTemp, nLength)
End Function

Public Sub writeINI(sINIFile As String, sSection As String, sKey _
As String, sValue As String)
Dim n As Integer
Dim sTemp As String
sTemp = sValue
'Replace any CR/LF characters with spaces
For n = 1 To Len(sValue)
If Mid$(sValue, n, 1) = vbCr Or Mid$(sValue, n, 1) = vbLf _
Then Mid$(sValue, n) = " "
Next n
n = WritePrivateProfileString(sSection, sKey, sTemp, sINIFile)
End Sub


   
Public Sub ProfileSaveItem(lpSectionName As String, _
                           lpKeyName As String, _
                           lpValue As String, _
                           lpFileName As String)
                           
   Call WritePrivateProfileString(lpSectionName, _
                                  lpKeyName, _
                                  lpValue, _
                                  lpFileName)

End Sub




Public Function sizeFlxGrid(query As String, ByRef flxGrid As MSFlexGrid, keyIni As String, Index As Integer) As Long
    conn.Open strcon
    rs.Open query, conn
    

        For i = 0 To rs.fields.Count - 1
        If keyIni <> "" Then
           If sGetINI(App.Path & "\size.ini", keyIni, rs(i).Name, 0) > 0 Then
                flxGrid.ColWidth(Index) = Format((sGetINI(App.Path & "\size.ini", keyIni, rs(i).Name, 0)) * 170, "###0")
           Else
                flxGrid.ColWidth(Index) = rs(i).DefinedSize * 170
                writeINI App.Path & "\size.ini", keyIni, rs(i).Name, Format(flxGrid.ColWidth(Index), "###0")
           End If
            Index = Index + 1
        End If
        Next

    rs.Close
    conn.Close
End Function
Public Function resizeFlxGrid(query As String, ByRef flxGrid As MSFlexGrid, keyIni As String, Index As Integer) As Long
If conn.State Then conn.Close
    conn.Open strcon
    rs.Open query, conn
    
    
        For i = 0 To rs.fields.Count - 1
             writeINI App.Path & "\size.ini", keyIni, rs(i).Name, Format(flxGrid.ColWidth(Index) / 170, "###0")
             Index = Index + 1
        Next
    
    rs.Close
    conn.Close
End Function
