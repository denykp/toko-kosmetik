VERSION 5.00
Begin VB.Form frmGantiGudang 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   1125
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3330
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1125
   ScaleWidth      =   3330
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdLogin 
      Caption         =   "&Ganti"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   855
      TabIndex        =   2
      Top             =   630
      Width           =   1500
   End
   Begin VB.ComboBox cmbGudang 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1485
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   180
      Width           =   1410
   End
   Begin VB.Label Label3 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   1
      Top             =   225
      Width           =   1005
   End
End
Attribute VB_Name = "frmGantiGudang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdLogin_Click()
    gudang = cmbGudang.text
    frmMain.StatusBar1.Panels(2).text = gudang
    Unload Me
End Sub

Private Sub Form_Load()
On Error Resume Next
    conn.Open
    cmbGudang.Clear
    rs.Open "select * from var_gudang order by gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    DropConnection
    SetComboText gudang, cmbGudang
End Sub
