VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmDetilJual 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1245
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   2505
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1245
   ScaleWidth      =   2505
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtHarga 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   540
      Width           =   1290
   End
   Begin VB.ComboBox txtHarga1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   990
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   2385
      Width           =   1230
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   600
      Left            =   195
      TabIndex        =   9
      Top             =   1410
      Visible         =   0   'False
      Width           =   645
      Begin VB.OptionButton opt1 
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   4
         Top             =   30
         Width           =   600
      End
      Begin VB.OptionButton opt1 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   0
         TabIndex        =   5
         Top             =   270
         Value           =   -1  'True
         Width           =   510
      End
   End
   Begin VB.TextBox txtDisc 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   990
      TabIndex        =   3
      Top             =   1440
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.TextBox txtQty 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   1410
      TabIndex        =   0
      Top             =   165
      Width           =   870
   End
   Begin VB.ComboBox cmbHarga 
      Height          =   315
      ItemData        =   "frmDetilJual.frx":0000
      Left            =   1020
      List            =   "frmDetilJual.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   2010
      Width           =   795
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   5175
      Top             =   90
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   10
      Top             =   585
      Width           =   780
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   105
      TabIndex        =   8
      Top             =   210
      Width           =   600
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Harga"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   7
      Top             =   2040
      Width           =   600
   End
End
Attribute VB_Name = "frmDetilJual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public disc, disc1, disc2, disc3, disc4, disc5 As Double
Public disc_tipe, disc1_tipe, disc2_tipe, disc3_tipe, disc4_tipe, disc5_tipe As Byte
Public parent As Form
Private Sub cmbHarga_Click()
    Select Case cmbHarga.text
    Case 1: disc = disc1
            disc_tipe = disc1_tipe
    Case 2: disc = disc2
            disc_tipe = disc2_tipe
    Case 3: disc = disc3
            disc_tipe = disc3_tipe
    Case 4: disc = disc4
            disc_tipe = disc4_tipe
    Case 5: disc = disc5
            disc_tipe = disc5_tipe
    End Select
    If disc_tipe = "1" Then txtHarga.text = harga_jual - disc Else txtHarga.text = harga_jual - (harga_jual * disc / 100)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        MySendKeys "{tab}"
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Load()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)
    
    cmbHarga.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    tutupform
End Sub

Private Sub txtDisc_GotFocus()
    txtDisc.SelStart = 0
    txtDisc.SelLength = Len(txtDisc.text)
End Sub

Private Sub txtDisc_Keydown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        Unload Me
    End If
End Sub
Private Sub tutupform()
    With parent
        .Enabled = True
        If .flxGrid.TextMatrix(.flxGrid.row, 1) <> "" Then
        If Not IsNumeric(txtDisc.text) Then txtDisc.text = "0"
        .qty = .qty - (.flxGrid.TextMatrix(.flxGrid.row, 3))
        
        .total = .total - (.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5))
        .flxGrid.TextMatrix(.flxGrid.row, 3) = txtQty.text
        If .flxGrid.TextMatrix(.flxGrid.row, 3) >= .flxGrid.TextMatrix(.flxGrid.row, 8) Then
            .flxGrid.TextMatrix(.flxGrid.row, 5) = .flxGrid.TextMatrix(.flxGrid.row, 10)
        Else
            .flxGrid.TextMatrix(.flxGrid.row, 5) = .flxGrid.TextMatrix(.flxGrid.row, 9)
        End If
        
'        If txtDisc.text >= 0 Then
'        .flxGrid.TextMatrix(.flxGrid.row, 5) = Format(.flxGrid.TextMatrix(.flxGrid.row, 4) - (IIf(opt1(0).value, txtDisc.text, (.flxGrid.TextMatrix(.flxGrid.row, 4) * txtDisc.text / 100))), "#,##0")
'        Else
'        .flxGrid.TextMatrix(.flxGrid.row, 5) = Format(txtHarga.text, "#,##0")
'        End If
        .flxGrid.TextMatrix(.flxGrid.row, 6) = Format(.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5), "#,##0")
        .flxGrid.TextMatrix(.flxGrid.row, 12) = txtDisc.text
        .flxGrid.TextMatrix(.flxGrid.row, 13) = IIf(Opt1(0).value = True, 1, 2)
        .qty = .qty + (.flxGrid.TextMatrix(.flxGrid.row, 3))
        .total = .total + (.flxGrid.TextMatrix(.flxGrid.row, 3) * .flxGrid.TextMatrix(.flxGrid.row, 5))
        .lblQty = Format(.qty, "#,##0")
        .lblTotal = Format(.total, "#,##0")
        .lblSisa = Format(.total - .lblDisc, "#,##0")
        .lblSubTotal = .lblSisa
'        If txtHarga.Enabled = True Then .flxgrid.TextMatrix(.flxgrid.row, 8) = txtHarga
        .LblNamaBarang = .flxGrid.TextMatrix(.flxGrid.row, 2)
        .lblQuantity = .flxGrid.TextMatrix(.flxGrid.row, 3) & " x " & Format(.flxGrid.TextMatrix(.flxGrid.row, 5), "#,##0") & "  = Rp. " & Format(.flxGrid.TextMatrix(.flxGrid.row, 6), "#,##0")
        .lblTotal = Format(.total, "#,##0")
        .lblSisa = Format(.total - .lblDisc, "#,##0")
        .lblSubTotal = .lblSisa
        End If
        DoEvents
        
    End With
        
End Sub

Private Sub txtDisc_KeyPress(KeyAscii As Integer)
NumberOnly KeyAscii
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga)
End Sub

Private Sub txtHarga_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And txtDisc.Visible = False Then
        Unload Me
    End If
End Sub

Private Sub txtHarga_KeyPress(KeyAscii As Integer)
NumberOnly KeyAscii
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty)

End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Or txtQty.text = "0" Then
        txtQty.text = 1
        txtQty.SetFocus
    End If
End Sub
