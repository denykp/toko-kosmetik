VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmDelete 
   Caption         =   "Transaksi"
   ClientHeight    =   8055
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12285
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   12285
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete 1 Bulan"
      Height          =   375
      Left            =   10035
      TabIndex        =   6
      Top             =   135
      Width           =   1230
   End
   Begin VB.CommandButton cmdrefresh 
      Caption         =   "&Refresh"
      Height          =   375
      Left            =   5280
      TabIndex        =   4
      Top             =   120
      Width           =   1095
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   120
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      CheckBox        =   -1  'True
      CustomFormat    =   "dd/MM/YYYYY"
      Format          =   88014849
      CurrentDate     =   40066
   End
   Begin VB.ComboBox cmb_transaksi 
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      Top             =   120
      Width           =   1935
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid flxgrid 
      Height          =   6735
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   12045
      _ExtentX        =   21246
      _ExtentY        =   11880
      _Version        =   393216
      FixedCols       =   0
      FocusRect       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   330
      Left            =   6435
      Top             =   90
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   582
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   1
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   8010
      TabIndex        =   5
      Top             =   135
      Width           =   1800
      _ExtentX        =   3175
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "MMMM - yyyy"
      Format          =   87883779
      CurrentDate     =   40066
   End
   Begin VB.Label Label1 
      Caption         =   "Transaksi"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmDelete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub refresh_data()
On Error Resume Next
If cmb_transaksi = "Penjualan" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy') AS Tanggal,[KODE CUSTOMER] from t_penjualanh"
ElseIf cmb_transaksi = "Retur Penjualan" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy') AS Tanggal,no_nota from t_returjualh"
ElseIf cmb_transaksi = "Pembelian" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy') AS Tanggal,[KODE SUPPLIER] from t_pembelianh"
ElseIf cmb_transaksi = "Pengambilan Barang" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy') AS Tanggal from t_adjustmenth"
ElseIf cmb_transaksi = "Stock Opname" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy')as tanggal,keterangan from t_stockopnameh"
ElseIf cmb_transaksi = "Transfer Barang" Then
    Adodc1.RecordSource = "Select ID,format(Tanggal, 'dd/mm/yyyy')as tanggal,gudang,tujuan,keterangan from t_transfergudangh"
End If
Adodc1.RecordSource = Adodc1.RecordSource & IIf(IsNull(DTPicker1.Value), "", " where format(tanggal, 'yyyy/MM/dd')='" & Format(DTPicker1.Value, "YYYY/MM/dd") & "'") & " order by id"

Set flxGrid.DataSource = Adodc1

    Adodc1.Refresh
    
    flxGrid.Refresh

End Sub
Private Sub cmb_transaksi_Click()
    refresh_data
End Sub

Private Sub cmdDelete_Click()
On Error GoTo err
Dim mode As Byte
If MsgBox("Apakah anda yakin hendak menghapus semua transaksi bulan " & Format(DTPicker2, "MMMM - yyyy") & "?", vbYesNo) = vbNo Then Exit Sub
mode = 1
If Not db1 Then Exit Sub

conn_fake.CursorLocation = adUseClient


If db2 Then
conn.CursorLocation = adUseClient
conn.Open strcon '"Provider=MSDASQL.1;DBQ=" & servname & ";Driver={Microsoft Access Driver (*.mdb)}"
conn.BeginTrans
End If
mode = 2

    If db2 Then conn.Execute "update t_penjualanh set pk='' where pk  in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
     
        .Execute "delete from t_penjualand where ID in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_pembayaran where NO_JUAL in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_penjualanh where ID in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        
        rs.Open "select id from t_returjualh where No_nota in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")", conn_fake
        If Not rs.EOF Then
            If db2 Then conn.Execute "update t_returjualh set pk='' where pk='" & rs(0) & "'"
        End If
        rs.Close
        .Execute "delete t_returjuald.* from t_returjualh inner join t_returjuald on t_returjuald.id = t_returjualh.id where No_nota in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_returjualh where No_nota in (select id from t_penjualanh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
   End With
   

    If db2 Then conn.Execute "update t_pembelianh set pk='' where pk in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        
        .Execute "delete from t_pembeliand where ID in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_bayarhutang where no_beli  in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute " delete t_returbelid.* from t_returbelih inner join t_returbelid on t_returbelid.id = t_returbelih.id where No_nota  in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
         rs.Open "select id from t_returbelih where No_nota in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")", conn_fake
             If Not rs.EOF Then
                 conn.Execute "update t_returbelih set pk='' where pk='" & rs(0) & "'"
             End If
             rs.Close
        .Execute "delete from t_returbelih where No_nota in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_pembelianh where ID in (select id from t_pembelianh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
   End With
   

    If db2 Then conn.Execute "update t_adjustmenth set pk='' where pk in (select id from t_adjustmenth where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        .Execute "delete from t_adjustmentd where ID in (select id from t_adjustmenth where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_adjustmenth where ID in (select id from t_adjustmenth where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    End With
    

    If db2 Then conn.Execute "update t_stockopnameh set pk='' where pk in (select id from t_stockopnameh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        conn_fake.Execute "delete from t_stockopnamed where ID in (select id from t_stockopnameh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        conn_fake.Execute "delete from t_stockopnameh where ID in (select id from t_stockopnameh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"

    End With
   
   

    If db2 Then conn.Execute "update t_transfergudangh set pk='' where pk in (select id from t_transfergudangh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        .Execute "delete from t_transfergudangd where ID in (select id from t_transfergudangh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_transfergudangh where ID in (select id from t_transfergudangh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    End With
    If db2 Then conn.Execute "update t_returjualh set pk='' where pk in (select id from t_returjualh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        .Execute "delete from t_returjuald where ID in (select id from t_returjualh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_returjualh where ID in (select id from t_returjualh where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    End With
    If db2 Then conn.Execute "update t_returbelih set pk='' where pk in (select id from t_returbelih where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    With conn_fake
        .Execute "delete from t_returbelid where ID in (select id from t_returbelih where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
        .Execute "delete from t_returbelih where ID in (select id from t_returbelih where month(tanggal)=" & Month(DTPicker2) & " and year(tanggal)=" & Year(DTPicker2) & ")"
    End With
  


If db2 Then conn.CommitTrans

conn_fake.CommitTrans
mode = 1
MsgBox "Proses hapus sudah selesai"
err:
If mode = 2 Then
    conn.RollbackTrans
    conn_fake.RollbackTrans
End If
DropConnection
End Sub


Private Sub cmdrefresh_Click()
    refresh_data
End Sub


Private Sub DTPicker1_Click()
refresh_data
End Sub

Private Sub flxgrid_DblClick()
    hapus
    Adodc1.Refresh
    flxGrid.Refresh
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 27 Then Unload Me

End Sub

Private Sub flxGrid_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

hapus
Adodc1.Refresh
flxGrid.Refresh
End If

End Sub
Private Sub hapus()
On Error GoTo err
Dim mode As Byte
mode = 1
If Not db1 Then Exit Sub

conn_fake.CursorLocation = adUseClient
 strcon '"Provider=MSDASQL.1;DBQ=" & servnameasli & ";Driver={Microsoft Access Driver (*.mdb)}"

If db2 Then
conn.CursorLocation = adUseClient
conn.Open strcon '"Provider=MSDASQL.1;DBQ=" & servname & ";Driver={Microsoft Access Driver (*.mdb)}"
conn.BeginTrans
End If
mode = 2
i = flxGrid.row
j = flxGrid.RowSel

For a = IIf(i < j, i, j) To IIf(i < j, j, i)
If cmb_transaksi = "Penjualan" Then
    If db2 Then conn.Execute "update t_penjualanh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
    With conn_fake
        .Execute "delete from t_penjualanh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_penjualand where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_pembayaran where NO_JUAL='" & flxGrid.TextMatrix(a, 0) & "'"
        
        rs.Open "select id from t_returjualh where No_nota='" & flxGrid.TextMatrix(a, 0) & "'", conn_fake
        If Not rs.EOF Then
            If db2 Then conn.Execute "update t_returjualh set pk='' where pk='" & rs(0) & "'"
        End If
        rs.Close
        .Execute "delete t_returjuald.* from t_returjualh inner join t_returjuald on t_returjuald.id = t_returjualh.id where No_nota ='" & flxGrid.TextMatrix(a, 0) & "' "
        .Execute "delete from t_returjualh where No_nota='" & flxGrid.TextMatrix(a, 0) & "'"
   End With
   
 ElseIf cmb_transaksi = "Pembelian" Then
    If db2 Then conn.Execute "update t_pembelianh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
    With conn_fake
        .Execute "delete from t_pembelianh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_pembeliand where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_bayarhutang where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute " delete t_returbelid.* from t_returbelih inner join t_returbelid on t_returbelid.id = t_returbelih.id where No_nota ='" & flxGrid.TextMatrix(a, 0) & "' "
         rs.Open "select id from t_returbelih where No_nota='" & flxGrid.TextMatrix(a, 0) & "'", conn_fake
             If Not rs.EOF Then
                 conn.Execute "update t_returbelih set pk='' where pk='" & rs(0) & "'"
             End If
             rs.Close
        .Execute "delete from t_returbelih where No_nota='" & flxGrid.TextMatrix(a, 0) & "'"
   End With
   
 ElseIf cmb_transaksi = "Pengambilan Barang" Then
    If db2 Then conn.Execute "update t_adjustmenth set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
    With conn_fake
        .Execute "delete from t_adjustmentd where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_adjustmenth where ID='" & flxGrid.TextMatrix(a, 0) & "'"
    End With
    
 ElseIf cmb_transaksi = "Stock Opname" Then
    If db2 Then conn.Execute "update t_stockopnameh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
    With conn_fake
        rs.Open "delete from t_stockopnamed where ID='" & flxGrid.TextMatrix(a, 0) & "'", conn_fake
        rs.Open "delete from t_stockopnameh where ID='" & flxGrid.TextMatrix(a, 0) & "'", conn_fake

    End With
   
   
 ElseIf cmb_transaksi = "Transfer Barang" Then
    If db2 Then conn.Execute "update t_transfergudangh set pk='' where pk='" & flxGrid.TextMatrix(a, 0) & "'"
    With conn_fake
        .Execute "delete from t_transfergudangd where ID='" & flxGrid.TextMatrix(a, 0) & "'"
        .Execute "delete from t_transfergudangh where ID='" & flxGrid.TextMatrix(a, 0) & "'"
    End With
    
  End If

Next a
If db2 Then conn.CommitTrans

conn_fake.CommitTrans
mode = 1

err:
If mode = 2 Then
    conn.RollbackTrans
    conn_fake.RollbackTrans
End If
DropConnection
End Sub
Private Sub Form_Activate()
Adodc1.ConnectionString = strcon

cmb_transaksi.AddItem "Penjualan"
cmb_transaksi.AddItem "Retur Penjualan"
cmb_transaksi.AddItem "Pembelian"
cmb_transaksi.AddItem "Pengambilan Barang"
cmb_transaksi.AddItem "Stock Opname"
cmb_transaksi.AddItem "Transfer Barang"

cmb_transaksi.SetFocus
cmb_transaksi = "Penjualan"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()

With flxGrid
    .ColWidth(0) = 2500
    .ColWidth(1) = 1800
    .ColWidth(2) = 1800
    
    End With
DTPicker1 = Now
DTPicker2 = Now
End Sub

Private Sub Command2_Click()

'Adodc1.RecordSource = "select ID from t_penjualan"
'MsgBox Adodc1.Recordset!id
'Adodc1.Recordset.MoveNext
End Sub



