VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAddBayarPiutang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Penerimaan Piutang"
   ClientHeight    =   4305
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6915
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   6915
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   2
      Left            =   1710
      Locked          =   -1  'True
      MaxLength       =   50
      TabIndex        =   24
      Top             =   2700
      Width           =   1680
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      Height          =   330
      Left            =   1755
      Picture         =   "frmAddBayarPiutang.frx":0000
      TabIndex        =   23
      Top             =   135
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      Height          =   645
      Left            =   4395
      Picture         =   "frmAddBayarPiutang.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3510
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      Height          =   645
      Left            =   2775
      Picture         =   "frmAddBayarPiutang.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3510
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchNota 
      Caption         =   "F3"
      Height          =   330
      Left            =   3195
      Picture         =   "frmAddBayarPiutang.frx":0306
      TabIndex        =   6
      Top             =   1035
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   1
      Left            =   1710
      Locked          =   -1  'True
      MaxLength       =   50
      TabIndex        =   1
      Top             =   2295
      Width           =   1680
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   1035
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      Height          =   645
      Left            =   1290
      Picture         =   "frmAddBayarPiutang.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3510
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTTanggal 
      Height          =   330
      Left            =   1710
      TabIndex        =   5
      Top             =   585
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   20709379
      CurrentDate     =   38927
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "Kartu"
      Height          =   240
      Left            =   135
      TabIndex        =   27
      Top             =   2745
      Width           =   1320
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   26
      Top             =   2745
      Width           =   105
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   25
      Top             =   2745
      Width           =   150
   End
   Begin VB.Label lblJumlahPiutang 
      BackStyle       =   0  'Transparent
      Height          =   240
      Left            =   1710
      TabIndex        =   22
      Top             =   1890
      Width           =   2850
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Height          =   240
      Left            =   1710
      TabIndex        =   21
      Top             =   1485
      Width           =   2850
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   19
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   18
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   1530
      TabIndex        =   17
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Customer"
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1485
      Width           =   1140
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   15
      Top             =   1485
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   90
      TabIndex        =   14
      Top             =   90
      Width           =   1545
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   13
      Top             =   3105
      Width           =   2130
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   12
      Top             =   2340
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   11
      Top             =   1080
      Width           =   150
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   10
      Top             =   2340
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   9
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Tunai"
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   2340
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nomor Nota"
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   1080
      Width           =   1140
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Piutang"
      Height          =   240
      Left            =   135
      TabIndex        =   20
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddBayarPiutang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from T_PEmbayaran where no_jual='" & lblNoTrans & "' order by ID desc", conn
    If Not rs.EOF Then
        No = Format((CLng(rs(0)) + 1), "00")
    Else
        No = Format("1", "00")
    End If
    rs.Close
    id = No
End Sub
Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "delete from t_pembayaran where id='" & lblNoTrans & "'"
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah dihapus"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = "select * from t_pembayaran"
    frmSearch.nmform = "frmAddpembayaran"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchNota_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.query = "SELECT * from t_penjualanh where total>[jumlah bayar]"
    frmSearch.nmform = "frmAddpembayaran"
    frmSearch.nmctrl = "txtField"
    frmSearch.col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, j As Byte

On Error GoTo err
    i = 0
    For j = 0 To 1
        If txtField(j).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(j).SetFocus
            Exit Sub
        End If
    Next
    conn.ConnectionString = strcon
    conn.Open

    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_pembayaran"
    End If
    add_data
    conn.CommitTrans
    i = 0
    MsgBox "Data sudah Tersimpan"
    conn.Close
    reset_form
    Exit Sub
err:
    If i = 1 Then
        If db2 Then conn.RollbackTrans
        If db1 Then conn_fake.RollbackTrans
    End If
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = "0"
    txtField(2).text = "0"
    txtField(0).SetFocus
    lblJumlahPiutang = "0"
    lblNamaSupplier = ""
    lblNoTrans = "-"
    DTTanggal = Now
    cmdSimpan.Visible = True
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_pembayaran"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "no_jual"
    fields(3) = "[jumlah bayar]"
    fields(4) = "Tunai"
    fields(5) = "Kartu"
    fields(6) = "keterangan"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTTanggal, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtField(0)
    nilai(3) = IIf(CLng(txtField(1) + txtField(2)) > lblJumlahPiutang, lblJumlahPiutang, CLng(txtField(1) + txtField(2)))
    nilai(4) = txtField(1)
    nilai(5) = txtField(2)
    nilai(6) = "" 'txtKeterangan.text
    tambah_data table_name, fields, nilai
End Sub
Public Sub cari_data()
Dim sudahbayar As Long
Dim diskon As Long
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    rs.Open "select * from t_pembayaran where no_jual='" & txtField(0) & "' and id<>'" & lblNoTrans & "'" & where, conn
    If Not rs.EOF Then
'        If (MsgBox("Nota tersebut sudah dilunasi, apakah anda ingin melihat pembayaran tersebut?", vbYesNo) = vbYes) Then
'            lblNoTrans = rs(0)
'            rs.Close
'        End If
        conn.Close
        cek_notrans
        Exit Sub
    End If
    rs.Close
    rs.Open "select [nama customer],disc_rp from t_penjualanh left join ms_customer on t_penjualanh.[kode customer]=ms_customer.[kode customer] where id='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = IIf(IsNull(rs(0)), 0, rs(0))
        diskon = rs(1)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        rs.Close
        rs.Open "select sum(jumlahbayar) from t_pembayaran where no_jual='" & txtField(0) & "'", conn
        sudahbayar = IIf(IsNull(rs(0)), 0, rs(0))
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        sudahbayar = 0
        diskon = 0
        lblNamaSupplier = ""
        lblJumlahPiutang = "0"
    End If
    rs.Close
    rs.Open "select sum(qty*(harga-iif(disc_tipe=1,disc,(harga*disc/100))-promosi)) from t_penjualand where id='" & txtField(0).text & "'", conn
    If Not rs.EOF Then lblJumlahPiutang = IIf(IsNull(rs(0)), 0, rs(0) - diskon)
    rs.Close
    lblJumlahPiutang = CLng(lblJumlahPiutang) - sudahbayar
    txtField(1).text = lblJumlahPiutang
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_notrans()
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    rs.Open "select * from t_pembayaran where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(2)
        txtField(1).text = rs(3)
        txtField(2).text = rs(4)
        DTTanggal = rs(1)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = False
        mnuSimpan.Enabled = False
        cmdSimpan.Visible = False
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        txtField(0).text = ""
        txtField(1).text = "0"
    End If
    If rs.State Then rs.Close
    conn.Close
    'cari_data
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        SendKeys "{Tab}"
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchNota_Click
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearch_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index))
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
