VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmAddBayarHutang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembayaran Hutang"
   ClientHeight    =   4155
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14250
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   14250
   Begin MSComCtl2.MonthView MonthView1 
      Height          =   2310
      Left            =   3735
      TabIndex        =   24
      Top             =   765
      Width           =   2640
      _ExtentX        =   4657
      _ExtentY        =   4075
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   0
      StartOfWeek     =   102629377
      CurrentDate     =   39976
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "F5"
      Height          =   330
      Left            =   1755
      Picture         =   "frmAddBayarHutang.frx":0000
      TabIndex        =   23
      Top             =   135
      Width           =   375
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      Height          =   645
      Left            =   4073
      Picture         =   "frmAddBayarHutang.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Enabled         =   0   'False
      Height          =   645
      Left            =   2453
      Picture         =   "frmAddBayarHutang.frx":0204
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchNota 
      Caption         =   "F3"
      Height          =   330
      Left            =   3195
      Picture         =   "frmAddBayarHutang.frx":0306
      TabIndex        =   6
      Top             =   1035
      Width           =   375
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   1
      Left            =   1710
      Locked          =   -1  'True
      MaxLength       =   50
      TabIndex        =   1
      Top             =   2295
      Width           =   1680
   End
   Begin VB.TextBox txtField 
      Height          =   330
      Index           =   0
      Left            =   1710
      TabIndex        =   0
      Top             =   1035
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      Height          =   645
      Left            =   968
      Picture         =   "frmAddBayarHutang.frx":0408
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3240
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTTanggal 
      Height          =   330
      Left            =   1710
      TabIndex        =   5
      Top             =   585
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   102694915
      CurrentDate     =   38927
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   3885
      Left            =   6480
      TabIndex        =   25
      Top             =   90
      Width           =   7620
      _ExtentX        =   13441
      _ExtentY        =   6853
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   465
      Left            =   3780
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   1
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblJumlahHutang 
      BackStyle       =   0  'Transparent
      Height          =   240
      Left            =   1710
      TabIndex        =   22
      Top             =   1890
      Width           =   2850
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Height          =   240
      Left            =   1710
      TabIndex        =   21
      Top             =   1485
      Width           =   2850
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   19
      Top             =   1890
      Width           =   105
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   18
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   1530
      TabIndex        =   17
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Supplier"
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   1485
      Width           =   1140
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   15
      Top             =   1485
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   90
      TabIndex        =   14
      Top             =   90
      Width           =   1545
   End
   Begin VB.Label Label21 
      BackStyle       =   0  'Transparent
      Caption         =   "* Harus Diisi"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   2835
      Width           =   2130
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1260
      TabIndex        =   12
      Top             =   2340
      Width           =   150
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1125
      TabIndex        =   11
      Top             =   1080
      Width           =   150
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   10
      Top             =   2340
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1530
      TabIndex        =   9
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Bayar"
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   2340
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nomor Nota"
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   1080
      Width           =   1140
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Hutang"
      Height          =   240
      Left            =   135
      TabIndex        =   20
      Top             =   1890
      Width           =   1320
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuHapus 
         Caption         =   "&Hapus"
         Shortcut        =   ^D
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddBayarHutang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim NoTrans2 As String
Private Sub nomor_baru()
Dim No As String

    rs.Open "select top 1 ID from t_bayarhutang where mid(ID,3,2)='" & Format(DTTanggal, "yy") & "' and mid(ID,5,2)='" & Format(DTTanggal, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "BY" & Format(DTTanggal, "yy") & Format(DTTanggal, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "BY" & Format(DTTanggal, "yy") & Format(DTTanggal, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub cmdHapus_Click()
Dim i As Byte
On Error GoTo err
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    conn.Execute "delete from t_bayarhutang where id='" & lblNoTrans & "'"
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah dihapus"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If err.Description <> "" Then MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdSearch_Click()
    
        frmSearch.connstr = strcon
    
    frmSearch.query = "select Id,tanggal,No_beli,jumlah_bayar,keterangan  from t_bayarhutang"
    frmSearch.nmform = "frmAddBayarHutang"
    frmSearch.nmctrl = "lblnotrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchNota_Click()
    
    frmSearch.connstr = strcon
'    frmSearch.txtSearch.text = txtField(0).text
    frmSearch.query = "select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang from t_pembelianh"
    frmSearch.nmform = "frmAddBayarHutang"
    frmSearch.nmctrl = "txtField"
    frmSearch.col = 0
    frmSearch.Index = 0
    frmSearch.proc = "cari_data"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i, J As Byte
Dim id As String
On Error GoTo err
    i = 0
    For J = 0 To 1
        If txtField(J).text = "" Then
            MsgBox "Semua field yang bertanda * harus diisi"
            txtField(J).SetFocus
            Exit Sub
        End If
    Next
    id = lblNoTrans
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_bayarhutang where id='" & lblNoTrans & "'"
        
    End If
    add_data
    conn.CommitTrans
    
    i = 0
    MsgBox "Data sudah Tersimpan"
    DropConnection
    reset_form
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    If rs.State Then rs.Close
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    txtField(0).text = ""
    txtField(1).text = ""
    txtField(0).SetFocus
    lblJumlahHutang = "0"
    lblNamaSupplier = ""
    lblNoTrans = "-"
    DTTanggal = Now
End Sub
Private Sub add_data()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(6)
    ReDim nilai(6)
    table_name = "t_bayarhutang"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "no_beli"
    fields(3) = "[jumlah bayar]"
    fields(4) = "keterangan"
    fields(5) = "pk"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTTanggal, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtField(0)
    nilai(3) = txtField(1)
    nilai(4) = "" 'txtKeterangan.text
    nilai(5) = NoTrans2
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Public Sub cari_data()
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from t_bayarhutang where no_beli='" & txtField(0) & "'" & where, conn
    If Not rs.EOF Then
        If rs(0) <> lblNoTrans Then
        If (MsgBox("Nota tersebut sudah dibayar, apakah anda ingin melihat pembayaran tersebut?", vbYesNo) = vbYes) Then
            lblNoTrans = rs(0)
            rs.Close
        Else
            lblNoTrans = "-"
        End If
        conn.Close
        cek_notrans
        Exit Sub
        End If
        
    Else
        lblNoTrans = "-"
    End If
    rs.Close
    rs.Open "select [nama supplier] from t_pembelianh left join ms_supplier on t_pembelianh.[kode supplier]=ms_supplier.[kode supplier] where id='" & txtField(0).text & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = IIf(IsNull(rs(0)), "", rs(0))
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
        rs.Close
        rs.Open "select iif(isnull(sum(qty*harga)),0,sum(qty*harga)) from t_pembeliand where id='" & txtField(0).text & "'", conn
        If Not rs.EOF Then lblJumlahHutang = rs(0)
        rs.Close
        
        rs.Open "select iif(isnull(sum(qty*harga_beli)),0,sum(qty*harga_beli)) from t_returbelid d inner join t_returbelih h on d.id=h.id where no_nota='" & txtField(0).text & "'", conn
        If Not rs.EOF Then lblJumlahHutang = CCur(lblJumlahHutang) - rs(0)
        txtField(1).text = lblJumlahHutang
        rs.Close
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        
        lblNamaSupplier = ""
        lblJumlahHutang = "0"
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_notrans()
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from t_bayarhutang where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        txtField(0).text = rs(2)
        txtField(1).text = rs(3)
        DTTanggal = rs(1)
        cmdHapus.Enabled = True
        mnuHapus.Enabled = True
    Else
        cmdHapus.Enabled = False
        mnuHapus.Enabled = False
        txtField(0).text = ""
        txtField(1).text = "0"
    End If
    If rs.State Then rs.Close
    conn.Close
    cari_data
End Sub

Private Sub DBGrid_Click()
    txtField(0).text = DBGrid.Columns(0).text
    cari_data
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchNota_Click
End Sub

Private Sub Form_Load()

    reset_form
    
    Adodc1.ConnectionString = strcon
    
   
End Sub

Private Sub mnuHapus_Click()
    cmdHapus_Click
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchNota_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub

Private Sub MonthView1_DateClick(ByVal DateClicked As Date)
    showgrid (DateClicked)
End Sub
Private Sub showgrid(tanggal As Date)
    Adodc1.RecordSource = "select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang  from t_pembelianh where format(jatuhtempo,'MM/dd/yyyy')='" & Format(tanggal, "MM/dd/yyyy") & "' and id not in (select no_beli from t_bayarhutang)"
    Set DBGrid.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid.Refresh
    If tanggal <> "12:00:00 AM" And Not Adodc1.Recordset.EOF Then MonthView1.DayBold(tanggal) = True
End Sub

Private Sub MonthView1_GetDayBold(ByVal StartDate As Date, ByVal count As Integer, State() As Boolean)
    Dim rs As New ADODB.Recordset
    Dim conn As New ADODB.Connection
    Dim A As Date
    
    conn.Open strcon
    
    rs.Open "select distinct format(jatuhtempo,'MM/dd/yyyy') from t_pembelianh where id not in (select no_beli from t_bayarhutang) and month(jatuhtempo)=" & Month(MonthView1) & " and year(jatuhtempo)=" & Year(MonthView1) & " order by format(jatuhtempo,'MM/dd/yyyy') desc", conn
    While Not rs.EOF
        State(Day(rs(0))) = True
        A = rs(0)
        MonthView1.DayBold(A) = True
        rs.MoveNext
    Wend
    showgrid A
'    If tanggal <> "12:00:00 AM" Then Count = Day(a)

    rs.Close
    conn.Close
    
End Sub


Private Sub txtField_GotFocus(Index As Integer)
    txtField(Index).SelStart = 0
    txtField(Index).SelLength = Len(txtField(Index))
End Sub

Private Sub txtField_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 And Index = 0 Then
        cari_data
    End If
End Sub

Private Sub txtField_LostFocus(Index As Integer)
    If Index = 0 Then cari_data
End Sub
