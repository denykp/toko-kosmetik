VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAddBarangMasuk 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barang Masuk"
   ClientHeight    =   7965
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10335
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   10335
   Begin VB.ComboBox cmbTipe 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmAddBarangMasuk.frx":0000
      Left            =   7200
      List            =   "frmAddBarangMasuk.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   39
      Top             =   495
      Width           =   1905
   End
   Begin VB.ComboBox cmbTujuan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7200
      TabIndex        =   2
      Top             =   900
      Width           =   2175
   End
   Begin VB.TextBox txtBukti 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   990
      Width           =   1590
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   30
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   29
      Top             =   180
      Value           =   1  'Checked
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   90
      TabIndex        =   19
      Top             =   1800
      Width           =   6990
      Begin VB.CommandButton cmdSearchBrg 
         Caption         =   "..."
         Height          =   330
         Left            =   2835
         Picture         =   "frmAddBarangMasuk.frx":0023
         TabIndex        =   20
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   8
         Top             =   1125
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1530
         TabIndex        =   7
         Top             =   1125
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   315
         TabIndex        =   6
         Top             =   1125
         Width           =   1050
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   5
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   4
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   27
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label lblID 
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   240
         Left            =   270
         TabIndex        =   26
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   25
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   24
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3240
         TabIndex        =   23
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   330
         Left            =   1575
         TabIndex        =   22
         Top             =   1305
         Width           =   600
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         Height          =   330
         Left            =   2520
         TabIndex        =   21
         Top             =   675
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   645
      Left            =   3817
      Picture         =   "frmAddBarangMasuk.frx":0125
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7110
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   3780
      Picture         =   "frmAddBarangMasuk.frx":0227
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   3
      Top             =   1350
      Width           =   3885
   End
   Begin VB.CommandButton cmdHapus 
      Caption         =   "&Hapus"
      Height          =   645
      Left            =   5257
      Picture         =   "frmAddBarangMasuk.frx":0329
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   7110
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   6697
      Picture         =   "frmAddBarangMasuk.frx":042B
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   7110
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      Height          =   645
      Left            =   2407
      Picture         =   "frmAddBarangMasuk.frx":052D
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   7110
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   20709379
      CurrentDate     =   38927
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   135
      Top             =   7110
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2490
      Left            =   135
      TabIndex        =   28
      Top             =   3690
      Width           =   9960
      _ExtentX        =   17568
      _ExtentY        =   4392
      _Version        =   393216
      Cols            =   7
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5715
      TabIndex        =   44
      Top             =   180
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7065
      TabIndex        =   43
      Top             =   180
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label23 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7065
      TabIndex        =   42
      Top             =   540
      Width           =   105
   End
   Begin VB.Label Label19 
      Caption         =   "Tipe"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5715
      TabIndex        =   41
      Top             =   540
      Width           =   960
   End
   Begin VB.Label lblGudang 
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7245
      TabIndex        =   40
      Top             =   180
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   38
      Top             =   6390
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5265
      TabIndex        =   37
      Top             =   6390
      Width           =   1140
   End
   Begin VB.Label Label13 
      Caption         =   "Asal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5715
      TabIndex        =   36
      Top             =   945
      Width           =   1320
   End
   Begin VB.Label Label10 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7065
      TabIndex        =   35
      Top             =   945
      Width           =   105
   End
   Begin VB.Label Label7 
      Caption         =   "No Bukti"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   34
      Top             =   1035
      Width           =   1275
   End
   Begin VB.Label Label1 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   33
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7740
      TabIndex        =   32
      Top             =   6390
      Width           =   1140
   End
   Begin VB.Label Label17 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6795
      TabIndex        =   31
      Top             =   6390
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label11 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   16
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   15
      Top             =   1395
      Width           =   105
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1395
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   13
      Top             =   135
      Width           =   1860
   End
End
Attribute VB_Name = "frmAddBarangMasuk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select kode_barang,nama, kode_rak,kode_tipe, kode_satuan from ms_barang"
Const queryTransfer As String = "select * from t_barangmasukh"
Dim totalQty As Currency
Dim mode As Byte
Dim harga_beli, harga_jual As Currency
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from t_barangmasukh where left(ID,2)='" & Format(DTPicker1, "yy") & "' and substring(ID,3,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub


Private Sub chkNumbering_Click()
If chkNumbering.Value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub

Private Sub cmdHapus_Click()
On Error GoTo err
    If MsgBox("Apakah anda yakin hendak menghapus?", vbYesNo) = vbYes Then
        conn.ConnectionString = strcon
        conn.Open
        conn.BeginTrans
        conn.Execute "delete from t_barangmasukh  where id='" & lblNoTrans & "'"
        conn.Execute "delete from t_barangmasukd  where id='" & lblNoTrans & "'"
        conn.CommitTrans
        conn.Close
        MsgBox "Data sudah dihapus"
        reset_form
    End If
    Exit Sub
err:
    conn.RollbackTrans
    conn.Close
End Sub


Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
    lblQty = totalQty
    For row = row To flxGrid.Rows - 1
        If row = flxGrid.Rows - 1 Then
            For col = 1 To 6
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To 6
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To 6
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.Rows = flxGrid.Rows - 1
    txtKdBrg.text = ""
    lblSatuan = ""
    txtQty.text = ""
    LblNamaBarang = ""
    txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" Then
        
        For i = 1 To flxGrid.Rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.Rows - 1
            flxGrid.Rows = flxGrid.Rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.TextMatrix(row, 4) = lblSatuan
        flxGrid.TextMatrix(row, 5) = harga_beli
        flxGrid.TextMatrix(row, 6) = harga_jual
        
        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
        lblQty = Format(totalQty, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.Rows - 2
End Sub

Private Sub cmdPrint_Click()
    printBukti
End Sub
Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    With CRPrint
        .reset
        .ReportFileName = App.Path & "\Report\Transferbarangmasuk.rpt"
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
        Next
        .SelectionFormula = " {t_barangmasukh.ID}='" & lblNoTrans & "'"
        .Destination = crptToWindow
        .ParameterFields(0) = "login;" + user + ";True"
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .Action = 1
    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddbarangmasuk"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    Set frmSearch.frm = Me
    frmSearch.Show
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddbarangmasuk"
    frmSearch.nmctrl = "lblNoTrans"
    
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.proc = "cek_notrans"
    Set frmSearch.frm = Me
    
    frmSearch.Show
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim row As Integer
On Error GoTo err

    conn.ConnectionString = strcon
    conn.Open
    If chkNumbering <> "1" Then
    rs.Open "select * from t_barangmasukh where id='" & txtID.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtID.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
    End If
    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
    Else
        conn.Execute "delete from t_barangmasukh where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_barangmasukd where ID='" & lblNoTrans & "'"
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    add_dataheader
    For row = 1 To flxGrid.Rows - 1
        If flxGrid.TextMatrix(row, 1) <> "" Then
            add_datadetail (row)
        End If
    Next
    
    conn.CommitTrans
    conn.Close
    MsgBox "Data sudah tersimpan"
    reset_form
    
    SendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtBukti.text = ""
    txtKeterangan.text = ""
    loadcombo
    totalQty = 0
    DTPicker1 = Now
    cmdClear_Click
    flxGrid.Rows = 1
    flxGrid.Rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_barangmasukh"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "tipe"
    fields(4) = "no_bukti"
    fields(5) = "keterangan"
    fields(6) = "asal"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "MM/dd/yyyy hh:mm:ss")
    nilai(2) = gudang
    nilai(3) = cmbTipe.text
    nilai(4) = txtBukti.text
    nilai(5) = txtKeterangan.text
    nilai(6) = cmbTujuan.text
    tambah_data table_name, fields, nilai
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "t_barangmasukd"
    fields(0) = "ID"
    fields(1) = "KODE_barang"
    fields(2) = "qty"
    fields(3) = "NO_URUT"
    fields(4) = "HARGA_BELI"
    fields(5) = "HARGA_JUAL"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
    nilai(4) = flxGrid.TextMatrix(row, 5)
    nilai(5) = flxGrid.TextMatrix(row, 6)
    
    tambah_data table_name, fields, nilai
End Sub

Public Sub cek_notrans()
    cmdPrint.Visible = False
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from t_barangmasukh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
    
        lblGudang = rs("gudang")
        SetComboText rs("tipe"), cmbTipe
        SetComboText rs("asal"), cmbTujuan
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtBukti.text = rs("no_bukti")
        totalQty = 0
        cmdPrint.Visible = True
        If rs.State Then rs.Close
        flxGrid.Rows = 1
        flxGrid.Rows = 2
        row = 1
        
        rs.Open "select d.kode_barang,m.nama, qty,m.kode_satuan,d.harga_beli,d.harga_jual from t_barangmasukd d inner join ms_barang m on d.kode_barang=m.kode_barang where d.id='" & lblNoTrans & "' order by no_urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
                flxGrid.TextMatrix(row, 4) = rs(3)
                flxGrid.TextMatrix(row, 5) = rs(4)
                flxGrid.TextMatrix(row, 6) = rs(5)
                row = row + 1
                totalQty = totalQty + rs(2)
                flxGrid.Rows = flxGrid.Rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty
        lblItem = flxGrid.Rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
        cek_kodebarang1
        LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
        lblNmBarang = flxGrid.TextMatrix(flxGrid.row, 2)
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
        lblSatuan = flxGrid.TextMatrix(flxGrid.row, 4)
        harga_beli = flxGrid.TextMatrix(flxGrid.row, 5)
        harga_jual = flxGrid.TextMatrix(flxGrid.row, 6)
    Else
        mode = 1
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
End Sub

Private Sub Form_Activate()
    SendKeys "{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        SendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    conn.ConnectionString = strcon
    conn.Open
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    lblGudang = gudang
    'End If
    'rs.Close
    conn.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 900
    flxGrid.ColWidth(4) = 1000
    flxGrid.ColWidth(5) = 0
    flxGrid.ColWidth(6) = 0

    
    flxGrid.TextMatrix(0, 1) = "Kode Barang"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Satuan"
    flxGrid.TextMatrix(0, 5) = "Harga Beli"
    flxGrid.TextMatrix(0, 6) = "Harga Jual"

End Sub
Private Sub loadcombo()
    If cmbTipe.ListCount > 0 Then cmbTipe.ListIndex = 0
    conn.ConnectionString = strcon
    conn.Open
'    cmbGudang.Clear
'    rs.Open "select * from ms_gudang order by kode_gudang", conn
'    While Not rs.EOF
'        cmbGudang.AddItem rs(0) & "-" & rs(1)
'        rs.MoveNext
'    Wend
'    rs.Close
'    If cmbGudang.ListCount > 0 Then cmbGudang.ListIndex = 0
    cmbTujuan.Clear
    rs.Open "select distinct asal from t_barangmasukh order by asal", conn
    While Not rs.EOF
        cmbTujuan.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    If cmbTujuan.ListCount > 0 Then cmbTujuan.ListIndex = 0
End Sub
Public Sub cek_kodebarang1()
Dim harga As Long
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select nama,kode_satuan,harga_beli,harga_jual from ms_barang where kode_barang='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(0)
        lblSatuan = rs(1)
        harga_beli = 0
        harga_jual = 0
    Else
        LblNamaBarang = ""
        lblSatuan = ""
        harga_beli = 0
        harga_jual = 0
    End If
    rs.Close
    conn.Close
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cek_kodebarang1
'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
    cek_kodebarang1
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub

