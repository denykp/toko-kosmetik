VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmAddPenjualan 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kasir"
   ClientHeight    =   9135
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   14745
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9135
   ScaleWidth      =   14745
   Begin VB.ComboBox cmbSales 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   2565
      Width           =   1950
   End
   Begin VB.CommandButton cmdBatal 
      BackColor       =   &H0080C0FF&
      Caption         =   "Batalkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   13320
      TabIndex        =   64
      Top             =   3060
      UseMaskColor    =   -1  'True
      Width           =   1410
   End
   Begin VB.TextBox txtDiscTotal 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   13005
      TabIndex        =   63
      Top             =   5805
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.CheckBox chkpromosi 
      Caption         =   "Promosi"
      Height          =   285
      Left            =   11790
      TabIndex        =   61
      Top             =   2115
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.TextBox txtNamaPelanggan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10035
      Locked          =   -1  'True
      TabIndex        =   51
      Top             =   2475
      Width           =   3705
   End
   Begin VB.ComboBox cmbNoTrans 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   50
      Top             =   2160
      Width           =   1950
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   330
      Left            =   13230
      TabIndex        =   47
      Top             =   5625
      Visible         =   0   'False
      Width           =   1185
      Begin VB.OptionButton Opt1 
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         TabIndex        =   13
         Top             =   45
         Value           =   -1  'True
         Width           =   600
      End
      Begin VB.OptionButton Opt1 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   630
         TabIndex        =   14
         Top             =   45
         Width           =   510
      End
   End
   Begin VB.TextBox txtDisc 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   13005
      TabIndex        =   15
      Top             =   5850
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.TextBox txtKodeCustomer 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10035
      TabIndex        =   3
      Top             =   2070
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchCust 
      Appearance      =   0  'Flat
      BackColor       =   &H0080C0FF&
      Caption         =   "F4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11295
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   44
      Top             =   2070
      Width           =   420
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10890
      MultiLine       =   -1  'True
      TabIndex        =   41
      Top             =   1800
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   1770
      Left            =   90
      TabIndex        =   29
      Top             =   45
      Width           =   14415
      Begin VB.Label lblQuantity 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   7065
         TabIndex        =   36
         Top             =   225
         Width           =   7215
      End
      Begin VB.Label lblNmBarang 
         BackColor       =   &H0080FFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Left            =   135
         TabIndex        =   35
         Top             =   225
         Width           =   9465
      End
      Begin VB.Label lblSubTotal 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0,00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   48
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   960
         Left            =   8145
         TabIndex        =   34
         Top             =   720
         Width           =   6090
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Rp"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   36
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   825
         Left            =   5220
         TabIndex        =   33
         Top             =   900
         Width           =   1860
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   36
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   780
         Left            =   2880
         TabIndex        =   32
         Top             =   900
         Width           =   2085
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   135
      TabIndex        =   21
      Top             =   2970
      Width           =   4785
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   4140
         TabIndex        =   60
         Top             =   1035
         Width           =   1005
      End
      Begin VB.Frame Frame4 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   600
         Left            =   3330
         TabIndex        =   57
         Top             =   1080
         Width           =   645
         Begin VB.OptionButton Opt1 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   0
            TabIndex        =   59
            Top             =   270
            Value           =   -1  'True
            Width           =   510
         End
         Begin VB.OptionButton Opt1 
            Caption         =   "Rp"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   0
            TabIndex        =   58
            Top             =   45
            Width           =   600
         End
      End
      Begin VB.TextBox txtHarga 
         Height          =   330
         Left            =   2925
         Locked          =   -1  'True
         TabIndex        =   46
         Top             =   495
         Width           =   1275
      End
      Begin VB.ComboBox cmbHarga 
         Height          =   315
         ItemData        =   "frmAddPenjualan.frx":0000
         Left            =   3330
         List            =   "frmAddPenjualan.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   2475
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.CommandButton cmdSearchItem 
         Appearance      =   0  'Flat
         Caption         =   "F3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4320
         MaskColor       =   &H00C0FFFF&
         TabIndex        =   27
         Top             =   45
         Width           =   420
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1170
         Visible         =   0   'False
         Width           =   1000
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         MaskColor       =   &H00C000C0&
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1170
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   1000
      End
      Begin VB.CommandButton cmdOk 
         Appearance      =   0  'Flat
         Caption         =   "Tambah"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   225
         MaskColor       =   &H00FF0000&
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1170
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   1000
      End
      Begin VB.TextBox txtQty 
         Height          =   330
         Left            =   1035
         TabIndex        =   8
         Top             =   495
         Width           =   870
      End
      Begin VB.TextBox txtKdBrg 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1035
         TabIndex        =   1
         Top             =   45
         Width           =   3120
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2115
         TabIndex        =   45
         Top             =   540
         Width           =   600
      End
      Begin VB.Label lblHarga 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   8775
         TabIndex        =   28
         Top             =   180
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   45
         TabIndex        =   26
         Top             =   45
         Width           =   1770
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Jumlah"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         TabIndex        =   25
         Top             =   540
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3375
         TabIndex        =   24
         Top             =   495
         Visible         =   0   'False
         Width           =   3165
      End
      Begin VB.Label lblKode 
         BackStyle       =   0  'Transparent
         Caption         =   "BH1029ABC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5715
         TabIndex        =   23
         Top             =   45
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.Label lblSatuan 
         BackStyle       =   0  'Transparent
         Caption         =   "Satuan"
         Height          =   330
         Left            =   3330
         TabIndex        =   22
         Top             =   675
         Visible         =   0   'False
         Width           =   870
      End
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   5550
      Left            =   120
      TabIndex        =   2
      Top             =   3510
      Width           =   13065
      _ExtentX        =   23045
      _ExtentY        =   9790
      _Version        =   393216
      Cols            =   14
      BackColor       =   16777215
      BackColorBkg    =   12632256
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   3
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdBayar 
      BackColor       =   &H0080C0FF&
      Caption         =   "&Bayar (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   13320
      MaskColor       =   &H80000003&
      TabIndex        =   4
      Top             =   4545
      UseMaskColor    =   -1  'True
      Width           =   1410
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H0080C0FF&
      Caption         =   "&Save"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   13320
      TabIndex        =   6
      Top             =   4050
      UseMaskColor    =   -1  'True
      Width           =   1410
   End
   Begin VB.CommandButton cmdNew 
      BackColor       =   &H0080C0FF&
      Caption         =   "&New"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   13320
      TabIndex        =   5
      Top             =   5040
      UseMaskColor    =   -1  'True
      Width           =   1410
   End
   Begin VB.CommandButton cmdReprint 
      BackColor       =   &H0080C0FF&
      Caption         =   "&Reprint (F6)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   13320
      TabIndex        =   7
      Top             =   3555
      UseMaskColor    =   -1  'True
      Width           =   1410
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   0
      Top             =   45
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H80000003&
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3015
      Picture         =   "frmAddPenjualan.frx":0014
      TabIndex        =   19
      Top             =   2160
      Visible         =   0   'False
      Width           =   375
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   14220
      Top             =   2250
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Sales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   65
      Top             =   2610
      Width           =   870
   End
   Begin VB.Label lblHadiah 
      BackColor       =   &H8000000E&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   4995
      TabIndex        =   62
      Top             =   2970
      Width           =   9375
   End
   Begin VB.Label lblTotalAsal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12645
      TabIndex        =   56
      Top             =   6030
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   "Total Asal"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12645
      TabIndex        =   55
      Top             =   5670
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   54
      Top             =   2205
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Pending"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   135
      TabIndex        =   53
      Top             =   2205
      Width           =   870
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama Pelanggan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8235
      TabIndex        =   52
      Top             =   2475
      Width           =   1770
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   9765
      TabIndex        =   49
      Top             =   1800
      Visible         =   0   'False
      Width           =   1050
   End
   Begin VB.Label lblDisc 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12690
      TabIndex        =   48
      Top             =   5580
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   13320
      TabIndex        =   43
      Top             =   7470
      Width           =   600
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   960
      Left            =   13455
      TabIndex        =   42
      Top             =   8010
      Width           =   1095
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12690
      TabIndex        =   40
      Top             =   5985
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblSisa 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12915
      TabIndex        =   39
      Top             =   5715
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12735
      TabIndex        =   38
      Top             =   6255
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   12960
      TabIndex        =   37
      Top             =   5805
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1005
      Left            =   13275
      TabIndex        =   31
      Top             =   6525
      Width           =   1275
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   13365
      TabIndex        =   30
      Top             =   6075
      Width           =   555
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "No. Pelanggan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8235
      TabIndex        =   20
      Top             =   2070
      Width           =   1680
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1125
      TabIndex        =   18
      Top             =   2115
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.Label lbljam 
      BackStyle       =   0  'Transparent
      Caption         =   "fadsfdsf"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   3330
      TabIndex        =   17
      Top             =   1845
      Width           =   2085
   End
   Begin VB.Label lblhari 
      BackStyle       =   0  'Transparent
      Caption         =   "dsfasdfdsaf"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   135
      TabIndex        =   16
      Top             =   1845
      Width           =   3165
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPenjualan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim no_urut As Byte
Dim kodebarang As String
Dim NoTrans2 As String
Public total, retur As Currency
Public qty As Long
Private Type bonus
    kategori As String
    batas As Currency
    hadiah As String
    total As Currency
End Type
Dim cekbonus() As bonus
Dim mode As Byte
Dim detail As Boolean
Dim harga_beli, harga_jual, harga1, harga2 As Currency
Dim grosir As Integer
Dim harga As Byte
Public printproperties As Boolean
Dim currentRow As Integer
Dim Index As Byte

Private Sub chkBonus_Click()
    If chkBonus.value = 1 Then
        harga = 0
    Else
        harga = harga_jual
    End If
End Sub

Private Sub chkKirim_GotFocus()
    total_belanja
End Sub

Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from T_PENJUALANH where left(ID,4)='TP" & Format(Now, "yy") & "' and mid(ID,5,2)='" & Format(Now, "MM") & "'  and mid(ID,7,2)='" & Format(Now, "dd") & "' order by clng(mid(ID,3,len(id))) desc", conn
    If Not rs.EOF Then
        No = "TP" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format((CLng(Mid(rs(0), 9, (Len(rs(0)) - 8))) + 1), "000")
    Else
        No = "TP" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format("1", "000")
    End If
    rs.Close
    lblNoTrans = No
End Sub


Private Sub chkpromosi_Click()
    For i = 1 To flxGrid.rows - 2
        mode = 2
        flxGrid.row = i
        txtKdBrg.text = flxGrid.TextMatrix(i, 1)
        cek_kodebarang1
        txtQty = flxGrid.TextMatrix(i, 3)
'            txtHarga = flxGrid.TextMatrix(i, 4)
        'SetComboText rs("harga"), cmbHarga
        SetComboText flxGrid.TextMatrix(flxGrid.row, 9), cmbHarga
        If flxGrid.TextMatrix(i, 13) = "1" Or flxGrid.TextMatrix(flxGrid.row, 13) = "0" Then
            Opt1(0).value = True
        Else
            Opt1(1).value = True
        End If
        txtDisc = flxGrid.TextMatrix(i, 12)
        cmdOK_Click
    Next
End Sub

Private Sub cmbHarga_Click()
    Select Case cmbHarga.text
    Case 1: harga_jual = harga1
    Case 2: harga_jual = harga2
    End Select
    txtHarga = harga_jual
End Sub

Private Sub cmbNoTrans_Click()
    If cmbNoTrans.text <> "Pending" Then
    lblNoTrans = cmbNoTrans
    cek_notrans
    loadcombo
    End If
End Sub

Private Sub cmdBatal_Click()
On Error GoTo err
Dim i As Byte
If lblNoTrans = "-" Then Exit Sub
    i = 0
    If MsgBox("Apakah anda yakin ingin menghapus nota ini?", vbYesNo) = vbYes Then
        conn.Open
        conn.BeginTrans
        i = 1
        deletekartustock "Penjualan", lblNoTrans, conn
        rs.Open "select * from t_penjualand where id='" & lblNoTrans & "'", conn
        While Not rs.EOF
             updatestock gudang, rs("kode barang"), rs("qty"), conn
            
        rs.MoveNext
        Wend
        rs.Close
        conn.Execute "delete from t_penjualanh where id='" & lblNoTrans & "'"
        conn.Execute "delete from t_penjualand where id='" & lblNoTrans & "'"

        conn.CommitTrans
        i = 0
        conn.Close
        reset_form
    End If
    Exit Sub
err:
    MsgBox err.Description
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
End Sub

Private Sub cmdBayar_Click()
Dim subtotal1 As Long
Dim diskon As Long
Dim asal As Long
On Error GoTo err
    If flxGrid.rows <= 2 Then
        MsgBox "Silahkan masukkan Barang yang akan dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
'    If lblTotalAsal.Visible Then
'        If lblTotal < lblTotalAsal Then
'            MsgBox "Total Koreksi harus sama atau lebih besar daripada total sebelumnya"
'            Exit Sub
'        End If
'    End If
    
    conn.Open strcon
    
    rs.Open "select total from t_penjualanh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        asal = rs(0)
    End If
    rs.Close
    conn.Close
    If simpan Then
        subtotal1 = 0
        diskon = 0
        For counter = 1 To flxGrid.rows - 1
        If flxGrid.TextMatrix(counter, 1) <> "" Then
            subtotal1 = subtotal1 + (flxGrid.TextMatrix(counter, 5) * flxGrid.TextMatrix(counter, 3))
        End If
        Next
        Set frmBayar.parent = Me
        frmBayar.lblTotal3 = Format(subtotal1, "#,##0")
        frmBayar.lblDiskonDetil = Format(subtotal1 - lblTotal, "#,##0")
        frmBayar.lblTotal = lblTotal
        frmBayar.lblpk = "Eceran"
        frmBayar.lblNoTrans = lblNoTrans
        frmBayar.NoTrans2 = NoTrans2
        frmBayar.lblAsal = Format(asal, "#,##0")
        
        frmBayar.Show
        Me.Enabled = False
        frmBayar.txtTunai.SetFocus
        frmBayar.cmdSimpanCetak.Enabled = True
    End If
Exit Sub
err:

End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    
    lblHarga = "0"
    lblNmBarang = ""
    lblQuantity = ""
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
End Sub

Private Sub cmdDelete_Click()
On Error GoTo err
Dim row, col As Integer
    If flxGrid.TextMatrix(flxGrid.row, 1) = "" Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    row = flxGrid.row
    qty = qty - flxGrid.TextMatrix(row, 3)
    total = total - (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
    If Opt1(0).value = True Then lblDisc = txtDisc.text Else lblDisc = Format(txtDisc * total / 100, "#,##0")
    lblQty = Format(qty, "#,##0")
    lblTotal = Format(total, "#,##0")
    lblSisa = Format(total - lblDisc, "#,##0")
    lblSubTotal = lblSisa
    For row = row To flxGrid.rows - 1
        If row = flxGrid.rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1
    currentRow = flxGrid.row
'    flxGrid.ColSel = 6
    flxGrid.rows = flxGrid.rows - 1
    lblItem = flxGrid.rows - 2
    txtKdBrg.text = ""
    lblSatuan = ""
    txtQty.text = ""
    harga_beli = 0
    harga_jual = 0
    harga = 0
    lblKode = ""
    LblNamaBarang = ""
'    txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub addbonus(kdbrg As String, qt As Integer)
Dim row As Integer
Dim promosi As Currency
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
    row = 0
    For i = 1 To flxGrid.rows - 2
        If (flxGrid.TextMatrix(i, 1) = kdbrg And flxGrid.TextMatrix(i, 5) = "0") Then row = i
    Next
    If row = 0 Then
        row = flxGrid.rows - 1
        flxGrid.rows = flxGrid.rows + 1
    End If
    flxGrid.TextMatrix(row, 1) = kdbrg
    If flxGrid.TextMatrix(row, 3) = "" Then
        flxGrid.TextMatrix(row, 3) = qt
    Else
        qty = qty - (flxGrid.TextMatrix(row, 3))
        flxGrid.TextMatrix(row, 3) = qt
    End If
    flxGrid.TextMatrix(row, 4) = 0
    flxGrid.TextMatrix(row, 5) = 0
    flxGrid.TextMatrix(row, 6) = 0
    qty = qty + (flxGrid.TextMatrix(row, 3))
    
    flxGrid.TextMatrix(row, 7) = 0
    flxGrid.TextMatrix(row, 8) = 0
    flxGrid.TextMatrix(row, 9) = 0
    flxGrid.TextMatrix(row, 10) = 1
    flxGrid.TextMatrix(row, 11) = 0
    flxGrid.TextMatrix(row, 12) = 0
    flxGrid.TextMatrix(row, 13) = 0

    
    conn.Open strcon
    
    rs.Open "select * from ms_barang where [kode barang]='" & kdbrg & "'", conn
    If Not rs.EOF Then
        flxGrid.TextMatrix(row, 2) = rs(1)
    End If
    rs.Close
    conn.Close
End Sub
Private Function cekpromosi(kdbrg As String, qty As Integer) As Currency
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection

    
    conn.Open strcon
    
    rs.Open "select * from ms_promosi p inner join ms_barang b on p.[kategori]=b.kategori where [kode barang]='" & kdbrg & "' and [tanggal dari]<=#" & Format(Now, "MM/dd/yyyy") & "# and ([fg_tanggal sampai]='0' or [tanggal sampai]>=#" & Format(Now, "MM/dd/yyyy") & "#) and (fg_jam='0' or ([jam dari]<=#" & Format(Now, "hh:mm") & "# and [jam sampai]>= #" & Format(Now, "hh:mm") & "#))", conn
    If Not rs.EOF Then
        If rs("rule") = 1 Then
            Dim total As Integer
            total = qty \ rs("qty1")
            cekpromosi = 0
            If total > 0 Then addbonus rs("kode barang2"), total * rs("qty2")
        ElseIf rs("rule") = 2 Then
            If qty >= rs("qty1") Then
                If rs("disc_tipe") = "1" Then
                    cekpromosi = rs("disc")
                Else
                    cekpromosi = harga_jual * rs("disc") / 100
                End If
            End If
        End If
    End If
    rs.Close
    conn.Close
End Function


Private Sub cmdNew_Click()
    reset_form
End Sub

Private Sub cmdOK_Click()
On Error GoTo err
Dim i As Integer
Dim row As Integer
Dim promosi As Currency
Dim kategori As String
Dim step As Byte
    row = 0
    step = 0 'tracer, begin
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        step = 1 'tracer, define row number
        If mode = 2 Then
            row = flxGrid.row
        Else
            For i = 1 To flxGrid.rows - 2
                If LCase(flxGrid.TextMatrix(i, 1)) = LCase(txtKdBrg) Then
                    row = i
                    Exit For
                End If
            Next
        End If
        If row = 0 Then
            row = flxGrid.rows - 1
            flxGrid.rows = flxGrid.rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row
        step = 2 'tracer, begin adding data to flexgrid
        kategori = getkategori(txtKdBrg, strcon)
        flxGrid.TextMatrix(row, 1) = lblKode
        kodebarang = lblKode
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            qty = qty - (flxGrid.TextMatrix(row, 3))
            Subtotal = Subtotal - (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
            total = total - (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
            If mode = 1 Then
            flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
            flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        If flxGrid.TextMatrix(row, 3) >= grosir And grosir > 0 Then
            harga_jual = harga2
        Else
            harga_jual = harga1
        End If
            txtHarga = harga_jual
        
        
        flxGrid.TextMatrix(row, 4) = lblSatuan
        promosi = IIf(chkpromosi, cekpromosi(flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3)), 0)
        If mode = 1 Then
        If promosi > 0 Then
        txtHarga = harga_jual - promosi
        End If

        flxGrid.TextMatrix(row, 12) = txtDisc
        flxGrid.TextMatrix(row, 13) = IIf(Opt1(0).value, "1", "2")
        
        
        
        
        End If
        
        If flxGrid.TextMatrix(row, 12) >= 0 And flxGrid.TextMatrix(row, 13) > 0 Then
        txtHarga = harga_jual - IIf(flxGrid.TextMatrix(row, 13) = "1", flxGrid.TextMatrix(row, 12), harga_jual * flxGrid.TextMatrix(row, 12) / 100)
        End If

        flxGrid.TextMatrix(row, 5) = Format(txtHarga, "#,##0")
        flxGrid.TextMatrix(row, 6) = Format(flxGrid.TextMatrix(row, 3) * txtHarga, "#,##0")
        qty = qty + (flxGrid.TextMatrix(row, 3))
        total = total + (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
        lblQty = Format(qty, "#,##0")
        lblItem = flxGrid.rows - 2
'        If Opt1(0).value = True Then lblDisc = txtDisc.text Else lblDisc = Format(txtDisc * total / 100, "#,##0")
        lblTotal = Format(total, "#,##0")
        lblSisa = Format(total - lblDisc, "#,##0")
        lblSubTotal = lblSisa
        
        flxGrid.TextMatrix(row, 7) = promosi
        
        flxGrid.TextMatrix(row, 4) = Format(harga_jual, "#,##0")
        flxGrid.TextMatrix(row, 8) = grosir
        flxGrid.TextMatrix(row, 9) = harga1
        
        flxGrid.TextMatrix(row, 10) = harga2
        
        flxGrid.TextMatrix(row, 11) = 0
        
        flxGrid.col = 1
'        flxGrid.ColSel = 6
        If row > 14 Then
            flxGrid.TopRow = row - 13
        Else
            flxGrid.TopRow = 1
        End If
        step = 3 'tracer, clear textboxes
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        txtDisc.text = "0"
        txtHarga.text = "0"
        harga = 0
        harga_jual = 0
        disc = 0
        disc1 = 0
        disc2 = 0
        disc3 = 0
        disc4 = 0
        disc5 = 0
        disc_tipe = 0
        disc1_tipe = 0
        disc2_tipe = 0
        disc3_tipe = 0
        disc4_tipe = 0
        disc5_tipe = 0
        lblKode = ""
'        txtKdBrg.SetFocus
        lblQuantity = flxGrid.TextMatrix(row, 3) & " x " & Format(flxGrid.TextMatrix(row, 5), "#,##0") & "  = Rp. " & Format(flxGrid.TextMatrix(row, 6), "#,##0")
        
    End If
    step = 4 'tracer, finished add process
    mode = 1
    If cmdClear.Enabled = True Then
    cmdClear_Click
    LblNamaBarang = flxGrid.TextMatrix(row, 2)
    lblQuantity = flxGrid.TextMatrix(row, 3) & " " & flxGrid.TextMatrix(row, 4) & " x " & Format(flxGrid.TextMatrix(row, 5), "#,##0") & "  = Rp. " & Format(flxGrid.TextMatrix(row, 6), "#,##0")
    End If
    Exit Sub
err:
    If step >= 3 Then
        mode = 1
        If cmdClear.Enabled = True Then cmdClear_Click
    End If
    MsgBox err.Description
End Sub

Private Sub cmdReset_Click()
    reset_form
    txtKdBrg.SetFocus
End Sub

Private Sub cmdReprint_Click()
    frmReprint.tipe = "Eceran"
    frmReprint.Show vbModal
    
End Sub

Private Sub cmdSearchCust_Click()
    frmSearch.connstr = strcon
    frmSearch.query = querycust
    frmSearch.nmform = "frmAddPenjualan"
    frmSearch.nmctrl = "txtKodeCustomer"
    frmSearch.keyIni = "ms_customer"
'    frmSearch.txtSearch.text = txtKodeCustomer.text
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_customer"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    
End Sub

Private Sub cmdSearchID_Click()
    search_id
    
End Sub
Public Sub search_id()
On Error Resume Next
    
        frmSearch.connstr = strcon
    
'    frmSearch.query = queryJual & " where convert(varchar(12),tanggal,101)='" & Format(Now, "MM/dd/yyyy") & "'"
    frmSearch.query = queryJual
    frmSearch.chkDate.Visible = True
    frmSearch.DTPicker1.Visible = True
    frmSearch.DTPicker1 = Now
    frmSearch.chkDate.value = 1
    
    frmSearch.nmform = "frmAddPenjualan"
    frmSearch.nmctrl = "lblNoTrans"
'    frmSearch.connstr = strcon
    frmSearch.col = 1
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
    
    
End Sub

Private Sub cmdSearchItem_Click()
    frmSearch.connstr = strcon
    If LCase(group) = "owner" Then
    frmSearch.query = querybrg
    Else
    frmSearch.query = querybrg
    End If
    frmSearch.nmform = "frmAddPenjualan"
    frmSearch.nmctrl = "txtKdBrg"
'    frmSearch.txtSearch.text = txtKdBrg.text
    frmSearch.keyIni = "ms_barang"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
    If flxGrid.rows <= 2 Then
        MsgBox "Silahkan masukkan Barang yang akan dibeli terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
    If lblTotalAsal.Visible Then
        If lblTotal < lblTotalAsal Then
            MsgBox "Total Koreksi harus sama atau lebih besar daripada total sebelumnya"
            Exit Sub
        End If
    End If
    If simpan Then cmdReset_Click
'validasi
'    If chkGuling = "1" Then
'        If txtNoRetur.text = "" Then
'            MsgBox "Nomor Retur harus diisi"
'            txtNoRetur.SetFocus
'            Exit Sub
'        Else
'            conn.ConnectionString = strcon
'            conn.Open
'            rs.Open "select * from t_returjualh where id='" & txtNoRetur.text & "'", conn
'            If rs.EOF Then
'                MsgBox "Nomor Retur yang anda masukkan tidak dapat ditemukan"
'                txtNoRetur.SetFocus
'                rs.Close
'                conn.Close
'                Exit Sub
'            End If
'            rs.Close
'            conn.Close
'        End If
'    End If
End Sub
Private Function simpan() As Boolean
Dim i As Byte
Dim id As String
Dim counter As Integer
Dim waktu As String
Dim error_retries As Integer
Dim error As Integer
On Error GoTo err
    i = 0
    
    error_retries = 10
    error = 0
    simpan = False
    If flxGrid.rows <= 2 Then
        MsgBox "Silahkan masukkan barang yang mau dibeli terlebih dulu"
        simpan = False
        Exit Function
'        frmPassword.Show vbModal
'        If Not frmPassword.right_harga Then Exit Function
    End If
'proses
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    id = lblNoTrans

    If lblNoTrans = "-" Then
        
        nomor_baru
        
        add_dataheader 1
    Else
        add_dataheader 2
        rs.Open "select * from t_penjualand where id='" & lblNoTrans & "'", conn
        While Not rs.EOF
             updatestock gudang, rs("kode barang"), rs("qty"), conn
        rs.MoveNext
        Wend
        rs.Close
        deletekartustock "Penjualan", lblNoTrans, conn
        conn.Execute "delete from t_penjualand where ID='" & lblNoTrans & "'"
        
    End If
    
    
    counter = 0
    no_urut = 0
    waktu = Format(Right(lblhari, Len(lblhari) - InStr(lblhari, ",")), "MM/dd/yy") & " " & Format(lbljam, "hh:mm:ss")
    For counter = 1 To flxGrid.rows - 1
        If flxGrid.TextMatrix(counter, 1) <> "" Then
            add_datadetail counter
            insertkartustock "Penjualan", lblNoTrans, Now, flxGrid.TextMatrix(counter, 1), flxGrid.TextMatrix(counter, 3) * -1, CStr(gudang), txtKeterangan, conn

            updatestock gudang, flxGrid.TextMatrix(counter, 1), flxGrid.TextMatrix(counter, 3) * -1, conn
            
            no_urut = no_urut + 1
        End If
        
    Next
    
    conn.CommitTrans
    
    i = 0
    simpan = True
    
    DropConnection

    Exit Function
err:
'    If error > error_retries Then
        If i = 1 Then
            conn.RollbackTrans
            
        End If
        simpan = False
        If rs.State Then rs.Close
        DropConnection
        If id <> lblNoTrans.Caption Then lblNoTrans.Caption = id
        MsgBox err.Description
'    Else
'        error = error + 1
'        Resume
'    End If
End Function
Public Sub loadcombo()
Dim conn As New ADODB.Connection
'    conn.mode = adModeShareDenyNone
    
    conn.ConnectionString = strcon
    
    conn.Open
    cmbNoTrans.Clear
    rs.Open "select id from t_penjualanh where bayar='0' order by id", conn
    While Not rs.EOF
        cmbNoTrans.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbSales.Clear
    rs.Open "select * from ms_sales order by [kode sales]", conn
    While Not rs.EOF
        cmbSales.AddItem rs(0) & "-" & rs(1)
        rs.MoveNext
    Wend
    rs.Close
    ReDim cekbonus(0)
    rs.Open "select * from ms_bonus", conn
    While Not rs.EOF
        ReDim Preserve cekbonus(UBound(cekbonus) + 1)
        cekbonus(UBound(cekbonus) - 1).kategori = rs(1)
        cekbonus(UBound(cekbonus) - 1).batas = rs(2)
        cekbonus(UBound(cekbonus) - 1).hadiah = rs(3)
        cekbonus(UBound(cekbonus) - 1).total = 0
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub
Public Sub reset_form()
On Error GoTo err
    lblNoTrans = "-"
    flxGrid.rows = 1
    flxGrid.rows = 2
    loadcombo

    Timer1.Interval = 1000
    lblhari = Format(Now, "dddd, dd - MMMM - yyyy")
    cmbHarga.Locked = False
    If right_TipeHarga = 1 Then cmbHarga.ListIndex = 0 Else cmbHarga.ListIndex = 1
    chkKirim.value = 0
    txtKeterangan.text = ""
    txtKodeCustomer.text = ""
    cek_customer
    txtDiscTotal.text = "0"
    txtDisc.text = "0"
    Label15.Visible = False
    lblTotalAsal.Visible = False
    lblTotalAsal = 0
    lblDisc = "0"
    lblQuantity = ""
    lblItem = "0"
    lblNmBarang = ""
    lblTotal = "0,00"
    lblSisa = "0,00"
    lblSubTotal = "0,00"
    Subtotal = 0
    qty = 0
    lblQty = "0"
    lblKode = ""
    LblNamaBarang = ""
    cmdClear_Click
    total = 0
    retur = 0
    
    mode = 1
    cmbSales.SetFocus
    Exit Sub
err:
    Resume Next
    'MsgBox err.Description
End Sub
Private Sub add_dataheader(action As Byte)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(11)
    ReDim nilai(11)
    
    table_name = "T_PENJUALANH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "[kode customer]"
    fields(3) = "gudang"
    fields(4) = "keterangan"
'    fields(5) = "total"
    fields(5) = "disc_tipe"
    fields(6) = "disc"
    fields(7) = "disc_rp"
    fields(8) = "[userid]"
    fields(9) = "pk"
    fields(10) = "sales"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(Right(lblhari, Len(lblhari) - InStr(lblhari, ",")) & " " & lbljam, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKodeCustomer.text
    nilai(3) = gudang
    nilai(4) = ""
'    nilai(5) = lblTotal
    nilai(5) = IIf(Opt1(0).value = True, "1", "2")
    nilai(6) = txtDiscTotal.text
    nilai(7) = IIf(Opt1(0).value = True, txtDiscTotal, txtDiscTotal * total / 100)
    nilai(8) = user
    nilai(9) = "Eceran"  'NoTrans2
    nilai(10) = ""
    If Len(cmbSales.text) > 0 Then nilai(10) = Left(cmbSales.text, InStr(cmbSales.text, "-") - 1)
    
    
    If action = 1 Then
        conn.Execute tambah_data2(table_name, fields, nilai)
        
    ElseIf action = 2 Then
        update_data table_name, fields, nilai, " id='" & lblNoTrans & "'", conn
        
        
    End If
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(12)
    ReDim nilai(12)
    
    table_name = "T_PENJUALAND"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "harga"
    fields(4) = "qtygrosir"
    fields(5) = "harga1"
    fields(6) = "harga2"
    fields(7) = "disc2"
    fields(8) = "disc2_tipe"
    fields(9) = "urut"
    fields(10) = "hpp"
    fields(11) = "promosi"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = Format(flxGrid.TextMatrix(row, 3), "###0.####")
    nilai(3) = Format(flxGrid.TextMatrix(row, 4), "###0.####")
    nilai(4) = flxGrid.TextMatrix(row, 8)
    nilai(5) = flxGrid.TextMatrix(row, 9)
    nilai(6) = flxGrid.TextMatrix(row, 10)
    nilai(7) = flxGrid.TextMatrix(row, 12)
    nilai(8) = flxGrid.TextMatrix(row, 13)
    nilai(9) = row
    nilai(10) = getHpp(flxGrid.TextMatrix(row, 1), strcon)
    nilai(11) = 0 'flxgrid.TextMatrix(row, 7)
    conn.Execute tambah_data2(table_name, fields, nilai)
    table_name = "hst_penjualand"
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Public Sub cek_notrans()
On Error GoTo err
Dim row As Integer
    
    conn.ConnectionString = strcon
    
    conn.Open
        rs.Open "select * from t_penjualanh where id='" & lblNoTrans & "'", conn
        If Not rs.EOF Then
            NoTrans2 = rs("pk")
            lblhari = Format(rs("tanggal"), "dddd, dd - MMMM - yyyy")
            lbljam = Format(rs("tanggal"), "hh : mm : ss")
            Timer1.Interval = 0
            
            txtKodeCustomer.text = rs("kode customer")
            txtKeterangan.text = rs("keterangan")
            SetComboTextLeft rs!sales, cmbSales
            cek_customer
            If Left(lblNoTrans, 2) = "TP" Then
                cmdBatal.Visible = True
            Else
                cmdBatal.Visible = False
            End If
            If rs("disc_tipe") = "1" Then Opt1(0).value = True Else Opt1(1).value = True
            txtDiscTotal.text = rs("disc")
            lblDisc = rs("disc_rp")
            If rs("bayar") = 1 Then
            Label15.Visible = True
            lblTotalAsal.Visible = True
            
            End If
            rs.Close
            flxGrid.rows = 1
            flxGrid.rows = 2
            total = 0
            
            qty = 0
            row = 1
            
            rs.Open "select d.[kode barang],m.[nama barang],qty,m.satuan, d.harga,d.qtygrosir,d.harga1,d.harga2,d.disc2,d.disc2_tipe,promosi from t_penjualand d inner join ms_barang m on d.[kode barang]=m.[kode barang] where d.id='" & lblNoTrans & "' order by urut", conn
            While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
                flxGrid.TextMatrix(row, 4) = Format(rs(4), "#,##0")
                flxGrid.TextMatrix(row, 5) = rs(4) - IIf(rs("disc2_tipe") = "0", IIf(rs("promosi") = 0, 0, rs("promosi")), IIf(rs("disc2_tipe") = "1", rs("disc2"), rs(4) * rs("disc2") / 100))
                flxGrid.TextMatrix(row, 6) = flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5)
                flxGrid.TextMatrix(row, 5) = Format(flxGrid.TextMatrix(row, 5), "#,##0")
                flxGrid.TextMatrix(row, 6) = Format(flxGrid.TextMatrix(row, 6), "#,##0")
                flxGrid.TextMatrix(row, 7) = rs(10)
                flxGrid.TextMatrix(row, 8) = rs(5)
                flxGrid.TextMatrix(row, 9) = rs(6)
                flxGrid.TextMatrix(row, 10) = rs(7)
                flxGrid.TextMatrix(row, 11) = 0 'rs(7)
                flxGrid.TextMatrix(row, 12) = rs(8)
                flxGrid.TextMatrix(row, 13) = rs(9)
                qty = qty + rs(2)
                total = total + flxGrid.TextMatrix(row, 6)
                row = row + 1
                flxGrid.rows = flxGrid.rows + 1
                rs.MoveNext
            Wend
            If rs.State Then rs.Close
            'If Opt1(0).Value = True Then lblDisc = txtDisc.text Else lblDisc = Format(txtDisc * total / 100, "#,##0")
            lblTotal = Format(total, "#,##0")
            'lblSisa = Format(total - lblDisc, "#,##0")
            lblSubTotal = lblTotal
            lblQty = Format(qty, "#,##0")
            'hitung_total
            lblTotalAsal = lblTotal
            lblItem = flxGrid.rows - 2
        End If
        If rs.State Then rs.Close
    DropConnection
    Exit Sub
err:
    If rs.State Then rs.Close
    DropConnection
    MsgBox err.Description
End Sub

Public Function cek_kodebarang1() As Boolean
On Error GoTo err
Dim harga As Long
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
Dim kode As String
Dim kdbrg As String
    
    conn.ConnectionString = strcon
    
    conn.Open
    kode = ""
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select distinct * from ms_barang where [kode barang]='" & kdbrg & "' and flag='1'", conn
    If Not rs.EOF Then
        If kode = "" Then kode = rs("kode barang")
        lblKode = kode
        lblSatuan = rs("satuan")
        harga1 = rs("harga1")
        harga2 = rs("harga2")
        grosir = rs("qtygrosir")
        txtDisc.text = rs("disc")
        Opt1(1).value = True
        
'        cmbHarga_Click
            txtHarga = harga1
        LblNamaBarang = rs(1)
        lblHarga = txtHarga.text
        If txtQty.text = "" Or Not IsNumeric(txtQty.text) Then txtQty.text = "1"
        Dim rs1 As New ADODB.Recordset
        Dim hrg As Long
        hrg = harga_jual
        
        lblNmBarang = LblNamaBarang
        If txtQty.text <> "" Then
            lblQuantity = txtQty.text & "  x " & Format(txtHarga, "#,##0") & "  = Rp. " & Format(((txtQty) * txtHarga), "#,##0")
        Else
            lblQuantity = ""
        End If
        cek_kodebarang1 = True
        If rs1.State Then rs1.Close

    Else
        LblNamaBarang = ""
        lblSatuan = ""
        disc1_tipe = 0
        disc2_tipe = 0
        disc3_tipe = 0
        disc4_tipe = 0
        disc5_tipe = 0
        disc1 = 0
        disc2 = 0
        disc3 = 0
        disc4 = 0
        disc5 = 0
        harga_jual = 0
        harga = 0
        cek_kodebarang1 = False
    End If
    rs.Close
    conn.Close
    
    Exit Function
err:
    DropConnection
    If rs1.State Then rs1.Close
    If rs.State Then rs.Close
    If err.Description <> "" Then MsgBox err.Description
End Function
Public Function cek_customer() As Boolean
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
cek_customer = False
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from ms_customer where [kode customer]='" & txtKodeCustomer.text & "' ", conn
    If Not rs.EOF Then
        cek_customer = True
        txtNamaPelanggan = rs(1)
        cmbHarga.Locked = True

        For i = 1 To flxGrid.rows - 2
            mode = 2
            flxGrid.row = i
            txtKdBrg.text = flxGrid.TextMatrix(i, 1)
            cek_kodebarang1
            txtQty = flxGrid.TextMatrix(i, 3)
'            txtHarga = flxGrid.TextMatrix(i, 4)
            SetComboText rs("harga"), cmbHarga
            If flxGrid.TextMatrix(i, 13) = "1" Then
                Opt1(0).value = True
            Else
                Opt1(1).value = True
            End If
            txtDisc = flxGrid.TextMatrix(i, 12)
            cmdOK_Click
        Next
        SetComboText rs("harga"), cmbHarga
'        Me.Enabled = True
    Else
        cek_customer = False
        txtNamaPelanggan = ""
        cmbHarga.Locked = False
        
    End If
    rs.Close
    conn.Close
    Exit Function
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Function
Public Sub fokuskodebarang()
On Error Resume Next
    txtKdBrg.SetFocus
    txtKdBrg.SelStart = Len(txtKdBrg)
End Sub

Private Sub hitung_ulang()
Dim counter As Integer
    counter = 0
    For counter = 1 To flxGrid.rows - 1
        If flxGrid.TextMatrix(counter, 1) <> "" Then
            flxGrid.col = 1
            flxGrid.row = counter
            flxGrid_Click
            cek_kodebarang1
            cmdOK_Click
        End If
    Next
End Sub

Private Sub cmdSimpan_GotFocus()
'    total_belanja
End Sub

Private Sub flxGrid_Click()
On Error GoTo err
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
'        mode = 2
'        currentRow = flxGrid.row
        kodebarang = flxGrid.TextMatrix(flxGrid.row, 1)
'        cmdClear.Enabled = True
'        cmdDelete.Enabled = True
'        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
'        cek_kodebarang1
'
'        LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
'        lblNmBarang = flxGrid.TextMatrix(flxGrid.row, 2)
'        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
'        lblSatuan = flxGrid.TextMatrix(flxGrid.row, 4)
'        SetComboText flxGrid.TextMatrix(flxGrid.row, 9), cmbHarga
'        'lblHarga = flxGrid.TextMatrix(flxGrid.row, 8)
'        lblQuantity = txtQty.text & "  x " & Format(harga, "#,##0") & "  = Rp. " & Format(((txtQty) * harga), "#,##0")
'
    Else
        mode = 1
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
       cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" And LCase(right_TipeHarga) <> "eceran" Then

            loaddetiljual
        End If
    End If
End Sub


Private Sub Form_Activate()
'    mySendKeys "{tab}"

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchItem_Click
    If KeyCode = vbKeyF4 Then cmdSearchCust_Click
    If KeyCode = vbKeyF6 Then cmdReprint_Click
    If KeyCode = vbKeyF2 Then cmdBayar_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = vbKeyReturn And Not detail Then
        MySendKeys "{tab}"
        KeyAscii = 0
    End If
    
End Sub

Private Sub Form_Load()
On Error GoTo err
    reset_form
    Me.Width = frmMain.Width - 300
    Me.Height = frmMain.Height - 1500
    total = 0
    lbljam = Format(Now, "hh : mm : ss")
    lblhari = Format(Now, "dddd, dd - MMMM - yyyy")
    
    sizeFlxGrid "select d.[kode barang],m.[nama barang],qty, d.harga,d.harga as stlhdisc,d.harga1 as total from t_penjualand d inner join ms_barang m on d.[kode barang]=m.[kode barang] ", flxGrid, "FlxPenjualan", 1
'
    flxGrid.ColWidth(0) = 350
'    flxGrid.ColWidth(1) = 2200
'    flxGrid.ColWidth(2) = 5200
'    flxGrid.ColWidth(3) = 600
'    flxGrid.ColWidth(4) = 1600
'
'    flxGrid.ColWidth(6) = 1600
    
'    flxGrid.ColWidth(5) = 1400
    flxGrid.ColWidth(7) = 1
    flxGrid.ColWidth(8) = 1
    flxGrid.ColWidth(9) = 1
    flxGrid.ColWidth(10) = 1
    flxGrid.ColWidth(11) = 1
    flxGrid.ColWidth(12) = 1
    flxGrid.ColWidth(13) = 1

    flxGrid.TextMatrix(0, 1) = "Kode"
    flxGrid.TextMatrix(0, 2) = "Nama"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Harga"
    flxGrid.TextMatrix(0, 5) = "Harga"
    flxGrid.TextMatrix(0, 6) = "Jumlah"
    flxGrid.TextMatrix(0, 7) = "Promosi"
    flxGrid.TextMatrix(0, 8) = "grosir"
    flxGrid.TextMatrix(0, 9) = "harga1"
    flxGrid.TextMatrix(0, 10) = "harga2"
    flxGrid.TextMatrix(0, 11) = "harga"
    flxGrid.TextMatrix(0, 12) = "disc"
    flxGrid.TextMatrix(0, 13) = "disc_tipe"
    flxGrid.ColAlignment(1) = 1
    flxGrid.ColAlignment(2) = 1
    printproperties = False
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
   
    resizeFlxGrid "select d.[kode barang],m.[nama barang],qty, d.harga,d.harga2 as stlhdisc,d.harga1 as total from t_penjualand d inner join ms_barang m on d.[kode barang]=m.[kode barang] ", flxGrid, "FlxPenjualan", 1

End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuKirim_Click()
    'cmdKirim_Click
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdBayar_Click
End Sub

Private Sub Opt1_Click(Index As Integer)
'    If Index = 1 Then
'        If txtDisc.text > 100 Then txtDisc = "0"
'        lblDisc.Visible = True
'    Else
'        lblDisc.Visible = False
'    End If
End Sub

Private Sub Timer1_Timer()
    lbljam = Format(Now, "hh : mm : ss")
End Sub

Private Sub txtDisc_Change()
    'hitung_total
End Sub

Private Sub total_belanja()
On Error Resume Next
    lblNmBarang = "Total Belanja"
    lblQuantity = "Rp. " & Format(lblSubTotal, "#,##0")
End Sub

Private Sub txtDiscTotal_LostFocus()
    If Opt1(0).value = True Then
        If txtDiscTotal > total Then
            txtDiscTotal.SetFocus
            MsgBox "diskon tidak boleh lebih daripada total penjualan"
        End If
    Else
        If CDbl(txtDiscTotal) > 100 Then
            txtDiscTotal.SetFocus
            MsgBox "diskon persen tidak boleh lebih daripada 100%"
        End If
    End If
    If Opt1(0).value = True Then lblDisc = txtDiscTotal.text Else lblDisc = Format(txtDiscTotal * total / 100, "#,##0")
    lblSisa = Format(total - lblDisc, "#,##0")
    lblSubTotal = lblSisa
End Sub


Private Sub txtKdBrg_GotFocus()
    detail = True
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo err
    If KeyCode = vbKeyUp Then
        If flxGrid.row > 1 Then
            flxGrid.row = flxGrid.row - 1
'            flxGrid.ColSel = 6
        End If
        currentRow = flxGrid.row
'        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then flxGrid.TextMatrix(flxGrid.row, 0) = "-"
        
    ElseIf KeyCode = vbKeyDown Then

        If flxGrid.row < flxGrid.rows - 2 Then
            flxGrid.row = flxGrid.row + 1
'            flxGrid.ColSel = 6
        End If
        currentRow = flxGrid.row
'        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then flxGrid.TextMatrix(flxGrid.row, 0) = "-"
    End If
        
'    If KeyCode = vbKeyF5 And kodebarang <> "" Then
'        Dim i As Integer
'        Dim row As Integer
'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = kodebarang Then row = i
'        Next
'        mode = 2
'        flxGrid.row = row
'        cmdClear.Enabled = True
'        cmdDelete.Enabled = True
'        txtKdBrg.text = flxGrid.TextMatrix(row, 1)
'        cek_kodebarang1
'        LblNamaBarang = flxGrid.TextMatrix(row, 2)
'        lblNmBarang = flxGrid.TextMatrix(row, 2)
'        txtQty.text = flxGrid.TextMatrix(row, 3)
'        lblSatuan = flxGrid.TextMatrix(row, 4)
'        txtHarga.text = flxGrid.TextMatrix(row, 6)
'        lblHarga = harga
'        txtQty.SetFocus
'        txtQty.SelStart = 0
'        txtQty.SelLength = Len(txtQty.text)
'        lblQuantity = txtQty.text & "  x " & Format(txtHarga.text, "#,##0") & "  = Rp. " & Format(((txtQty) * (txtHarga.text)), "#,##0")
'    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Public Sub loaddetiljual()
    Me.Enabled = False
    With frmDetilJual
    Set .parent = Me
    .top = flxGrid.top + ((flxGrid.row - flxGrid.TopRow) * 350) + 350
    .Left = flxGrid.Left + (flxGrid.Width / 3)
    .Show
    .txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
    .txtHarga = flxGrid.TextMatrix(flxGrid.row, 4)
'    .txtHarga.AddItem Format(flxGrid.TextMatrix(flxGrid.row, 9), "#,##0")
'    .txtHarga.AddItem Format(flxGrid.TextMatrix(flxGrid.row, 10), "#,##0")
'
'    SetComboText flxGrid.TextMatrix(flxGrid.row, 4), .txtHarga
    End With
    If LCase(flxGrid.TextMatrix(flxGrid.row, 2)) = "penyesuaian" Then frmDetilJual.txtHarga.Enabled = True
    
    frmDetilJual.txtDisc.text = IIf(flxGrid.TextMatrix(flxGrid.row, 13) > 0, flxGrid.TextMatrix(flxGrid.row, 12), IIf(flxGrid.TextMatrix(flxGrid.row, 7) > 0, flxGrid.TextMatrix(flxGrid.row, 7), flxGrid.TextMatrix(flxGrid.row, 11)))
    If flxGrid.TextMatrix(flxGrid.row, 13) = "1" Then
        frmDetilJual.Opt1(0).value = True
    ElseIf flxGrid.TextMatrix(flxGrid.row, 13) = "2" Then
        frmDetilJual.Opt1(1).value = True
    ElseIf flxGrid.TextMatrix(flxGrid.row, 13) = "0" Then
        If flxGrid.TextMatrix(flxGrid.row, 7) > 0 Then
        frmDetilJual.Opt1(0).value = True
        Else
        If flxGrid.TextMatrix(flxGrid.row, 10) = "1" Then
        frmDetilJual.Opt1(0).value = True
        Else
        frmDetilJual.Opt1(1).value = True
        End If
        End If
    End If

End Sub
Public Sub search_cari()
    cek_kodebarang1
    cmdOK_Click
End Sub
Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error GoTo err
    Dim row As Integer
    If KeyAscii = 13 Then
    If txtKdBrg.text = "" Then
'        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
'            loaddetiljual
'        End If
'        KeyAscii = 0
    ElseIf Left(txtKdBrg.text, 1) = "*" Then
        Dim i As Integer
        
        Dim qty As Integer
        qty = Right(txtKdBrg.text, Len(txtKdBrg.text) - 1)
'        If CInt(qty) < 1 Then
'            MsgBox "Quantity tidak boleh minus"
'            Exit Sub
'        End If
        row = flxGrid.row
'        For i = 1 To flxGrid.Rows - 1
'            If flxGrid.TextMatrix(i, 1) = kodebarang And flxGrid.TextMatrix(i, 5) > 0 Then
'                row = i
'                Exit For
'            End If
'        Next
        txtKdBrg.text = flxGrid.TextMatrix(row, 1)
        cek_kodebarang1
        txtQty.text = qty
        mode = 2
        cmdOK_Click
    ElseIf Left(txtKdBrg.text, 1) = "/" Then


        Dim disc As Double
        disc = Right(txtKdBrg.text, Len(txtKdBrg.text) - 1)
        If CInt(disc) < 0 Or CInt(disc) > 100 Then
            MsgBox "Diskon tidak boleh minus dan tidak boleh lebih dari 100"
            Exit Sub
        End If
        row = flxGrid.row
        txtKdBrg.text = flxGrid.TextMatrix(row, 1)
        txtQty.text = flxGrid.TextMatrix(row, 3)
        cek_kodebarang1
        flxGrid.TextMatrix(row, 12) = disc
        flxGrid.TextMatrix(row, 13) = 2
        mode = 2
        cmdOK_Click
    Else
        cek_kodebarang1
        If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        kodebarang = txtKdBrg.text
'        If InStr(1, txtKdBrg.text, "*") > 0 Then
'            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
'            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
'        End If
        cmdOK_Click
        
        txtKdBrg.SetFocus
        Else
        MsgBox "Kode yang anda masukkan belum terdaftar atau tidak aktif"
        txtKdBrg.SelStart = 0
        txtKdBrg.SelLength = Len(txtKdBrg.text)
        End If
    End If
    End If
    Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If txtKdBrg.text <> "" Then
    If LblNamaBarang = "" Then
    If cek_kodebarang1 = True Then
        kodebarang = txtKdBrg.text
        kodebarang = lblKode
        'cmdOK_Click
        'txtKdBrg.SetFocus
    Else
'        MsgBox "Kode yang anda masukkan belum terdaftar atau harga nya Nol"
'        txtKdBrg.SetFocus
'        txtKdBrg.SelStart = 0
'        txtKdBrg.SelLength = Len(txtKdBrg.Text)
    End If
    Else
        kodebarang = txtKdBrg.text
        kodebarang = lblKode
    End If
    End If
    detail = False
End Sub

Private Sub txtKeterangan_GotFocus()
    total_belanja
End Sub

Private Sub txtKodeCustomer_GotFocus()
    txtKodeCustomer.SelStart = 0
    txtKodeCustomer.SelLength = Len(txtKodeCustomer.text)
End Sub

Private Sub txtKodeCustomer_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyDown = 13 Then cek_customer
End Sub

Private Sub txtKodeCustomer_LostFocus()
On Error Resume Next
    If Not cek_customer And txtKodeCustomer.text <> "" Then
        MsgBox "Kode yang anda masukkan salah atau sudah kadaluarsa"
        txtKodeCustomer.SetFocus
    Else
        txtKdBrg.SetFocus
    End If
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub


Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdOK_Click
End Sub

Private Sub txtQty_LostFocus()
On Error GoTo err
    If txtQty.text <> "" And IsNumeric(txtQty.text) And IsNumeric(harga) Then
    lblQuantity = txtQty.text & "  x " & Format(harga, "#,##0") & "  = Rp. " & Format(((txtQty) * harga), "#,##0")
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Function security() As Boolean
Dim i As Byte
On Error GoTo err
security = False
    conn.ConnectionString = strcon
    conn.Open
    
    security = True
    conn.Close
    i = 0

    Exit Function
err:
    If conn.State Then conn.Close
    If rs.State Then rs.Close
    MsgBox err.Description
End Function
