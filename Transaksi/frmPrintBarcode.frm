VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPrintBarcode 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Print Barcode"
   ClientHeight    =   7125
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12990
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   12990
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Print (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   10170
      Picture         =   "frmPrintBarcode.frx":0000
      TabIndex        =   48
      Top             =   5130
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Frame Frame1 
      Caption         =   "Setting"
      Height          =   4965
      Left            =   10170
      TabIndex        =   39
      Top             =   45
      Width           =   2715
      Begin VB.ComboBox cmbRatio 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmPrintBarcode.frx":0102
         Left            =   1305
         List            =   "frmPrintBarcode.frx":010F
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   4050
         Width           =   1005
      End
      Begin VB.TextBox txtDots 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   16
         Text            =   "35"
         Top             =   4365
         Width           =   915
      End
      Begin VB.TextBox txtLabelWidth 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   8
         Text            =   "580"
         Top             =   270
         Width           =   915
      End
      Begin VB.TextBox txtLabelHeight 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   9
         Text            =   "144"
         Top             =   675
         Width           =   915
      End
      Begin VB.TextBox txtLeft1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         TabIndex        =   10
         Text            =   "40"
         Top             =   1710
         Width           =   915
      End
      Begin VB.TextBox txtLeft2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   11
         Text            =   "315"
         Top             =   1710
         Width           =   915
      End
      Begin VB.TextBox txtMarginNama 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   12
         Text            =   "10"
         Top             =   2520
         Width           =   915
      End
      Begin VB.TextBox txtMarginBarcode 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   13
         Text            =   "35"
         Top             =   2880
         Width           =   915
      End
      Begin VB.TextBox txtMarginHarga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   14
         Text            =   "90"
         Top             =   3240
         Width           =   915
      End
      Begin VB.Label Label15 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Dots"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   51
         Top             =   4410
         Width           =   1050
      End
      Begin VB.Label Label13 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "H:W Ratio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   50
         Top             =   4050
         Width           =   1050
      End
      Begin VB.Label Label12 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Barcode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   49
         Top             =   3690
         Width           =   1050
      End
      Begin VB.Label Label11 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label Width"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   47
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label2 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label Height"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   46
         Top             =   765
         Width           =   1050
      End
      Begin VB.Label Label4 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Left Margin 1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   45
         Top             =   1440
         Width           =   1050
      End
      Begin VB.Label Label5 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Left Margin 2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1305
         TabIndex        =   44
         Top             =   1440
         Width           =   960
      End
      Begin VB.Label Label6 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Top Margin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   43
         Top             =   2205
         Width           =   1050
      End
      Begin VB.Label Label9 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Nama"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   42
         Top             =   2565
         Width           =   1050
      End
      Begin VB.Label Label7 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Barcode"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   41
         Top             =   2925
         Width           =   1050
      End
      Begin VB.Label Label10 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   40
         Top             =   3285
         Width           =   1050
      End
   End
   Begin VB.TextBox txtPrinter 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   990
      TabIndex        =   38
      Top             =   45
      Width           =   4245
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1080
      Picture         =   "frmPrintBarcode.frx":0168
      TabIndex        =   36
      Top             =   5985
      Width           =   1230
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Paper Feed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   8640
      Picture         =   "frmPrintBarcode.frx":026A
      TabIndex        =   35
      Top             =   6480
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Paper Feed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   8640
      Picture         =   "frmPrintBarcode.frx":036C
      TabIndex        =   34
      Top             =   5940
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdCalibrate 
      Caption         =   "Calibrate"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   2475
      Picture         =   "frmPrintBarcode.frx":046E
      TabIndex        =   33
      Top             =   5985
      Width           =   1230
   End
   Begin VB.CommandButton cmdFeed 
      Caption         =   "Paper Feed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   5355
      Picture         =   "frmPrintBarcode.frx":0570
      TabIndex        =   32
      Top             =   5985
      Width           =   1230
   End
   Begin VB.CommandButton cmdLoad2 
      Caption         =   "&Load Excel 2"
      Height          =   330
      Left            =   7515
      TabIndex        =   31
      Top             =   1080
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9945
      Top             =   585
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Excel 1"
      Height          =   330
      Left            =   7515
      TabIndex        =   27
      Top             =   675
      Width           =   1230
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2085
      Left            =   135
      TabIndex        =   18
      Top             =   585
      Width           =   6990
      Begin VB.PictureBox Picture2 
         BackColor       =   &H8000000C&
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2790
         ScaleHeight     =   375
         ScaleWidth      =   465
         TabIndex        =   29
         Top             =   225
         Width           =   465
         Begin VB.CommandButton cmdSearchBrg 
            BackColor       =   &H8000000C&
            Caption         =   "F3"
            Height          =   330
            Left            =   0
            Picture         =   "frmPrintBarcode.frx":0672
            TabIndex        =   30
            Top             =   45
            Width           =   420
         End
      End
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BorderStyle     =   0  'None
         Height          =   465
         Left            =   90
         ScaleHeight     =   465
         ScaleWidth      =   3615
         TabIndex        =   28
         Top             =   1485
         Width           =   3615
         Begin VB.CommandButton cmdDelete 
            BackColor       =   &H8000000C&
            Caption         =   "&Hapus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2430
            TabIndex        =   5
            Top             =   45
            Width           =   1050
         End
         Begin VB.CommandButton cmdClear 
            BackColor       =   &H8000000C&
            Caption         =   "&Baru"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1215
            TabIndex        =   4
            Top             =   45
            Width           =   1050
         End
         Begin VB.CommandButton cmdOk 
            BackColor       =   &H8000000C&
            Caption         =   "&Tambahkan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            TabIndex        =   3
            Top             =   45
            Width           =   1050
         End
      End
      Begin VB.TextBox txtHarga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   2
         Top             =   1080
         Width           =   1140
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   1
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   0
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label Label8 
         BackColor       =   &H8000000C&
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   26
         Top             =   1125
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   25
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label lblID 
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   240
         Left            =   270
         TabIndex        =   24
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   23
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   22
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         Caption         =   "caption"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   600
         Left            =   3285
         TabIndex        =   21
         Top             =   315
         Width           =   3480
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   330
         Left            =   3825
         TabIndex        =   20
         Top             =   1305
         Width           =   600
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         Height          =   330
         Left            =   2520
         TabIndex        =   19
         Top             =   675
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   6810
      Picture         =   "frmPrintBarcode.frx":0774
      TabIndex        =   7
      Top             =   5985
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3915
      Picture         =   "frmPrintBarcode.frx":0876
      TabIndex        =   6
      Top             =   5985
      Width           =   1230
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2895
      Left            =   135
      TabIndex        =   17
      Top             =   2790
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   5106
      _Version        =   393216
      Cols            =   5
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000C&
      BackStyle       =   0  'Transparent
      Caption         =   "Printer :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   270
      TabIndex        =   37
      Top             =   135
      Width           =   870
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmPrintBarcode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select [kode barang],[nama barang], Satuan, Harga from ms_barang where flag='1'"
Const queryTransfer As String = "select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang  from t_pembelianh"
Dim total As Currency
Dim mode As Byte
Dim foc As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim NoTrans2 As String
Dim colname() As String

Private Sub cmbRatio_Click()
    writeINI App.Path & "\barcode.ini", "SETTING", "BARCODE RATIO", Trim(Right(cmbRatio.text, 3))
End Sub

Private Sub cmdCalibrate_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer
Open txtPrinter For Output As 1
printstring = ""
printstring = "<ESC>A<ESC>CA<ESC>Z" '<ESC>CP1<ESC>CI2
    print_barcode printstring
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    txtHarga.text = "0"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
    totalQty = totalQty - flxGrid.TextMatrix(flxGrid.row, 4)
    row = flxGrid.row

    For row = row To flxGrid.rows - 1
        If row = flxGrid.rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1
        
    flxGrid.rows = flxGrid.rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 4
    txtKdBrg.text = ""
    lblSatuan = ""
    txtQty.text = "1"
    txtHarga.text = "0"
    LblNamaBarang = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdFeed_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer
Open txtPrinter For Output As 1
printstring = ""



'printstring = "<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000") & "<ESC>A300000000"
'print_barcode printstring
'    printstring = "<ESC>H035<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U "
'    print_barcode printstring
'
'    print_barcode "<ESC>Q1"
    print_barcode "<ESC>A <ESC>Z"
    print_barcode "<ESC>A<ESC>*<ESC>Z"
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub loadexcelfile1(filename As String)
On Error GoTo err
Dim exapp As Object
Dim exwb As Object
Dim exws As Object
Dim range As String
Dim col As Byte
Dim i As Integer
Set exapp = CreateObject("excel.application")
Set exwb = exapp.Workbooks.Open(filename)
Set exws = exwb.Sheets("Sheet1")
col = Asc("A")
range = Chr(col) & "1"

exws.Activate

While exws.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exws.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

i = 2
With exws
While .range("A" & CStr(i)) <> ""
    txtKdBrg.text = .range(Chr(getcolindex("kodebarang", colname) + 65) & CStr(i))
    If Not cek_kodebarang1 Then
        LblNamaBarang = .range(Chr(getcolindex("Nama", colname) + 65) & CStr(i))
    End If
    txtQty.text = .range(Chr(getcolindex("stok", colname) + 65) & CStr(i))
    txtHarga.text = Format(CCur(Trim(.range(Chr(getcolindex("HargaJual", colname) + 65) & CStr(i)))), "###0.00")

    cmdOK_Click
    i = i + 1
Wend
End With

exwb.Close
exapp.Application.Quit
Set exapp = Nothing
Set exwb = Nothing
Set exws = Nothing
Exit Sub
err:
MsgBox err.Description
exwb.Close
exapp.Application.Quit
Set exapp = Nothing
Set exwb = Nothing
Set exws = Nothing
End Sub

Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim conn As New ADODB.Connection
Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=""Excel 8.0;IMEX=1;TypeGuessRows=0"""
con.Open stcon
If db2 Then conn.Open strconasli
If db1 Then conn_fake.Open strcon
rs.Open "select kodebarang,nama,hargajual,stok from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" And Not IsNull(rs("kodebarang")) And rs("kodebarang") <> "" Then
            txtKdBrg.text = rs("kodebarang")
            If Not cek_kodebarang1 Then
            LblNamaBarang = rs("nama")
            End If
            txtQty.text = rs("stok")
            txtHarga.text = Format(rs("HargaJual"), "###0.00")
            cmdOK_Click
'        End If
'        rs2.Close
    End If
    rs.MoveNext
Wend
If rs.State Then rs.Close
If con.State Then con.Close
If conn.State Then conn.Close
Exit Sub
err:
MsgBox err.Description
If rs.State Then rs.Close
If con.State Then con.Close
DropConnection
End Sub

Private Sub cmdLoad2_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.rows - 1
            flxGrid.rows = flxGrid.rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row
        
        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 4) = "" Then
            
            flxGrid.TextMatrix(row, 4) = txtQty
        Else
            totalQty = totalQty - flxGrid.TextMatrix(row, 4)
            If mode = 1 Then
                flxGrid.TextMatrix(row, 4) = CDbl(flxGrid.TextMatrix(row, 4)) + CDbl(txtQty)
                
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 4) = CDbl(txtQty)
            End If
            
        End If
        flxGrid.TextMatrix(row, 3) = txtHarga.text
        totalQty = totalQty + flxGrid.TextMatrix(row, 4)
        If row > 14 Then
            flxGrid.TopRow = row - 13
        Else
            flxGrid.TopRow = 1
        End If
        

        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        txtHarga.text = "0"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1

End Sub

Private Sub cmdSearchBrg_Click()
On Error Resume Next
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "txtKdBrg"
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub reset_form()

    cmdClear_Click
    flxGrid.rows = 1
    flxGrid.rows = 2
End Sub

Private Sub cmdSimpan_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer

Open txtPrinter For Output As 1
printstring = ""
'printstring = "<ESC>A<ESC>A1" & Format(((totalQty \ 2) + IIf(totalQty Mod 2 = 0, 0, 1)) * 140, "0000") & "0560<ESC>A300000000"

X = 0
'print_barcode "<ESC>A<ESC>*<ESC>Z"
'printstring = "<ESC>*<ESC>A<ESC>EX0<ESC>Z<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000") & "<ESC>Z"
'    print_barcode printstring
     
 For i = 1 To flxGrid.rows - 2
    
    For A = 0 To (flxGrid.TextMatrix(i, 4) \ 2) - 1
    If X >= 56 Then
    printstring = "<ESC>Q1<ESC>Z<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000")
    print_barcode printstring
    X = 0
    End If
    printstring = "<ESC>A3H0000V" & Format((X * txtLabelHeight), "0000")
    print_barcode printstring
    printstring = "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>D101050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring

    printstring = "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>D101050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring
        X = X + 1
    Next
    
    If flxGrid.TextMatrix(i, 4) Mod 2 = 1 Then
    If X >= 56 Then
    printstring = "<ESC>Q1<ESC>Z<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000") '<ESC>A<ESC>Z
    print_barcode printstring
    X = 0
    End If
    printstring = "<ESC>A3H0000V" & Format((X * txtLabelHeight), "0000")
    print_barcode printstring
    printstring = "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>D101050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring
    X = X + 1
    End If
Next
    If X > 0 Then
    print_barcode "<ESC>Z" '<ESC>Z"
    'print_barcode "" '<ESC>A<ESC>Z"
    'print_barcode "<ESC>A<ESC>*<ESC>Z"
    End If
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1
End Sub

Private Sub print_barcode(prn As String)

    prn = Replace(prn, "<ESC>", Chr$(27))
    prn = Replace(prn, "<STX>", Chr$(2))
    prn = Replace(prn, "<ETX>", Chr$(3))
    prn = Replace(prn, "<ENQ>", Chr$(5))
    prn = Replace(prn, "<CAN>", Chr$(24))
    Print #1, prn

End Sub



Private Sub Command2_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer
Open txtPrinter For Output As 1
printstring = ""
printstring = "<ESC>A<ESC>CE1<ESC>Z"
    print_barcode printstring
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1

End Sub

Private Sub Command1_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer
Open txtPrinter For Output As 1
printstring = ""
printstring = "<ESC>A(space)<ESC>Z"
    print_barcode printstring
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1

End Sub

Private Sub Command3_Click()
On Error GoTo err
'    Shell App.Path & "\cancelprint.bat", vbHide
    Open txtPrinter For Output As 1



    print_barcode "<ESC>A<ESC>*<ESC>Z"
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1


End Sub

Private Sub cmdPrint_Click()
On Error GoTo err
Dim printstring As String
Dim X As Integer

Open txtPrinter For Output As 1
printstring = ""
'printstring = "<ESC>A<ESC>A1" & Format(((totalQty \ 2) + IIf(totalQty Mod 2 = 0, 0, 1)) * 140, "0000") & "0560<ESC>A300000000"

X = 0
print_barcode "<ESC>A<ESC>*<ESC>Z"
printstring = "<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000") & "<ESC>Z<ESC>A"
    print_barcode printstring
     
 For i = 1 To flxGrid.rows - 2
    
'    For a = 0 To (flxGrid.TextMatrix(i, 4) \ 2) - 1
'    If x >= 56 Then
'    printstring = "<ESC>Q1<ESC>Z<ESC>A<ESC>A1" & Format(txtLabelHeight, "0000") & Format(txtLabelWidth, "0000")
'    print_barcode printstring
'    x = 0
'    End If
    If flxGrid.TextMatrix(i, 4) > 1 Then
    printstring = "<ESC>A3H0000V0000"
    print_barcode printstring
    printstring = "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>" & Trim(Right(cmbRatio.text, 3)) & "1" & Format(txtDots, "00") & "050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring

    printstring = "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>" & Trim(Right(cmbRatio.text, 3)) & "1" & Format(txtDots, "00") & "050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft2, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring
        print_barcode "<ESC>Q" & (flxGrid.TextMatrix(i, 4) \ 2) & "<ESC>Z"
    End If
'        x = x + 1
'    Next
    
    If flxGrid.TextMatrix(i, 4) Mod 2 = 1 Then
    
    printstring = "<ESC>A3H0000V0000"
    print_barcode printstring
    printstring = "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginNama, "000") & "<ESC>L0103<ESC>U" & flxGrid.TextMatrix(i, 2) & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginBarcode, "000") & "<ESC>" & Trim(Right(cmbRatio.text, 3)) & "1" & Format(txtDots, "00") & "050*" & flxGrid.TextMatrix(i, 1) & "*" & _
        "<ESC>H" & Format(txtLeft1, "000") & "<ESC>V" & Format(txtMarginHarga, "000") & "<ESC>L0101<ESC>MRp.  " & Format(flxGrid.TextMatrix(i, 3), "###0") & ",-"
        print_barcode printstring
    print_barcode "<ESC>Q1<ESC>Z"
    End If
Next
'    If x > 0 Then
'    print_barcode "<ESC>Q1" '<ESC>Z"
'    'print_barcode "" '<ESC>A<ESC>Z"
    print_barcode "<ESC>A<ESC>*<ESC>Z"
'    End If
    Close #1
    Exit Sub
err:
    MsgBox err.Description
    Close #1
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
                End If
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 4)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtQty.SetFocus
        End If
    End If
    
End Sub

Private Sub flxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub

Private Sub Form_Activate()
'    SendKeys "{tab}"
End Sub
'Private Sub cmdSimpan_Click()
'lpFileName = App.Path & "\barcode.ini"
'
'   lpSectionName1 = "Form"
'   lpKeyName1 = "Picture"
'   lpValue1 = txtalamat
'
'   lpSectionName2 = "Label"
'   lpKeyName2 = "Name"
'   lpValue2 = txtnama
'Call ProfileSaveItem(lpSectionName1, _
'                     lpKeyName1, _
'                     lpValue1, _
'                    lpFileName)
'Call ProfileSaveItem(lpSectionName2, _
'                     lpKeyName2, _
'                     lpValue2, _
'                    lpFileName)
'
'If fso.FileExists("conf.ini") Then
'writeINI "conf.ini", "Form", "Backcolor", Me.cdlg.Color
'Else
'fso.CreateTextFile "conf.ini"
'writeINI "conf.ini", "Form", "Backcolor", Me.cdlg.Color
'End If
'End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    txtPrinter = getPrinterName
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    conn.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 900
    flxGrid.ColWidth(4) = 1200
    
    
    flxGrid.TextMatrix(0, 1) = "kode barang"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "nama"
    flxGrid.TextMatrix(0, 3) = "Harga"
    flxGrid.TextMatrix(0, 4) = "Qty"
    txtLabelWidth = sGetINI(App.Path & "\barcode.ini", "SETTING", "LABEL WIDTH", 0)
    txtLabelHeight = sGetINI(App.Path & "\barcode.ini", "SETTING", "LABEL HEIGHT", 0)
    txtLeft1 = sGetINI(App.Path & "\barcode.ini", "SETTING", "MARGIN LEFT1", 0)
    txtLeft2 = sGetINI(App.Path & "\barcode.ini", "SETTING", "MARGIN LEFT2", 0)
    txtMarginNama = sGetINI(App.Path & "\barcode.ini", "SETTING", "MARGIN NAMA", 0)
    txtMarginBarcode = sGetINI(App.Path & "\barcode.ini", "SETTING", "MARGIN BARCODE", 0)
    txtMarginHarga = sGetINI(App.Path & "\barcode.ini", "SETTING", "MARGIN HARGA", 0)
    SetComboTextRight sGetINI(App.Path & "\barcode.ini", "SETTING", "BARCODE RATIO", 0), cmbRatio
    txtDots = sGetINI(App.Path & "\barcode.ini", "SETTING", "BARCODE DOTS", 0)
    
    
End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select [nama barang],harga from ms_barang where [kode barang]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(0)
        cek_kodebarang1 = True
        txtHarga.text = IIf(IsNull(rs(1)), 0, rs(1))
        
        
    Else
        'LblNamaBarang = ""
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
    
    
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_barang(kode As String, conn As ADODB.Connection) As Boolean
Dim rs As New ADODB.Recordset
    
    rs.Open "select [nama barang] from ms_barang where [kode barang]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        'LblNamaBarang = ""
        cek_barang = False
    End If
    rs.Close
    
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdPrint_Click
End Sub

Private Sub txtDots_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "BARCODE DOTS", txtDots
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next
    
    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        
        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtLabelHeight_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "LABEL HEIGHT", txtLabelHeight
End Sub

Private Sub txtLabelWidth_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "LABEL WIDTH", txtLabelWidth
End Sub

Private Sub txtLeft1_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "MARGIN LEFT1", txtLeft1
End Sub


Private Sub txtLeft2_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "MARGIN LEFT2", txtLeft2
End Sub


Private Sub txtMarginBarcode_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "MARGIN BARCODE", txtMarginBarcode
End Sub

Private Sub txtMarginHarga_LostFocus()
    writeINI App.Path & "\barcode.ini", "SETTING", "MARGIN HARGA", txtMarginHarga
End Sub

Private Sub txtMarginNama_LostFocus()
     writeINI App.Path & "\barcode.ini", "SETTING", "MARGIN NAMA", txtMarginNama
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub

Private Sub txtQty_LostFocus()
    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub




Private Sub txtPrinter_LostFocus()
    setPrinter txtPrinter
End Sub


Public Function getPrinterName() As String
On Error Resume Next
    Dim filename As String

    filename = App.Path & "\PrinterName.txt"
    Open filename For Input As #1
    Line Input #1, getPrinterName
    Close #1
End Function

Public Sub setPrinter(ByVal printername As String)
On Error GoTo err
    Dim filename As String

    'FileName = Left(App.Path, InStr(1, App.Path, "\") - 1) & "\ServerName.txt"
    filename = App.Path & "\PrinterName.txt"
    Open filename For Output As #1
    Print #1, printername
    Close #1
err:
End Sub



