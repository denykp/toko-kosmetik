VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmAddPO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Purchase Order"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10275
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   10275
   Begin VB.CommandButton cmdCekRetur 
      Caption         =   "Cek Retur"
      Height          =   330
      Left            =   4410
      TabIndex        =   33
      Top             =   630
      Width           =   915
   End
   Begin VB.CommandButton cmdLoadPO 
      Caption         =   "Load"
      Height          =   330
      Left            =   8820
      TabIndex        =   32
      Top             =   180
      Width           =   510
   End
   Begin VB.TextBox txtPO 
      Height          =   330
      Left            =   6885
      TabIndex        =   29
      Top             =   180
      Width           =   1410
   End
   Begin VB.CommandButton cmdSearchPO 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8370
      Picture         =   "frmAddPO.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.CommandButton cmdAddSupplier 
      Caption         =   "Add"
      Height          =   330
      Left            =   3555
      TabIndex        =   27
      Top             =   630
      Width           =   825
   End
   Begin VB.TextBox txtSales 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1665
      TabIndex        =   24
      Top             =   1395
      Width           =   1770
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "&Baru"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   3045
      Picture         =   "frmAddPO.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   7965
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   4620
      Picture         =   "frmAddPO.frx":0544
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   7965
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   90
      Top             =   8235
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdSearchID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      Picture         =   "frmAddPO.frx":0646
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   135
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin MSComCtl2.DTPicker DTTanggal 
      Height          =   330
      Left            =   7380
      TabIndex        =   2
      Top             =   1035
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   20709379
      CurrentDate     =   38927
   End
   Begin VB.CommandButton cmdSearchSupp 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3150
      Picture         =   "frmAddPO.frx":0748
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   630
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1665
      TabIndex        =   0
      Top             =   630
      Width           =   1410
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1665
      TabIndex        =   1
      Top             =   1755
      Width           =   3885
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6195
      Picture         =   "frmAddPO.frx":084A
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7965
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Simpan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   1425
      Picture         =   "frmAddPO.frx":094C
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   7965
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTTiba 
      Height          =   330
      Left            =   7380
      TabIndex        =   3
      Top             =   1440
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   20709379
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   5190
      Left            =   90
      TabIndex        =   4
      Top             =   2205
      Width           =   7305
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   6
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   212
      Columns.Count   =   6
      Columns(0).Width=   2275
      Columns(0).Caption=   "Kd Barang"
      Columns(0).Name =   "Kode Barang"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   1
      Columns(1).Width=   5398
      Columns(1).Caption=   "Nama Barang"
      Columns(1).Name =   "Nama Barang"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1905
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "Qty"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).NumberFormat=   "#,##0"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1931
      Columns(3).Caption=   "Satuan"
      Columns(3).Name =   "Satuan"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "konversi"
      Columns(4).Name =   "konversi"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   2
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Kode Barang"
      Columns(5).Name =   "Disc_persen"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   12885
      _ExtentY        =   9155
      _StockProps     =   79
      BackColor       =   -2147483636
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label26 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6705
      TabIndex        =   31
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label25 
      BackStyle       =   0  'Transparent
      Caption         =   "PO"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6120
      TabIndex        =   30
      Top             =   225
      Width           =   510
   End
   Begin VB.Label Label22 
      BackStyle       =   0  'Transparent
      Caption         =   "Sales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   26
      Top             =   1395
      Width           =   1320
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   25
      Top             =   1395
      Width           =   105
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   45
      TabIndex        =   23
      Top             =   7560
      Width           =   780
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   720
      TabIndex        =   22
      Top             =   7560
      Width           =   105
   End
   Begin VB.Label lblGudang 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   900
      TabIndex        =   21
      Top             =   7560
      Width           =   1005
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7245
      TabIndex        =   17
      Top             =   1485
      Width           =   105
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal Tiba"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   5850
      TabIndex        =   16
      Top             =   1485
      Width           =   1320
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   7245
      TabIndex        =   15
      Top             =   1080
      Width           =   105
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5850
      TabIndex        =   14
      Top             =   1080
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   13
      Top             =   1755
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1755
      Width           =   1320
   End
   Begin VB.Label lblNmSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   11
      Top             =   1080
      Width           =   3840
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   8
      Top             =   675
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   7
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuPosting 
         Caption         =   "&Posting"
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybrg As String = "select kode_barang,nama_barang, merk, kode_jenis, satuan,satuan_1,konversi_1,satuan_2,konversi_2, min_qty, harga, hpp from v_ms_barang_hpp"
Const querySupplier As String = "select kode_Supplier,nama_Supplier, Alamat, Telp, Fax, Email, No_Rekening, Contact_Person, Lain_Lain from ms_Supplier"
Const queryPO As String = "select * from t_poh"
Public where As String
Dim subtotal, total As Currency
Dim ppn As Currency
Dim disc As Currency
Dim counter As Integer
Dim satuan(3) As String
Dim konversi(3) As Double
Dim harga(3) As Currency
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from T_POH where left(ID,2)='" & Format(DTTanggal, "yy") & "' and substring(ID,3,2)='" & Format(DTTanggal, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = Format(DTTanggal, "yy") & Format(DTTanggal, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = Format(DTTanggal, "yy") & Format(DTTanggal, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub
Private Sub printPO()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    With CRPrint
        .reset
        .ReportFileName = App.Path & "\Report\PO.rpt"
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(0).Value & ";UID=" & user & ";PWD=" & pwd & ";"
        Next
        .SelectionFormula = " {T_POH.ID}='" & lblNoTrans & "'"
        .Destination = crptToWindow
        .ParameterFields(0) = "login;" + user + ";True"
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .action = 1
        conn.Execute "update t_poh set print_fg='1' where id='" & lblNoTrans & "'"
    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdAddSupplier_Click()
    Me.Enabled = False
    frmMasterSupplier.Show
'    Set frmMasterSupplier.frm = Me
End Sub

Private Sub cmdApprove_Click()
On Error GoTo err
'    If MsgBox("Apakah anda yakin hendak mem posting Pembelian ini?", vbYesNo, "Posting?") = vbYes Then
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Execute "update t_poh set post_fg='1' where id='" & lblNoTrans & "'"
'    'conn.Execute "exec sp_updatehrgpkk '" & txtNoTB.Text & "','" & Format(DTPicker1, "MM/dd/yyyy hh:mm:ss") & "','B'"
'    conn.Close
'    MsgBox "Data pembelian dengan nomor " & lblNoTrans & " telah berhasil di Posting"
    reset_form
'    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdCekRetur_Click()
    frmSearch.query = "select * from v_returbeli where gudang='" & gudang & "' and kode_supplier='" & txtSupplier.text & "' and flag='0'"
    frmSearch.nmform = "frmAddReturBeli"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.connstr = strcon
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoadPO_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    rs.Open "select t_pod.kode_barang,nama_barang,qty,t_pod.satuan,t_pod.konversi from t_pod inner join " & dbname1 & ".dbo.ms_barang on t_pod.kode_barang=ms_barang.kode_barang where id='" & txtPO.text & "' order by no_urut", conn
    While Not rs.EOF
       DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(0)
       rs.MoveNext
    Wend
    rs.Close
    conn.Close
    Exit Sub
err:
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub cmdPrint_Click()
    printPO
End Sub

Private Sub cmdSearchID_Click()
    search_id
'    where = ""
'    frmPassword.Show
'    frmPassword.nmform = "frmAddPO"
'    frmPassword.func = "search_id"
'    frmPassword.Left = cmdSearchID.Left + cmdSearchID.Width
'    frmPassword.Top = cmdSearchID.Top + 800
End Sub
Public Sub search_id()
    If where <> "" Then
    If InStr(1, queryPO, "where") > 0 Then
    frmSearch.query = queryPO & " and " & where
    Else
    frmSearch.query = queryPO & " where " & where
    End If
    Else
    frmSearch.query = queryPO
    End If
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.connstr = strcon
    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSearchPO_Click()
    frmSearch.query = queryPO
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtPO"
    frmSearch.connstr = strcon
    frmSearch.txtSearch.text = txtPO.text
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSearchSupp_Click()
    frmSearch.query = querySupplier
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "txtSupplier"
    frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_POH where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_POD where ID='" & lblNoTrans & "'"
    End If
    add_dataheader
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.Rows
        If DBGrid.Columns(0).text <> "" Or DBGrid.Columns(1).text <> "" Then
'            rs.Open "select distinct satuan,satuan_1,konversi_1,satuan_2,konversi_2 from ms_barang where kode_barang='" & DBGrid.Columns(0).text & "'", conn
'            If Not rs.EOF Then
'                If rs(0) = DBGrid.Columns(3).text Then DBGrid.Columns(8).text = 1
'                If rs(1) = DBGrid.Columns(3).text Then DBGrid.Columns(8).text = Trim(rs(2))
'                If rs(3) = DBGrid.Columns(3).text Then DBGrid.Columns(8).text = Trim(rs(4))
'            End If
'            rs.Close
            add_datadetail
        End If
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    conn.Close
    conn.CommitTrans
    conn.Close
    MsgBox "Data telah tersimpan"
    cmdPrint.Enabled = True
    'reset_form
    'DBGrid.SetFocus
    SendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    If conn.State Then conn.Close
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub
Private Sub reset_form()
    lblNoTrans = "-"
    DBGrid.RemoveAll
    txtSupplier.text = ""
    lblNmSupplier = "-"
    txtKeterangan.text = ""
    txtSales.text = ""

    DTTanggal = Now
    DTTiba = Now
    cmdPrint.Enabled = False
    subtotal = 0
    total = 0
    lblGudang = gudang
    cmdPrint.Enabled = False
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(8)
    ReDim nilai(8)
    
    table_name = "T_POH"
    fields(0) = "id"
    fields(1) = "kode_supplier"
    fields(2) = "tanggal"
    fields(3) = "tanggal_tiba"
    fields(4) = "keterangan"
    fields(5) = "GUDANG"
    fields(6) = "print_fg"
    fields(7) = "sales"
    
    nilai(0) = lblNoTrans
    nilai(1) = txtSupplier.text
    nilai(2) = Format(DTTanggal, "MM/dd/yyyy")
    nilai(3) = Format(DTTiba, "MM/dd/yyyy")
    nilai(4) = txtKeterangan.text
    nilai(5) = gudang
    nilai(6) = "0"
    nilai(7) = txtSales.text
    tambah_data table_name, fields, nilai
End Sub
Private Sub add_datadetail()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(7)
    ReDim nilai(7)
    
    table_name = "T_POD"
    fields(0) = "ID"
    fields(1) = "kode_barang"
    fields(2) = "qty"
    fields(3) = "satuan"
    fields(4) = "konversi"
    fields(5) = "no_urut"
    fields(6) = "nama_barang"
    
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(5).text
    nilai(2) = DBGrid.Columns(2).text
    nilai(3) = Trim(Left(DBGrid.Columns(3).text, 20))
    nilai(4) = IIf(IsNumeric(DBGrid.Columns(4).text), DBGrid.Columns(4).text, 0)
    nilai(5) = counter
    nilai(6) = DBGrid.Columns(1).text
    tambah_data table_name, fields, nilai
End Sub

Private Sub DBGrid_BtnClick()
    frmSearch.query = querybrg & " where flag='1'"
    frmSearch.nmform = "frmAddPO"
    frmSearch.nmctrl = "DBGrid"
    frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show
End Sub
Public Sub cek_kodebarang()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select distinct nama_barang,satuan,satuan_1,satuan_2,konversi_2,kode_barang from ms_barang where kode_barang='" & DBGrid.Columns(0).text & "' or barcode='" & DBGrid.Columns(0).text & "' or (barcode_1='" & DBGrid.Columns(0).text & "' and len(barcode_1)>0)", conn
    If Not rs.EOF Then
        DBGrid.Columns(1).text = rs(0)
        satuan(0) = rs(1)
        satuan(1) = rs(2)
        satuan(2) = rs(3)
        DBGrid.Columns(3).text = rs(3)
        DBGrid.Columns(4).text = CInt(rs(4))
        DBGrid.Columns(5).text = rs(5)
        If Not IsNumeric(DBGrid.Columns(2).text) Then DBGrid.Columns(2).text = "0"

    Else
        DBGrid.Columns(1).text = ""
        DBGrid.Columns(3).text = ""
        DBGrid.Columns(4).text = ""
        DBGrid.Columns(5).text = ""
    End If
    rs.Close
    conn.Close
End Sub
Public Sub cek_kodesupplier2()
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select distinct nama_supplier from ms_supplier where kode_supplier='" & DBGrid.Columns(0).text & "'", conn
    If Not rs.EOF Then
        DBGrid.Columns(1).text = rs(0)
        
    Else
        DBGrid.Columns(1).text = ""
        
    End If
    rs.Close
    conn.Close
End Sub
Public Sub cek_PO()

End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
'    reset_form
    cmdPrint.Enabled = False
    mnuPrint.Enabled = False
    cmdApprove.Enabled = True
    mnuPosting.Enabled = True
    conn.ConnectionString = strcon
    conn.Open
        rs.Open "select * from t_poh where id='" & lblNoTrans & "'", conn
        If Not rs.EOF Then
            DTTanggal = rs("tanggal")
            DTTiba = rs("tanggal_tiba")
            txtSupplier.text = rs("kode_supplier")
            cek_kodesupplier
            txtKeterangan.text = rs("keterangan")
            txtSales.text = rs("sales")
            lblGudang = rs("gudang")
            cmdPrint.Enabled = True
            mnuPrint.Enabled = True
            If rs("print_fg") = "1" And rs("flag") = "0" Then
            cmdApprove.Enabled = True
            mnuPosting.Enabled = True
            End If
            rs.Close
            subtotal = 0
            DBGrid.RemoveAll
            DBGrid.FieldSeparator = "#"
            rs.Open "select d.kode_barang,d.nama_barang,qty,d.satuan,d.konversi from t_pod d where d.id='" & lblNoTrans & "' order by no_urut", conn
            While Not rs.EOF
                DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4) & "#" & rs(0)
                subtotal = subtotal + 0
                rs.MoveNext
            Wend

            
            
            rs.Close
            
        End If
        If rs.State Then rs.Close
    conn.Close
End Sub
Public Sub cek_kodesupplier()
Dim rs As New ADODB.Recordset
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select distinct nama_supplier from ms_supplier where kode_supplier='" & txtSupplier.text & "'", conn
    If Not rs.EOF Then
        lblNmSupplier = rs(0)
    Else
        lblNmSupplier = "-"
    End If
    rs.Close
    conn.Close
End Sub

Private Sub DBGrid_ComboCloseUp()
On Error GoTo err
Dim temp As String
    Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select distinct satuan,satuan_1,konversi_1,satuan_2,konversi_2 from ms_barang where kode_barang='" & DBGrid.Columns(0).text & "'", conn
    If Not rs.EOF Then
        If DBGrid.Columns(3).ListIndex = 0 Then DBGrid.Columns(4).text = DBGrid.Columns(2).text
        If DBGrid.Columns(3).ListIndex = 1 Then DBGrid.Columns(4).text = Trim(DBGrid.Columns(2).text * rs(2))
        If DBGrid.Columns(3).ListIndex = 2 Then DBGrid.Columns(4).text = Trim(DBGrid.Columns(2).text * rs(4))
    End If
    rs.Close
    conn.Close
    Exit Sub
err:
    If conn.State Then conn.Close
    MsgBox err.Description
End Sub

Private Sub DBGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        SendKeys "{tab}"
    End If
End Sub

Private Sub DBGrid_KeyUp(KeyCode As Integer, Shift As Integer)
    If DBGrid.col = 0 And DBGrid.Columns(0).text <> "" Then
        cek_kodebarang
    End If

End Sub

Private Sub DBGrid_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim temp As String
    DBGrid.Columns(3).RemoveAll
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select distinct nama_barang,satuan,satuan_1,konversi_1,satuan_2,konversi_2,harga from ms_barang where kode_barang='" & DBGrid.Columns(0).text & "'", conn
    If Not rs.EOF Then
        DBGrid.Columns(3).AddItem rs(1)
        DBGrid.Columns(3).AddItem rs(2)
        DBGrid.Columns(3).AddItem rs(4)
    End If
    rs.Close
    conn.Close
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then SendKeys "{tab}"
End Sub

Private Sub Form_Load()
    reset_form
    DBGrid.Columns(4).NumberFormat = "#,##0.00"
    DBGrid.Columns(4).DataType = 6

End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPosting_Click()
    cmdApprove_Click
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub


Private Sub txtSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then cmdSearchSupp_Click
End Sub

Private Sub txtSupplier_KeyPress(KeyAscii As Integer)
    cek_kodesupplier
End Sub

Private Sub txtSupplier_LostFocus()
    cek_kodesupplier
End Sub
