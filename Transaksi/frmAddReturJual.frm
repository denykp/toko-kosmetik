VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAddReturJual 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Penjualan"
   ClientHeight    =   7395
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9780
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   9780
   Begin VB.Frame Frame2 
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   135
      TabIndex        =   27
      Top             =   1395
      Width           =   6990
      Begin VB.ComboBox cmbHarga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1035
         Width           =   1320
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   2
         Top             =   225
         Width           =   1500
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   3
         Top             =   630
         Width           =   1140
      End
      Begin VB.CommandButton cmdOk 
         BackColor       =   &H00C0FFC0&
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   315
         TabIndex        =   5
         Top             =   1440
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         BackColor       =   &H00C0FFC0&
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1530
         TabIndex        =   6
         Top             =   1440
         Width           =   1050
      End
      Begin VB.CommandButton cmdDelete 
         BackColor       =   &H00C0FFC0&
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2745
         TabIndex        =   7
         Top             =   1440
         Width           =   1050
      End
      Begin VB.CommandButton cmdSearchBrg 
         BackColor       =   &H00C0FFC0&
         Caption         =   "F3"
         Height          =   330
         Left            =   2835
         TabIndex        =   28
         Top             =   225
         Width           =   375
      End
      Begin VB.Label Label9 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   180
         TabIndex        =   39
         Top             =   1080
         Width           =   600
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H0080FF80&
         Height          =   330
         Left            =   1260
         TabIndex        =   35
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   420
         Left            =   3240
         TabIndex        =   34
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label Label8 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   180
         TabIndex        =   33
         Top             =   675
         Width           =   600
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label lblID 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4050
         TabIndex        =   31
         Top             =   1485
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H0080FF80&
         Height          =   285
         Left            =   270
         TabIndex        =   30
         Top             =   1530
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblStock 
         BackStyle       =   0  'Transparent
         ForeColor       =   &H00000000&
         Height          =   330
         Left            =   2520
         TabIndex        =   29
         Top             =   675
         Visible         =   0   'False
         Width           =   870
      End
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5018
      TabIndex        =   38
      Top             =   6885
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BackColor       =   &H00C0C0FF&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3195
      MaskColor       =   &H00FFFF80&
      TabIndex        =   26
      Top             =   675
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNoNota 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1710
      TabIndex        =   0
      Top             =   675
      Width           =   1410
   End
   Begin VB.ComboBox cmbTipeRetur 
      Height          =   315
      ItemData        =   "frmAddReturJual.frx":0000
      Left            =   7785
      List            =   "frmAddReturJual.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   23
      Top             =   1125
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      MaskColor       =   &H8000000F&
      Picture         =   "frmAddReturJual.frx":0030
      TabIndex        =   20
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1710
      TabIndex        =   1
      Top             =   1035
      Width           =   3885
   End
   Begin VB.CommandButton cmdApprove 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Approve"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3578
      MaskColor       =   &H8000000F&
      TabIndex        =   10
      Top             =   6885
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H00C0C0FF&
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   6413
      MaskColor       =   &H8000000F&
      TabIndex        =   11
      Top             =   6885
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H00C0C0FF&
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   2138
      MaskColor       =   &H8000000F&
      TabIndex        =   9
      Top             =   6885
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   5130
      TabIndex        =   12
      Top             =   180
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   249954307
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   2940
      Left            =   135
      TabIndex        =   8
      Top             =   3465
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   5186
      _Version        =   393216
      Cols            =   6
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6030
      TabIndex        =   41
      Top             =   6435
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7020
      TabIndex        =   40
      Top             =   6435
      Width           =   1680
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1125
      TabIndex        =   37
      Top             =   6480
      Width           =   1140
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   180
      TabIndex        =   36
      Top             =   6480
      Width           =   735
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   25
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   24
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7560
      TabIndex        =   22
      Top             =   1215
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   21
      Top             =   1215
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4410
      TabIndex        =   19
      Top             =   6030
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5490
      TabIndex        =   18
      Top             =   6030
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   17
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4995
      TabIndex        =   16
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   15
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   14
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   13
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddReturJual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Const queryJual As String = "Select ID,Tanggal,[Kode Customer],Total,Bayar,userid from t_penjualanh"
Const queryRetur As String = "Select ID,Tanggal,No_Nota,Gudang,Keterangan,UserID from t_ReturjualH"
Public jual As Boolean
Dim qty As Long
Dim total As Long
Dim NoTrans2 As String
Dim mode As Byte
Dim detail As Boolean
Dim currentRow As Integer
Dim harga_jual As Currency
Dim harga_beli As Currency
Private Sub nomor_baru()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset

    conn.Open strcon
    rs.Open "select top 1 ID from T_RETURjualH where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "RJ" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "RJ" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    conn.Close
End Sub



Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
        frmSearch.connstr = strcon
    frmSearch.query = queryJual
    frmSearch.nmform = "frmAddReturJual"
    frmSearch.nmctrl = "txtNoNota"
    frmSearch.keyIni = "t_penjualan"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_nota"
    frmSearch.loadgrid frmSearch.query
'    frmSearch.txtSearch.text = txtNoNota.text
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddReturJual"
    frmSearch.nmctrl = "txtKdBrg"
    frmSearch.connstr = strcon
    frmSearch.keyIni = "ms_barang"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_kodebarang1"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchID_Click()
    
    frmSearch.connstr = strcon
    
    frmSearch.query = queryRetur
    frmSearch.nmform = "frmAddReturjual"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.keyIni = "t_returJual"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    
    frmSearch.Show vbModal
End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
Dim row As Integer
'    reset_form
    cmdSimpan.Enabled = True
    cmdApprove.Enabled = False
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from t_returjualh where id='" & lblNoTrans & "' and gudang='" & gudang & "'", conn
    If Not rs.EOF Then
        NoTrans2 = rs("pk")
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtNoNota.text = rs("no_nota")
        If rs("status") = 1 Then
            cmdSimpan.Enabled = False
        Else
            cmdSimpan.Enabled = True
        End If
        rs.Close
        qty = 0
        flxGrid.rows = 1
        flxGrid.rows = 2
        total = 0
        row = 1
        rs.Open "select distinct a.[kode barang],b.[nama barang],a.qty,a.harga_jual,a.harga_beli from (t_returjuald a inner join ms_barang b on a.[kode barang]=b.[kode barang])  where a.id='" & lblNoTrans & "'", conn
        While Not rs.EOF
            flxGrid.TextMatrix(row, 1) = rs(0)
            flxGrid.TextMatrix(row, 2) = rs(1)
            flxGrid.TextMatrix(row, 3) = rs(2)
            flxGrid.TextMatrix(row, 4) = rs(4)
            flxGrid.TextMatrix(row, 5) = rs(3)
            qty = qty + rs(2)
            total = total + (rs(2) * rs(3))
            flxGrid.rows = flxGrid.rows + 1
            row = row + 1
            rs.MoveNext
        Wend
        lblQty = Format(qty, "#,##0")
        lblTotal = Format(total, "#,##0")
    End If
    rs.Close
    flxGrid.row = 1
    conn.Close
    
End Sub
Private Sub posting(kdbrg As String)
End Sub
Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.rows - 1
            If flxGrid.TextMatrix(i, 1) = lblKode Then row = i
        Next
        If row = 0 Then
            row = flxGrid.rows - 1
            flxGrid.rows = flxGrid.rows + 1
        End If
        flxGrid.TextMatrix(row, 1) = lblKode
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
            total = total - (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        flxGrid.TextMatrix(row, 4) = harga_beli
        flxGrid.TextMatrix(row, 5) = cmbHarga.text 'harga_jual
        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
        total = total + (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 4
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblStock = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.rows - 2
End Sub
Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblStock = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    mode = 1
    cmbHarga.Clear
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    row = flxGrid.row
    If flxGrid.rows <= 2 Then Exit Sub
    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
    total = total - (flxGrid.TextMatrix(row, 3) * flxGrid.TextMatrix(row, 5))
    lblQty = totalQty
    For row = row To flxGrid.rows - 2
        If row = flxGrid.rows Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.rows = flxGrid.rows - 1
    lblItem = flxGrid.rows - 2
    lblTotal = Format(total, "#,##0")
    flxGrid.col = 0
    'flxGrid.ColSel = 3
    txtKdBrg.text = ""
    lblStock = ""
    txtQty.text = "1"
    LblNamaBarang = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub
Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim counter As Integer
Dim no_urut As Integer
On Error GoTo err
    
    If flxGrid.rows < 3 Or txtNoNota.text = "" Then
        MsgBox "Silahkan masukkan Nota dan Barang yang akan diretur terlebih dahulu"
        txtNoNota.SetFocus
        Exit Sub
    End If
    id = lblNoTrans
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    
    If lblNoTrans = "-" Then
        nomor_baru
    Else
            deletekartustock "Retur Jual", lblNoTrans, conn
            conn.Execute "delete from t_returjualh where ID='" & lblNoTrans & "'"
            rs.Open "select * from t_returjuald where id='" & lblNoTrans & "'", conn
            While Not rs.EOF
                updatestock gudang, rs("kode barang"), rs("qty") * -1, conn
                
            rs.MoveNext
            Wend
            rs.Close
            conn.Execute "delete from t_returjuald where ID='" & lblNoTrans & "'"
    End If
    add_dataheader
    counter = 0
    Dim J As Integer
    For J = 1 To flxGrid.rows - 2
        add_datadetail J
        insertkartustock "Retur Jual", lblNoTrans, DTPicker1, flxGrid.TextMatrix(J, 1), flxGrid.TextMatrix(J, 3), CStr(gudang), txtKeterangan, conn
        updatestock gudang, flxGrid.TextMatrix(J, 1), flxGrid.TextMatrix(J, 3), conn
        
        
    Next
    counter = 0
    conn.CommitTrans
    
    i = 0
    For J = 1 To flxGrid.rows - 2
        flxGrid.row = J
        posting flxGrid.TextMatrix(J, 1)
    Next
    conn.Execute "update t_returjualh set status='1' where id='" & lblNoTrans & "'"
    
    DropConnection
    i = 0
    MsgBox "Data sudah tersimpan"
    reset_form

    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtNoNota = ""
    lblQty = "0,00"
    lblTotal = "0,00"
    txtKeterangan.text = ""
    DTPicker1 = Now
    qty = 0
    cmdApprove.Enabled = False
    total = 0
    
    txtKdBrg.text = ""
    txtQty.text = "1"
    flxGrid.rows = 1
    flxGrid.rows = 2
    cmdSimpan.Enabled = True
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(7)
    ReDim nilai(7)
    
    table_name = "T_RETURjualH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "gudang"
    fields(4) = "no_nota"
    fields(5) = "userid"
    fields(6) = "pk"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = gudang
    nilai(4) = txtNoNota.text
    nilai(5) = user
    nilai(6) = ""
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "T_RETURjualD"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "harga_beli"
    fields(4) = "harga_jual"
    fields(5) = "URUT"
    
        
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = flxGrid.TextMatrix(row, 4)
    nilai(4) = flxGrid.TextMatrix(row, 5)
    nilai(5) = row

       
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub


Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
        cek_kodebarang1
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
        txtQty.SetFocus
        End If
    End If
End Sub

'Public Sub cek_PO()
'    conn.ConnectionString = strcon
'    conn.Open
'    rs.Open "select nama_supplier from ms_supplier where kode_supplier='" & txtPO.text & "'", conn
'    If Not rs.EOF Then
'        lblNmSupplier = rs(0)
'    Else
'        lblNmSupplier = ""
'    End If
'    rs.Close
'    conn.Close
'End Sub



Private Sub Form_Activate()
'    mySendKeys "{tab}"
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub Form_Load()
    reset_form
    sizeFlxGrid "select a.[kode barang],b.[nama barang],a.qty,a.harga_jual from (t_returjuald a inner join ms_barang b on a.[kode barang]=b.[kode barang])", flxGrid, "FlxReturJual", 1
    flxGrid.ColWidth(0) = 300
'    flxGrid.ColWidth(1) = 1100
'    flxGrid.ColWidth(2) = 2800
'    flxGrid.ColWidth(3) = 900
'    flxGrid.ColWidth(5) = 1200
    flxGrid.ColWidth(4) = 0
    

    
    flxGrid.TextMatrix(0, 1) = "Kode Barang"
    flxGrid.ColAlignment(1) = 1 'vbAlignLeft
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Harga Beli"
    flxGrid.TextMatrix(0, 5) = "Harga Jual"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    resizeFlxGrid "select a.[kode barang],b.[nama barang],a.qty,a.harga_jual from (t_returjuald a inner join ms_barang b on a.[kode barang]=b.[kode barang])", flxGrid, "FlxReturJual", 1

End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub
Public Sub cek_nota()
'    DBGrid.RemoveAll
'    DBGrid.FieldSeparator = "#"
    qty = 0
    
    conn.Open strcon
    
    Dim where As String
    If lblNoTrans <> "-" Then where = " and id<>'" & lblNoTrans & "'"
    rs.Open "select * from t_returjualh where no_nota='" & txtNoNota & "' " & where, conn
    If Not rs.EOF Then
        If (MsgBox("Nota tersebut sudah diretur, apakah anda ingin melihat retur tersebut?", vbYesNo) = vbYes) Then
            lblNoTrans = rs!id
            rs.Close
        End If
        conn.Close
        cek_notrans
        Exit Sub
    End If
    rs.Close
'    rs.Open "select d.[kode barang],m.[nama barang],d.qty,(d.harga-disc-disc_total) as disc,d.hpp from q_juald d inner join ms_barang m on d.[kode barang]=m.[kode barang] where id='" & txtNoNota.text & "' order by urut", conn
'    While Not rs.EOF
'        flxGrid.TextMatrix(row, 1) = rs(0)
'        flxGrid.TextMatrix(row, 1) = rs(1)
'        flxGrid.TextMatrix(row, 1) = 0
'        qty = qty + 1
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Close
    lblQty = Format(qty, "#,##0")
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo err
Dim kode As String
    
    conn.ConnectionString = strcon
    
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select m.[nama barang],m.[kode barang],harga1,harga2 from ms_barang m where m.[kode barang]='" & txtKdBrg & "' and flag='1'", conn
    If Not rs.EOF Then
        cek_kodebarang1 = True
        LblNamaBarang = rs(0)
        cmbHarga.Clear
        cmbHarga.AddItem rs(2)
        cmbHarga.AddItem rs(3)
        harga_beli = getHpp(rs(1), strcon)
        lblKode = rs(1)
        If rs.State Then rs.Close
        rs.Open "select harga,harga1,harga2 from t_penjualand where id='" & txtNoNota & "' and [kode barang]='" & txtKdBrg & "'", conn
        If Not rs.EOF Then
            cmbHarga.Clear
            cmbHarga.AddItem rs(1)
            cmbHarga.AddItem rs(2)
            SetComboText rs(0), cmbHarga
            harga_jual = rs(0)
        Else
            If cmbHarga.ListCount > 0 Then cmbHarga.ListIndex = 0
            harga_jual = cmbHarga.text
        End If
        rs.Close
        
        
        'lblStock = getStock2(txtKdBrg.text, gudang, strcon)
        
    Else
        cek_kodebarang1 = False
        LblNamaBarang = ""
        'lblStock = "0"
        harga_jual = 0
        harga_beli = 0
        lblKode = ""
    End If
    If rs.State Then rs.Close
    conn.Close
    Exit Function
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Function

Private Sub fokusnota()
On Error Resume Next
    txtNoNota.SetFocus
End Sub


Private Sub txtNoNota_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then
        cmdSearch_Click
    End If
End Sub

Private Sub txtNoNota_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cek_nota
    End If
End Sub

Private Sub txtNoNota_LostFocus()
    cek_nota
End Sub
Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then
        cmdSearchBrg_Click
    End If
    If KeyCode = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        cek_kodebarang1
'        cmdOK_Click

'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If Not cek_kodebarang1 And txtKdBrg <> "" Then
        MsgBox "Kode yang anda masukkan salah"
        txtKdBrg.SetFocus
    End If
    foc = 0
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub
