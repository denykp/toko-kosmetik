VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAddReturBeli 
   BackColor       =   &H00C0FFC0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Retur Pembelian"
   ClientHeight    =   6690
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   9780
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6690
   ScaleWidth      =   9780
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H00FFFF80&
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5040
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   5985
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearch 
      BackColor       =   &H00FFFF80&
      Caption         =   "F3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3195
      MaskColor       =   &H8000000F&
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   630
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtNoNota 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1710
      TabIndex        =   0
      Top             =   675
      Width           =   1410
   End
   Begin VB.ComboBox cmbTipeRetur 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmAddReturBeli.frx":0000
      Left            =   7785
      List            =   "frmAddReturBeli.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   1125
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H00FFFF80&
      Caption         =   "F5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      MaskColor       =   &H8000000F&
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   180
      UseMaskColor    =   -1  'True
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1710
      TabIndex        =   1
      Top             =   1035
      Width           =   3885
   End
   Begin VB.CommandButton cmdApprove 
      BackColor       =   &H00FFFF80&
      Caption         =   "&Approve"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3510
      MaskColor       =   &H8000000F&
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   5985
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H00FFFF80&
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   6480
      MaskColor       =   &H8000000F&
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5985
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H00FFFF80&
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   2070
      MaskColor       =   &H8000000F&
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   5985
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   5130
      TabIndex        =   6
      Top             =   180
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   110952451
      CurrentDate     =   38927
   End
   Begin SSDataWidgets_B.SSDBGrid DBGrid 
      Height          =   3615
      Left            =   180
      TabIndex        =   2
      Top             =   1665
      Width           =   9195
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   "#"
      Col.Count       =   5
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      RowSelectionStyle=   2
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   450
      ExtraHeight     =   265
      Columns.Count   =   5
      Columns(0).Width=   2275
      Columns(0).Caption=   "Kd Barang"
      Columns(0).Name =   "Kode Barang"
      Columns(0).CaptionAlignment=   0
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   7805
      Columns(1).Caption=   "Nama Barang"
      Columns(1).Name =   "Nama Barang"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1905
      Columns(2).Caption=   "Qty"
      Columns(2).Name =   "Qty"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,##0"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "Qty Jual"
      Columns(3).Name =   "Qty Jual"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "Harga_beli"
      Columns(4).Name =   "Harga_beli"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      _ExtentX        =   16219
      _ExtentY        =   6376
      _StockProps     =   79
      BackColor       =   -2147483636
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   19
      Top             =   675
      Width           =   1320
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   18
      Top             =   675
      Width           =   105
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7560
      TabIndex        =   16
      Top             =   1215
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Tipe Retur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6750
      TabIndex        =   15
      Top             =   1215
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4410
      TabIndex        =   13
      Top             =   5445
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5490
      TabIndex        =   12
      Top             =   5445
      Width           =   1320
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4320
      TabIndex        =   11
      Top             =   225
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4995
      TabIndex        =   10
      Top             =   225
      Width           =   105
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   9
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   90
      TabIndex        =   8
      Top             =   1035
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   135
      TabIndex        =   7
      Top             =   135
      Width           =   2040
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSimpan 
         Caption         =   "&Simpan"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddReturBeli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Const querybrg As String = "select m.kode_barang,nama_barang, merk, kode_jenis, satuan_2, min_qty, m.harga from ms_barang m "
Const queryJual As String = "Select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang  from t_pembelianh"
Const queryRetur As String = "Select * from t_returbeliH"
Public jual As Boolean
Dim qty As Long
Dim NoTrans2 As String
Dim mode As Byte
Dim detail As Boolean
Dim currentRow As Integer

Private Sub nomor_baru()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
No = ""
    conn.Open strcon
    rs.Open "select top 1 ID from T_returbeliH where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "RB" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    conn.Close
End Sub
Private Sub cmdApprove_Click()
On Error GoTo err
Dim counter As Integer
    conn.ConnectionString = strcon
    conn.Open
    conn.Execute "update t_returbelih set flag='1' where id='" & lblNoTrans & "'"
    
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.rows
        If DBGrid.Columns(0).text <> "" Then
            conn.Execute "exec sp_updatehrgpkk '" & lblNoTrans & "','" & DBGrid.Columns(0).text & "','" & DBGrid.Columns(2).text & "','R','" & gudang & "'"
        End If
        DBGrid.MoveNext
        counter = counter + 1
    Wend
    
    conn.Close
    MsgBox "Data retur dengan nomor " & lblNoTrans & " telah berhasil di approve"
    reset_form
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSearch_Click()
    
        frmSearch.connstr = strcon
    
    frmSearch.query = queryJual
    frmSearch.nmform = "frmAddreturbeli"
    frmSearch.nmctrl = "txtNoNota"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_nota"
    frmSearch.loadgrid frmSearch.query
'    frmSearch.txtSearch.text = txtNoNota.text
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub


Private Sub cmdSearchID_Click()
    
    frmSearch.connstr = strcon
    
    frmSearch.query = queryRetur
    frmSearch.nmform = "frmAddreturbeli"
    frmSearch.nmctrl = "lblNoTrans"
    
    frmSearch.col = 1
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    
    frmSearch.Show vbModal
End Sub
Public Sub cek_notrans()
Dim rs As New ADODB.Recordset
'    reset_form
    cmdSimpan.Enabled = True
    cmdApprove.Enabled = False
    
    conn.ConnectionString = strcon
    
    conn.Open
    rs.Open "select * from t_returbelih where id='" & lblNoTrans & "' and gudang='" & gudang & "'", conn
    If Not rs.EOF Then
        NoTrans2 = rs("pk")
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtNoNota.text = rs("no_nota")
        rs.Close
        qty = 0
        DBGrid.RemoveAll
        DBGrid.FieldSeparator = "#"
        rs.Open "select a.[kode barang],b.[nama barang],a.qty,c.qty,a.harga_beli from (t_returbelid a inner join ms_barang b on a.[kode barang]=b.[kode barang]) inner join t_pembeliand c on a.[kode barang]=c.[kode barang]  where a.id='" & lblNoTrans & "' and c.id='" & txtNoNota.text & "'", conn
        While Not rs.EOF
            DBGrid.AddItem rs(0) & "#" & rs(1) & "#" & rs(2) & "#" & rs(3) & "#" & rs(4)
            qty = qty + rs(2)
            rs.MoveNext
        Wend
        lblQty = Format(qty, "#,##0")
    End If
    rs.Close
    conn.Close
    
End Sub

Private Sub cmdSimpan_Click()
Dim i As Byte
Dim id As String
Dim counter As Integer
Dim no_urut As Integer
On Error GoTo err
    
    If txtNoNota.text = "" Or DBGrid.rows < 1 Then
        MsgBox "Silahkan masukkan Nota dan Barang yang akan diretur terlebih dahulu"
        txtNoNota.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        deletekartustock "Retur Beli", lblNoTrans, conn
        
        rs.Open "select * from t_returbelid where id='" & lblNoTrans & "'", conn
        While Not rs.EOF
            updatestock gudang, rs("kode barang"), rs("qty"), conn
            rs.MoveNext
        Wend
        rs.Close
        conn.Execute "delete from t_returbelih where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_returbelid where ID='" & lblNoTrans & "'"
    
    End If
    add_dataheader
    counter = 0
    DBGrid.MoveFirst
    While counter < DBGrid.rows
        add_datadetail counter
        updatestock gudang, DBGrid.Columns(0).text, DBGrid.Columns(2).text * -1, conn
        insertkartustock "Retur Beli", lblNoTrans, DTPicker1, DBGrid.Columns(0).text, DBGrid.Columns(2).text * -1, CStr(gudang), txtKeterangan, conn
        
        counter = counter + 1
        DBGrid.MoveNext
    Wend
    
    conn.CommitTrans
    
    DropConnection
    i = 0
    MsgBox "Data sudah tersimpan"
    reset_form

    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Private Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    lblQty = "0,00"
    txtKeterangan.text = ""
    DTPicker1 = Now
    qty = 0
    cmdApprove.Enabled = False
    DBGrid.RemoveAll
    
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)
    
    table_name = "T_returbeliH"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "gudang"
    fields(4) = "no_nota"
    fields(5) = "pk"
    
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = gudang
    nilai(4) = txtNoNota.text
    nilai(5) = NoTrans2
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "T_returbeliD"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty"
    fields(3) = "harga_beli"
    fields(4) = "URUT"
        
    nilai(0) = lblNoTrans
    nilai(1) = DBGrid.Columns(0).text
    nilai(2) = DBGrid.Columns(2).text
    nilai(3) = DBGrid.Columns(4).text
    nilai(4) = row
       
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Private Sub DBGrid_AfterColUpdate(ByVal ColIndex As Integer)
    qty = qty + DBGrid.Columns(2).text
    lblQty = Format(qty, "#,##0")
End Sub

Private Sub DBGrid_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If CLng(DBGrid.Columns(2).text) > CLng(DBGrid.Columns(3).text) Then
        MsgBox "Jumlah Retur tidak boleh lebih besar daripada jumlah yang dibeli (" & DBGrid.Columns(3).text & ")"
        Cancel = True
    Else
        qty = qty - OldValue
    End If
End Sub

'Public Sub cek_PO()
'    conn.ConnectionString = strcon
'    conn.Open
'    rs.Open "select nama_supplier from ms_supplier where kode_supplier='" & txtPO.text & "'", conn
'    If Not rs.EOF Then
'        lblNmSupplier = rs(0)
'    Else
'        lblNmSupplier = ""
'    End If
'    rs.Close
'    conn.Close
'End Sub



Private Sub Form_Activate()
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then
        cmdSearch_Click
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then MySendKeys "{tab}"
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSimpan_Click()
    cmdSimpan_Click
End Sub
Public Sub cek_nota()

    DBGrid.RemoveAll
    DBGrid.FieldSeparator = "#"
    qty = 0
    
    conn.Open strcon
    
    Dim where As String
    If lblNoTrans <> "-" Then where = " and id<>'" & lblNoTrans & "'"
    rs.Open "select * from t_returbelih where no_nota='" & txtNoNota & "' " & where, conn
    If Not rs.EOF Then
        If (MsgBox("Nota tersebut sudah diretur, apakah anda ingin melihat retur tersebut?", vbYesNo) = vbYes) Then
            lblNoTrans = rs!id
            rs.Close
        End If
        conn.Close
        cek_notrans
        Exit Sub
    End If
    rs.Close
    rs.Open "select d.[kode barang],m.[nama barang],d.qty,d.harga from t_pembeliand d inner join ms_barang m on d.[kode barang]=m.[kode barang] where id='" & txtNoNota.text & "' order by urut", conn
    While Not rs.EOF
        DBGrid.AddItem rs(0).value & "#" & rs(1).value & "#" & rs(2).value & "#" & rs(2).value & "#" & rs(3).value
        
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    lblQty = Format(qty, "#,##0")
    
End Sub
Private Sub fokusnota()
On Error Resume Next
    txtNoNota.SetFocus
End Sub
Private Sub txtNoNota_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cek_nota
    End If
End Sub

Private Sub txtNoNota_LostFocus()
    cek_nota
End Sub
