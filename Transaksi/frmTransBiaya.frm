VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTransBiaya 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembayaran Biaya"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   8805
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   8805
   Begin VB.ComboBox cmbKodeBiaya 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1620
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   945
      Width           =   3075
   End
   Begin VB.TextBox txtJumlah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   2
      Top             =   1710
      Width           =   1545
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   1350
      Width           =   3885
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H008080FF&
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3150
      TabIndex        =   4
      Top             =   2250
      Width           =   1320
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   645
      Left            =   450
      Picture         =   "frmTransBiaya.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   8640
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H008080FF&
      Caption         =   "F5"
      Height          =   330
      Left            =   3780
      TabIndex        =   11
      Top             =   180
      Width           =   465
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H008080FF&
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   4515
      TabIndex        =   5
      Top             =   2250
      Width           =   1320
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H008080FF&
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1755
      TabIndex        =   3
      Top             =   2250
      Width           =   1320
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   6
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   112656387
      CurrentDate     =   38927
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8460
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Jenis Biaya"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   17
      Top             =   990
      Width           =   1275
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   16
      Top             =   990
      Width           =   105
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   15
      Top             =   1755
      Width           =   1275
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   14
      Top             =   1755
      Width           =   105
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   1395
      Width           =   1275
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   12
      Top             =   1395
      Width           =   105
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   10
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   8
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "&Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "&Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmTransBiaya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const querybiaya As String = "select nomer_biaya,Tanggal,Keterangan,jumlah from t_biaya"
Dim total As Currency
Dim NoTrans2 As String
Dim mode As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim foc As Byte
Dim colname() As String
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 nomer_biaya from t_biaya where mid(nomer_biaya,3,2)='" & Format(DTPicker1, "yy") & "' and mid(nomer_biaya,5,2)='" & Format(DTPicker1, "MM") & "' order by nomer_biaya desc", conn
    If Not rs.EOF Then
        No = "BY" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "BY" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
'    printBukti
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = querybiaya
    frmSearch.nmform = "frmtransbiaya"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.keyIni = "t_biaya"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
On Error GoTo err
    If cmbKodeBiaya.text = "" Then
        MsgBox "Silahkan pilih jenis biaya terlebih dahulu"
        cmbKodeBiaya.SetFocus
        Exit Sub
    End If
    If txtJumlah.text = "0" Then
        MsgBox "Silahkan masukkan jumlah biaya terlebih dahulu"
        txtJumlah.SetFocus
        Exit Sub
    End If
    
    conn.Open strcon
    conn.BeginTrans
    i = 1
    id = lblNoTrans
    If lblNoTrans = "-" Then
        nomor_baru
    Else
        conn.Execute "delete from t_biaya where nomer_biaya='" & lblNoTrans & "'"
'        deletebukukas lblNoTrans, conn
'        deletebukubank lblNoTrans, conn
    End If
    add_dataheader
'    If Opt2(0).value = True Then
'        insertbukukas lblNoTrans, DTPicker1, txtJumlah * -1, "Pembayaran Biaya " & Mid(cmbKodeBiaya.text, InStr(1, cmbKodeBiaya.text, "-") + 1, Len(cmbKodeBiaya.text)) & ", " & txtKeterangan, conn
'    Else
'        insertbukubank lblNoTrans, Left(cmbNoRek.text, InStr(1, cmbNoRek.text, " -") - 1), DTPicker1, txtJumlah * -1, "Pembayaran Biaya " & Mid(cmbKodeBiaya.text, InStr(1, cmbKodeBiaya.text, "-") + 1, Len(cmbKodeBiaya.text)) & ", " & txtKeterangan, conn
'    End If
    conn.CommitTrans
    i = 0
    DropConnection
    MsgBox "Data sudah tersimpan"
    reset_form
    MySendKeys "{tab}"
Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
    End If
    DropConnection
    
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
On Error Resume Next
    lblNoTrans = "-"
    txtKeterangan.text = ""

    txtJumlah = "0"
    
    cmdSimpan.Enabled = True
    txtKeterangan.SetFocus
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "t_biaya"
    fields(0) = "nomer_biaya"
    fields(1) = "tanggal"
    fields(2) = "keterangan"
    fields(3) = "jumlah"
'    fields(4) = "cara_bayar"
    fields(4) = "kode_biaya"
'    fields(6) = "kode_bank"

    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtKeterangan.text
    nilai(3) = txtJumlah.text
'    nilai(4) = IIf(Opt2(0).value = True, "0", "1")
    nilai(4) = Left(cmbKodeBiaya.text, InStr(1, cmbKodeBiaya.text, "-") - 1)
'    If frBank.Visible = True And cmbNoRek.text <> "" Then
'    nilai(6) = Left(cmbNoRek.text, InStr(1, cmbNoRek.text, "-") - 1)
'    Else
'    nilai(6) = ""
'    End If
    
    tambah_data table_name, fields, nilai
End Sub
Public Sub cek_notrans()
Dim conn As New ADODB.Connection
    conn.ConnectionString = strcon
    cmdPrint.Visible = False
    conn.Open

    rs.Open "select * from t_biaya where nomer_biaya='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        DTPicker1 = rs("tanggal")
        txtKeterangan.text = rs("keterangan")
        txtJumlah.text = rs("jumlah")
        SetComboTextLeft rs!kode_biaya, cmbKodeBiaya
'        SetComboTextLeft rs!kode_bank, cmbNoRek
'        If rs!cara_bayar = "0" Then
'            Opt2(0).value = True
'        Else
'            Opt2(1).value = True
'        End If
        
        'cmdSimpan.Enabled = False
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    
'    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
'    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    DTPicker1 = Now
    reset_form
    total = 0
    
    lblGudang = gudang
    
    
    
End Sub
Private Sub loadcombo()
    conn.ConnectionString = strcon
    conn.Open
    cmbKodeBiaya.Clear
    rs.Open "select * from ms_biaya order by kode_biaya", conn
    While Not rs.EOF
        cmbKodeBiaya.AddItem rs(0) & " - " & rs(1)
        rs.MoveNext
    Wend
    rs.Close
'    cmbNoRek.Clear
'    rs.Open "select * from ms_bank order by kode_bank", conn
'    While Not rs.EOF
'        cmbNoRek.AddItem rs(0) & " - " & rs(2)
'        rs.MoveNext
'    Wend
'    rs.Close
    conn.Close
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

'Private Sub Opt2_Click(Index As Integer)
'If Index = 1 Then
'frBank.Visible = True
'Else
'frBank.Visible = False
'End If
'End Sub

Private Sub txtJumlah_GotFocus()
    txtJumlah.SelStart = 0
    txtJumlah.SelLength = Len(txtJumlah.text)
End Sub

Private Sub txtJumlah_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtJumlah.text = calc(txtJumlah.text)
    End If
End Sub

Private Sub txtJumlah_LostFocus()
    If Not IsNumeric(txtJumlah.text) Then txtJumlah.text = "0"
End Sub

