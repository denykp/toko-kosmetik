VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddPembelian 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembelian"
   ClientHeight    =   8640
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   13620
   ForeColor       =   &H8000000F&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8640
   ScaleWidth      =   13620
   Begin VB.Frame Frame2 
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1845
      Left            =   135
      TabIndex        =   25
      Top             =   1980
      Width           =   7575
      Begin VB.TextBox txtPPN 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3780
         TabIndex        =   9
         Top             =   1395
         Width           =   780
      End
      Begin VB.TextBox txtDisc2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3780
         TabIndex        =   8
         Top             =   1035
         Width           =   780
      End
      Begin VB.TextBox txtDisc1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3780
         TabIndex        =   7
         Top             =   675
         Width           =   780
      End
      Begin VB.TextBox txtHargaJual2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   57
         Top             =   2025
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.CommandButton cmdSearchBrg 
         BackColor       =   &H0080FFFF&
         Caption         =   "F3"
         Height          =   360
         Left            =   2745
         TabIndex        =   55
         Top             =   225
         Width           =   450
      End
      Begin VB.TextBox txtHargaJual1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   10
         Top             =   1710
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   1470
         Left            =   6120
         ScaleHeight     =   1470
         ScaleWidth      =   1140
         TabIndex        =   50
         Top             =   180
         Width           =   1140
         Begin VB.CommandButton cmdDelete 
            BackColor       =   &H0080FFFF&
            Caption         =   "&Hapus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            MaskColor       =   &H00000000&
            TabIndex        =   13
            Top             =   945
            Width           =   1050
         End
         Begin VB.CommandButton cmdClear 
            BackColor       =   &H0080FFFF&
            Caption         =   "&Baru"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            MaskColor       =   &H00000000&
            TabIndex        =   12
            Top             =   495
            Width           =   1050
         End
         Begin VB.CommandButton cmdOk 
            BackColor       =   &H0080FFFF&
            Caption         =   "&Tambahkan"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   0
            MaskColor       =   &H00000000&
            TabIndex        =   11
            Top             =   60
            Width           =   1050
         End
      End
      Begin VB.TextBox txtHarga 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   6
         Top             =   990
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   5
         Top             =   630
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   4
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label lblQtyGrosir 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "kategori"
         Height          =   330
         Left            =   1800
         TabIndex        =   75
         Top             =   2385
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.Label lblDisc 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "kategori"
         Height          =   330
         Left            =   1215
         TabIndex        =   74
         Top             =   2385
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.Label lblKonversi2 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   330
         Left            =   3060
         TabIndex        =   73
         Top             =   2025
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label lblKonversi1 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   330
         Left            =   2565
         TabIndex        =   72
         Top             =   2025
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label lblMerk 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Merk"
         Height          =   330
         Left            =   1980
         TabIndex        =   71
         Top             =   1395
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4590
         TabIndex        =   70
         Top             =   1440
         Width           =   165
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4590
         TabIndex        =   69
         Top             =   1080
         Width           =   165
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4590
         TabIndex        =   68
         Top             =   675
         Width           =   165
      End
      Begin VB.Label Label21 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "PPN"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3105
         TabIndex        =   67
         Top             =   1395
         Width           =   600
      End
      Begin VB.Label Label13 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Disc2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3105
         TabIndex        =   66
         Top             =   1035
         Width           =   600
      End
      Begin VB.Label Label11 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Disc1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3105
         TabIndex        =   65
         Top             =   675
         Width           =   600
      End
      Begin VB.Label lblkategori 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "kategori"
         Height          =   330
         Left            =   1260
         TabIndex        =   54
         Top             =   1395
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.Label Label15 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Jual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   315
         TabIndex        =   53
         Top             =   1800
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label Label8 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Harga"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   46
         Top             =   1035
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   6480
         TabIndex        =   31
         Top             =   1305
         Width           =   870
      End
      Begin VB.Label lblID 
         BackStyle       =   0  'Transparent
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   240
         Left            =   270
         TabIndex        =   30
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   29
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   28
         Top             =   675
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "caption"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3450
         TabIndex        =   27
         Top             =   300
         Width           =   600
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2520
         TabIndex        =   26
         Top             =   675
         Width           =   600
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Cara Bayar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3555
      TabIndex        =   2
      Top             =   1350
      Width           =   2535
      Begin VB.OptionButton Opt2 
         Caption         =   "Kredit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1260
         TabIndex        =   62
         Top             =   180
         Value           =   -1  'True
         Width           =   870
      End
      Begin VB.OptionButton Opt2 
         Caption         =   "Tunai"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   180
         TabIndex        =   61
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frJT 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   6165
      TabIndex        =   58
      Top             =   1440
      Width           =   3300
      Begin MSComCtl2.DTPicker DTJatuhTempo 
         Height          =   330
         Left            =   1575
         TabIndex        =   3
         Top             =   0
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   85852163
         CurrentDate     =   38927
      End
      Begin VB.Label Label20 
         BackStyle       =   0  'Transparent
         Caption         =   ":"
         Height          =   240
         Left            =   1440
         TabIndex        =   60
         Top             =   45
         Width           =   105
      End
      Begin VB.Label Label18 
         BackStyle       =   0  'Transparent
         Caption         =   "Jatuh Tempo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   59
         Top             =   45
         Width           =   1320
      End
   End
   Begin VB.CommandButton cmdSetting 
      BackColor       =   &H008080FF&
      Caption         =   "Setting Import"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8145
      TabIndex        =   56
      Top             =   3555
      Width           =   1230
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H008080FF&
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1665
      TabIndex        =   15
      Top             =   7965
      Width           =   1230
   End
   Begin VB.CommandButton cmdSave 
      BackColor       =   &H008080FF&
      Caption         =   "&Save Excel "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8145
      TabIndex        =   52
      Top             =   3150
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdLoad2 
      BackColor       =   &H008080FF&
      Caption         =   "&Load Excel 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8145
      TabIndex        =   51
      Top             =   2700
      Width           =   1230
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9945
      Top             =   135
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdLoad 
      BackColor       =   &H008080FF&
      Caption         =   "&Load Excel 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8145
      TabIndex        =   49
      Top             =   2295
      Width           =   1230
   End
   Begin VB.CommandButton cmdSearchSupplier 
      Appearance      =   0  'Flat
      BackColor       =   &H008080FF&
      Caption         =   "F3"
      Height          =   375
      Left            =   2970
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   44
      Top             =   990
      Width           =   540
   End
   Begin VB.TextBox txtKodeSupplier 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   990
      Width           =   1230
   End
   Begin VB.TextBox txtBukti 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1620
      TabIndex        =   1
      Top             =   1440
      Width           =   1860
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   33
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   32
      Top             =   180
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.CommandButton cmdSearchID 
      BackColor       =   &H008080FF&
      Caption         =   "F5"
      Height          =   375
      Left            =   3570
      TabIndex        =   24
      Top             =   135
      Width           =   540
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   12510
      TabIndex        =   17
      Top             =   1260
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H008080FF&
      Caption         =   "&Keluar (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3075
      TabIndex        =   16
      Top             =   7965
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H008080FF&
      Caption         =   "&Simpan (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   180
      TabIndex        =   14
      Top             =   7965
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   19
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   85852163
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   3300
      Left            =   135
      TabIndex        =   18
      Top             =   4005
      Width           =   13245
      _ExtentX        =   23363
      _ExtentY        =   5821
      _Version        =   393216
      Cols            =   21
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   64
      Top             =   1440
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   63
      Top             =   630
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6840
      TabIndex        =   48
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7830
      TabIndex        =   47
      Top             =   7515
      Width           =   1950
   End
   Begin VB.Label lblNamaSupplier 
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3600
      TabIndex        =   45
      Top             =   1035
      Width           =   3120
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   5595
      TabIndex        =   43
      Top             =   90
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   6975
      TabIndex        =   42
      Top             =   90
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label23 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   41
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   40
      Top             =   1035
      Width           =   960
   End
   Begin VB.Label lblGudang 
      BackStyle       =   0  'Transparent
      Caption         =   "Gudang"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   7095
      TabIndex        =   39
      Top             =   90
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label Label16 
      BackStyle       =   0  'Transparent
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   38
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2025
      TabIndex        =   37
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "No Faktur"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   36
      Top             =   1485
      Width           =   1275
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4500
      TabIndex        =   35
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label Label17 
      BackStyle       =   0  'Transparent
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3555
      TabIndex        =   34
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   23
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   12330
      TabIndex        =   22
      Top             =   1320
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   10980
      TabIndex        =   21
      Top             =   1290
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      BackStyle       =   0  'Transparent
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   20
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSettingExcel 
         Caption         =   "Setting Excel"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddPembelian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const queryTransfer As String = "select ID,Tanggal,JatuhTempo,Invoice,[Kode Supplier],Keterangan,Gudang  from t_pembelianh"
Dim total As Currency
Dim mode As Byte
Dim foc As Byte
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Dim NoTrans2 As String
Dim colname() As String
Private Sub nomor_baru()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
No = ""
    conn.Open strcon
    rs.Open "select top 1 ID from T_pembelianH where mid(ID,3,4)='" & Format(DTPicker1, "yyMM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "PB" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "PB" & Format(DTPicker1, "yyMM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    conn.Close
End Sub
Private Sub nomor_bayarhutang()
Dim No As String
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
No = ""
    conn.Open strcon
    rs.Open "select top 1 ID from T_bayarhutang where mid(ID,3,4)='" & Format(DTPicker1, "yyMM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "BH" & Format(DTPicker1, "yyMM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "BH" & Format(DTPicker1, "yyMM") & Format("1", "00000")
    End If
    rs.Close
    NoTrans2 = No
    conn.Close
End Sub

Private Sub chkNumbering_Click()
If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub

'Private Sub cmdHapus_Click()
'Dim mode As Byte
'On Error GoTo err
'    mode = 0
'    If MsgBox("Apakah anda yakin hendak menghapus?", vbYesNo) = vbYes Then
'        conn.ConnectionString = strcon
'        conn.Open
'        conn.BeginTrans
'        mode = 1
'        conn.Execute "delete from t_pembelianh  where id='" & lblNoTrans & "'"
'        conn.Execute "delete from t_pembeliand  where id='" & lblNoTrans & "'"
'        conn.CommitTrans
'        mode = 0
'        conn.Close
'        MsgBox "Data sudah dihapus"
'        reset_form
'    End If
'    Exit Sub
'err:
'    If mode = 1 Then conn.RollbackTrans
'    dropconnection
'End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    lblSatuan = ""
    LblNamaBarang = ""
    lblkategori = ""
    txtQty.text = "1"
    txtHarga.text = "0"
    txtHargaJual1.text = "0"
    txtHargaJual2.text = "0"
    txtDisc1.text = "0"
    txtDisc2.text = "0"
    txtPPN.text = "0"
    
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    If flxGrid.rows <= 2 Then Exit Sub
    flxGrid.TextMatrix(flxGrid.row, 0) = ""

    row = flxGrid.row
    totalQty = totalQty - (flxGrid.TextMatrix(row, 20))
    total = total - (flxGrid.TextMatrix(row, 10))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")

    For row = row To flxGrid.rows - 1
        If row = flxGrid.rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1
        
    flxGrid.rows = flxGrid.rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 10
    txtKdBrg.text = ""
    lblSatuan = ""
    txtQty.text = "1"
    txtHarga.text = "0"
    txtHargaJual1.text = "0"
    LblNamaBarang = ""
    lblkategori = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    lblItem = flxGrid.rows - 1
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdLoad_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile1 CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub loadexcelfile1(filename As String)
On Error GoTo err
Dim exapp As Object
Dim exwb As Object
Dim exws As Object
Dim range As String
Dim col As Byte
Dim colkodebarang As String
Dim colnama As String
Dim colkategori As String
Dim colmerk As String
Dim colqty As String
Dim colhargabeli As String
Dim colhargajual1 As String
Dim colhargajual2 As String
Dim colkonversi1 As String
Dim colkonversi2 As String
Dim colqtygrosir As String
Dim coldisc As String
Dim coldisc1 As String
Dim coldisc2 As String
Dim colppn As String
Dim colindexkodebarang As String
Dim colindexnama As String
Dim colindexkategori As String
Dim colindexmerk As String
Dim colindexqty As String
Dim colindexhargabeli As String
Dim colindexhargajual1 As String
Dim colindexhargajual2 As String
Dim colindexqtygrosir As String
Dim colindexdisc As String
Dim colindexkonversi1 As String
Dim colindexkonversi2 As String
Dim colindexdisc1 As String
Dim colindexdisc2 As String
Dim colindexppn As String


Dim i As Integer
Set exapp = CreateObject("excel.application")
Set exwb = exapp.Workbooks.Open(filename)
Set exws = exwb.Sheets("Sheet1")
col = Asc("A")
range = Chr(col) & "1"

exws.Activate

While exws.range(range) <> ""
    ReDim Preserve colname(col - 65)
    colname(col - 65) = exws.range(range)
    col = col + 1
    range = Chr(col) & "1"
Wend

colkodebarang = sGetINI(App.Path & "\import.ini", "Pembelian", "Kode Barang", "kodebarang")
colnama = sGetINI(App.Path & "\import.ini", "Pembelian", "Nama", "nama")
colkategori = sGetINI(App.Path & "\import.ini", "Pembelian", "Kategori", "kategori")
colmerk = sGetINI(App.Path & "\import.ini", "Pembelian", "merk", "merk")
colhargabeli = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Beli", "hargapokok")
colhargajual1 = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Jual1", "hargajual1")
colhargajual2 = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Jual2", "hargajual2")
colqty = sGetINI(App.Path & "\import.ini", "Pembelian", "Qty", "stok")
colqtygrosir = sGetINI(App.Path & "\import.ini", "Pembelian", "Qty Grosir", "qtygrosir")
coldisc = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc", "disc")
coldisc1 = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc1", "disc1")
coldisc2 = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc2", "disc2")
colppn = sGetINI(App.Path & "\import.ini", "Pembelian", "ppn", "ppn")
colkonversi1 = sGetINI(App.Path & "\import.ini", "Pembelian", "konversi1", "konversi1")
colkonversi2 = sGetINI(App.Path & "\import.ini", "Pembelian", "konversi2", "konversi2")


colindexkodebarang = getcolindex(colkodebarang, colname)
colindexnama = getcolindex(colnama, colname)
colindexkategori = getcolindex(colkategori, colname)
colindexmerk = getcolindex(colmerk, colname)
colindexqty = getcolindex(colqty, colname)
colindexhargabeli = getcolindex(colhargabeli, colname)
colindexhargajual1 = getcolindex(colhargajual1, colname)
colindexhargajual2 = getcolindex(colhargajual2, colname)
colindexqtygrosir = getcolindex(colqtygrosir, colname)
colindexdisc = getcolindex(coldisc, colname)
colindexdisc1 = getcolindex(coldisc1, colname)
colindexdisc2 = getcolindex(coldisc2, colname)
colindexppn = getcolindex(colppn, colname)
colindexkonversi1 = getcolindex(colkonversi1, colname)
colindexkonversi2 = getcolindex(colkonversi2, colname)
i = 2
With exws
While .range("A" & CStr(i)) <> ""
   
    txtKdBrg.text = .range(Chr(colindexkodebarang + 65) & CStr(i))
    If Not cek_kodebarang1 Then
        LblNamaBarang = .range(Chr(colindexnama + 65) & CStr(i))
        lblkategori = .range(Chr(colindexkategori + 65) & CStr(i))
        lblMerk = .range(Chr(colindexmerk + 65) & CStr(i))
        
    End If
    txtQty.text = .range(Chr(colindexqty + 65) & CStr(i))
    txtDisc1.text = .range(Chr(colindexdisc1 + 65) & CStr(i))
    txtDisc2.text = .range(Chr(colindexdisc2 + 65) & CStr(i))
    txtPPN.text = .range(Chr(colindexppn + 65) & CStr(i))
    txtHarga.text = Format(CCur(Trim(.range(Chr(colindexhargabeli + 65) & CStr(i)))), "###0.00")
    txtHargaJual1.text = Format(CCur(Trim(.range(Chr(colindexhargajual1 + 65) & CStr(i)))), "###0.00")
    txtHargaJual2.text = Format(CCur(Trim(.range(Chr(colindexhargajual2 + 65) & CStr(i)))), "###0.00")
    lblKonversi1 = .range(Chr(colindexkonversi1 + 65) & CStr(i))
    lblKonversi2 = .range(Chr(colindexkonversi2 + 65) & CStr(i))
    cmdOK_Click
    
    
    flxGrid.TextMatrix(flxGrid.row, 11) = .range(Chr(colindexdisc + 65) & CStr(i))
    flxGrid.TextMatrix(flxGrid.row, 12) = .range(Chr(colindexqtygrosir + 65) & CStr(i))
    flxGrid.TextMatrix(flxGrid.row, 13) = .range(Chr(colindexhargajual1 + 65) & CStr(i))
    flxGrid.TextMatrix(flxGrid.row, 14) = .range(Chr(colindexhargajual2 + 65) & CStr(i))
    flxGrid.TextMatrix(flxGrid.row, 15) = .range(Chr(colindexkategori + 65) & CStr(i))
    
    i = i + 1
Wend
End With

exwb.Close
exapp.Application.Quit
Set exapp = Nothing
Set exwb = Nothing
Set exws = Nothing
Exit Sub
err:
MsgBox err.Description
exwb.Close
exapp.Application.Quit
Set exapp = Nothing
End Sub

Private Sub loadexcelfile(filename As String)
On Error GoTo err
Dim rs As New ADODB.Recordset
Dim con As New ADODB.Connection
Dim rs2 As New ADODB.Recordset
Dim conn As New ADODB.Connection

Dim stcon As String
stcon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filename & ";Persist Security Info=False;Extended Properties=""Excel 8.0;IMEX=1;TypeGuessRows=0"""
con.Open stcon
conn.Open strcon
Dim colkodebarang As String
Dim colnama As String
Dim colkategori As String
Dim colmerk As String
Dim colqty As String
Dim colhargabeli As String
Dim colhargajual1 As String
Dim colhargajual2 As String
Dim colkonversi1 As String
Dim colkonversi2 As String
Dim colqtygrosir As String
Dim coldisc As String
Dim coldisc1 As String
Dim coldisc2 As String
Dim colppn As String
colkodebarang = sGetINI(App.Path & "\import.ini", "Pembelian", "Kode Barang", "kodebarang")
colnama = sGetINI(App.Path & "\import.ini", "Pembelian", "Nama", "nama")
colkategori = sGetINI(App.Path & "\import.ini", "Pembelian", "Kategori", "kategori")
colmerk = sGetINI(App.Path & "\import.ini", "Pembelian", "merk", "merk")
colhargabeli = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Beli", "hargapokok")
colhargajual1 = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Jual1", "hargajual1")
colhargajual2 = sGetINI(App.Path & "\import.ini", "Pembelian", "Harga Jual2", "hargajual2")
colqty = sGetINI(App.Path & "\import.ini", "Pembelian", "Qty", "stok")
colqtygrosir = sGetINI(App.Path & "\import.ini", "Pembelian", "Qty Grosir", "qtygrosir")
coldisc = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc", "disc")
coldisc1 = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc1", "disc1")
coldisc2 = sGetINI(App.Path & "\import.ini", "Pembelian", "Disc2", "disc2")
colppn = sGetINI(App.Path & "\import.ini", "Pembelian", "ppn", "ppn")
colkonversi1 = sGetINI(App.Path & "\import.ini", "Pembelian", "konversi1", "konversi1")
colkonversi2 = sGetINI(App.Path & "\import.ini", "Pembelian", "konversi2", "konversi2")
rs.Open "select * from [sheet1$]", con
While Not rs.EOF
    If rs(0) <> "E" And Not IsNull(rs(colkodebarang)) And rs(colkodebarang) <> "" Then
'        rs2.Open "select * from ms_barang where [kode barang]='" & rs("kodebarang") & "'", conn
'        If rs2.EOF Then
''            newitem rs("kodebarang"), rs("namabarang"), rs("disc1"), rs("disc2"), rs("disc3"), rs("disc4"), rs("disc5"),  rs("hargajual")
'        Else
    '        updateitem rs("kodebarang"), rs("namabarang"), rs("disc1"), rs("disc2"), rs("disc3"), rs("disc4"), rs("disc5"), rs("hargajual")
            txtKdBrg.text = rs(colkodebarang)
            If Not cek_kodebarang1 Then
            LblNamaBarang = rs(colnama)
            lblkategori = rs(colkategori)
            lblMerk = rs(colmerk)
            End If
            txtQty.text = rs(colqty)
            txtHarga.text = Format(rs(colhargabeli), "###0.00")
            txtHargaJual1.text = Format(rs(colhargajual1), "###0.00")
            txtHargaJual2.text = Format(rs(colhargajual2), "###0.00")
            txtDisc1.text = rs(coldisc1)
            txtDisc2.text = rs(coldisc2)
            txtPPN.text = rs(colppn)
            lblKonversi1 = rs(colkonversi1)
            lblKonversi2 = rs(colkonversi2)
            cmdOK_Click
            flxGrid.TextMatrix(flxGrid.row, 11) = rs(coldisc)
            flxGrid.TextMatrix(flxGrid.row, 12) = rs(colqtygrosir)
            flxGrid.TextMatrix(flxGrid.row, 13) = Format(rs(colhargajual1), "###0.00")
            flxGrid.TextMatrix(flxGrid.row, 14) = Format(rs(colhargajual2), "###0.00")
            
            flxGrid.TextMatrix(flxGrid.row, 15) = IIf(IsNull(rs(colkategori)), "", rs(colkategori))
            
'        End If
'        rs2.Close
    End If
    rs.MoveNext
Wend
If rs.State Then rs.Close
If con.State Then con.Close
If conn.State Then conn.Close
Exit Sub
err:
MsgBox err.Description
If rs.State Then rs.Close
If con.State Then con.Close
DropConnection
End Sub
Private Sub updateitem(kodebarang As String, namabarang As String, harga1 As String, harga2 As String, disc As String, qtygrosir As String, conn As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(6)
    ReDim nilai(6)

    table_name = "MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    Dim counter As Integer
    counter = 1
    If harga1 > 0 Then
        counter = counter + 1
        fields(counter) = "Harga1"
        nilai(counter) = harga1
    End If
    If harga2 > 0 Then
        counter = counter + 1
        fields(counter) = "Harga2"
        nilai(counter) = harga2
    End If
    If qtygrosir > 0 Then
        counter = counter + 1
        fields(counter) = "qtygrosir"
        nilai(counter) = qtygrosir
    End If
    If disc > 0 Then
        counter = counter + 1
        fields(counter) = "disc"
        nilai(counter) = disc
    End If
    
    nilai(0) = kodebarang
    nilai(1) = Replace(namabarang, "'", "''")
    ReDim Preserve fields(counter + 1)
    ReDim Preserve nilai(counter + 1)
    update_data table_name, fields, nilai, "[kode barang]='" & nilai(0) & "'", conn
    ReDim Preserve fields(counter + 3)
    ReDim Preserve nilai(counter + 3)

    table_name = "hst_MS_BARANG"
    fields(counter + 1) = "userid"
    fields(counter + 2) = "tanggal"
    
    nilai(counter + 1) = user
    nilai(counter + 2) = Format(Now, "yyyy/MM/dd hh:mm:ss")
    
    tambah_data table_name, fields, nilai

End Sub
Private Sub newitem(kodebarang As String, namabarang As String, kategori As String, merk As String, hargajual1 As String, hargajual2 As String, disc As String, qtygrosir As String, konversi1 As String, konversi2 As String, ByRef conn2 As ADODB.Connection)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(4)
    ReDim nilai(4)

    table_name = "rpt_MS_BARANG"
    fields(0) = "[KODE BARANG]"
    fields(1) = "[NAMA barang]"
    fields(2) = "[KATEGORI]"
    fields(3) = "[merk]"
    nilai(0) = kodebarang
    nilai(1) = Replace(namabarang, "'", "''")
    nilai(2) = kategori
    nilai(3) = merk
    conn2.Execute tambah_data2(table_name, fields, nilai)
    
    ReDim Preserve fields(12)
    ReDim Preserve nilai(12)
    table_name = "MS_BARANG"
    fields(4) = "SATUAN"
    fields(5) = "Harga1"
    fields(6) = "harga2"
    fields(7) = "qtygrosir"
    fields(8) = "disc"
    fields(9) = "konversi1"
    fields(10) = "konversi2"
    fields(11) = "flag"
    
    
    
    nilai(4) = "pcs"
    nilai(5) = hargajual1
    nilai(6) = hargajual2
    nilai(7) = qtygrosir
    nilai(8) = disc
    nilai(9) = konversi1
    nilai(10) = konversi2
    nilai(11) = 1
    conn2.Execute tambah_data2(table_name, fields, nilai)
    ReDim Preserve fields(14)
    ReDim Preserve nilai(14)

    table_name = "hst_MS_BARANG"
    fields(12) = "userid"
    fields(13) = "tanggal"
    nilai(12) = user
    nilai(13) = Format(Now, "yyyy/MM/dd hh:mm:ss")
    conn2.Execute tambah_data2(table_name, fields, nilai)
End Sub

Private Sub cmdLoad2_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowOpen
    If CommonDialog1.filename <> "" Then
    If fs.FileExists(CommonDialog1.filename) Then
        loadexcelfile CommonDialog1.filename
    Else
        MsgBox "File tidak dapat ditemukan"
    End If
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.rows - 1
            flxGrid.rows = flxGrid.rows + 1
        End If
        flxGrid.TextMatrix(flxGrid.row, 0) = ""
        flxGrid.row = row
        currentRow = flxGrid.row

        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        flxGrid.TextMatrix(row, 3) = txtQty
        If flxGrid.TextMatrix(row, 20) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 20))
            total = total - (flxGrid.TextMatrix(row, 10))
            flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
        End If
        
        Dim pcs() As String
        Dim konversi(2) As Integer
        konversi(0) = lblKonversi1
        konversi(1) = lblKonversi2
        konversi(2) = 1
        Dim qtypcs As Double
        qtypcs = 0
         pcs = Split(txtQty, ".")
         
        For A = UBound(pcs) To 0 Step -1
            qtypcs = qtypcs + (CInt(pcs(A)) * konversi(2 - UBound(pcs) + A))
        Next
        flxGrid.TextMatrix(row, 20) = qtypcs
        
        flxGrid.TextMatrix(row, 4) = lblSatuan
        flxGrid.TextMatrix(row, 5) = txtHarga.text
        flxGrid.TextMatrix(row, 6) = txtDisc1.text
        flxGrid.TextMatrix(row, 7) = txtDisc2.text
        flxGrid.TextMatrix(row, 8) = txtPPN.text
        flxGrid.TextMatrix(row, 9) = txtHarga * (((100 - txtDisc1) / 100) * ((100 - txtDisc2) / 100) * ((100 + txtPPN) / 100))
        flxGrid.TextMatrix(row, 10) = flxGrid.TextMatrix(row, 20) * flxGrid.TextMatrix(row, 9)
        
        flxGrid.TextMatrix(flxGrid.row, 11) = lblDisc
        flxGrid.TextMatrix(flxGrid.row, 12) = lblQtyGrosir
        
        flxGrid.TextMatrix(row, 13) = txtHargaJual1.text
        flxGrid.TextMatrix(row, 14) = txtHargaJual2.text
        flxGrid.TextMatrix(row, 16) = txtHargaJual1.text
        flxGrid.TextMatrix(row, 15) = lblkategori
        flxGrid.TextMatrix(row, 17) = lblMerk
        flxGrid.TextMatrix(row, 18) = lblKonversi1
        flxGrid.TextMatrix(row, 19) = lblKonversi2
        flxGrid.row = row
        flxGrid.col = 0
        flxGrid.ColSel = 13
        totalQty = totalQty + flxGrid.TextMatrix(row, 20)
        total = total + (flxGrid.TextMatrix(row, 10))
        lblQty = Format(totalQty, "#,##0")
        lblTotal = Format(total, "#,##0")
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang = ""
        lblkategori = ""
        lblMerk = ""
        lblKonversi1 = "1"
        lblKonversi2 = "1"
        txtQty.text = "1"
        txtHarga.text = "0"
        txtHargaJual1.text = "0"
        txtHargaJual2.text = "0"
        lblDisc = "0"
        lblQtyGrosir = "0"
'        txtDisc1 = "0"
'        txtDisc2 = "0"
        
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.rows - 2
End Sub

Private Sub posting(kdbrg As String, i As Integer)
Dim stock, stock2 As Long
Dim hpp1, hpp2, newhpp As Currency
stock = getStock4(kdbrg, strcon)

hpp1 = getHpp(kdbrg, strcon)
stock2 = flxGrid.TextMatrix(i, 3)
hpp2 = flxGrid.TextMatrix(i, 5)
newhpp = IIf(stock <= 0, hpp2, ((stock * hpp1) + (stock2 * hpp2)) / IIf((stock + stock2) <= 0, 1, (stock + stock2)))
conn.Execute "update hpp set hpp='" & newhpp & "' where [kode barang]='" & kdbrg & "'"

End Sub
Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transferpembelian.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_pembelianh.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdReset_Click()
    reset_form
End Sub

Private Sub cmdSave_Click()
On Error GoTo err
Dim fs As New Scripting.FileSystemObject
    CommonDialog1.filter = "*.xls"
    CommonDialog1.filename = "Excel Filename"
    CommonDialog1.ShowSave
    If CommonDialog1.filename <> "" Then
        saveexcelfile CommonDialog1.filename
    End If
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub saveexcelfile(filename As String)
On Error GoTo err
Dim exapp As New Excel.Application
Dim exwb As Excel.Workbook
Dim exws As Excel.Worksheet
Dim range As String
Dim col As Byte
Dim i As Integer
'Set exapp = CreateObject("excel.application")

Set exwb = exapp.Workbooks.Add
Set exws = exwb.Sheets("Sheet1")

col = Asc("A")
range = Chr(col) & "1"

'While exapp.Workbooks.Application.range(range) <> ""
'    ReDim Preserve colname(col - 65)
'    colname(col - 65) = exws.range(range)
'    col = col + 1
'    range = Chr(col) & "1"
'Wend
exws.range(Chr(0 + 65) & "1") = "H"
exws.range(Chr(1 + 65) & "1") = "KodeBarang"
exws.range(Chr(3 + 65) & "1") = "Nama"
exws.range(Chr(2 + 65) & "1") = "Kategori"
exws.range(Chr(4 + 65) & "1") = "HargaPokok"
exws.range(Chr(5 + 65) & "1") = "HargaJual"
exws.range(Chr(6 + 65) & "1") = "Stok"
'exws.range(Chr(6 + 65) & "1") = "Disc1"
'exws.range(Chr(7 + 65) & "1") = "Disc2"
'exws.range(Chr(8 + 65) & "1") = "Disc3"
'exws.range(Chr(9 + 65) & "1") = "Disc4"
'exws.range(Chr(10 + 65) & "1") = "Disc5"

i = 2
Dim hargajual As Long
Dim hargapokok As Double
Dim kategori As String
Dim disc1, disc2, disc3, disc4, disc5 As Double
conn.Open strcon
    While i < flxGrid.rows
        disc1 = 0
        disc2 = 0
        disc3 = 0
        disc4 = 0
        disc5 = 0

        rs.Open "select m.*,h.hpp from ms_barang m inner join hpp h on m.[kode barang]=h.[kode barang] where m.[kode barang]='" & flxGrid.TextMatrix(i - 1, 1) & "' and h.gudang='" & gudang & "'", conn
        If Not rs.EOF Then
            kategori = IIf(flxGrid.TextMatrix(i - 1, 13) = "", rs("kategori"), flxGrid.TextMatrix(i - 1, 13))
            hargajual = IIf(flxGrid.TextMatrix(i - 1, 12) = "", rs("harga"), flxGrid.TextMatrix(i - 1, 12))
'            disc1 = IIf(flxGrid.TextMatrix(i - 1, 7) = "", rs("disc1"), flxGrid.TextMatrix(i - 1, 7))
'            disc2 = IIf(flxGrid.TextMatrix(i - 1, 8) = "", rs("disc2"), flxGrid.TextMatrix(i - 1, 8))
'            disc3 = IIf(flxGrid.TextMatrix(i - 1, 9) = "", rs("disc3"), flxGrid.TextMatrix(i - 1, 9))
'            disc4 = IIf(flxGrid.TextMatrix(i - 1, 10) = "", rs("disc4"), flxGrid.TextMatrix(i - 1, 10))
'            disc5 = IIf(flxGrid.TextMatrix(i - 1, 11) = "", rs("disc5"), flxGrid.TextMatrix(i - 1, 11))
'
        Else
            hargajual = 0
            hargapokok = 0
            kategori = ""
        End If
        rs.Close
        exws.range(Chr(0 + 65) & CStr(i)) = "D"
        exws.range(Chr(1 + 65) & CStr(i)) = "'" & flxGrid.TextMatrix(i - 1, 1)
        exws.range(Chr(3 + 65) & CStr(i)) = flxGrid.TextMatrix(i - 1, 2)
        exws.range(Chr(4 + 65) & CStr(i)) = flxGrid.TextMatrix(i - 1, 5)
        exws.range(Chr(5 + 65) & CStr(i)) = hargajual
        exws.range(Chr(2 + 65) & CStr(i)) = kategori
        exws.range(Chr(6 + 65) & CStr(i)) = flxGrid.TextMatrix(i - 1, 3)
'        exws.range(Chr(6 + 65) & CStr(i)) = disc1
'        exws.range(Chr(7 + 65) & CStr(i)) = disc2
'        exws.range(Chr(8 + 65) & CStr(i)) = disc3
'        exws.range(Chr(9 + 65) & CStr(i)) = disc4
'        exws.range(Chr(10 + 65) & CStr(i)) = disc5
        i = i + 1
    Wend
    conn.Close
err:
exwb.SaveAs filename
exwb.Close
exapp.Application.Quit
Set exws = Nothing
Set exwb = Nothing
Set exapp = Nothing
If conn.State Then conn.Close
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "txtKdBrg"
    
    frmSearch.connstr = strcon
    frmSearch.keyIni = "ms_barang"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryTransfer
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.keyIni = "t_pembelian"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchSupplier_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select [Kode Supplier],[Nama Supplier],Alamat,Telp,Fax,Email,[No Rekening],[Contact Person],Keterangan from ms_supplier"
    frmSearch.nmform = "frmAddpembelian"
    frmSearch.nmctrl = "txtKodeSupplier"
    frmSearch.keyIni = "ms_supplier"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_supplier"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSetting_Click()
    frmSettingExcel.list = "Kode Barang,Kategori,Merk,Nama,Harga Beli,disc1,disc2,ppn,Harga Jual1,Harga Jual2,Qty Grosir,Disc,Qty,Konversi1,Konversi2"
    frmSettingExcel.modul = "Pembelian"
    frmSettingExcel.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
Dim conn2 As New ADODB.Connection
On Error GoTo err
'    If flxGrid.rows <= 2 Then
'        MsgBox "Silahkan masukkan barang yang ingin dibeli terlebih dahulu"
'        txtKdBrg.SetFocus
'        Exit Sub
'    End If
    If txtKodeSupplier.text = "" Then
        MsgBox "Silahkan masukkan Supplier terlebih dahulu"
        txtKodeSupplier.SetFocus
        Exit Sub
    End If
    id = lblNoTrans
    
    conn.ConnectionString = strcon
    conn.Open
    
    conn2.Open strcon
    
    If chkNumbering <> "1" Then
    rs.Open "select * from t_pembelianh where id='" & txtID.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtID.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        DropConnection
        Exit Sub
    End If
    rs.Close
    End If
    conn.BeginTrans
    
    i = 1
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
    Else
        deletekartustock "Pembelian", lblNoTrans, conn
        conn.Execute "delete from t_pembelianh where ID='" & lblNoTrans & "'"
        rs.Open "select * from t_pembeliand where ID='" & lblNoTrans & "'", conn
        While Not rs.EOF
            updatestock gudang, rs("kode barang"), rs("qty") * -1, conn
            rs.MoveNext
        Wend
        rs.Close
        conn.Execute "delete from t_pembeliand where ID='" & lblNoTrans & "'"
    
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    add_dataheader
    For row = 1 To flxGrid.rows - 2
        If flxGrid.TextMatrix(row, 1) <> "" Then
                With flxGrid
                If Not cek_kategori(.TextMatrix(row, 15), conn) Then
                    conn.Execute "insert into var_kategori values ('" & .TextMatrix(row, 15) & "')"
                End If
                If Not cek_merk(.TextMatrix(row, 17), conn) Then
                    conn.Execute "insert into var_merk values ('" & .TextMatrix(row, 17) & "')"
                End If
                If Not cek_barang(.TextMatrix(row, 1), conn) Then
                    newitem .TextMatrix(row, 1), .TextMatrix(row, 2), .TextMatrix(row, 15), .TextMatrix(row, 17), .TextMatrix(row, 13), .TextMatrix(row, 14), .TextMatrix(row, 11), .TextMatrix(row, 12), .TextMatrix(row, 18), .TextMatrix(row, 19), conn
                Else
                    updateitem .TextMatrix(row, 1), .TextMatrix(row, 2), .TextMatrix(row, 13), .TextMatrix(row, 14), .TextMatrix(row, 11), .TextMatrix(row, 12), conn
                End If
                End With
                rs.Open "select * from stock where [kode barang]='" & flxGrid.TextMatrix(row, 1) & "' and gudang='" & gudang & "'", conn
                If rs.EOF Then
                    rs.Close
                    conn.Execute "insert into stock  (gudang,[kode barang],stock)  values ('" & gudang & "','" & flxGrid.TextMatrix(row, 1) & "',0)"
                
                End If
                If rs.State Then rs.Close
                rs.Open "select * from hpp where [kode barang]='" & flxGrid.TextMatrix(row, 1) & "'", conn
                If rs.EOF Then
                    rs.Close
                    
                    conn.Execute "insert into hpp ([kode barang],hpp)  values ('" & flxGrid.TextMatrix(row, 1) & "',0)"
                    conn.Execute "update hpp set hpp=" & flxGrid.TextMatrix(row, 5) & " where [kode barang]='" & flxGrid.TextMatrix(row, 1) & "'"
                    
                End If
                If rs.State Then rs.Close
            add_datadetail (row)
            insertkartustock "Pembelian", lblNoTrans, DTPicker1, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3), CStr(gudang), txtKeterangan, conn
        End If
    Next
    
    conn.CommitTrans
    conn.BeginTrans
    
    
    
    For row = 1 To flxGrid.rows - 2
        posting flxGrid.TextMatrix(row, 1), row
        updatestock gudang, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3), conn
        
            
    Next
    conn.Execute "update t_pembelianh set status='1' where id='" & lblNoTrans & "'"
    If Opt2(0).value = True Then
    nomor_bayarhutang
    conn.Execute "insert into t_bayarhutang (id,tanggal,no_beli,[jumlah bayar],keterangan) values ('" & NoTrans2 & "','" & Format(DTPicker1, "yyyy/MM/dd") & "','" & lblNoTrans & "'," & Format(lblTotal, "###0.##") & ",'')"
    NoTrans2 = ""
    End If
    conn.CommitTrans
   
    i = 0
    DropConnection
    
    MsgBox "Data sudah tersimpan"
    reset_form
    
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    txtBukti.text = ""
    txtKeterangan.text = ""
    txtKodeSupplier.text = ""
    loadcombo
    totalQty = 0
    total = 0
    lblTotal = 0
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTJatuhTempo = Now
    cmdClear_Click
    flxGrid.rows = 1
    flxGrid.rows = 2
    cmdSimpan.Enabled = True
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(12)
    ReDim nilai(12)
    table_name = "t_pembelianh"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "[kode supplier]"
    fields(4) = "invoice"
    fields(5) = "keterangan"
    fields(6) = "jatuhtempo"
    fields(7) = "pk"
    fields(8) = "USERID"
    fields(9) = "CARA_BAYAR"
    fields(10) = "TOTAL"
    fields(11) = "JUMLAH_BAYAR"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = gudang
    nilai(3) = txtKodeSupplier.text
    nilai(4) = txtBukti.text
    nilai(5) = txtKeterangan.text
    nilai(6) = Format(DTJatuhTempo, "yyyy/MM/dd hh:mm:ss")
    nilai(7) = NoTrans2
    nilai(8) = user
    nilai(9) = IIf(Opt2(0).value = True, "1", "2")
    nilai(10) = total
    nilai(11) = IIf(Opt2(0).value = True, total, "0")
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(10)
    ReDim nilai(10)
    
    table_name = "t_pembeliand"
    fields(0) = "ID"
    fields(1) = "[kode barang]"
    fields(2) = "qty_beli"
    fields(3) = "qty"
    fields(4) = "URUT"
    fields(5) = "HARGA_BELI"
    fields(6) = "DISC1"
    fields(7) = "DISC2"
    fields(8) = "PPN"
    fields(9) = "HARGA"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = flxGrid.TextMatrix(row, 20)
    nilai(4) = row
    nilai(5) = flxGrid.TextMatrix(row, 5)
    nilai(6) = flxGrid.TextMatrix(row, 6)
    nilai(7) = flxGrid.TextMatrix(row, 7)
    nilai(8) = flxGrid.TextMatrix(row, 8)
    nilai(9) = flxGrid.TextMatrix(row, 9)
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
'    cmdPrint.Visible = False
    
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from t_pembelianh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
'        NoTrans2 = rs("pk")
        If rs!cara_bayar = "1" Then
            Opt2(0).value = True
        Else
            Opt2(1).value = True
        End If
        DTPicker1 = rs("tanggal")
        DTJatuhTempo = rs("jatuhtempo")
        txtKeterangan.text = rs("keterangan")
        txtKodeSupplier.text = rs("kode supplier")
        txtBukti.text = rs("invoice")
        totalQty = 0
        total = 0
'        cmdPrint.Visible = True
        If rs("status") = 1 Then
'            cmdPrint.Enabled = False
            cmdSimpan.Enabled = False
            mnuSave.Enabled = False
        Else
            cmdSimpan.Enabled = True
            mnuSave.Enabled = True
        End If
        If rs.State Then rs.Close
        flxGrid.rows = 1
        flxGrid.rows = 2
        row = 1
        
        rs.Open "select d.[kode barang],m.[nama barang],kategori,merk, qty_beli,qty,d.harga_beli,d.disc1,d.disc2,d.ppn,harga,harga1,harga2,disc,qtygrosir,konversi1,konversi2 from t_pembeliand d inner join ms_barang m on d.[kode barang]=m.[kode barang] where d.id='" & lblNoTrans & "' order by urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs!qty_beli
                flxGrid.TextMatrix(row, 4) = ""
                flxGrid.TextMatrix(row, 5) = rs!harga_beli
                flxGrid.TextMatrix(row, 6) = rs!disc1
                flxGrid.TextMatrix(row, 7) = rs!disc2
                flxGrid.TextMatrix(row, 8) = rs!ppn
                flxGrid.TextMatrix(row, 9) = rs!harga
                flxGrid.TextMatrix(row, 10) = Format(rs!qty * rs!harga, "#,##0")
                flxGrid.TextMatrix(row, 11) = rs!disc
                flxGrid.TextMatrix(row, 12) = rs!qtygrosir
                flxGrid.TextMatrix(row, 13) = rs!harga1
                flxGrid.TextMatrix(row, 14) = rs!harga2
                flxGrid.TextMatrix(row, 16) = rs!harga1
                flxGrid.TextMatrix(row, 15) = rs!kategori
                flxGrid.TextMatrix(row, 17) = rs!merk
                flxGrid.TextMatrix(row, 18) = rs!konversi1
                flxGrid.TextMatrix(row, 19) = rs!konversi2
                flxGrid.TextMatrix(row, 20) = rs!qty
                row = row + 1
                totalQty = totalQty + rs!qty
                total = total + (rs!qty * rs!harga)
                flxGrid.rows = flxGrid.rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty
        lblTotal = total
        lblItem = flxGrid.rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub flxGrid_Click()
'    currentRow = flxGrid.row
'    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
'
'        mode = 2
'        cmdClear.Enabled = True
'        cmdDelete.Enabled = True
'        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
'        If Not cek_kodebarang1 Then
'            LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
'            End If
'        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
'        txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 5)
'    Else
'        mode = 1
'        cmdClear.Enabled = False
'        cmdDelete.Enabled = False
'    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    ElseIf KeyCode = vbKeyReturn Then
        If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
            mode = 2
            cmdClear.Enabled = True
            cmdDelete.Enabled = True
            txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
            If Not cek_kodebarang1 Then
                LblNamaBarang = flxGrid.TextMatrix(flxGrid.row, 2)
                
                End If
            txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
            txtHarga.text = flxGrid.TextMatrix(flxGrid.row, 5)
            txtDisc1.text = flxGrid.TextMatrix(flxGrid.row, 6)
            txtDisc2.text = flxGrid.TextMatrix(flxGrid.row, 7)
            txtPPN.text = flxGrid.TextMatrix(flxGrid.row, 8)
            
            txtHargaJual1.text = flxGrid.TextMatrix(flxGrid.row, 13)
            txtHargaJual2.text = flxGrid.TextMatrix(flxGrid.row, 14)
            txtQty.SetFocus
        End If
    End If
    
End Sub

Private Sub flxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub

Private Sub Form_Activate()
'    mySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
'    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
'    If KeyCode = vbKeyF4 Then cmdSearchSupplier_Click
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    conn.ConnectionString = strcon
    conn.Open
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    'End If
    'rs.Close
    
    flxGrid.ColWidth(0) = 300
'    flxGrid.ColWidth(1) = 1200
'    flxGrid.ColWidth(2) = 2800
'    flxGrid.ColWidth(3) = 900
    
    flxGrid.ColWidth(5) = 1000
    flxGrid.ColWidth(6) = 1000
    flxGrid.ColWidth(7) = 1000
    flxGrid.ColWidth(8) = 1000
    flxGrid.ColWidth(9) = 1000
    flxGrid.ColWidth(10) = 1000
    flxGrid.ColWidth(4) = 0
    flxGrid.ColWidth(11) = 0
    flxGrid.ColWidth(12) = 0
    flxGrid.ColWidth(13) = 0
    flxGrid.ColWidth(14) = 0
    flxGrid.ColWidth(15) = 0
    flxGrid.ColWidth(16) = 0
    flxGrid.ColWidth(17) = 0
    flxGrid.ColWidth(18) = 0
    flxGrid.ColWidth(19) = 0
    flxGrid.ColWidth(20) = 0
    
    conn.Close
    'newitem rs("kodebarang"), rs("namabarang"), rs("disc1"), rs("disc2"), rs("disc3"), rs("disc4"), rs("disc5"),  rs("hargajual")
    sizeFlxGrid "select d.[kode barang],m.[nama barang], qty,'' as satuan,harga_beli,d.disc1,d.disc2,ppn,harga,0 as total from t_pembeliand d inner join ms_barang m on d.[kode barang]=m.[kode barang]", flxGrid, "FlxPembelian", 1

    
    
    
    If right_hpp = True Then
        txtHarga.Visible = True
        Label8.Visible = True
        lblTotal.Visible = True
        Label10.Visible = True
    End If
    If right_harga = True Then
'        flxGrid.ColWidth(13) = 0 '1000
'        txtHargaJual.Visible = True
'        Label15.Visible = True
    End If
    
    
    
    flxGrid.TextMatrix(0, 1) = "kode barang"
    flxGrid.ColAlignment(1) = 1 'vbAlignLeft
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    
    flxGrid.TextMatrix(0, 2) = "nama"
    flxGrid.TextMatrix(0, 3) = "Qty"
    flxGrid.TextMatrix(0, 4) = "Satuan"
    flxGrid.TextMatrix(0, 5) = "Harga"
    flxGrid.TextMatrix(0, 6) = "Disc1"
    flxGrid.TextMatrix(0, 7) = "Disc2"
    flxGrid.TextMatrix(0, 8) = "PPN"
    flxGrid.TextMatrix(0, 9) = "Netto"
    flxGrid.TextMatrix(0, 10) = "Total"
    flxGrid.TextMatrix(0, 11) = "disc"
    flxGrid.TextMatrix(0, 12) = "qtygrosir"
    flxGrid.TextMatrix(0, 13) = "hargajual1"
    flxGrid.TextMatrix(0, 14) = "hargajual2"
    flxGrid.TextMatrix(0, 15) = "kategori"
    flxGrid.TextMatrix(0, 16) = "Harga Jual special"
    flxGrid.TextMatrix(0, 17) = "Merk"
    flxGrid.TextMatrix(0, 18) = "konversi1"
    flxGrid.TextMatrix(0, 19) = "konversi2"
    flxGrid.TextMatrix(0, 20) = "QtyPcs"
    

End Sub
Private Sub loadcombo()
'    conn.ConnectionString = strcon
'    conn.Open
'    conn.Close
End Sub
Public Function cek_kodebarang1() As Boolean
On Error GoTo ex
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select [nama barang],kategori,merk,harga1,harga2,konversi1,konversi2,disc,qtygrosir from ms_barang where [kode barang]='" & txtKdBrg & "'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(0)
        cek_kodebarang1 = True
        lblkategori = rs!kategori
        lblMerk = rs!merk
        lblQtyGrosir = rs!qtygrosir
        lblDisc = rs!disc
        txtHargaJual1 = rs!harga1
        txtHargaJual2 = rs!harga2
        lblKonversi1 = rs!konversi1
        lblKonversi2 = rs!konversi2
        
        rs.Close
        rs.Open "select top 1 harga_beli,disc1,disc2,ppn from t_pembeliand where [kode barang]='" & txtKdBrg.text & "' order by id desc"
        If Not rs.EOF Then
            txtHarga.text = IIf(IsNull(rs(0)), 0, rs(0))
            txtDisc1.text = IIf(IsNull(rs(1)), 0, rs(1))
            txtDisc2.text = IIf(IsNull(rs(2)), 0, rs(2))
            txtPPN.text = IIf(IsNull(rs(3)), 0, rs(3))
        Else
            txtHarga.text = getHpp(txtKdBrg.text, strcon)
            txtDisc1.text = 0
            txtDisc2.text = 0
            txtPPN.text = 0
        End If
        rs.Close
        
    Else
        LblNamaBarang = ""
        lblkategori = ""
        lblMerk = ""
        lblDisc = "0"
        lblQtyGrosir = "0"
        txtHargaJual1 = 0
        txtHargaJual2 = 0
        lblKonversi1 = 1
        lblKonversi2 = 1
        cek_kodebarang1 = False
        GoTo ex
    End If
    If rs.State Then rs.Close
    
    
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_barang(kode As String, conn As ADODB.Connection) As Boolean
Dim rs As New ADODB.Recordset
    
    rs.Open "select [nama barang] from ms_barang where [kode barang]='" & kode & "'", conn
    If Not rs.EOF Then
        cek_barang = True
    Else
        'LblNamaBarang = ""
        cek_barang = False
    End If
    rs.Close
    
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Sub cek_supplier()
On Error GoTo err
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select [nama Supplier] from ms_supplier where [kode supplier]='" & txtKodeSupplier & "'", conn
    If Not rs.EOF Then
        lblNamaSupplier = rs(0)
    Else
        lblNamaSupplier = ""
    End If
    rs.Close
    conn.Close
    
err:
End Sub



Private Sub Form_Unload(Cancel As Integer)
resizeFlxGrid "select d.[kode barang],m.[nama barang], qty,'' as satuan,harga_beli,d.disc1,d.disc2,ppn,harga,0 as total from t_pembeliand d inner join ms_barang m on d.[kode barang]=m.[kode barang]", flxGrid, "FlxPembelian", 1

End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub

Private Sub mnuSettingExcel_Click()
    cmdSetting_Click
End Sub

Private Sub Opt2_Click(Index As Integer)
    If Index = 0 Then frJT.Visible = False
    If Index = 1 Then frJT.Visible = True
End Sub

Private Sub txtDisc1_GotFocus()
    txtDisc1.SelStart = 0
    txtDisc1.SelLength = Len(txtDisc1.text)
End Sub

Private Sub txtDisc2_GotFocus()
    txtDisc2.SelStart = 0
    txtDisc2.SelLength = Len(txtDisc2.text)
End Sub

Private Sub txtHarga_GotFocus()
    txtHarga.SelStart = 0
    txtHarga.SelLength = Len(txtHarga.text)
End Sub

Private Sub txtHarga_LostFocus()
    If Not IsNumeric(txtHarga.text) Then txtHarga.text = "0"
End Sub

Private Sub txtHargaJual1_GotFocus()
    txtHargaJual1.SelStart = 0
    txtHargaJual1.SelLength = Len(txtHargaJual1.text)
End Sub

Private Sub txtHargaJual1_LostFocus()
    If Not IsNumeric(txtHargaJual1.text) Then txtHargaJual1.text = "0"
End Sub

Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtKdBrg.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
On Error Resume Next
    
    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        
        cek_kodebarang1
'        cari_id
    End If

End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    If txtKdBrg.text <> "" Then
        If Not cek_kodebarang1 Then
            MsgBox "Kode yang anda masukkan salah"
            txtKdBrg.SetFocus
        End If
    End If
    foc = 0
End Sub

Private Sub txtKodeSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchSupplier_Click
End Sub

Private Sub txtKodeSupplier_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cek_supplier
End Sub

Private Sub txtKodeSupplier_LostFocus()
    cek_supplier
End Sub

Private Sub txtPPN_Change()
'    txtPPN.SelStart = 0
'    txtPPN.SelLength = Len(txtPPN.text)
End Sub

Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub

Private Sub txtQty_LostFocus()
'    If Not IsNumeric(txtQty.text) Then txtQty.text = "1"
End Sub
