VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmBayarHutang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembayaran Hutang"
   ClientHeight    =   3975
   ClientLeft      =   45
   ClientTop       =   675
   ClientWidth     =   5085
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   5085
   Begin VB.CommandButton cmdSimpan 
      BackColor       =   &H008080FF&
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   405
      TabIndex        =   2
      Top             =   3240
      Width           =   1320
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H008080FF&
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   3165
      TabIndex        =   4
      Top             =   3240
      Width           =   1320
   End
   Begin VB.CommandButton cmdReset 
      BackColor       =   &H008080FF&
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   1800
      TabIndex        =   3
      Top             =   3240
      Width           =   1320
   End
   Begin VB.TextBox txtJumlahBayar 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1620
      TabIndex        =   1
      Top             =   2475
      Width           =   1770
   End
   Begin VB.TextBox txtNoBeli 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   990
      Width           =   1770
   End
   Begin VB.CommandButton cmdSearchBeli 
      Appearance      =   0  'Flat
      BackColor       =   &H008080FF&
      Caption         =   "F3"
      Height          =   330
      Left            =   3510
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   10
      Top             =   990
      Width           =   450
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   330
      Left            =   3555
      Picture         =   "frmBayarHutang.frx":0000
      TabIndex        =   6
      Top             =   135
      Width           =   450
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   5
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   110362627
      CurrentDate     =   38927
   End
   Begin VB.Label lblSupplier 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1665
      TabIndex        =   23
      Top             =   1440
      Width           =   4155
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   22
      Top             =   1440
      Width           =   960
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1485
      TabIndex        =   21
      Top             =   1440
      Width           =   60
   End
   Begin VB.Label lblSisaBayar 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1755
      TabIndex        =   20
      Top             =   2115
      Width           =   1590
   End
   Begin VB.Label lblJumlah 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1890
      TabIndex        =   19
      Top             =   1800
      Width           =   1455
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Bayar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   18
      Top             =   2520
      Width           =   960
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   17
      Top             =   2520
      Width           =   105
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Sisa Bayar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   2160
      Width           =   960
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   ": Rp."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1485
      TabIndex        =   15
      Top             =   2160
      Width           =   360
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   14
      Top             =   1800
      Width           =   960
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   ": Rp."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1485
      TabIndex        =   13
      Top             =   1800
      Width           =   360
   End
   Begin VB.Label Label19 
      BackStyle       =   0  'Transparent
      Caption         =   "No Pembelian"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   1035
      Width           =   960
   End
   Begin VB.Label Label23 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1485
      TabIndex        =   11
      Top             =   1035
      Width           =   105
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   9
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   8
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1620
      TabIndex        =   7
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnuData 
      Caption         =   "Data"
   End
End
Attribute VB_Name = "frmBayarHutang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub nomor_baru()
Dim No As String
    rs.Open "select top 1 ID from t_bayarhutang where mid(ID,3,4)='" & Format(DTPicker1, "yyMM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "BH" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "BH" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdReset_Click()
reset_form
End Sub

Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryBayarHutang
    frmSearch.nmform = "frmBayarHutang"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.keyIni = "t_pembelian"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_notrans"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearchbeli_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "select * from q_hutang where sisa>1"
    frmSearch.nmform = "frmBayarHutang"
    frmSearch.nmctrl = "txtNoBeli"
    frmSearch.keyIni = "hutang"
    frmSearch.col = 0
    frmSearch.Index = -1
    
    frmSearch.proc = "cek_hutang"
    frmSearch.loadgrid frmSearch.query
    Set frmSearch.frm = Me
    'frmSearch.cmbSort.ListIndex = 1
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
On Error GoTo err
Dim id As String
Dim i As Byte
    i = 0
    If lblSupplier = "" Then
        MsgBox "Nomor pembelian yang anda masukkan salah"
        Exit Sub
    End If
    If txtJumlahBayar > lblSisaBayar Or txtJumlahBayar < 0 Then
        MsgBox "Jumlah bayar tidak boleh lebih besar daripada sisa bayar"
        Exit Sub
    End If
    id = lblNoTrans
    conn.Open strcon
    conn.BeginTrans
    i = 1
    If lblNoTrans = "" Then
        nomor_baru
    Else
        rs.Open "select * from t_bayarhutang where ID='" & lblNoTrans & "'", conn
        If Not rs.EOF Then
            conn.Execute "update t_pembelian set jumlah_bayar=jumlah_bayar-" & rs![jumlah bayar] & " where id='" & rs!no_beli & "'"
        End If
        rs.Close
        conn.Execute "delete from t_bayarhutang where id='" & lblNoTrans & "'"
    End If
    add_bayar
    conn.Execute "update t_pembelianh set jumlah_bayar=jumlah_bayar+" & txtJumlahBayar & " where id='" & txtNoBeli & "'"
    conn.CommitTrans
    i = 0
    
    conn.Close
    MsgBox "Data sudah tersimpan"
    reset_form
    Exit Sub
err:
    If i = 1 Then conn.RollbackTrans
    MsgBox err.Description
    lblNoTrans = id
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub
Private Sub add_bayar()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(5)
    ReDim nilai(5)
    table_name = "t_bayarhutang"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "no_beli"
    fields(3) = "sudah_bayar"
    fields(4) = "[jumlah bayar]"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = txtNoBeli
    nilai(3) = lblJumlah - lblSisaBayar
    nilai(4) = txtJumlahBayar
    
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF3 Then cmdSearchbeli_Click
    If KeyCode = vbKeyF5 Then cmdSearchID_Click
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF2 Then cmdSimpan_Click
End Sub

Private Sub reset_form()
On Error Resume Next
    DTPicker1 = Now
    txtNoBeli = ""
    txtJumlahBayar = "0"
    lblNoTrans = ""
    txtNoBeli.SetFocus
End Sub
Public Sub cek_hutang()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
conn.Open strcon
rs.Open "select * from t_pembelianh h inner join ms_supplier m on h.[kode supplier]=m.[kode supplier] where id='" & txtNoBeli & "'", conn
If Not rs.EOF Then
    lblSupplier = rs![nama supplier]
    lblJumlah = rs!total
    lblSisaBayar = rs!total - rs!jumlah_bayar
    txtJumlahBayar = rs!total - rs!jumlah_bayar
Else
    lblSupplier = ""
    lblJumlah = "0"
    lblSisaBayar = "0"
    txtJumlahBayar = "0"
End If
rs.Close
conn.Close
    Exit Sub
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub
Public Sub cek_notrans()
On Error GoTo err
Dim conn As New ADODB.Connection
Dim rs As New ADODB.Recordset
conn.Open strcon
rs.Open "select * from t_bayarhutang  where id='" & lblNoTrans & "'", conn
If Not rs.EOF Then
    DTPicker1 = rs!tanggal
    txtNoBeli = rs!no_beli
    cek_hutang
    lblSisaBayar = lblJumlah - rs!sudah_bayar
    txtJumlahBayar = rs![jumlah bayar]
End If
rs.Close
conn.Close
    Exit Sub
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    If conn.State Then conn.Close
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{tab}"
    End If
End Sub

Private Sub Form_Load()
    reset_form
End Sub

Private Sub txtJumlahBayar_GotFocus()
    txtJumlahBayar.SelStart = 0
    txtJumlahBayar.SelLength = Len(txtJumlahBayar)
End Sub

