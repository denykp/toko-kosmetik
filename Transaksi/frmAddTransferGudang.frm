VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmAddTransferGudang 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transfer antar Gudang"
   ClientHeight    =   9090
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   10515
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9090
   ScaleWidth      =   10515
   Begin VB.CommandButton cmdEdit 
      Caption         =   "Edit"
      Height          =   330
      Left            =   4185
      Picture         =   "frmAddTransferGudang.frx":0000
      TabIndex        =   40
      Top             =   180
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.ComboBox cmbGudangAsal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   7650
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   360
      Width           =   1545
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset (Ctrl+R)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4682
      Picture         =   "frmAddTransferGudang.frx":0102
      TabIndex        =   39
      Top             =   8280
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print (Ctrl+P)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3377
      Picture         =   "frmAddTransferGudang.frx":0204
      TabIndex        =   38
      Top             =   8280
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.ComboBox cmbGudangTujuan 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   7650
      TabIndex        =   3
      Text            =   "Combo1"
      Top             =   855
      Width           =   1545
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   9945
      Top             =   135
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   29
      Top             =   135
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkNumbering 
      Caption         =   "Otomatis"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   28
      Top             =   180
      Value           =   1  'Checked
      Width           =   1545
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H8000000C&
      Caption         =   "Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1770
      Left            =   135
      TabIndex        =   18
      Top             =   1755
      Width           =   6990
      Begin VB.CommandButton cmdSearchBrg 
         Caption         =   "F3"
         Height          =   330
         Left            =   2835
         Picture         =   "frmAddTransferGudang.frx":0306
         TabIndex        =   19
         Top             =   270
         Width           =   375
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Hapus"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2790
         TabIndex        =   8
         Top             =   1215
         Width           =   1050
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "&Baru"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1575
         TabIndex        =   7
         Top             =   1215
         Width           =   1050
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Tambahkan"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   6
         Top             =   1215
         Width           =   1050
      End
      Begin VB.TextBox txtQty 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   5
         Top             =   675
         Width           =   1140
      End
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   4
         Top             =   270
         Width           =   1500
      End
      Begin VB.Label lblDefQtyJual 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   285
         Left            =   270
         TabIndex        =   26
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label lblID 
         Caption         =   "Label12"
         ForeColor       =   &H8000000F&
         Height          =   240
         Left            =   270
         TabIndex        =   25
         Top             =   1395
         Width           =   870
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         Caption         =   "Kode Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   24
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000C&
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   23
         Top             =   720
         Width           =   600
      End
      Begin VB.Label LblNamaBarang 
         BackColor       =   &H8000000C&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3240
         TabIndex        =   22
         Top             =   270
         Width           =   3525
      End
      Begin VB.Label lblKode 
         BackColor       =   &H8000000C&
         Caption         =   "Label12"
         ForeColor       =   &H8000000C&
         Height          =   330
         Left            =   3825
         TabIndex        =   21
         Top             =   1305
         Width           =   600
      End
      Begin VB.Label lblSatuan 
         BackColor       =   &H8000000C&
         Height          =   330
         Left            =   2520
         TabIndex        =   20
         Top             =   675
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdSearchID 
      Caption         =   "F5"
      Height          =   330
      Left            =   3780
      Picture         =   "frmAddTransferGudang.frx":0408
      TabIndex        =   17
      Top             =   180
      Width           =   375
   End
   Begin VB.TextBox txtKeterangan 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1620
      TabIndex        =   1
      Top             =   990
      Width           =   3885
   End
   Begin VB.CommandButton cmdApprove 
      Caption         =   "&Approve"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   5957
      Picture         =   "frmAddTransferGudang.frx":050A
      TabIndex        =   10
      Top             =   8280
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "E&xit (Esc)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   7213
      Picture         =   "frmAddTransferGudang.frx":060C
      TabIndex        =   11
      Top             =   8280
      Width           =   1230
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "&Save (F2)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   2072
      Picture         =   "frmAddTransferGudang.frx":070E
      TabIndex        =   9
      Top             =   8280
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1620
      TabIndex        =   0
      Top             =   585
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   582
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   -2147483638
      CustomFormat    =   "dd/MM/yyyy hh:mm:ss"
      Format          =   165609475
      CurrentDate     =   38927
   End
   Begin MSFlexGridLib.MSFlexGrid flxGrid 
      Height          =   3660
      Left            =   135
      TabIndex        =   27
      Top             =   3645
      Width           =   9960
      _ExtentX        =   17568
      _ExtentY        =   6456
      _Version        =   393216
      Cols            =   4
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   9945
      Top             =   630
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label2 
      Caption         =   "Asal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   37
      Top             =   405
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7470
      TabIndex        =   36
      Top             =   405
      Width           =   105
   End
   Begin VB.Label Label23 
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7470
      TabIndex        =   35
      Top             =   900
      Width           =   105
   End
   Begin VB.Label Label19 
      Caption         =   "Tujuan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   6120
      TabIndex        =   34
      Top             =   900
      Width           =   960
   End
   Begin VB.Label Label16 
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1350
      TabIndex        =   33
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label lblItem 
      Alignment       =   1  'Right Justify
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2295
      TabIndex        =   32
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label lblQty 
      Alignment       =   1  'Right Justify
      Caption         =   "0,00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4770
      TabIndex        =   31
      Top             =   7515
      Width           =   1140
   End
   Begin VB.Label Label17 
      Caption         =   "Qty"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3825
      TabIndex        =   30
      Top             =   7515
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   16
      Top             =   630
      Width           =   1320
   End
   Begin VB.Label Label11 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   15
      Top             =   630
      Width           =   105
   End
   Begin VB.Label Label6 
      Caption         =   ":"
      Height          =   240
      Left            =   1485
      TabIndex        =   14
      Top             =   990
      Width           =   105
   End
   Begin VB.Label Label4 
      Caption         =   "Keterangan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   13
      Top             =   990
      Width           =   1275
   End
   Begin VB.Label lblNoTrans 
      Caption         =   "0000001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1845
      TabIndex        =   12
      Top             =   135
      Width           =   1860
   End
   Begin VB.Menu mnu 
      Caption         =   "Data"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuReset 
         Caption         =   "Reset"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmAddTransferGudang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Const queryTransfer As String = "select ID,Tanggal,Keterangan,Gudang,Tujuan from t_transfergudangh"
Dim total As Currency
Dim foc As Byte
Dim mode As Byte
Dim NoTrans2 As String
Dim totalQty As Integer
Dim harga_beli, harga_jual As Currency
Dim currentRow As Integer
Private Sub nomor_baru()
Dim No As String
    
    rs.Open "select top 1 ID from t_transfergudangh where mid(ID,3,2)='" & Format(DTPicker1, "yy") & "' and mid(ID,5,2)='" & Format(DTPicker1, "MM") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "TG" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format((CLng(Right(rs(0), 5)) + 1), "00000")
    Else
        No = "TG" & Format(DTPicker1, "yy") & Format(DTPicker1, "MM") & Format("1", "00000")
    End If
    rs.Close
    lblNoTrans = No
    
End Sub


Private Sub chkNumbering_Click()
If chkNumbering.value = "1" Then
        txtID.Visible = False
    Else
        txtID.Visible = True
    End If
End Sub

Private Sub cmdApprove_Click()
    conn.Open
    For row = 1 To flxGrid.rows - 2
        If flxGrid.TextMatrix(row, 1) <> "" Then
            updatestock cmbGudangAsal, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3) * -1, conn
            updatestock cmbGudangTujuan, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3), conn
        End If
    Next
    conn.Execute "update t_transfergudangh set status='2' where id='" & lblNoTrans & "'"
    conn.Close
End Sub

Private Sub cmdEdit_Click()
    frmPassword.Left = 5280
    frmPassword.top = 1230
'    frmPassword.proc = "edit"
'    Set frmPassword.frm = Me
    frmPassword.Show vbModal

    
End Sub
Public Sub edit()
    Frame2.Visible = True
    flxGrid.Enabled = True
    cmdSimpan.Enabled = True
    cmdEdit.Visible = False
End Sub
Private Sub cmdHapus_Click()
End Sub

Private Sub cmdClear_Click()
    txtKdBrg.text = ""
    LblNamaBarang = ""
    txtQty.text = "1"
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    chkGuling = 0
    chkKirim = 0
End Sub

Private Sub cmdDelete_Click()
Dim row, col As Integer
    flxGrid.TextMatrix(flxGrid.row, 0) = ""
        If flxGrid.row > 1 Then flxGrid.row = flxGrid.row - 1
        
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        flxGrid.TextMatrix(flxGrid.row, 0) = "-"
    Else
        flxGrid.row = 0
    End If
    
    
    row = flxGrid.row
    totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
    lblQty = totalQty
    For row = row To flxGrid.rows - 1
        If row = flxGrid.rows - 1 Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
            Exit For
        ElseIf flxGrid.TextMatrix(row + 1, 1) = "" Then
            For col = 1 To flxGrid.cols - 1
                flxGrid.TextMatrix(row, col) = ""
            Next
        ElseIf flxGrid.TextMatrix(row + 1, 1) <> "" Then
            For col = 1 To flxGrid.cols - 1
            flxGrid.TextMatrix(row, col) = flxGrid.TextMatrix(row + 1, col)
            Next
        End If
    Next
    flxGrid.rows = flxGrid.rows - 1
    flxGrid.col = 0
    flxGrid.ColSel = 3
    txtKdBrg.text = ""
    lblSatuan = ""
    txtQty.text = "1"
    
    LblNamaBarang = ""
    'txtKdBrg.SetFocus
    mode = 1
    cmdClear.Enabled = False
    cmdDelete.Enabled = False
    
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
Dim i As Integer
Dim row As Integer
    row = 0
    If txtKdBrg.text <> "" And LblNamaBarang <> "" Then
        
        For i = 1 To flxGrid.rows - 1
            If flxGrid.TextMatrix(i, 1) = txtKdBrg.text Then row = i
        Next
        If row = 0 Then
            row = flxGrid.rows - 1
            flxGrid.rows = flxGrid.rows + 1
        End If
        
        flxGrid.row = row
        
        
        flxGrid.TextMatrix(row, 1) = txtKdBrg.text
        flxGrid.TextMatrix(row, 2) = LblNamaBarang
        If flxGrid.TextMatrix(row, 3) = "" Then
            flxGrid.TextMatrix(row, 3) = txtQty
        Else
            totalQty = totalQty - (flxGrid.TextMatrix(row, 3))
            
            If mode = 1 Then
                flxGrid.TextMatrix(row, 3) = CDbl(flxGrid.TextMatrix(row, 3)) + CDbl(txtQty)
            ElseIf mode = 2 Then
                flxGrid.TextMatrix(row, 3) = CDbl(txtQty)
            End If
        End If
        totalQty = totalQty + flxGrid.TextMatrix(row, 3)
        lblQty = Format(totalQty, "#,##0")
        flxGrid.col = 0
        flxGrid.ColSel = 3
        'flxGrid.TextMatrix(row, 7) = lblKode
        If row > 8 Then
            flxGrid.TopRow = row - 7
        Else
            flxGrid.TopRow = 1
        End If
        harga_beli = 0
        harga_jual = 0
        txtKdBrg.text = ""
        lblSatuan = ""
        LblNamaBarang = ""
        txtQty.text = "1"
        
        txtKdBrg.SetFocus
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
    mode = 1
    lblItem = flxGrid.rows - 2
End Sub


Private Sub posting(kdbrg As String, i As Integer)

End Sub
Private Sub printBukti()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
'    With CRPrint
'        .reset
'        .ReportFileName = App.Path & "\Report\Transfertransfergudang.rpt"
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & servname & ";DSQ=" & conn.Properties(4).Value & ";UID=" & user & ";PWD=" & pwd & ";"
'        Next
'        .SelectionFormula = " {t_transfergudangh.ID}='" & lblNoTrans & "'"
'        .Destination = crptToWindow
'        .ParameterFields(0) = "login;" + user + ";True"
'        .WindowTitle = "Cetak" & PrintMode
'        .WindowState = crptMaximized
'        .WindowShowPrintBtn = True
'        .WindowShowExportBtn = True
'        .action = 1
'    End With
    conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub cmdPrint_Click()
    cetaknota lblNoTrans
    conn.Open
    conn.Execute "update t_transfergudangh set status='1' where id='" & lblNoTrans & "'"
    conn.Close
End Sub

Private Sub cmdReset_Click()
reset_form
End Sub

Private Sub cmdSearchBrg_Click()
    frmSearch.query = querybrg
    frmSearch.nmform = "frmAddTransferGudang"
    frmSearch.nmctrl = "txtKdBrg"
        frmSearch.connstr = strcon
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "search_cari"
    frmSearch.loadgrid frmSearch.query
    If Not frmSearch.Adodc1.Recordset.EOF Then frmSearch.cmbKey.ListIndex = 2
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub
Public Sub search_cari()
On Error Resume Next
    If cek_kodebarang1 Then MySendKeys "{tab}"
End Sub



Private Sub cmdSearchID_Click()
        frmSearch.connstr = strcon
    frmSearch.query = queryTransfer & " where (gudang='" & gudang & "' and (status='0' or status='1')) or (tujuan='" & gudang & "' and status='1')"
    frmSearch.nmform = "frmAddtransfergudang"
    frmSearch.nmctrl = "lblNoTrans"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_notrans"
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmdSimpan_Click()
Dim i As Integer
Dim id As String
Dim row As Integer
On Error GoTo err
    i = 0
    id = lblNoTrans
    If flxGrid.rows <= 2 Then
        MsgBox "Silahkan masukkan Barang yang akan ditransfer terlebih dahulu"
        txtKdBrg.SetFocus
        Exit Sub
    End If
    If cmbGudangTujuan.text = "" Then
        MsgBox "Gudang tujuan tidak boleh kosong"
        cmbGudang.SetFocus
        Exit Sub
    End If
    If cmbGudangTujuan.text = gudang Then
        MsgBox "Gudang tujuan tidak boleh sama dengan gudang asal"
        cmbGudang.SetFocus
        Exit Sub
    End If
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    
    If chkNumbering <> "1" Then
    rs.Open "select * from t_transfergudangh where id='" & txtID.text & "'", conn
    If Not rs.EOF Then
        MsgBox "Nomor ID " & txtID.text & " sudah digunakan, silahkan menggunakan ID yang lain"
        rs.Close
        conn.Close
        Exit Sub
    End If
    rs.Close
    End If
    
    i = 1
    If lblNoTrans = "-" And chkNumbering = "1" Then
        nomor_baru
    Else
        
'        rs.Open "select * from t_transfergudangd d inner join t_transfergudangh h on d.id=h.id where ID='" & lblNoTrans & "'", conn
'        While Not rs.EOF
'            updatestock rs("gudang"), rs("kode barang"), rs("qty"), conn
'            updatestock rs("tujuan"), rs("kode barang"), rs("qty") * -1, conn
'            rs.MoveNext
'        Wend
'        rs.Close
        conn.Execute "delete from t_transfergudangh where ID='" & lblNoTrans & "'"
        conn.Execute "delete from t_transfergudangd where ID='" & lblNoTrans & "'"
        
    End If
    If chkNumbering = "0" Then lblNoTrans = txtID.text
    add_dataheader
    For row = 1 To flxGrid.rows - 2
        If flxGrid.TextMatrix(row, 1) <> "" Then
            add_datadetail (row)
            
'            updatestock cmbGudangAsal, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3) * -1, conn
'            updatestock cmbGudangTujuan, flxGrid.TextMatrix(row, 1), flxGrid.TextMatrix(row, 3), conn
'
        End If
    Next
'    For row = 1 To flxGrid.rows - 2
'        posting flxGrid.TextMatrix(row, 1), row
'    Next
    conn.Execute "update t_transfergudangh set status='1'  where id='" & lblNoTrans & "'"
    conn.CommitTrans
    
    i = 0
    conn.Close
    
    MsgBox "Data sudah tersimpan"
    cetaknota lblNoTrans
    reset_form
    
    MySendKeys "{tab}"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
    If id <> lblNoTrans Then lblNoTrans = id
    MsgBox err.Description
End Sub
Public Sub cetaknota(no_nota As String)
On Error GoTo err
    With CRPrint
        .reset
        .ReportFileName = App.Path & "\Report\transfer.rpt"
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            'If ServerType = "mysql" Or ServerType = "mssql" Then
                .LogonInfo(i) = "DSN=" & sname & ";UID=Admin" '& ";PWD=" & pwd & ";"
            'ElseIf ServerType = "access" Then
                '.DataFiles(i) = servname
            'End If
        Next
        .SelectionFormula = "{t_transfergudangh.id}='" & no_nota & "'"
'        .PrinterSelect
        .ProgressDialog = False
        .Destination = crptToPrinter
        .action = 1
    End With
    Exit Sub
err:
MsgBox err.Description
End Sub
Public Sub reset_form()
    lblNoTrans = "-"
    Frame2.Visible = True
    flxGrid.Enabled = True
    txtKeterangan.text = ""
    cmdPrint.Visible = False
    cmdEdit.Visible = False
    loadcombo
    totalQty = 0
    total = 0
    SetComboText gudang, cmbGudangAsal
    For i = 0 To cmbGudangTujuan.ListCount
        If cmbGudangTujuan.list(i) <> gudang And cmbGudangTujuan.list(i) <> "" Then
            cmbGudangTujuan.ListIndex = i
            Exit For
        End If
    Next
    lblQty = 0
    lblItem = 0
    DTPicker1 = Now
    DTPicker2 = Now
    cmdClear_Click
    flxGrid.rows = 1
    flxGrid.rows = 2
End Sub
Private Sub add_dataheader()
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    ReDim fields(7)
    ReDim nilai(7)
    table_name = "t_transfergudangh"
    fields(0) = "id"
    fields(1) = "tanggal"
    fields(2) = "gudang"
    fields(3) = "keterangan"
    fields(4) = "tujuan"
    fields(5) = "pk"
    fields(6) = "[userid]"
        
    nilai(0) = lblNoTrans
    nilai(1) = Format(DTPicker1, "yyyy/MM/dd hh:mm:ss")
    nilai(2) = cmbGudangAsal
    nilai(3) = txtKeterangan.text
    nilai(4) = cmbGudangTujuan
    nilai(5) = ""
    nilai(6) = user
    conn.Execute tambah_data2(table_name, fields, nilai)
    

End Sub
Private Sub add_datadetail(row As Integer)
    Dim fields() As String
    Dim nilai() As String
    Dim table_name As String
    
    ReDim fields(5)
    ReDim nilai(5)
    
    table_name = "t_transfergudangd"
    fields(0) = "ID"
    fields(1) = "[KODE barang]"
    fields(2) = "qty"
    fields(3) = "URUT"
    fields(4) = "HPP"
    
    nilai(0) = lblNoTrans
    nilai(1) = flxGrid.TextMatrix(row, 1)
    nilai(2) = flxGrid.TextMatrix(row, 3)
    nilai(3) = row
    nilai(4) = getHpp(flxGrid.TextMatrix(row, 1), strcon)
    conn.Execute tambah_data2(table_name, fields, nilai)
    
End Sub

Public Sub cek_notrans()
'    cmdPrint.Visible = False
    conn.ConnectionString = strcon
    conn.Open

    rs.Open "select * from t_transfergudangh where id='" & lblNoTrans & "'", conn
    If Not rs.EOF Then
        Frame2.Visible = False
        flxGrid.Enabled = False
        cmdEdit.Visible = True
        SetComboText rs("gudang"), cmbGudangAsal
        DTPicker1 = rs("tanggal")
        SetComboText rs("tujuan"), cmbGudangTujuan
        txtKeterangan.text = rs("keterangan")
        cmdPrint.Visible = True
        totalQty = 0
        total = 0
        If rs("status") = 1 Then
            cmdSimpan.Enabled = False
            mnuSave.Enabled = False
        Else
            cmdSimpan.Enabled = True
            mnuSave.Enabled = True
            cmdDelete.Enabled = True
        End If
        If rs.State Then rs.Close
        flxGrid.rows = 1
        flxGrid.rows = 2
        row = 1
        
        rs.Open "select d.[kode barang],m.[nama barang], qty from t_transfergudangd d inner join ms_barang m on d.[kode barang]=m.[kode barang] where d.id='" & lblNoTrans & "' order by urut", conn
        While Not rs.EOF
                flxGrid.TextMatrix(row, 1) = rs(0)
                flxGrid.TextMatrix(row, 2) = rs(1)
                flxGrid.TextMatrix(row, 3) = rs(2)
 
                row = row + 1
                totalQty = totalQty + rs(2)
                
                flxGrid.rows = flxGrid.rows + 1
                rs.MoveNext
        Wend
        rs.Close
        lblQty = totalQty
        
        lblItem = flxGrid.rows - 2
    End If
    If rs.State Then rs.Close
    conn.Close
End Sub

Private Sub Command1_Click()

End Sub

Private Sub flxGrid_Click()
    If flxGrid.TextMatrix(flxGrid.row, 1) <> "" Then
        
        mode = 2
        cmdClear.Enabled = True
        cmdDelete.Enabled = True
        txtKdBrg.text = flxGrid.TextMatrix(flxGrid.row, 1)
        cek_kodebarang1
        txtQty.text = flxGrid.TextMatrix(flxGrid.row, 3)
        
    Else
        mode = 1
        cmdClear.Enabled = False
        cmdDelete.Enabled = False
    End If
End Sub

Private Sub flxGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        cmdDelete_Click
    End If
End Sub

Private Sub flxGrid_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtQty.SetFocus
End Sub


Private Sub Form_Activate()
On Error Resume Next
    MySendKeys "{tab}"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchBrg_Click
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
On Error Resume Next
    If KeyAscii = vbKeyReturn Then
        If foc = 1 And txtKdBrg.text = "" Then
        Else
        KeyAscii = 0
        MySendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Form_Load()
    loadcombo
    reset_form
    total = 0
    conn.ConnectionString = strcon
    conn.Open
    'rs.Open "select * from ms_gudang where kode_gudang='" & gudang & "'", conn
    'If Not rs.EOF Then
    
    
    'End If
    'rs.Close
    conn.Close
    flxGrid.ColWidth(0) = 300
    flxGrid.ColWidth(1) = 1100
    flxGrid.ColWidth(2) = 2800
    flxGrid.ColWidth(3) = 900
    
    flxGrid.TextMatrix(0, 1) = "Kode Barang"
    flxGrid.ColAlignment(2) = 1 'vbAlignLeft
    flxGrid.TextMatrix(0, 2) = "Nama Barang"
    flxGrid.TextMatrix(0, 3) = "Qty"


End Sub
Private Sub loadcombo()
    conn.ConnectionString = strcon
    conn.Open
    cmbGudangAsal.Clear
    cmbGudangTujuan.Clear
    rs.Open "select * from var_gudang", conn
    While Not rs.EOF
        cmbGudangAsal.AddItem rs(0)
        cmbGudangTujuan.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub
Public Function cek_kodebarang1() As Boolean
Dim kode As String
    conn.ConnectionString = strcon
    conn.Open
    If InStr(1, txtKdBrg, "*") > 0 Then kdbrg = Mid(txtKdBrg, InStr(1, txtKdBrg, "*") + 1, Len(txtKdBrg)) Else kdbrg = txtKdBrg
    rs.Open "select [nama barang] from ms_barang where [kode barang]='" & txtKdBrg & "' and flag='1'", conn
    If Not rs.EOF Then
        LblNamaBarang = rs(0)
        cek_kodebarang1 = True

    Else
        LblNamaBarang = ""
        cek_kodebarang1 = False
    End If
    rs.Close
    conn.Close
End Function

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuOpen_Click()
    cmdSearchID_Click
End Sub

Private Sub mnuReset_Click()
    reset_form
End Sub

Private Sub mnuSave_Click()
    cmdSimpan_Click
End Sub


Private Sub txtKdBrg_GotFocus()
    txtKdBrg.SelStart = 0
    txtKdBrg.SelLength = Len(txtQty.text)
    foc = 1
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = 13 And txtKdBrg.text = "" Then
        flxGrid_Click
    End If
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If InStr(1, txtKdBrg.text, "*") > 0 Then
            txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
            txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
        End If
        cek_kodebarang1
'        cari_id
    End If
End Sub

Private Sub txtKdBrg_LostFocus()
On Error Resume Next
    If InStr(1, txtKdBrg.text, "*") > 0 Then
        txtQty.text = Left(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") - 1)
        txtKdBrg.text = Mid(txtKdBrg.text, InStr(1, txtKdBrg.text, "*") + 1, Len(txtKdBrg.text))
    End If
    cek_kodebarang1
    foc = 0
End Sub



Private Sub txtQty_GotFocus()
    txtQty.SelStart = 0
    txtQty.SelLength = Len(txtQty.text)
End Sub

Private Sub txtQty_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        txtQty.text = calc(txtQty.text)
    End If
End Sub

