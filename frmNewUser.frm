VERSION 5.00
Begin VB.Form frmNewUser 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New User"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   7095
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   7095
   Begin VB.ComboBox cmbGroup 
      Height          =   315
      Left            =   1170
      TabIndex        =   7
      Text            =   "Combo1"
      Top             =   540
      Width           =   2130
   End
   Begin VB.CommandButton cmdDel 
      Caption         =   "Hapus"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2880
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1035
      Width           =   1125
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   4140
      Picture         =   "frmNewUser.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   180
      Width           =   375
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4455
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1035
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1260
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1035
      Width           =   1125
   End
   Begin VB.TextBox txtUserID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1155
      TabIndex        =   1
      Top             =   180
      Width           =   2895
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Group :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   6
      Top             =   585
      Width           =   930
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   0
      Top             =   210
      Width           =   975
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmNewUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim asc1 As New AscSecurity.Cls_security
Dim seltab As Byte
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdDel_Click()
On Error GoTo err
    If txtUserID.text <> "" Then
        conn.ConnectionString = strcon
        conn.Open
        conn.Execute "delete from login where username='" & txtUserID & "'"
        conn.Execute "delete from user_group where username='" & txtUserID & "'"
        
        DropConnection
        MsgBox "New User Added"
    Else
        MsgBox "Nama harus diisi"
    End If
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    DropConnection
End Sub

Private Sub cmdOK_Click()
On Error GoTo err
Dim counter As Byte
    If txtUserID.text <> "" Then

        conn.ConnectionString = strcon
        conn.Open
        rs.Open "select * from login where username='" & txtUserID.text & "'", conn
        If Not rs.EOF Then
            conn.Execute "update user_group set [group]='" & cmbGroup.text & "' where username='" & txtUserID & "'"
        Else
            conn.Execute "insert into login values ('" & txtUserID & "','','" & modul & "')"
            conn.Execute "insert into user_group values ('" & txtUserID & "','" & cmbGroup.text & "','" & modul & "')"
        End If
        If rs.State Then rs.Close
        DropConnection
        MsgBox "New User Added"
    Else
        MsgBox "Nama harus diisi"
    End If
    Exit Sub
err:
    MsgBox err.Description
    If rs.State Then rs.Close
    DropConnection
End Sub

Private Sub reset()
    txtUserID.text = ""
End Sub


Private Sub cmdSearchID_Click()
    frmSearch.connstr = strcon
    frmSearch.query = "Select username from login"
    frmSearch.nmform = "frmNewUser"
    frmSearch.nmctrl = "txtUserID"
    frmSearch.keyIni = "login"
    frmSearch.proc = "cari_data"
    frmSearch.Index = -1
    frmSearch.col = 0
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF3 Then cmdSearchID_Click
End Sub

Private Sub Form_Load()
    conn.ConnectionString = strcon
    conn.Open
    cmbGroup.Clear
    rs.Open "select distinct group from var_usergroup", conn
    While Not rs.EOF
        cmbGroup.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
    seltab = 0
End Sub

Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOK_Click
End Sub

Public Sub cari_data()
    conn.ConnectionString = strcon
    conn.Open
    If txtUserID.text <> "" Then
    rs.Open "select * from login where username='" & txtUserID.text & "'", conn
    If Not rs.EOF Then
        rs.Close
        rs.Open "select group from user_group where username='" & txtUserID.text & "'", conn
        If Not rs.EOF Then
            SetComboText rs(0), cmbGroup
        Else
            cmbGroup.ListIndex = 1
        End If
        rs.Close
        
        counter = 0
        
    Else
        If cmbGroup.ListCount > 0 Then cmbGroup.ListIndex = 0
        counter = 0
        counter = 0
        
    End If
    If rs.State Then rs.Close
    End If
    conn.Close
End Sub

Private Sub txtUserID_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cari_data
End Sub

Private Sub txtUserID_LostFocus()
    cari_data
End Sub
