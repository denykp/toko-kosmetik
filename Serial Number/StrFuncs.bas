Attribute VB_Name = "StrFuncs"

Const B32Map = "0123456789ABCDEFGHJKLMNPRSTVWXYZ"

Public Function RemoveDashes(ByVal StrIn As String) As String
  RemoveDashes = Replace(StrIn, "-", "")
End Function

Public Function ShiftStrLeft(ByVal StrIn As String, ByVal Bits As Long) As String
  Dim CurPos As Long
  Dim WorkStr As String
  Dim RetStr As String
  Dim CurByteVal As Byte
  Dim BitMask As Byte
  Dim InvMask As Byte
  Dim ShiftBits As Byte
  Dim WholeBytes As Long
  Dim LeftPart As Byte
  Dim RightPart As Byte
  Dim Carry As Byte
  Dim PrevChar As Byte
  Dim TrimMask As Byte
  
  
  WholeBytes = Int(Bits / 8)
  ShiftBits = Bits Mod 8
  
  BitMask = 255 - (2 ^ (8 - ShiftBits) - 1)
  InvMask = Not (BitMask)
  TrimMask = (2 ^ ShiftBits) - 1
  
  CurPos = 1
  StrLen = Len(StrIn)
  StrBits = StrLen * 8
  WorkStr = StrIn
  
  
  If (StrBits > Bits) Then
    If (WholeBytes > 0) Then
      WorkStr = Right(WorkStr, StrLen - WholeBytes)
      
      
      For CurPos = 1 To WholeBytes
        WorkStr = WorkStr & Chr(0)
      Next CurPos
      
      
      RetStr = WorkStr
    End If
    
    If (ShiftBits > 0) Then
    
      For CurPos = 1 To Len(WorkStr)
        CurByteVal = Asc(Mid(WorkStr, CurPos, 1))
        LeftPart = (CurByteVal And BitMask) And &HFF
        RightPart = (CurByteVal And InvMask) And &HFF
        
        LeftPart = Int(LeftPart / (2 ^ (8 - ShiftBits)))
        RightPart = (RightPart * (2 ^ ShiftBits))
          
        If CurPos = 1 Then
          PrevChar = (RightPart)
          RetStr = ""
        Else
          PrevChar = PrevChar Or LeftPart
          RetStr = RetStr & Chr(PrevChar)
          PrevChar = RightPart
        End If
        
        Next CurPos
        PrevChar = (PrevChar Or (LeftPart And Not (TrimMask)))
        RetStr = RetStr & Chr(PrevChar)
      
    End If
    
  Else
    
    For CurPos = 1 To StrLen
      RetStr = RetStr & Chr(0)
    Next CurPos
  End If
  
  ShiftStrLeft = RetStr
  
End Function


Public Function ShiftStrRight(ByVal StrIn As String, ByVal Bits As Long) As String
  Dim CurPos As Long
  Dim WorkStr As String
  Dim RetStr As String
  Dim CurByteVal As Byte
  Dim BitMask As Byte
  Dim InvMask As Byte
  Dim ShiftBits As Byte
  Dim WholeBytes As Long
  Dim LeftPart As Byte
  Dim RightPart As Byte
  Dim Carry As Byte
  Dim PrevChar As Byte
  Dim TrimMask As Byte
  
  
  WholeBytes = Int(Bits / 8)
  ShiftBits = Bits Mod 8
  
  BitMask = 255 - ((2 ^ ShiftBits) - 1)
  InvMask = Not (BitMask)
  TrimMask = (2 ^ ShiftBits) - 1
  
  CurPos = 1
  StrLen = Len(StrIn)
  StrBits = StrLen * 8
  
  WorkStr = StrIn
  
  If (StrBits > Bits) Then
  
    
    If (WholeBytes > 0) Then
      WorkStr = Left(WorkStr, StrLen - WholeBytes)
      
      For CurPos = 1 To WholeBytes
        WorkStr = Chr(0) & WorkStr
      Next CurPos
      
      
      RetStr = WorkStr
    End If
    
    If (ShiftBits > 0) Then
    
      RetStr = ""
    
      For CurPos = Len(WorkStr) To 1 Step -1
      
        CurByteVal = Asc(Mid(WorkStr, CurPos, 1))
        
        LeftPart = CurByteVal And BitMask
        LeftPart = LeftPart / (2 ^ ShiftBits)
        
        RightPart = CurByteVal And InvMask
        RightPart = RightPart * (2 ^ (8 - ShiftBits))
        
        If CurPos = Len(WorkStr) Then
          Carry = LeftPart
        Else
          CurByteVal = RightPart Or Carry
          Carry = LeftPart
          RetStr = Chr(CurByteVal) & RetStr
        End If
        
      Next CurPos
   
      RetStr = Chr(Carry) & RetStr
      
    End If
    
  Else
    
    For CurPos = 1 To StrLen
      RetStr = RetStr & Chr(0)
    Next CurPos
  End If
  
  ShiftStrRight = RetStr
  
End Function


Public Function Base32Enc(ByVal StrIn As String) As String
  Dim CurBit As Long
  Dim Mask32 As Byte
  Dim CurPos As Long
  Dim CurVal As Byte
  Dim StrBits As Long
  Dim BitsProc As Long
  Dim WorkStr As String
  Dim RetStr As String
  Dim CurConv As String
    
  RetStr = ""
  WorkStr = StrIn
  StrBits = Len(StrIn) * 8
  StrGroups = Int(StrBits / 5)
  
  If (StrBits Mod 5) <> 0 Then StrGroups = StrGroups + 1
  
  StrChar = Len(StrIn)
  BitsProc = 0
  Mask32 = &H1F
  
  
  For CurPos = 1 To StrGroups
    CurVal = Asc(Mid(WorkStr, Len(WorkStr), 1))
    CurVal = (CurVal And Mask32) + 1
    CurConv = Mid(B32Map, CurVal, 1)
    WorkStr = ShiftStrRight(WorkStr, 5)
    RetStr = CurConv & RetStr
  Next CurPos
  
  Base32Enc = RetStr
  
End Function

Public Function Base32Dec(ByVal StrIn As String) As String
  Dim CurPos As Long
  Dim CurVal As Byte
  Dim CurChr As String
  Dim RetStr As String
  Dim WorkStr As String
  Dim Carry As Byte
  Dim CarryMask As Byte
  Dim CurMask As Byte
  Dim ThisVal As Byte
  Dim ThisChar As String
  Dim ShiftBits As Long
  Dim OutBytes As Long
  Dim InBits As Long
  
  
  BitsProc = 0
  BaseMask = &H1F
  Carry = 0
  WorkStr = StrIn
  
  InBits = Len(StrIn) * 5
  OutBytes = Int(InBits / 8)
  
  For CurPos = 1 To OutBytes
    RetStr = RetStr & Chr(0)
  Next CurPos
    
  
  For CurPos = 1 To Len(StrIn)
  
    CurChr = Mid(WorkStr, CurPos, 1)
    CurVal = InStr(1, B32Map, CurChr)
    CurVal = CurVal - 1
    
    RetStr = ShiftStrLeft(RetStr, 5)
    ThisChar = Mid(RetStr, Len(RetStr), 1)
    RetStr = Left(RetStr, Len(RetStr) - 1)
    
 
    ThisVal = Asc(ThisChar)
    ThisVal = ThisVal Or CurVal
    ThisChar = Chr(ThisVal)
    RetStr = RetStr & ThisChar
  Next CurPos
  
  Base32Dec = RetStr
  
End Function


Public Function HexStrToBinStr(ByVal StrIn As String) As String
  Dim StrOut As String
  Dim Ch As Long
  Dim HexByte As String
  Dim ByteVal As Long
  Dim ByteCh As String
  
  StrOut = ""
  
  For Ch = 1 To Len(StrIn) Step 2
    HexByte = Mid(StrIn, Ch, 2)
    ByteVal = Val("&H" & HexByte)
    ByteCh = Chr(ByteVal)
    StrOut = StrOut & ByteCh
  Next Ch
  
  HexStrToBinStr = StrOut
  
End Function


Public Function BinStrToHexStr(ByVal StrIn As String) As String
  Dim StrOut As String
  Dim Ch As Long
  Dim HexByte As String
  Dim HexChr As String
  
  StrOut = ""
  
   For Ch = 1 To Len(StrIn)
    HexByte = Mid(StrIn, Ch, 1)
    HexChr = Hex$(Asc(HexByte))
    If Len(HexChr) = 1 Then HexChr = "0" & HexChr
    StrOut = StrOut & HexChr
  Next Ch
  
  BinStrToHexStr = StrOut
  
End Function

