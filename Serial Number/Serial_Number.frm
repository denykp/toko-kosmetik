VERSION 5.00
Begin VB.Form frmSerialNumber 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registration"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7335
   Icon            =   "Serial_Number.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2415
   ScaleWidth      =   7335
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00808080&
      Height          =   1575
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   7095
      Begin VB.TextBox txtserial_number 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2040
         TabIndex        =   7
         Text            =   "Please Enter Licensee and/or Serial Number"
         Top             =   720
         Width           =   4815
      End
      Begin VB.TextBox ProdName 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   2040
         TabIndex        =   6
         Text            =   "HD-Detect"
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton Cmdcreate 
         Caption         =   "&Activate"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5520
         TabIndex        =   4
         Top             =   1110
         Width           =   1335
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Activation Key :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Key : "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.TextBox UserName 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   1695
      Width           =   975
   End
   Begin VB.TextBox KeyCode 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   240
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   855
      Width           =   4815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Silahkan menghubungi kami untuk memperoleh Activation Key pada komputer anda."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   510
      Left            =   135
      TabIndex        =   9
      Top             =   135
      Width           =   7020
   End
   Begin VB.Label lblread_serial 
      BackColor       =   &H80000005&
      Height          =   255
      Left            =   1680
      TabIndex        =   5
      Top             =   1695
      Width           =   3495
   End
End
Attribute VB_Name = "frmSerialNumber"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Cmdcreate_Click()

If txtserial_number = serial Then
l = 1 ' perubahan
Dim lCreate As Long

Dim tSA As SECURITY_ATTRIBUTES
Dim hKey As Long
RegCreateKeyEx HKEY_LOCAL_MACHINE, "SOFTWARE\toko", 0, "", REG_OPTION_NON_VOLATILE, _
                 KEY_ALL_ACCESS, tSA, nBufferKey, lCreate

RegSetValueExStr nBufferKey, "Key", 0, REG_SZ, serial, Len(serial)

RegCloseKey nBufferKey
MsgBox "Selamat Produk anda telah diaktifkan, Click OK untuk melanjutkan"
Unload Me
frmLogin.Show

Else

MsgBox "Invalid Key"
End If
End Sub

Private Sub Form_Load()
Dim drvNumber As Long
Dim di As DRIVE_INFO

drvNumber = PRIMARY_MASTER
di = GetDriveInfo(drvNumber)

UserName = cUsername
ProdName = Trim$(di.SerialNumber)


End Sub



Private Sub lblread_serial_Change()
    txtserial_number.text = lblread_serial
    txtserial_number.Enabled = False
    Cmdcreate.Enabled = False
End Sub

Private Sub txtserial_number_Change()
If txtserial_number = "" Then
txtserial_number.ToolTipText = "Please Enter Licensee and/or Serial Number"
End If
End Sub

Private Sub txtserial_number_GotFocus()
txtserial_number.SelStart = 0
txtserial_number.SelLength = 42

End Sub

