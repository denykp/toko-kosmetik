VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPrintListBarang 
   Caption         =   "Database Utility"
   ClientHeight    =   8280
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   15825
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   8280
   ScaleWidth      =   15825
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   9315
      TabIndex        =   10
      Top             =   6750
      Width           =   1050
   End
   Begin VB.CommandButton cmdAddAll 
      Caption         =   "Add All"
      Height          =   375
      Left            =   7200
      TabIndex        =   9
      Top             =   270
      Width           =   1050
   End
   Begin MSDataGridLib.DataGrid DBGrid1 
      Height          =   6540
      Left            =   180
      TabIndex        =   8
      Top             =   855
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   11536
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   22
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   645
      Left            =   1935
      TabIndex        =   4
      Top             =   45
      Width           =   3660
      Begin VB.TextBox txtKdBrg 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   0
         Top             =   180
         Width           =   2220
      End
      Begin VB.Label Label14 
         BackColor       =   &H8000000C&
         BackStyle       =   0  'Transparent
         Caption         =   "Nama Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   45
         TabIndex        =   5
         Top             =   225
         Width           =   1050
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Refresh"
      Height          =   375
      Left            =   5715
      TabIndex        =   3
      Top             =   270
      Width           =   1050
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   555
      Left            =   13680
      TabIndex        =   2
      Top             =   6750
      Width           =   1680
   End
   Begin MSFlexGridLib.MSFlexGrid DBGrid2 
      Height          =   5775
      Left            =   9360
      TabIndex        =   1
      Top             =   765
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   10186
      _Version        =   393216
      Cols            =   3
      BackColorBkg    =   -2147483633
      FocusRect       =   2
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   465
      Left            =   11160
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   16
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label2 
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   9090
      TabIndex        =   7
      Top             =   270
      Width           =   1680
   End
   Begin VB.Label Label1 
      Caption         =   "List Barang"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   135
      TabIndex        =   6
      Top             =   315
      Width           =   1680
   End
   Begin VB.Menu mnuExit 
      Caption         =   "E&xit"
   End
End
Attribute VB_Name = "frmPrintListBarang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim queryJual, queryBeli As String
Dim total As Long
Dim totalhapus As Long
Const querybrg As String = "select [Kode Barang],[Nama Barang],Kategori,Merk from ms_barang where flag='1'"
Dim search As String

Private Sub cmdAddAll_Click()
'For a = IIf(i < j, i, j) To IIf(i < j, j, i)
Adodc1.Recordset.MoveFirst
While Not Adodc1.Recordset.EOF
        
        For i = 1 To DBGrid2.rows - 1
            If DBGrid2.TextMatrix(i, 1) = DBGrid1.Columns(0).text Then
                GoTo ne
            End If
        Next
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 1) = DBGrid1.Columns(0).text
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 2) = DBGrid1.Columns(1).text
        DBGrid2.rows = DBGrid2.rows + 1
        
ne:
    Adodc1.Recordset.MoveNext
Wend
'Next

End Sub

Private Sub cmdClear_Click()
    clear
End Sub

Private Sub cmdPrint_Click()
Dim strFormula As String
Dim sname As String
Dim No As Byte
'    If db1 Then
        sname = servname
'    Else
'        Unload Me
'    End If

    conn.Open strcon
    rs.Open "select max([no]) from itemlist", conn
    If Not rs.EOF Then
        If rs(0) >= 3 Then
            conn.Execute "delete from itemlist where [no]<=1"
            conn.Execute "update itemlist set [no]=[no]-1 where [no]>1"
            No = 3
        Else
            If IsNull(rs(0)) Then
            No = 1
            Else
            No = rs(0) + 1
            End If
        End If
    Else
        No = 1
    End If
    
    For i = 1 To DBGrid2.rows - 2
        conn.Execute "insert into itemlist values (" & No & ",'" & DBGrid2.TextMatrix(i, 1) & "','" & Replace(DBGrid2.TextMatrix(i, 2), "'", "''") & "','" & i & "')"
        'strFormula = strFormula & "'" & DBGrid2.TextMatrix(i, 1) & "',"
    Next
    conn.Close
    'strFormula = "{ms_barang.kode barang} in [" & Left(strFormula, Len(strFormula) - 1) & "]"
    With CRPrint
        .reset
        .ReportFileName = App.Path & "\Report\namabarang.rpt"
'        For i = 0 To .RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & sname & ";UID=Admin" '& ";PWD=" & pwd & ";"
'        Next
        For i = 0 To .RetrieveDataFiles - 1
            .DataFiles(i) = servname
        Next
        .Password = Chr$(10) & dbpwd
        .SelectionFormula = strFormula
        .destination = crptToWindow
        .PrinterSelect
        .SelectionFormula = "{itemlist.no}=" & No
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        
        .action = 1
        'If strfilename = "\Report\Sales\Faktur Pajak.rpt" Or strfilename = "\Report\Sales\Faktur Non Pajak.rpt" Then
        '    Conn.Execute "update tr_pi_h set fktr_fg='Y' where no_pi='" & Trim(frmPrintFaktur.txtPI_NO.Text) & "'"
        'End If
    End With
    
End Sub

Private Sub Command1_Click()
    loadgrid
End Sub



Private Sub DBGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then DBGrid1_DblClick
        
End Sub

Private Sub DBGrid2_DblClick()
    batalhapus
End Sub

Private Sub DBGrid2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        batalhapus
    End If
End Sub
Private Sub batalhapus()
If DBGrid2.rows > 2 Then
        row = DBGrid2.row

        For row = row To DBGrid2.rows - 2
            If row = DBGrid2.rows Then
                For col = 1 To 2
                    DBGrid2.TextMatrix(row, col) = ""
                Next
                Exit For
            ElseIf DBGrid2.TextMatrix(row + 1, 1) = "" Then
                For col = 1 To 2
                    DBGrid2.TextMatrix(row, col) = ""
                Next
            ElseIf DBGrid2.TextMatrix(row + 1, 1) <> "" Then
                For col = 1 To 2
                DBGrid2.TextMatrix(row, col) = DBGrid2.TextMatrix(row + 1, col)
                Next
            End If
        Next
        DBGrid2.rows = DBGrid2.rows - 1
End If
End Sub
Private Sub DBGrid1_DblClick()

'For a = IIf(i < j, i, j) To IIf(i < j, j, i)
        For i = 1 To DBGrid2.rows - 1
            If DBGrid2.TextMatrix(i, 1) = DBGrid1.Columns(0).text Or DBGrid2.TextMatrix(i, 2) = DBGrid1.Columns(1).text Then
                Exit Sub
            End If
        Next
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 1) = DBGrid1.Columns(0).text
        DBGrid2.TextMatrix(DBGrid2.rows - 1, 2) = DBGrid1.Columns(1).text
        DBGrid2.rows = DBGrid2.rows + 1
'Next
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub
Private Sub clear()
    DBGrid2.rows = 1
    DBGrid2.rows = 2
    DBGrid1.Columns(0).Width = 2000
    DBGrid1.Columns(1).Width = 6000
    DBGrid2.ColWidth(0) = 500
    DBGrid2.ColWidth(1) = 0
    DBGrid2.ColWidth(2) = 6500

    DBGrid2.TextMatrix(0, 1) = "Kode"
    DBGrid2.TextMatrix(0, 2) = "NamaBarang"

End Sub
Private Sub Form_Load()
    clear
    queryJual = "Select [Kode Barang],[Nama Barang],Kategori,Merk from ms_barang order by [Kode Barang]"
    loadgrid
    
    
    
    
End Sub
Private Sub reset_form()
totalhapus = 0
total = 0
lblTotal = Format(total, "#,##0")
lblTotalHapus = Format(totalhapus, "#,##0")
DBGrid2.rows = 1
DBGrid2.rows = 2
End Sub
Private Sub loadgrid()
On Error GoTo err
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
Dim where As String
    
    Adodc1.ConnectionString = strcon
    If txtKdBrg.text <> "" Then
        where = " and [nama barang] like '%" & Replace(txtKdBrg.text, "'", "''") & "%' or  [kategori] like '%" & Replace(txtKdBrg.text, "'", "''") & "%' or  [merk] like '%" & Replace(txtKdBrg.text, "'", "''") & "%'"
    End If
    Adodc1.RecordSource = querybrg & " " & where & " order by [kode barang]"
    
    
'    Set DBGrid1.DataSource = Adodc1
    Set DBGrid1.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid1.Refresh
'    DBGrid1.Rows = 1
'    DBGrid1.Rows = 2
'
'    If Not Adodc1.Recordset.EOF Then
'    Adodc1.Recordset.MoveFirst
'    row = 1
'
'    While Not Adodc1.Recordset.EOF
'    DBGrid1.TextMatrix(row, 1) = Adodc1.Recordset(0)
'    DBGrid1.TextMatrix(row, 2) = Adodc1.Recordset(1)
'    Adodc1.Recordset.MoveNext
'    row = row + 1
'    DBGrid1.Rows = DBGrid1.Rows + 1
'    DoEvents
'    Wend
'
'
'    End If
'    DBGrid1.ColWidth(0) = 500
'    DBGrid1.ColAlignment(1) = 1
'    DBGrid1.Refresh
    Exit Sub
err:
    MsgBox err.Description
End Sub

Private Sub mnuExit_Click()
Unload Me
End Sub

Private Sub txtKdBrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyUp Then
        If Not Adodc1.Recordset.BOF Then Adodc1.Recordset.MovePrevious
    End If
    If KeyCode = vbKeyDown Then
    If Not Adodc1.Recordset.EOF Then Adodc1.Recordset.MoveNext
    End If
End Sub

Private Sub txtKdBrg_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        If search = txtKdBrg.text Then
            DBGrid1_DblClick
        Else
            loadgrid
            search = txtKdBrg.text
        End If
    End If
    
End Sub
