VERSION 5.00
Begin VB.Form frmBayar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pembayaran"
   ClientHeight    =   6330
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   5745
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6330
   ScaleWidth      =   5745
   Begin VB.CommandButton cmdSearchRetur 
      Caption         =   "F5"
      Height          =   330
      Left            =   4500
      Picture         =   "frmBayar.frx":0000
      TabIndex        =   41
      Top             =   2520
      Width           =   375
   End
   Begin VB.TextBox txtRetur 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2655
      TabIndex        =   38
      Top             =   2475
      Width           =   1770
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   915
      Left            =   675
      ScaleHeight     =   885
      ScaleWidth      =   4305
      TabIndex        =   25
      Top             =   6525
      Visible         =   0   'False
      Width           =   4335
      Begin VB.TextBox txtCharge 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   2745
         MaxLength       =   3
         TabIndex        =   1
         Top             =   450
         Width           =   915
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   330
         TabIndex        =   26
         Top             =   90
         Width           =   1230
         Begin VB.OptionButton Opt1 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   645
            TabIndex        =   6
            Top             =   45
            Width           =   510
         End
         Begin VB.OptionButton Opt1 
            Caption         =   "Rp"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   -15
            TabIndex        =   5
            Top             =   45
            Value           =   -1  'True
            Width           =   600
         End
      End
      Begin VB.TextBox txtDisc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   1890
         TabIndex        =   0
         Top             =   45
         Width           =   1770
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3735
         TabIndex        =   37
         Top             =   495
         Width           =   330
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Charge"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   315
         TabIndex        =   36
         Top             =   495
         Width           =   1410
      End
   End
   Begin VB.TextBox txtKartu 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2700
      TabIndex        =   3
      Top             =   3960
      Width           =   1770
   End
   Begin VB.CommandButton cmdSimpanCetak 
      Caption         =   "Bayar (F2)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1920
      TabIndex        =   4
      Top             =   4710
      Width           =   1770
   End
   Begin VB.CommandButton cmdSimpan 
      Caption         =   "Simpan"
      Height          =   375
      Left            =   5760
      TabIndex        =   8
      Top             =   6300
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.TextBox txtTunai 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2700
      TabIndex        =   2
      Top             =   3465
      Width           =   1770
   End
   Begin VB.TextBox txtTujuan 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   5760
      MultiLine       =   -1  'True
      TabIndex        =   9
      Top             =   5535
      Visible         =   0   'False
      Width           =   2940
   End
   Begin VB.CheckBox chkKirim 
      Caption         =   "DP"
      Height          =   240
      Left            =   5805
      TabIndex        =   7
      Top             =   5220
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1860
      Left            =   780
      ScaleHeight     =   1830
      ScaleWidth      =   4305
      TabIndex        =   27
      Top             =   540
      Visible         =   0   'False
      Width           =   4335
      Begin VB.Label Label16 
         BackStyle       =   0  'Transparent
         Caption         =   "Sisa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   495
         TabIndex        =   35
         Top             =   1395
         Width           =   1230
      End
      Begin VB.Label lblSisa 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1935
         TabIndex        =   34
         Top             =   1395
         Width           =   1905
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "Sudah Bayar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   495
         TabIndex        =   33
         Top             =   945
         Width           =   1545
      End
      Begin VB.Label lblSudahBayar 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1935
         TabIndex        =   32
         Top             =   945
         Width           =   1905
      End
      Begin VB.Label lblAsal 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1935
         TabIndex        =   31
         Top             =   90
         Width           =   1905
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Asal"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   495
         TabIndex        =   30
         Top             =   90
         Width           =   1230
      End
      Begin VB.Label lblSekarang 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1935
         TabIndex        =   29
         Top             =   495
         Width           =   1905
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Sekarang"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   495
         TabIndex        =   28
         Top             =   495
         Width           =   1230
      End
   End
   Begin VB.Label lblpk 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   135
      TabIndex        =   43
      Top             =   675
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.Label Label15 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Retur"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   42
      Top             =   2925
      Width           =   1410
   End
   Begin VB.Label lblRetur 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2655
      TabIndex        =   40
      Top             =   2925
      Width           =   1905
   End
   Begin VB.Label Label13 
      BackStyle       =   0  'Transparent
      Caption         =   "Retur"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   39
      Top             =   2520
      Width           =   645
   End
   Begin VB.Label lblNoTrans 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2970
      TabIndex        =   24
      Top             =   225
      Width           =   1545
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "No Nota "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   23
      Top             =   225
      Width           =   1230
   End
   Begin VB.Label label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Kartu"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      TabIndex        =   22
      Top             =   4005
      Width           =   1590
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Diskon"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1170
      TabIndex        =   21
      Top             =   1170
      Width           =   1230
   End
   Begin VB.Label lblDiskonDetil 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2610
      TabIndex        =   20
      Top             =   1170
      Width           =   1905
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1170
      TabIndex        =   19
      Top             =   765
      Width           =   1230
   End
   Begin VB.Label lblTotal3 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2610
      TabIndex        =   18
      Top             =   765
      Width           =   1905
   End
   Begin VB.Label lblDisc 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6435
      TabIndex        =   17
      Top             =   4545
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Kembali"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   420
      Left            =   1215
      TabIndex        =   16
      Top             =   5850
      Width           =   1365
   End
   Begin VB.Label lblKembalian 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   600
      Left            =   2700
      TabIndex        =   15
      Top             =   5760
      Width           =   2130
   End
   Begin VB.Label lblTotal2 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   420
      Left            =   2700
      TabIndex        =   14
      Top             =   5265
      Width           =   2175
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   420
      Left            =   1215
      TabIndex        =   13
      Top             =   5310
      Width           =   1185
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Uang"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      TabIndex        =   12
      Top             =   3510
      Width           =   1590
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2610
      TabIndex        =   11
      Top             =   1575
      Width           =   1905
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1170
      TabIndex        =   10
      Top             =   1575
      Width           =   1230
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H0000C000&
      BackStyle       =   1  'Opaque
      FillColor       =   &H8000000F&
      FillStyle       =   0  'Solid
      Height          =   1365
      Left            =   780
      Top             =   600
      Width           =   4335
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H8000000F&
      BackStyle       =   1  'Opaque
      Height          =   1185
      Left            =   855
      Top             =   3330
      Width           =   4200
   End
   Begin VB.Menu mnumnu 
      Caption         =   "Menu"
      Begin VB.Menu mnuDP 
         Caption         =   "DP"
         Shortcut        =   ^D
      End
   End
End
Attribute VB_Name = "frmBayar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim id As String
Dim id2 As String
Public NoTrans2 As String
Dim sudahbayar As Long
Dim sname As String
Public parent As Form
Private Sub nomor_jualbaru()
Dim No As String
    rs.Open "select top 1 ID from T_PENJUALANH where left(ID,4)='PJ" & Format(Now, "yy") & "' and mid(ID,5,2)='" & Format(Now, "MM") & "'  and mid(ID,7,2)='" & Format(Now, "dd") & "' order by clng(mid(ID,3,len(id))) desc", conn
    If Not rs.EOF Then
        No = "PJ" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format((CLng(Mid(rs(0), 9, (Len(rs(0)) - 8))) + 1), "000")
    Else
        No = "PJ" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format("1", "000")
    End If
    rs.Close
    lblNoTrans = No
End Sub
Private Sub nomor_baru()
On Error GoTo err
Dim No As String

    rs.Open "select top 1 ID from T_Pembayaran where left(ID,4)='BY" & Format(Now, "yy") & "' and mid(ID,5,2)='" & Format(Now, "MM") & "'  and mid(ID,7,2)='" & Format(Now, "dd") & "' order by ID desc", conn
    If Not rs.EOF Then
        No = "BY" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format((CLng(Mid(rs(0), 9, (Len(rs(0)) - 8))) + 1), "000")
    Else
        No = "BY" & Format(Now, "yy") & Format(Now, "MM") & Format(Now, "dd") & Format("1", "000")
    End If
    rs.Close
    id = No
Exit Sub
err:
MsgBox err.Description
Resume Next
End Sub
Private Sub chkKirim_Click()
    If chkKirim.value = True Then
'        txtTujuan.Visible = True
    Else
'        txtTujuan.Visible=false
    End If
End Sub

Private Sub cmdSearchRetur_Click()
    frmSearch.connstr = strcon
    frmSearch.query = queryReturbayar
    frmSearch.nmform = "frmBayar"
    frmSearch.nmctrl = "txtRetur"
    
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cek_Retur"
    
    frmSearch.loadgrid frmSearch.query
    frmSearch.cmbSort.ListIndex = 1
    frmSearch.requery
    Set frmSearch.frm = Me
    
    frmSearch.Show vbModal
End Sub
Public Function cek_Retur() As Boolean
On Error Resume Next
    cek_Retur = False
    conn.Open strcon
    rs.Open "select sum(qty*harga_jual) from t_returjuald where id='" & txtRetur.text & "' and id not in (select distinct no_retur from t_pembayaran)", conn
    If Not rs.EOF Then
        lblRetur = rs(0)
        cek_Retur = True
        txtKartu = 0
        txtTunai.SetFocus
    End If
    rs.Close
    conn.Close
    lblTotal2 = Format((CCur(lblTotal) - CCur(lblDisc) - CCur(lblRetur)) + ((CCur(lblTotal) - CCur(lblDisc)) * CCur(txtCharge.text) / 100), "#,##0")
    lblKembalian = Format(CLng(txtTunai.text) - (CLng(lblTotal2) - sudahbayar), "#,##0")
    If lblKembalian < 0 Then lblKembalian = 0
End Function
Private Sub cmdSimpan_Click()
    simpan
End Sub
Private Sub validasi()
'    If CLng(txtTunai.text) < CLng(lblTotal2) And txtTunai.text <> "0" Then
'        MsgBox "Jumlah uang harus sama atau lebih besar dari total penjualan"
'        txtJumlahUang.SetFocus
'        Exit Sub
'    End If

End Sub
Private Function simpan() As Boolean
Dim bayar As Long
Dim i As Byte
On Error GoTo err
    simpan = False
i = 0
If Not IsNumeric(txtTunai.text) Then txtTunai.text = "0"
If Not IsNumeric(txtKartu.text) Then txtKartu.text = "0"
If Not IsNumeric(txtDisc.text) Then txtDisc.text = "0"

If CLng(lblTotal2) > (CLng(txtTunai.text) + CLng(txtKartu.text) + CLng(lblRetur)) Then
    If sudahbayar > 0 Then
        bayar = CLng(lblTotal2) - sudahbayar
    Else
        bayar = CLng(lblTotal2)
    End If
Else
    bayar = lblTotal2 - sudahbayar
End If
If bayar < 0 Then
    simpan = True
    Exit Function
End If
    validasi
    sudahbayar = sudahbayar + bayar
    conn.ConnectionString = strcon
    conn.Open
    If Left(lblNoTrans, 2) = "TP" Then nomor_jualbaru
    conn.BeginTrans
    
    i = 1
    conn.Execute "update t_penjualanh set id='" & lblNoTrans & "' where id='" & parent.lblNoTrans & "'"
    conn.Execute "update t_penjualand set id='" & lblNoTrans & "' where id='" & parent.lblNoTrans & "'"
    conn.Execute "update tmp_kartustock set id='" & lblNoTrans & "' where id='" & parent.lblNoTrans & "'"
    conn.Execute "update t_penjualanh set disc_tipe=" & IIf(Opt1(0).value = True, "1", "2") & "," & _
            "disc=" & txtDisc.text & ",disc_rp=" & CLng(lblDisc) & ",total=" & CLng(lblTotal2) & ",charge=" & txtCharge.text & "," & _
            "[jumlah bayar]=" & CLng(lblTotal2) & ",kirim='" & chkKirim.value & "',alamatkirim='" & txtTujuan.text & "' where id='" & lblNoTrans & "'"
    
    If id = "" Then
        nomor_baru
        conn.Execute "insert into t_pembayaran values ('" & id & "',format(now,'MM/dd/yyyy'),'" & lblNoTrans & "','" & txtRetur & "','" & CLng(txtTunai.text) & "','" & txtKartu.text & "'," & CLng(bayar) & ",'')"
        
    Else
        conn.Execute "update t_pembayaran set tunai='" & CLng(txtTunai.text) & "',kartu='" & txtKartu.text & "',jumlahbayar=" & CLng(bayar) & " where id='" & id & "' and no_jual='" & lblNoTrans & "'"
        
    End If
    
    conn.Execute "update t_penjualanh set bayar='1',total='" & lblTotal2 & "' where id='" & lblNoTrans & "'"
    
    conn.CommitTrans
    
    
    simpan = True
    i = 0
    DropConnection

    Exit Function
err:
    MsgBox err.Description
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    simpan = False
    DropConnection
End Function
Private Sub bayar()
On Error GoTo err
Dim i As Byte
    i = 0
    
    conn.ConnectionString = strcon
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "update t_penjualanh set bayar='1',total='" & lblTotal2 & "' where id='" & lblNoTrans & "'"
    conn.CommitTrans
    i = 0
    DropConnection
    Exit Sub
err:
    MsgBox err.Description
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
End Sub

Private Sub cmdSimpanCetak_Click()
On Error GoTo err
'    If chkKirim.Value = 0 And CLng(txtTunai.text) + CLng(txtKartu.text) < (CLng(lblTotal2) - sudahbayar) Then
'        MsgBox "Jumlah pembayaran tidak boleh lebih kecil daripada jumlah total"
'        txtTunai.SetFocus
'        Exit Sub
'    End If
    If Not cmdSimpanCetak.Enabled Then
        Exit Sub
    End If
    cmdSimpanCetak.Enabled = False
    
    If Not simpan Then Exit Sub
    'bayar
    cetaknota
    addprintstatus lblNoTrans
    MsgBox "Tekan enter untuk menutup jendela pembayaran dan mulai transaksi baru"
    parent.reset_form
    Unload Me
'    cmdSimpanCetak.Enabled = True
    
    Exit Sub
err:
    MsgBox err.Description
End Sub
Private Sub cetaknota()
Dim query As String
Dim qryName As String
Dim rs As New ADODB.Recordset
On Error GoTo err
    
       
    conn.Open strcon
    conn.Execute "drop view Q_notaprint"
    conn.Execute "create view Q_notaprint as select * from Q_nota where id='" & lblNoTrans & "' "
    conn.Close
    With parent.CRPrint
        
'        If LCase(lblpk) = "eceran" Then
        .ReportFileName = App.Path & "\Report\Nota.rpt"
'        Else
'        .ReportFileName = App.Path & "\Report\Nota grosir.rpt"
'        End If

        For i = 0 To .RetrieveDataFiles - 1
           .DataFiles(i) = servname
        Next
        .Password = Chr$(10) & dbpwd
        .ParameterFields(0) = "NamaPerusahaan;" & GNamaPerusahaan & ";true"
        .ParameterFields(1) = "Alamat;" & GAlamatPerusahaan & ";true"
        .SelectionFormula = "{q_notaprint.id}='" & lblNoTrans & "'"
'        If Not parent.printproperties Then
'            .PrinterSelect
'            parent.printproperties = True
'        End If
        Dim PrinterLoop As printer
        For Each PrinterLoop In Printers

        If PrinterLoop.DeviceName = printername Then
                .printername = printername
                .PrinterDriver = PrinterLoop.DriverName
                .PrinterPort = PrinterLoop.Port

        End If
        Next
        .ProgressDialog = False
        .destination = crptToPrinter
        .action = 1
        
    End With
    
    Exit Sub
err:
   If err.Number <> 20520 Then MsgBox err.Description
   If err.Number <> -2147217865 Then MsgBox err.Description Else Resume Next
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then cmdSimpanCetak_Click
    If KeyCode = vbKeyDown Then MySendKeys "{tab}"
    If KeyCode = vbKeyUp Then MySendKeys "+{tab}"
    If KeyCode = vbKeyF5 Then cmdSearchRetur_Click
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then MySendKeys "{tab}"
    If KeyAscii = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
On Error Resume Next
    
        sname = servnameasli
    
    If cmdSimpanCetak.Enabled = False Then cmdSimpanCetak.Enabled = True
    cmbCaraBayar.ListIndex = 0
    txtCharge.text = "0"
    id = ""
    
    conn.Open strcon
    rs.Open "select * from t_penjualanh where id='" & parent.lblNoTrans & "'", conn
    If Not rs.EOF Then
        lblTotal = parent.lblTotal
        lblBayar = rs("bayar")
        If rs("disc_tipe") = 1 Then Opt1(0).value = True Else Opt1(1).value = True
        txtDisc = rs("disc")
        txtCharge = rs("charge")
        txtDisc_LostFocus
        If Opt1(0).value = True Then lblDisc = txtDisc.text Else lblDisc = Format(txtDisc * lblTotal / 100, "#,##0")
        lblTotal2 = Format((CCur(lblTotal) - CCur(lblDisc) - CCur(lblRetur)) + ((CCur(lblTotal) - CCur(lblDisc)) * CCur(txtCharge.text) / 100), "#,##0")
        chkKirim = rs("kirim")
        rs.Close
        rs.Open "select sum(jumlahbayar) from t_pembayaran where no_jual='" & parent.lblNoTrans & "'", conn
        sudahbayar = IIf(IsNull(rs(0)), 0, rs(0))
        lblSudahBayar = Format(sudahbayar, "#,##0")
        lblSisa = Format(lblTotal2 - sudahbayar, "#,##0")
        
        If Not IsNull(rs(0)) Then
            Picture2.Visible = True
            If sudahbayar > CLng(lblTotal2) Then
                lblSisa.Visible = False
                Label16.Visible = False
                txtTunai.Enabled = False
                txtKartu.Enabled = False
                rs.Close
                rs.Open "select top 1 * from t_pembayaran where no_jual='" & parent.lblNoTrans & "' order by id desc", conn
                If Not rs.EOF Then
                id = rs(0)
                txtTunai.text = rs("tunai")
                txtKartu.text = rs("kartu")
                End If
                rs.Close
                rs.Open "select sum(jumlahbayar) from t_pembayaran where no_jual='" & parent.lblNoTrans & "' and id<'" & id & "'", conn
                sudahbayar = IIf(IsNull(rs(0)), 0, rs(0))
                cmdSimpanCetak.SetFocus
            Else
                lblSisa.Visible = True
                Label16.Visible = True
                txtTunai.Enabled = True
                txtKartu.Enabled = True
            End If
        Else
            Picture2.Visible = False
        End If
    Else
        sudahbayar = 0
        Opt1(0).value = True
        txtDisc = "0"
        lblTotal2 = lblTotal
    End If
    If rs.State Then rs.Close
    conn.Close
    lblSekarang = lblTotal2
    txtTunai.text = "0"
    txtKartu.text = "0"


End Sub
    
Private Sub Form_Unload(Cancel As Integer)
    parent.Enabled = True
'    simpan
End Sub

Private Sub mnuDP_Click()
    If chkKirim.value = 1 Then
    chkKirim.value = 0
    Else
    chkKirim.value = 1
    End If
End Sub

Private Sub txtCharge_GotFocus()
    txtCharge.SelStart = 0
    txtCharge.SelLength = Len(txtCharge.text)
End Sub

Private Sub txtCharge_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub

Private Sub txtCharge_LostFocus()
    If Not IsNumeric(txtCharge.text) Then txtCharge.text = "0"
    If CLng(txtCharge.text) > 100 Then txtCharge.text = "100"
    lblTotal2 = Format((CCur(lblTotal) - CCur(lblDisc) - CCur(lblRetur)) + ((CCur(lblTotal) - CCur(lblDisc)) * CCur(txtCharge.text) / 100), "#,##0")
    If txtTunai.text = "0" Then
    txtKartu.text = lblTotal2
    End If
End Sub

Private Sub txtDisc_GotFocus()
    txtDisc.SelStart = 0
    txtDisc.SelLength = Len(txtDisc.text)
End Sub

Private Sub txtDisc_KeyPress(KeyAscii As Integer)
    NumberOnly KeyAscii
End Sub

Private Sub txtDisc_LostFocus()
On Error Resume Next
    If Not IsNumeric(txtDisc.text) Then txtDisc.text = "0"
    If Opt1(0).value = True Then
        If CCur(txtDisc.text) > CCur(lblTotal) Then
            txtDisc.SetFocus
            MsgBox "diskon tidak boleh lebih daripada total penjualan"
        End If
    Else
        If CDbl(txtDisc) > 100 Then
            txtDisc.SetFocus
            MsgBox "diskon persen tidak boleh lebih daripada 100%"
        End If
    End If
    If lblDisc > 0 Then lblDisc.Visible = True
    If Opt1(0).value = True Then lblDisc = txtDisc.text Else lblDisc = Format(txtDisc * lblTotal / 100, "#,##0")
    lblTotal2 = Format((CCur(lblTotal) - CCur(lblDisc) - CCur(lblRetur)) + ((CCur(lblTotal) - CCur(lblDisc)) * CCur(txtCharge.text) / 100), "#,##0")
    
End Sub

Private Sub txtJumlahUang_GotFocus()
    txtJumlahUang.SelStart = 0
    txtJumlahUang.SelLength = Len(txtJumlahUang.text)
End Sub

Private Sub txtJumlahUang_KeyDown(KeyCode As Integer, Shift As Integer)
On Error Resume Next
    If KeyCode = vbKeyF2 Then
    If chkKirim.value = 1 Then
        chkKirim.value = 0
    Else
        chkKirim.value = 1
    End If
    End If
End Sub

Private Sub txtKartu_GotFocus()
    txtKartu.SelStart = 0
    txtKartu.SelLength = Len(txtKartu.text)
End Sub

Private Sub txtKartu_KeyPress(KeyAscii As Integer)
On Error Resume Next
    NumberOnly KeyAscii
End Sub

Private Sub txtKartu_LostFocus()
On Error Resume Next
    If Not IsNumeric(txtKartu.text) Then txtKartu.text = "0"
    If Not IsNumeric(txtTunai.text) Then txtTunai.text = "0"
'    If CLng(txtTunai.text) + CLng(txtKartu.text) < (lblTotal2 - sudahbayar) Then
'        chkKirim.Value = 1
'    Else
'        chkKirim.Value = 0
'    End If
End Sub

Private Sub txtRetur_LostFocus()
    If txtRetur.text <> "" Then
    If Not cek_Retur Then
    MsgBox "Nomor tersebut belum ada atau sudah terpakai"
    txtRetur.SetFocus
    End If
    End If
End Sub

Private Sub txtTunai_GotFocus()
    txtTunai.SelStart = 0
    txtTunai.SelLength = Len(txtTunai.text)
End Sub

Private Sub txtTunai_KeyPress(KeyAscii As Integer)
On Error Resume Next
    NumberOnly KeyAscii
End Sub

Private Sub txttunai_LostFocus()
On Error Resume Next
    If Not IsNumeric(txtKartu.text) Then txtKartu.text = "0"
    If Not IsNumeric(txtTunai.text) Then txtTunai.text = "0"
        If CLng(txtTunai) >= (CLng(lblTotal2) - sudahbayar) Then
         lblKembalian = Format(CLng(txtTunai.text) - (CLng(lblTotal2) - sudahbayar), "#,##0")
'         cmdSimpanCetak.SetFocus
        Else
'            txtKartu = (CLng(lblTotal2) - sudahbayar) - txtTunai
'            txtKartu.SetFocus
        End If
End Sub


