VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3255
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   3255
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1035
      TabIndex        =   4
      Top             =   990
      Width           =   1185
   End
   Begin VB.TextBox txtPassword 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1395
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   540
      Width           =   1770
   End
   Begin VB.TextBox txtUserName 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1395
      TabIndex        =   0
      Top             =   135
      Width           =   1770
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   45
      TabIndex        =   3
      Top             =   585
      Width           =   1320
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nama"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   45
      TabIndex        =   1
      Top             =   180
      Width           =   1320
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public right_stock, right_hpp, right_alert, right_harga As Boolean
Public right_hargaManual As Boolean
Public right_TipeHarga As String
Public nmform As String
Public func As String
Public frm As Form
Private Sub cmdOK_Click()
On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    rs.Open "select * from login where username='" & txtUserName.text & "'", conn
    If Not rs.EOF Then
        If rs(1) = RC4(LCase(txtPassword), katasandi) Then
        rs.Close
        rs.Open "select * from var_usergroup g inner join user_group u on g.[group]=u.[group] where username='" & txtUserName & "'", conn
        
        If Not rs.EOF Then
            right_hpp = CBool(rs("hpp"))
            right_stock = CBool(rs("stock"))
            right_alert = CBool(rs("alert"))
            right_harga = CBool(rs("ubahharga"))
            right_hargaManual = CBool(rs("hargaManual"))
            
            If right_hargaManual Then frmAddPenjualan.loaddetiljual
        End If
        rs.Close
        Else
            MsgBox "Password yang anda masukkan salah"
        End If
    Else
        MsgBox "Username tidak ditemukan"
        txtPassword.text = ""
        txtUserName.text = ""
        txtUserName.SetFocus
    End If
    
err:
    If rs.State Then rs.Close
    If conn.State Then conn.Close
    
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
right_stock = False
right_hpp = False
right_alert = False
right_harga = False
right_hargaManual = False

End Sub

Private Sub txtPwd_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cmdOK_Click
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        MySendKeys "{Tab}"
    End If
End Sub

