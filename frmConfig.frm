VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Config"
   ClientHeight    =   2280
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   5520
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   5520
   Begin VB.ComboBox txtPrinter 
      Height          =   315
      Left            =   1290
      TabIndex        =   10
      Top             =   240
      Width           =   2535
   End
   Begin VB.ComboBox cmbButtonColor 
      Height          =   315
      Left            =   5355
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   945
      Visible         =   0   'False
      Width           =   1140
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4770
      Top             =   2070
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox cmbBackColor 
      Height          =   315
      Left            =   5355
      TabIndex        =   4
      Text            =   "Combo1"
      Top             =   540
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.CommandButton cmdSearchID 
      Height          =   330
      Left            =   4140
      Picture         =   "frmConfig.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   180
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3375
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1665
      Width           =   1125
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   765
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1665
      Width           =   1125
   End
   Begin VB.TextBox txtBGPicture 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   5340
      TabIndex        =   3
      Top             =   135
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Printer Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   135
      TabIndex        =   9
      Top             =   255
      Width           =   975
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Button Color :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4320
      TabIndex        =   8
      Top             =   990
      Visible         =   0   'False
      Width           =   930
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Back Color :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4320
      TabIndex        =   7
      Top             =   585
      Visible         =   0   'False
      Width           =   930
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "BG Picture : "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4320
      TabIndex        =   2
      Top             =   165
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Menu mnuOk 
      Caption         =   "&Ok"
   End
   Begin VB.Menu mnuCancel 
      Caption         =   "&Cancel"
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Dim asc1 As New AscSecurity.Cls_security
Dim seltab As Byte
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    setConfig txtBGPicture.text, cmbBackColor, cmbButtonColor, txtPrinter ', txtDriver, txtPort
    getConfig
    frmMain.StatusBar1.Panels(4).text = printername
    Unload Me
End Sub

Private Sub cmdSearchID_Click()
    CommonDialog1.filter = "*.jpg|*.gif|*.bmp"
    CommonDialog1.filename = BG
    CommonDialog1.ShowOpen
    txtBGPicture.text = CommonDialog1.filename
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyF9 Then frmDeleteTrans.Show vbModal
    If KeyCode = vbKeyF8 Then frmDelete.Show vbModal
End Sub

Private Sub Form_Load()
    getConfig
    txtBGPicture.text = BG
    cmbBackColor = BGColor1
    cmbButtonColor = btncolor
    Dim PrinterLoop As printer
    For Each PrinterLoop In Printers
        txtPrinter.AddItem PrinterLoop.DeviceName
    Next
    txtPrinter = printername
End Sub

Private Sub mnuCancel_Click()
Unload Me
End Sub

Private Sub mnuok_Click()
cmdOK_Click
End Sub

Public Sub cari_data(ByVal key As String)

End Sub

