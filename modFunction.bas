Attribute VB_Name = "modFunction"
Option Explicit
Public passcode_result As Boolean
Public BG, BGColor1 As String
Public btncolor As String
Public Function calc(text As String) As Double
Dim A, B, C As Double

        If InStr(1, text, "*") > 0 And InStr(1, text, "+") > 0 Then
            If InStr(1, text, "*") > InStr(1, text, "+") Then
                A = CDbl(Left(text, InStr(1, text, "+") - 1))
                B = CDbl(Mid(text, InStr(1, text, "+") + 1, (InStr(1, text, "*")) - (InStr(1, text, "+") + 1)))
                C = CDbl(Mid(text, InStr(1, text, "*") + 1, Len(text) - InStr(1, text, "*")))
                calc = (A + B) * C
            Else
                A = CDbl(Left(text, InStr(1, text, "*") - 1))
                B = CDbl(Mid(text, InStr(1, text, "*") + 1, (InStr(1, text, "+")) - (InStr(1, text, "*") + 1)))
                C = CDbl(Mid(text, InStr(1, text, "+") + 1, Len(text) - InStr(1, text, "+")))
                calc = (A * B) + C
            End If
            Exit Function
        ElseIf InStr(1, text, "+") Then
            calc = CDbl(Left(text, InStr(1, text, "+") - 1)) + CDbl(Mid(text, InStr(1, text, "+") + 1, Len(text) - InStr(1, text, "+")))
            Exit Function
        ElseIf InStr(1, text, "/") Then
            calc = CDbl(Left(text, InStr(1, text, "/") - 1)) / CDbl(Mid(text, InStr(1, text, "/") + 1, Len(text) - InStr(1, text, "/")))
            Exit Function
        ElseIf InStr(1, text, "*") Then
            calc = CDbl(Left(text, InStr(1, text, "*") - 1)) * CDbl(Mid(text, InStr(1, text, "*") + 1, Len(text) - InStr(1, text, "*")))
            Exit Function
        Else
            calc = text
        End If


End Function
Public Function getDiscArray(text As String) As Double
    Dim A() As Double
    Dim pointer As Integer
    Dim i As Integer
    pointer = 1
    If text = "" Then text = "0"
    ReDim A(0)
    While InStr(pointer + 1, text, "+") > 0
        If UBound(A) >= 1 Then
            ReDim Preserve A(UBound(A) + 1) As Double
        Else
            ReDim A(1)
        End If
        
        A(UBound(A) - 1) = Mid(text, pointer, InStr(pointer + 1, text, "+") - pointer)
        pointer = InStr(pointer + 1, text, "+")
    Wend
    ReDim Preserve A(UBound(A) + 1) As Double
    A(UBound(A) - 1) = Mid(text, pointer, Len(text) - (pointer - 1))
    Dim disc As Double
    For i = 0 To UBound(A) - 1
        If disc > 0 Then
            disc = disc * ((100 - A(i)) / 100)
        Else
            disc = ((100 - A(i)) / 100)
        End If
    Next
    getDiscArray = disc * 100
End Function
Public Function CountRow(strFrom As String) As Integer
    Dim strSQL As String
    
    CountRow = 0
    
    strSQL = "SELECT COUNT(*) " & strFrom
    rs.Open strSQL, conn
    If Not rs.EOF Then CountRow = rs(0)
    
    rs.Close
End Function

'Generate No PI atau No PO
Public Function GenNumber(No_For As String) As String   'No_For (PI atau PO)
    Dim strSQL As String
    Dim MaxNo As String
    Dim No As Integer
    Dim CurNo As String         ' Current No
    Dim fieldnm As String
    
    fieldnm = "NO_" & No_For
    
    strSQL = "SELECT MAX(" & fieldnm & ") FROM TR_" & No_For & "_H where left(" & fieldnm & ",2)='" & Right(Year(Date), 2) & "'"
    rs.Open strSQL, conn, adOpenForwardOnly, adLockOptimistic
    If Not IsNull(rs(0)) Then
        MaxNo = rs(0)
    Else
        MaxNo = "00000000"
    End If
    
    No = CInt(Mid(MaxNo, 3, Len(MaxNo)))
    No = No + 1
    CurNo = Right("00" + CStr(Year(Date)), 2) & Right("000000" + CStr(No), 6)
    
    GenNumber = CurNo

    rs.Close
End Function


Public Function Confirm_ReInsert(Action_Fg As String) As Boolean
    Dim Response As String
    
    If Action_Fg = "Insert" Then
        Confirm_ReInsert = False
        Response = MsgBox("Data telah berhasil disimpan." & vbCrLf & _
                           "Apakah anda ingin memasukkan data lain?", _
                           vbQuestion + vbYesNo, "Data")
        If Response = vbYes Then Confirm_ReInsert = True
    Else
        MsgBox "Data telah berhasil disimpan."
    End If
End Function
Public Function cekpasscode(ByRef result As Boolean) As Boolean
    frmPassword.Show
End Function

Public Function getConfig() As String
On Error Resume Next
    Dim filename As String
    filename = App.Path & "\Config.txt"
    Open filename For Input As #1
    Line Input #1, BG
    Line Input #1, btncolor
    Line Input #1, BGColor1
    Line Input #1, printername
'    Line Input #1, printerdriver
'    Line Input #1, printerport
    Close #1
    
End Function
Public Function setConfig(BG As String, BGColor1 As String, btncolor As String, printer As String) As String
On Error Resume Next
    Dim filename As String
    
    
    filename = App.Path & "\Config.txt"
    Open filename For Output As #1
    Print #1, BG
    Print #1, BGColor1
    Print #1, btncolor
    Print #1, printer
'    Print #1, driver
'    Print #1, port
    Close #1
    
End Function
Public Function getHpp(ByVal kdbrg As String, cnstr As String) As Currency
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select hpp from hpp where [kode barang]='" & kdbrg & "'", conn
If Not rs.EOF Then
    getHpp = rs(0)
Else
'    rs.Close
'    rs.Open "select hppawal from ms_barang where [kode barang]='" & kdbrg & "'", conn
'    If Not rs.EOF Then
'        getHpp = rs(0)
'    Else
        getHpp = 0
'    End If
End If
rs.Close
conn.Close
End Function
Public Function getStock(ByVal kdbrg As String, ByVal kdgdg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from q_stock1 where [kode barang]='" & kdbrg & "' and gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    getStock = rs(0)
Else
    getStock = 0
End If
rs.Close
conn.Close

End Function
Public Function getStock2(ByVal kdbrg As String, ByVal kdgdg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from stock where [kode barang]='" & kdbrg & "' and gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    getStock2 = rs(0)
Else
    getStock2 = 0
End If
rs.Close
conn.Close

End Function
Public Function getStock3(ByVal kdbrg As String, ByVal kdgdg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from stock where [kode barang]='" & kdbrg & "' and gudang='" & kdgdg & "'", conn
If Not rs.EOF Then
    getStock3 = rs(0)
Else
    getStock3 = 0
End If
rs.Close
conn.Close

End Function
Public Function getStock4(ByVal kdbrg As String, cnstr As String) As Long
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select stock from stock where [kode barang]='" & kdbrg & "'", conn
If Not rs.EOF Then
    getStock4 = rs(0)
Else
    getStock4 = 0
End If
rs.Close
conn.Close

End Function

Public Function getkategori(ByVal kdbrg As String, cnstr) As String
Dim rs As New ADODB.Recordset
Dim conn As New ADODB.Connection
conn.Open cnstr
rs.Open "select kategori from ms_barang where [kode barang]='" & kdbrg & "'", conn
If Not rs.EOF Then
    getkategori = rs(0)
Else
    getkategori = 0
End If
rs.Close
conn.Close

End Function

Public Function RC4(ByVal Expression As String, ByVal Password As String) As String
On Error Resume Next
Dim RB(0 To 255) As Integer, x As Long, y As Long, Z As Long, key() As Byte, ByteArray() As Byte, Temp As Byte
If Len(Password) = 0 Then
    Exit Function
End If
If Len(Expression) = 0 Then
    Exit Function
End If

If Len(Password) > 256 Then
    key() = StrConv(Left$(Password, 256), vbFromUnicode)
Else
    key() = StrConv(Password, vbFromUnicode)
End If
For x = 0 To 255
    RB(x) = x
Next x
x = 0
y = 0
Z = 0
For x = 0 To 255
    y = (y + RB(x) + key(x Mod Len(Password))) Mod 256
    Temp = RB(x)
    RB(x) = RB(y)
    RB(y) = Temp
Next x
x = 0
y = 0
Z = 0
ByteArray() = StrConv(Expression, vbFromUnicode)
For x = 0 To Len(Expression)
    y = (y + 1) Mod 256
    Z = (Z + RB(y)) Mod 256
    Temp = RB(y)
    RB(y) = RB(Z)
    RB(Z) = Temp
    ByteArray(x) = ByteArray(x) Xor (RB((RB(y) + RB(Z)) Mod 256))
Next x
RC4 = StrConv(ByteArray, vbUnicode)
End Function
Public Function getcolindex(col As String, colname() As String) As Byte
On Error Resume Next
Dim i As Integer

For i = 0 To UBound(colname)
If LCase(colname(i)) = LCase(col) Then getcolindex = i
Next i
     
End Function
Public Function cek_kategori(kode As String, conn As ADODB.Connection) As Boolean
Dim rs As New ADODB.Recordset
    
    rs.Open "select * from var_kategori where kategori='" & kode & "'", conn
    If Not rs.EOF Then
        cek_kategori = True
    Else
        'LblNamaBarang = ""
        cek_kategori = False
    End If
    rs.Close
    
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function
Public Function cek_merk(kode As String, conn As ADODB.Connection) As Boolean
Dim rs As New ADODB.Recordset
    
    rs.Open "select * from var_merk where merk='" & kode & "'", conn
    If Not rs.EOF Then
        cek_merk = True
    Else
        'LblNamaBarang = ""
        cek_merk = False
    End If
    rs.Close
    
    Exit Function
ex:
    If rs.State Then rs.Close
    DropConnection
End Function



