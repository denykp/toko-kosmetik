VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmPrintHarga 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Harga"
   ClientHeight    =   1830
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1830
   ScaleWidth      =   3195
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSearch2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2565
      Picture         =   "frmPrintHarga.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   720
      Width           =   375
   End
   Begin VB.CommandButton cmdSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2565
      Picture         =   "frmPrintHarga.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   270
      Width           =   375
   End
   Begin VB.TextBox txtKodeSampai 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1260
      TabIndex        =   5
      Top             =   720
      Width           =   1230
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H80000003&
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1620
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1305
      Width           =   1065
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H80000003&
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   450
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1305
      Width           =   1005
   End
   Begin VB.TextBox txtKodeDari 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1260
      TabIndex        =   0
      Top             =   270
      Width           =   1230
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   45
      Top             =   900
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Sampai Kode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   4
      Top             =   765
      Width           =   1095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Dari Kode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   1
      Top             =   315
      Width           =   1095
   End
End
Attribute VB_Name = "frmPrintHarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public kode As String

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    On Error GoTo err
    Dim counter As Byte
    Dim kode(3), nama(3) As String
    Dim harga(3) As Long
    For i = 0 To 2
        kode(i) = ""
        nama(i) = ""
        harga(i) = 0
    Next
    conn.ConnectionString = strcon
    conn.Open
    counter = 0
    
    conn.Execute "delete from harga"
    rs.Open "select * from ms_barang where kode_barang>='" & txtKodeDari.text & "' and kode_barang<='" & txtKodeSampai.text & "' order by kode_barang", conn
    While Not rs.EOF
        kode(counter) = rs("kode_barang")
        nama(counter) = rs("nama_barang")
        harga(counter) = rs("harga")
        If counter = 2 Then
            conn.Execute "insert into harga values ('" & kode(0) & "','" & nama(0) & "','" & harga(0) & "','" & kode(1) & "','" & nama(1) & "','" & harga(1) & "','" & kode(2) & "','" & nama(2) & "','" & harga(2) & "')"
            For i = 0 To 2
                kode(i) = ""
                nama(i) = ""
                harga(i) = 0
            Next
            counter = 0
        Else
            counter = counter + 1
        End If
        
        rs.MoveNext
    Wend
    rs.Close
    If kode(0) <> "" Or kode(1) <> "" Or kode(2) <> "" Then
        conn.Execute "insert into harga values ('" & kode(0) & "','" & nama(0) & "','" & harga(0) & "','" & kode(1) & "','" & nama(1) & "','" & harga(1) & "','" & kode(2) & "','" & nama(2) & "','" & harga(2) & "')"
    End If
    conn.Close

    frmprint.strfilename = "\Report\harga.rpt"
    frmprint.strFormula = ""
    frmprint.Show
    Exit Sub
err:
    MsgBox err.Description
    Resume Next

End Sub

Private Sub cmdSearch_Click()
    
    frmSearch.query = "select Kode_barang as [Kode_barang], nama_barang as [Nama_Barang], " & _
    "Merk, NAMA_JENIS as [Nama_Jenis],Kode_Tipe,[Satuan],[Satuan_1],[Satuan_2],[Konversi_1],[Konversi_2],[MIN_QTY] as [Min_Qty], [Harga]    from ms_Barang a inner join ms_jenis b on a.kode_jenis=b.kode_jenis" ' order by kode_barang"
    frmSearch.nmform = "frmPrintHarga"
    frmSearch.nmctrl = "txtKodeDari"
    
        frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal
End Sub

Private Sub cmdSearch2_Click()
    frmSearch.query = "select Kode_barang as [Kode_barang], nama_barang as [Nama_Barang], " & _
    "Merk, NAMA_JENIS as [Nama_Jenis],Kode_Tipe,[Satuan],[Satuan_1],[Satuan_2],[Konversi_1],[Konversi_2],[MIN_QTY] as [Min_Qty], [Harga]    from ms_Barang a inner join ms_jenis b on a.kode_jenis=b.kode_jenis" ' order by kode_barang"
    frmSearch.nmform = "frmPrintHarga"
    frmSearch.nmctrl = "txtKodeSampai"
        frmSearch.connstr = strcon
    Set frmSearch.frm = Me
    frmSearch.loadgrid frmSearch.query
    frmSearch.Show vbModal

End Sub

Private Sub Form_Load()
    frmMasterBarang.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMasterBarang.Enabled = True
End Sub
