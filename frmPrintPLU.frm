VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmPrintPLU 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print"
   ClientHeight    =   1440
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1440
   ScaleWidth      =   3195
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdClear 
      BackColor       =   &H80000003&
      Caption         =   "&Baru"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   810
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H80000003&
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2115
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   810
      Width           =   1065
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H80000003&
      Caption         =   "&Tambah"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   45
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   810
      Width           =   1005
   End
   Begin VB.TextBox txtJumlah 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1260
      TabIndex        =   0
      Text            =   "1"
      Top             =   270
      Width           =   1770
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   45
      Top             =   900
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Jumlah Baris"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   135
      TabIndex        =   1
      Top             =   270
      Width           =   1095
   End
End
Attribute VB_Name = "frmPrintPLU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public kode As String

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdClear_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.Execute "delete from plu"
    conn.Close
    MsgBox "Data sudah dibersihkan"
    Exit Sub
err:
    MsgBox err.Description
    Resume Next

End Sub

Private Sub cmdPrint_Click()
    On Error GoTo err
    conn.ConnectionString = strcon
    conn.Open
    conn.Execute "insert into PLU select barcode,barcode,harga," & txtJumlah.text & " from ms_barang where kode_barang='" & kode & "'"
    MsgBox "Data sudah ditambahkan"
    txtJumlah.text = ""
    conn.Close
    
    Exit Sub
err:
    MsgBox err.Description
    Resume Next

End Sub

Private Sub Form_Load()
    frmMasterBarang.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMasterBarang.Enabled = True
End Sub
