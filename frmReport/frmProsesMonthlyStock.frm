VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmProcessMonthlyStock 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Proses Monthly"
   ClientHeight    =   2355
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   4515
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   4515
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbBulan 
      Height          =   315
      Left            =   1845
      TabIndex        =   5
      Top             =   810
      Width           =   1050
   End
   Begin VB.CommandButton cmdKeluar 
      BackColor       =   &H80000003&
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   2340
      Picture         =   "frmProsesMonthlyStock.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1485
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      BackColor       =   &H80000003&
      Caption         =   "&Proses"
      Height          =   645
      Left            =   720
      Picture         =   "frmProsesMonthlyStock.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1485
      UseMaskColor    =   -1  'True
      Width           =   1230
   End
   Begin VB.ComboBox cmbTahun 
      Height          =   315
      Left            =   1845
      TabIndex        =   0
      Top             =   450
      Width           =   1050
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   90
      Top             =   855
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1710
      TabIndex        =   7
      Top             =   855
      Width           =   105
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Bulan"
      Height          =   240
      Left            =   810
      TabIndex        =   6
      Top             =   855
      Width           =   780
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Tahun"
      Height          =   240
      Left            =   810
      TabIndex        =   2
      Top             =   495
      Width           =   825
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      Height          =   240
      Left            =   1710
      TabIndex        =   1
      Top             =   495
      Width           =   105
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuProses 
         Caption         =   "&Proses"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmProcessMonthlyStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
On Error GoTo err
Dim i As Byte
    i = 0
    
    conn.ConnectionString = strcon
    
    conn.Open
    conn.BeginTrans
    i = 1
    conn.Execute "EXEC sp_GENMONTHLYSTOCK '" & cmbTahun.text & "','" & cmbBulan.text & "'"
    conn.CommitTrans
    i = 0
    conn.Close
    MsgBox "Proses selesai"
    Exit Sub
err:
    If i = 1 Then
        conn.RollbackTrans
        
    End If
    DropConnection
MsgBox err.Description
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    cmbTahun.Clear
    For i = 2006 To Year(Now)
        cmbTahun.AddItem i
    Next
    cmbBulan.Clear
    For i = "01" To "12"
        cmbBulan.AddItem i
    Next
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuProses_Click()
    cmdPrint_Click
End Sub
