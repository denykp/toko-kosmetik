VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmLaporanA 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   6255
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6615
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6255
   ScaleWidth      =   6615
   ShowInTaskbar   =   0   'False
   Begin VB.Frame frMerk 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   180
      TabIndex        =   51
      Top             =   2025
      Width           =   3570
      Begin VB.ComboBox cmbMerk 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   52
         Top             =   135
         Width           =   2250
      End
      Begin VB.Label Label4 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   54
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label2 
         Caption         =   "Merk"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   53
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frSales 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   135
      TabIndex        =   47
      Top             =   4905
      Visible         =   0   'False
      Width           =   2670
      Begin VB.ComboBox cmbSales 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmLaporanA.frx":0000
         Left            =   1215
         List            =   "frmLaporanA.frx":000D
         TabIndex        =   48
         Top             =   135
         Width           =   1320
      End
      Begin VB.Label Label20 
         Caption         =   "Sales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   50
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label19 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   49
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Generate Kartu Stock"
      Height          =   465
      Left            =   180
      TabIndex        =   46
      Top             =   5535
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.CheckBox chkGroupby 
      Caption         =   "Group By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5310
      TabIndex        =   12
      Top             =   1395
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.Frame frGroup 
      Height          =   1455
      Left            =   3870
      TabIndex        =   44
      Top             =   1635
      Visible         =   0   'False
      Width           =   2445
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   45
         TabIndex        =   16
         Top             =   1080
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   45
         TabIndex        =   15
         Top             =   765
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   45
         TabIndex        =   14
         Top             =   450
         Width           =   2310
      End
      Begin VB.OptionButton optGroup 
         Caption         =   "per Item"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   45
         TabIndex        =   13
         Top             =   135
         Width           =   2310
      End
   End
   Begin VB.Frame frSupCus 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   180
      TabIndex        =   40
      Top             =   3195
      Visible         =   0   'False
      Width           =   3570
      Begin VB.ComboBox cmbSupCus 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   4
         Top             =   135
         Width           =   2355
      End
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2790
         ScaleHeight     =   375
         ScaleWidth      =   465
         TabIndex        =   41
         Top             =   135
         Width           =   465
      End
      Begin VB.Label lblSupCus 
         Caption         =   "Supplier"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   43
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label16 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   42
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame frKodeBarang 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   180
      TabIndex        =   34
      Top             =   2610
      Width           =   3570
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2790
         ScaleHeight     =   375
         ScaleWidth      =   465
         TabIndex        =   38
         Top             =   135
         Width           =   465
         Begin VB.CommandButton cmdSearchKdBrg 
            Caption         =   "..."
            Height          =   375
            Left            =   0
            TabIndex        =   39
            Top             =   0
            Width           =   420
         End
      End
      Begin VB.TextBox txtKdBarang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   3
         Top             =   135
         Width           =   1545
      End
      Begin VB.Label Label15 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   36
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label14 
         Caption         =   "Barang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   35
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame frUserID 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   180
      TabIndex        =   31
      Top             =   135
      Width           =   2310
      Begin VB.ComboBox cmbUserID 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   810
         TabIndex        =   11
         Top             =   135
         Width           =   1320
      End
      Begin VB.Label Label13 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   675
         TabIndex        =   33
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label12 
         Caption         =   "User"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   32
         Top             =   180
         Width           =   510
      End
   End
   Begin VB.Frame frMember 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   2925
      TabIndex        =   28
      Top             =   3825
      Visible         =   0   'False
      Width           =   3435
      Begin VB.CommandButton cmdSearchCustomer 
         Caption         =   "..."
         Height          =   375
         Left            =   2745
         TabIndex        =   45
         Top             =   90
         Width           =   420
      End
      Begin VB.TextBox txtKodeCustomer 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   6
         Top             =   135
         Width           =   1545
      End
      Begin VB.Label Label11 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   30
         Top             =   180
         Width           =   105
      End
      Begin VB.Label Label10 
         Caption         =   "Customer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   29
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.Frame FrKategori 
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   180
      TabIndex        =   25
      Top             =   1440
      Width           =   3570
      Begin VB.ComboBox cmbKategori 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1215
         TabIndex        =   2
         Top             =   135
         Width           =   2250
      End
      Begin VB.Label Label9 
         Caption         =   "Kategori"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   27
         Top             =   180
         Width           =   870
      End
      Begin VB.Label Label8 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1080
         TabIndex        =   26
         Top             =   180
         Width           =   105
      End
   End
   Begin VB.Frame frTanggal 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   135
      TabIndex        =   21
      Top             =   945
      Width           =   4785
      Begin MSComCtl2.DTPicker DTDari 
         Height          =   330
         Left            =   1170
         TabIndex        =   0
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   94502915
         CurrentDate     =   38986
      End
      Begin MSComCtl2.DTPicker DTSampai 
         Height          =   330
         Left            =   3060
         TabIndex        =   1
         Top             =   90
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   94502915
         CurrentDate     =   38986
      End
      Begin VB.Label Label1 
         Caption         =   "Tanggal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   90
         TabIndex        =   24
         Top             =   135
         Width           =   780
      End
      Begin VB.Label Label3 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1035
         TabIndex        =   23
         Top             =   135
         Width           =   105
      End
      Begin VB.Label Label6 
         Caption         =   "s/d"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2700
         TabIndex        =   22
         Top             =   135
         Width           =   285
      End
   End
   Begin VB.Frame frDetail 
      Height          =   780
      Left            =   3465
      TabIndex        =   20
      Top             =   0
      Width           =   1905
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   90
         ScaleHeight     =   600
         ScaleWidth      =   1455
         TabIndex        =   37
         Top             =   135
         Width           =   1455
         Begin VB.OptionButton Option2 
            Caption         =   "Rekap"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   10
            Top             =   300
            Width           =   1635
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Detail"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Value           =   -1  'True
            Width           =   1635
         End
      End
   End
   Begin VB.Frame frGudang 
      BorderStyle     =   0  'None
      Height          =   555
      Left            =   105
      TabIndex        =   17
      Top             =   3780
      Visible         =   0   'False
      Width           =   2805
      Begin VB.ComboBox cmbGudang 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   5
         Top             =   135
         Width           =   1050
      End
      Begin VB.Label Label5 
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1170
         TabIndex        =   19
         Top             =   180
         Width           =   105
      End
      Begin VB.Label lblGudang 
         Caption         =   "Gudang"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   180
         Width           =   870
      End
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3360
      Picture         =   "frmLaporanA.frx":0026
      TabIndex        =   8
      Top             =   5580
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1740
      Picture         =   "frmLaporanA.frx":0128
      TabIndex        =   7
      Top             =   5580
      Width           =   1230
   End
   Begin VB.Line Line1 
      X1              =   90
      X2              =   5895
      Y1              =   900
      Y2              =   900
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public tipe As Byte
Public habis As Boolean

Private Sub chkCrByr_Click(Index As Integer)

End Sub

Private Sub cmdSearchCustomer_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    frmSearch.query = "select [kode customer],[nama customer],alamat,telp as telepon from ms_customer"
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKodecustomer"
    frmSearch.keyIni = "ms_customer"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = ""
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub cmbKategori_Click()
    txtKdBarang.text = ""
End Sub

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim filtergudang, filtertanggal, filterbulan, filtertahun, filtercarabayar, filter3, filteruserid, filterkodebarang As String
Dim filterkategori, filtersupplier, filtersales, filtersupcus, filtercustomer, filtermerk As String
Dim filterEceran As String
Dim ext As String, sBulanPrev As String, sTahunPrev As String
Dim query() As String
Dim qryName As String
Dim param3 As String

    Select Case filename
        Case "\Report\Laporan Pembelian":
            filtergudang = "gudang"
            filtertanggal = "tanggal"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            filtersupplier = "[kode supplier]"
            ReDim query(0) As String
            query(0) = " SELECT * " & _
                        "FROM q_pembelian ^^filtertanggalfiltergudangfiltersupplierfilterkodebarangfiltermerkfilterkategori"
            qryName = "q_lappembelian"
        Case "\Report\Laporan Pindah Gudang":
            filtergudang = "gudang"
            filtertanggal = "tanggal"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            
            ReDim query(0) As String
            query(0) = " SELECT * " & _
                        "FROM q_pindahgudang ^^filtertanggalfiltergudangfilterkodebarangfiltermerkfilterkategori"
            qryName = "q_lappindahgudang"
        Case "\Report\Laporan Penjualan", "\Report\Laporan Penjualan Bersih":
            filtergudang = "gudang"
            filtertanggal = "tanggal"
            filtercarabayar = "carabayar"
            filteruserid = "userid"
            filterkodebarang = "[kode barang]"
            filterkategori = "kategori"
            filtermerk = "merk"
            filter3rdparty = "[kode customer]"
            filtercustomer = "[kode customer]"
            filtersales = "sales"
            
            ReDim query(0) As String
            query(0) = "select * from q_penjualan " & _
            "  ^^filteruseridfilter3rdpartyfiltertanggalfilterkodebarangfiltercustomerfiltersalesfiltermerkfilterkategori"
            qryName = "q_lappenjualan"
                        
        Case "\Report\Laporan Retur Penjualan":
            filtergudang = "gudang"
            filtertanggal = "TANGGAL"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            filtercustomer = "[kode customer]"
            ReDim query(0) As String
            query(0) = "SELECT * from q_returjual ^^filtertanggalfiltergudangfilterkodebarangfiltercustomerfilterkategorifiltermerk"
            qryName = "q_lapreturJual"
        Case "\Report\Laporan Retur Pembelian":
            filtergudang = "gudang"
            filtertanggal = "TANGGAL"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            filtersupplier = "[kode supplier]"
            ReDim query(0) As String
            query(0) = "SELECT * from q_returbeli ^^filtertanggalfiltergudangfilterkodebarangfiltersupplierfilterkategorifiltermerk"
            qryName = "q_lapreturBeli"
        Case "\Report\Laporan Stock":
            filtergudang = "gudang"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            ReDim query(0) As String
            query(0) = "SELECT stock.GUDANG,stock.STOCK, rpt_ms_barang.[kode barang],rpt_ms_barang.[nama barang], rpt_ms_barang.kategori,rpt_ms_barang.merk " & _
                       " from stock stock INNER JOIN ms_barang rpt_ms_barang ON stock.[KODE BARANG]= rpt_ms_barang.[kode barang] "
            qryName = "q_lapstock"
            If habis = True Then
                query(0) = query(0) & " where stock<=0 filtergudangfilterkodebarangfilterkategori"
            Else
                query(0) = query(0) & " ^^filtergudangfilterkodebarangfilterkategori"
            End If
        Case "\Report\Laporan Stock Opname"
            filtergudang = "gudang"
            filtertanggal = "tanggal"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            ReDim query(0) As String
            query(0) = "select * from q_opname" & _
                       " ^^filtertanggalfiltergudangfilterkodebarangfilterkategorifiltermerk"
            qryName = "q_lapopname"
        Case "\Report\Laporan Koreksi Stock"
            filtergudang = "gudang"
            filtertanggal = "tanggal"
            filterkategori = "kategori"
            filtermerk = "merk"
            filterkodebarang = "[kode barang]"
            ReDim query(0) As String
            query(0) = "select * from q_adjustment ^^filtertanggalfiltergudangfilterkodebarangfilterkategorifiltermerk"
            qryName = "q_lapkoreksi"
        Case "\Report\Laporan Kartu Stock":
            filtertanggal = "tanggal"
            filterkodebarang = "tmp_kartustock.[kode barang]"
            ReDim query(0) As String
            query(0) = "select tmp_kartustock.id,tmp_kartustock.gudang,tmp_kartustock.tipe,tmp_kartustock.tanggal,tmp_kartustock.[kode barang],tmp_kartustock.masuk,tmp_kartustock.keluar,tmp_kartustock.stockawal,rpt_ms_barang.[nama barang]  from tmp_kartustock inner join rpt_ms_barang on tmp_kartustock.[kode barang]=rpt_ms_barang.[kode barang] ^^filtertanggalfilterkodebarang"
            qryName = "q_lapkartustock"
        Case "\Report\Laporan Pembayaran Biaya":
            filtertanggal = "tanggal"
            ReDim query(0) As String
            query(0) = " select * from qr_biaya ^^filtertanggal"
            qryName = "q_lapbiaya"
        Case "\Report\Laporan Bayar Hutang":
            filtertanggal = "tanggal"
            filtersupplier = "[kode supplier]"
            ReDim query(0) As String
            query(0) = " select * from q_BayarHutang ^^filtertanggalfiltersupplier"
            qryName = "q_lapBayarHutang"
        Case "\Report\Laporan Laba Rugi":
            generate_RL DTDari, DTSampai
            ReDim query(0) As String
            query(0) = " select * from tmp_rl"
            qryName = "q_lapRL"
        Case "\Report\Laporan Kasir":
            filtertanggal = "tanggal"
            filteruserid = "user"
            ReDim query(0) As String
            query(0) = "select * from t_tutupkasir ^^filteruseridfiltertanggal"
            qryName = "q_tutupkasir"
    End Select
    Formula = ""
    'apply filter to query
    If frTanggal.Visible = True Then
        If DTSampai.Visible = True Then
            ReplaceQuery query, "filtertanggal", "format(" & filtertanggal & ",'yyyy/MM/dd')>'" & Format(DTDari - 1, "yyyy/MM/dd") & "' and format(" & filtertanggal & ",'yyyy/MM/dd')<'" & Format(DTSampai + 1, "yyyy/MM/dd") & "'"
        Else
            ReplaceQuery query, "filtertanggal", "" & filtertanggal & "='" & Format(DTDari - 1, "yyyy/MM/dd") & "' and cdate(" & filtertanggal & ")<'" & Format(DTSampai + 1, "yyyy/MM/dd") & "'"
        End If
    
    Else
        ReplaceQuery query, "filtertanggal", ""
    End If
    
    
    If cmbGudang.text <> "" Then
        ReplaceQuery query, "filtergudang", filtergudang & "='" & cmbGudang.text & "'"
    Else
        ReplaceQuery query, "filtergudang", ""
    End If
    If frKodeBarang.Visible = True And txtKdBarang.text <> "" Then
        ReplaceQuery query, "filterkodebarang", filterkodebarang & "='" & txtKdBarang.text & "'"
    Else
        ReplaceQuery query, "filterkodebarang", ""
    End If
    
    If frUserID.Visible = True Then
        If cmbUserID.text <> "" Then
            ReplaceQuery query, "filteruserid", filteruserid & "='" & cmbUserID.text & "'"
        Else
            ReplaceQuery query, "filteruserid", ""
        End If
    Else
        ReplaceQuery query, "filteruserid", ""
    End If
    If frMember.Visible = True Then
        If txtKodeCustomer.text <> "" Then
            ReplaceQuery query, "filtercustomer", filtercustomer & "='" & txtKodeCustomer & "'"
        Else
            ReplaceQuery query, "filtercustomer", ""
        End If
    Else
        ReplaceQuery query, "filtercustomer", ""
    End If
    If frSales.Visible = True Then
        If cmbSales.text <> "" Then
            ReplaceQuery query, "filtersales", filtersales & "='" & cmbSales.text & "'"
        Else
            ReplaceQuery query, "filtersales", ""
        End If
    Else
        ReplaceQuery query, "filtercustomer", ""
    End If
    If frSupCus.Visible = True Then
        If cmbSupCus.text <> "" Then
            ReplaceQuery query, "filtersupplier", filtersupplier & "='" & cmbSupCus & "'"
        Else
            ReplaceQuery query, "filtersupplier", ""
        End If
    Else
        ReplaceQuery query, "filter3rdparty", ""
    End If
    If FrKategori.Visible = True Then
        If cmbKategori.text <> "" Then
            ReplaceQuery query, "filterkategori", filterkategori & "='" & cmbKategori.text & "'"
        Else
            ReplaceQuery query, "filterkategori", ""
        End If
    Else
        ReplaceQuery query, "filterkategori", ""
    End If
    If frMerk.Visible = True Then
        If cmbMerk.text <> "" Then
            ReplaceQuery query, "filtermerk", filtermerk & "='" & cmbMerk.text & "'"
        Else
            ReplaceQuery query, "filtermerk", ""
        End If
    Else
        ReplaceQuery query, "filtermerk", ""
    End If
    
    
    'If OptTipe(0).value = True And OptTipe(0).Visible = True Then
'        If filename = "\Report\Laporan Stock" Then generate_dailystock DTDari
'    ElseIf OptTipe(1).value = True And OptTipe(1).Visible = True Then
'        'If filename = "\Report\Laporan Stock" Then generate_monthlystock cmbBulan & "/01/" & cmbTahun
'    ElseIf OptTipe(2).value = True And OptTipe(2).Visible = True Then
'        If filename = "\Report\Laporan Stock" Then generate_yearlystock "01/01/" & cmbTahun
''        frmprint.strfilename = filename & " " & OptTipe(2).Caption & IIf(chkTipe.value = 1, " " & chkTipe.Caption, "") & ".rpt"
'    Else
''        frmprint.strfilename = filename & ".rpt"
'    End If
    
'    If filename = "\Report\Laporan Mutasi Stock" Then generate_mutasistock DTDari, DTSampai, cmbGudang.text
    
    frmprint.strfilename = filename & ".rpt"
    
'    If filename = "\Report\Laporan Mutasi Stock" Then
'        If OptTipe(2).Visible = True And OptTipe(2).value = True Then
'            frmprint.strfilename = "\Report\Laporan Mutasi Stock Per Tahun.rpt"
'        End If
'    End If
'
    If habis = True And frmprint.strfilename = "\Report\Laporan Stock.rpt" Then
       frmprint.strfilename = "\Report\Laporan Stock.rpt"
'         ReplaceQuery query, "filterkategori", filterkategori & "='" & cmbKategori.text & "'"

        If Formula <> "" Then Formula = Formula & " and "
        
        Formula = Formula & " {" & qryName & ".stock}<=0"
        habis = False
'        frmprint.strFormula = Formula
    End If
    
    param3 = ""
    If Option1.value = True Then
        param3 = "1"
    Else
        param3 = "0"
    End If
    frmprint.query = ""
    For i = 0 To UBound(query)
        If Right(Trim(query(i)), 5) = "where" Then query(i) = Replace(query(i), "where", "")
        query(i) = Replace(query(i), "^^", "")
        query(i) = Replace(query(i), "where and", "where")
        query(i) = Replace(query(i), "where  and", "where")

        query(i) = Trim(frmprint.query & query(i)) & ";"

         frmprint.query = query(i)
    Next
frmprint.qryName = qryName
    
'    frmprint.query = Left(frmprint.query, Len(frmprint.query) - 1)
'    frmprint.strFormula = Formula
    
    frmprint.param1 = Format(DTDari, "dd/MM/yyyy")
    frmprint.param2 = "s/d " & Format(DTSampai, "dd/MM/yyyy")
    
    
    frmprint.strFormula = Formula
    frmprint.param3 = param3
    frmprint.param4 = ""
    If chkGroupby.value = "1" Then
    For i = 0 To optGroup.UBound
    If optGroup(i).value Then frmprint.param4 = i
    Next
    End If
    frmprint.Show vbModal
End Sub
Public Sub cekbarang()
    cmbKategori.text = ""
End Sub
Private Sub cmdSearchKdBrg_Click()
    If db2 Then
        frmSearch.connstr = strconasli
    Else
        frmSearch.connstr = strcon
    End If
    
    frmSearch.query = querybrg
    frmSearch.nmform = "frmLaporanA"
    frmSearch.nmctrl = "txtKdBarang"
    frmSearch.keyIni = "ms_barang"
    frmSearch.col = 0
    frmSearch.Index = -1
    frmSearch.proc = "cekbarang"
    frmSearch.loadgrid frmSearch.query
'    frmSearch.cmbSort.ListIndex = 1
    Set frmSearch.frm = Me
    frmSearch.Show vbModal
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
    If KeyCode = vbKeyDown Then Call MySendKeys("{tab}")
    If KeyCode = vbKeyUp Then Call MySendKeys("+{tab}")
End Sub

Private Sub Form_Load()
    If db2 Then
    conn.ConnectionString = strconasli
    Else
    conn.ConnectionString = strcon
    End If
    conn.Open
    DTDari = Now
    DTSampai = Now
    cmbGudang.Clear
    cmbGudang.AddItem ""
    rs.Open "select * from var_gudang order by gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbKategori.Clear
    cmbKategori.AddItem ""
    rs.Open "select kategori from var_kategori  order by [kategori]", conn
    While Not rs.EOF
        cmbKategori.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbMerk.Clear
    cmbMerk.AddItem ""
    rs.Open "select merk from var_merk  order by [merk]", conn
    While Not rs.EOF
        cmbMerk.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    cmbSupCus.Clear
    
    rs.Open "select [Kode supplier],[Nama Supplier] from ms_supplier order by [kode supplier]", conn
    While Not rs.EOF
        cmbSupCus.AddItem rs(0) 'rs(0) & "-" &
        rs.MoveNext
    Wend
    rs.Close
    
    cmbSales.Clear
    
    rs.Open "select [kode sales] from ms_sales order by [kode sales]", conn
    While Not rs.EOF
        cmbSales.AddItem rs(0) 'rs(0) & "-" &
        rs.MoveNext
    Wend
    rs.Close
    rs.Open "select * from login order by username", conn
    While Not rs.EOF
        cmbUserID.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    
    conn.Close
    Select Case filename
        
        Case "\Report\Laporan Penjualan", "\Report\Laporan Penjualan Bersih":

            frGudang.Visible = True
            frMember.Visible = True
            frUserID.Visible = True
            
            frSales.Visible = True
        Case "\Report\Laporan Pembelian", "\Report\Laporan Retur Pembelian":
            'frGudang.Visible = True
            frSupCus.Visible = True
            optGroup(1).Caption = "per Supplier"
            optGroup(2).Caption = "per Item per Supplier"
            optGroup(3).Caption = "per Supplier per Item"
        Case "\Report\Laporan StockOpname":
            FrKategori.Visible = True
            frKodeBarang.Visible = True
            Option1.value = True
            frDetail.Visible = False
        Case "\Report\Laporan Hutang":
            
            frGroup.Visible = False
            
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = False
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGudang.Visible = False
        Case "\Report\Laporan Member":
            frTanggal.Visible = True
            frMember.Visible = False
            
        Case "\Report\Laporan Penjualan HPP":
            frGudang.Visible = True
        Case "\Report\Laporan Pembayaran Hutang":
            frGudang.Visible = True
            frSupCus.Visible = True
            lblSupCus = "Supplier"
            frTanggal.Visible = True
            frKodeBarang.Visible = False
            FrKategori.Visible = False
            frGroup.Visible = False
        Case "\Report\Laporan Pembayaran Biaya":
            FrKategori.Visible = False
            frMerk.Visible = False
            frSupCus.Visible = False
            frKodeBarang.Visible = False
        Case "\Report\Laporan Bayar Hutang":
            FrKategori.Visible = False
            frMerk.Visible = False
            frSupCus.Visible = True
            frKodeBarang.Visible = False
        Case "\Report\Laporan Cashier":
            
            frTanggal.Visible = True
            DTSampai.Visible = False
            Label6.Visible = False
            frUserID.Visible = True
        Case "\Report\Laporan Kartu Stock":
            frMerk.Visible = False
            frTanggal.Visible = True
            frDetail.Visible = False
            chkGroupby.Visible = False
            frGudang.Visible = True
            FrKategori.Visible = False
            frKodeBarang.Visible = True
            frGroup.Visible = False
        Case "\Report\Laporan Stock":
            
            Option1.value = True
            DTSampai.Visible = False
            Label6.Visible = False
            frGudang.Visible = True
            frDetail.Visible = False
            frTanggal.Visible = False
            frGroup.Visible = False
            FrKategori.Visible = True
            chkGroupby.Visible = False
        Case "\Report\Laporan Mutasi Stock":
            frTanggal.Visible = True
            frDetail.Visible = False
            frGudang.Visible = False
            frGroup.Visible = False
            Option1.value = True
            chkGroupby.Visible = False
        Case "\Report\Laporan Kasir":
            frGroup.Visible = False
            chkGroupby.Visible = False
            frGudang.Visible = False
            FrKategori.Visible = False
            frKodeBarang.Visible = False
            frMerk.Visible = False
            frSupCus.Visible = False
            frMember.Visible = False
            frSales.Visible = False
            Command1.Visible = False
            frDetail.Visible = False
    End Select
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub

Private Sub txtKdBarang_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cekbarang
End Sub

Private Sub txtKdBarang_LostFocus()
    cekbarang
End Sub






