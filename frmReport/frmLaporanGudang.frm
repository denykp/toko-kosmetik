VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanGudang 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   2280
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   3975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   3975
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      Height          =   645
      Left            =   2250
      Picture         =   "frmLaporanGudang.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1170
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   645
      Left            =   630
      Picture         =   "frmLaporanGudang.frx":0102
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1170
      Width           =   1230
   End
   Begin VB.ComboBox cmbGudang 
      Height          =   315
      Left            =   1755
      TabIndex        =   0
      Top             =   405
      Width           =   1050
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   90
      Top             =   855
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label2 
      Caption         =   "Gudang"
      Height          =   240
      Left            =   270
      TabIndex        =   2
      Top             =   450
      Width           =   1320
   End
   Begin VB.Label Label5 
      Caption         =   ":"
      Height          =   240
      Left            =   1620
      TabIndex        =   1
      Top             =   450
      Width           =   105
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanGudang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public tipe As Byte

Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    If filename = "\Report\Laporan Stock AkhirJual.rpt" Or filename = "\Report\Laporan Stock Akhir.rpt" Then
        If tipe = 2 Then
            Formula = "{v_stockjual.stockakhir} <= {MS_BARANG.MIN_QTY}"
        End If
        If cmbGudang.text <> "" Then
            If Formula <> "" Then
                Formula = Formula & " and "
            End If
            Formula = " {ms_gudang.kode_gudang}='" & cmbGudang.text & "'"
        End If
        
    End If
    frmprint.strfilename = filename
    frmprint.strFormula = Formula
    frmprint.Show
End Sub
Private Sub cetakLaporan()
On Error GoTo err
    
    With CRPrint
        .reset
        .ReportFileName = App.Path & filename
        For i = 0 To CRPrint.RetrieveLogonInfo - 1
            '.LogonInfo(i) = "DSN=" & servname
            .DataFiles(i) = servname
        Next
        .SelectionFormula = Formula
        .Destination = crptToWindow
        .ParameterFields(0) = "login;" + user + ";True"
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        .action = 1
    End With
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub Form_Load()
    
    conn.ConnectionString = strcon
    
    conn.Open
    cmbGudang.Clear
    cmbGudang.AddItem ""
    rs.Open "select * from ms_gudang order by kode_gudang", conn
    While Not rs.EOF
        cmbGudang.AddItem rs(0)
        rs.MoveNext
    Wend
    rs.Close
    conn.Close
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub
