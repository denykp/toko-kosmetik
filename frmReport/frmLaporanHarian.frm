VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmLaporanHarian 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Out Laporan"
   ClientHeight    =   1620
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1620
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   510
      Left            =   4590
      TabIndex        =   5
      Top             =   990
      Visible         =   0   'False
      Width           =   1230
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1530
      TabIndex        =   2
      Top             =   360
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   116916227
      CurrentDate     =   39094
   End
   Begin VB.CommandButton cmdKeluar 
      Caption         =   "&Keluar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   3150
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   945
      Width           =   1230
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   1530
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   945
      Width           =   1230
   End
   Begin Crystal.CrystalReport CRPrint 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   330
      Left            =   3285
      TabIndex        =   6
      Top             =   360
      Visible         =   0   'False
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd/MM/yyyy"
      Format          =   116916227
      CurrentDate     =   39094
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   ":"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1305
      TabIndex        =   4
      Top             =   405
      Width           =   240
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Tanggal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   90
      TabIndex        =   3
      Top             =   405
      Width           =   1185
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuPrint 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuKeluar 
         Caption         =   "&Keluar"
         Shortcut        =   ^X
      End
   End
End
Attribute VB_Name = "frmLaporanHarian"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public filename As String
Public Formula As String
Public tipe As Byte
Dim sname As String
Dim query() As String
Private Sub cmdKeluar_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
Dim filtergudang, filtertanggal, filtercarabayar, filter3, filteruserid As String
Dim ext As String
Dim param3 As String
    Select Case filename
        Case "\Report\Laporan Penjualan Harian":
            filtertanggal = "{tmp_lapharian.tanggal}"
            filteruserid = "{tmp_lapharian.userid}"
            
            generate_dailyreport DTPicker1
            ReDim query(0)
            query(0) = "select * from tmp_lapharian"
        Case "\Report\Laporan RL":
            generate_RL DTPicker1, DTPicker2
    End Select
    Formula = ""
        If filtertanggal <> "" Then
    'tanggal
        Formula = filtertanggal & ">=#" & Format(DTPicker1, "MM/dd/yyyy") & "# and " & filtertanggal & "<#" & Format(DTPicker1 + 1, "MM/dd/yyyy") & "#"
        End If

    If filteruserid <> "" Then
    If LCase(group) <> "owner" Then
        If Formula <> "" Then Formula = Formula & " and "
        Formula = Formula & filteruserid & "='" & user & "'"
    End If
    End If
        cetakLaporan (filename & ".rpt")
    
End Sub
Private Sub cetakLaporan(filename As String)
On Error GoTo err
Dim rs() As New ADODB.Recordset
    conn.Open strcon
    With CRPrint
        .reset
        .ReportFileName = App.Path & filename
'        For i = 0 To CRPrint.RetrieveLogonInfo - 1
'            .LogonInfo(i) = "DSN=" & sname & ";UID=Admin;PWD=3pmadmin;DSQ="
'        Next
    '.LogOnServer "Pdsmon.dll", strcon, "", "Admin", "3pmadmin"
        For i = 0 To .RetrieveDataFiles - 1
            .DataFiles(i) = servname
        Next
        .Password = Chr$(10) & dbpwd
'        ReDim rs(UBound(query))
'        For i = 0 To UBound(query)
'            rs(i).Open query(i), conn, adOpenForwardOnly
'            .SetTablePrivateData i, 3, rs(i)
'        Next
        .ParameterFields(0) = "login;" + user + ";True"
        .ParameterFields(1) = "tglDari;" + Format(DTPicker1, "dd/MM/yyyy") + ";True"
        .ParameterFields(2) = "tglSampai;" + IIf(right_hpp, "1", "0") + ";True"
        .ParameterFields(3) = "Detail;1;True"
        
        .Destination = crptToWindow
        .WindowTitle = "Cetak" & PrintMode
        .WindowState = crptMaximized
        .WindowShowPrintBtn = True
        .WindowShowExportBtn = True
        
        .action = 1
    End With
    If conn.State Then conn.Close
    Exit Sub
err:
    MsgBox err.Description
    Resume Next
End Sub

Private Sub Command1_Click()
    generate_dailystock DTPicker1
    generate_monthlystock DTPicker1
    generate_yearlystock DTPicker1
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    DTPicker1 = Now
    sname = servname
End Sub

Private Sub mnuKeluar_Click()
    Unload Me
End Sub

Private Sub mnuPrint_Click()
    cmdPrint_Click
End Sub
