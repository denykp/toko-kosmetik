VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmStockAkhir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stock Akhir"
   ClientHeight    =   5835
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8595
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5835
   ScaleWidth      =   8595
   Begin VB.ComboBox cmbSort 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmStockAkhir.frx":0000
      Left            =   3015
      List            =   "frmStockAkhir.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   540
      Width           =   1050
   End
   Begin VB.ComboBox cmbSortby 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1215
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   540
      Width           =   1725
   End
   Begin VB.ComboBox cmbKey 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3780
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   135
      Visible         =   0   'False
      Width           =   1725
   End
   Begin VB.TextBox txtSearch 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   0
      Text            =   "%"
      Top             =   135
      Width           =   2445
   End
   Begin MSDataGridLib.DataGrid DBGrid 
      Height          =   4830
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   8520
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   465
      Left            =   7155
      Top             =   45
      Visible         =   0   'False
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   820
      ConnectMode     =   1
      CursorLocation  =   2
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   1
      LockType        =   1
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Urutkan"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      TabIndex        =   6
      Top             =   540
      Width           =   1095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Kata Cari"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      TabIndex        =   5
      Top             =   180
      Width           =   1095
   End
End
Attribute VB_Name = "frmStockAkhir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public query As String
Public nmform As String
Public nmctrl As String
Public connstr As String
Public frm As Form
Private Sub cmbKey_Click()
    requery
End Sub

Private Sub requery()
Dim key As String
    If cmbSort.text <> "" And cmbSortby.text <> "" Then
        key = ""
        For i = 1 To cmbKey.ListCount - 1
            key = key & "+cast([" & cmbKey.List(i) & "] as nvarchar)"
        Next
        key = Right(key, Len(key) - 1)
        If InStr(query, "where") > 0 Then
            Adodc1.RecordSource = query & " and " & key & " like '" & txtSearch.text & "' order by [" & cmbSortby.text & "] " & cmbSort.text
        Else
        Adodc1.RecordSource = query & " where " & key & " like '" & txtSearch.text & "' order by [" & cmbSortby.text & "] " & cmbSort.text
        End If
        Set DBGrid.DataSource = Adodc1
        Adodc1.Refresh
        DBGrid.Refresh
        
        
    Else
    If cmbKey.text = "" And cmbSort.text <> "" And cmbSortby.text <> "" Then
        If InStr(query, "where") > 0 Then
            Adodc1.RecordSource = query & "  order by [" & cmbSortby.text & "] " & cmbSort.text
        Else
        Adodc1.RecordSource = query & "  order by [" & cmbSortby.text & "] " & cmbSort.text
        End If
        
        Set DBGrid.DataSource = Adodc1
        Adodc1.Refresh
        DBGrid.Refresh
        End If
    End If
    For i = 0 To Adodc1.Recordset.fields.count - 1
        
        If Adodc1.Recordset(i).DefinedSize > 50 Then
            panjang = 50
        ElseIf Adodc1.Recordset(i).DefinedSize < 8 Then
            panjang = 8
        Else
            panjang = Adodc1.Recordset(i).DefinedSize
        End If
        DBGrid.Columns(i).Width = (panjang * 100)
        Next
End Sub

Private Sub cmbSort_Click()
    requery
End Sub

Private Sub cmbSortby_Click()
    requery
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
    Me.Move 0, 0
    Me.Width = frmMain.Width - 400
    Me.Height = frmMain.Height - 1880
    DBGrid.Width = Me.Width - 200
    DBGrid.Height = Me.Height - 1800
    
    Adodc1.ConnectionString = strcon
    
    
    loadgrid (query)
End Sub
Private Sub loadgrid(query As String)
Dim row, i As Long
Dim isi As String
Dim panjang As Byte
    Adodc1.RecordSource = ""
    row = 1
    Adodc1.RecordSource = query
    Set DBGrid.DataSource = Adodc1
    Adodc1.Refresh
    DBGrid.Refresh
    cmbKey.Clear
    cmbSortby.Clear
    cmbKey.AddItem ""
    For i = 0 To Adodc1.Recordset.fields.count - 1
    cmbKey.AddItem Adodc1.Recordset(i).Name
    cmbSortby.AddItem Adodc1.Recordset(i).Name
    Next
    cmbKey.ListIndex = 0
    cmbSortby.ListIndex = 0
    cmbSort.ListIndex = 0
End Sub

Private Sub txtSearch_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        requery
    End If
End Sub
